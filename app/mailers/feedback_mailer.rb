class FeedbackMailer < ActionMailer::Base
  default from: "autoresponse@#{ENV['DOMAIN']}"

  def auto_forward(feedback)
    @feedback = feedback
    sender = @feedback.email || User.find_by_id(@feedback.user_id).email
    subject = @feedback.subject
    mail(from: sender, reply_to: sender, to: "support@#{ENV['DOMAIN']}", subject: subject)
  end

end
