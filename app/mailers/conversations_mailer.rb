class ConversationsMailer < ActionMailer::Base
  default from: "EdisonPlan Updates <notifications@#{ENV['DOMAIN']}>"

  add_template_helper ApplicationHelper

  def notify_discussion(sender, target_email, discussion)
    @sender = sender
    @discussion = discussion
    mail(from: "#{sender.name || sender.email} <notifications@#{ENV['DOMAIN']}>",
         to: target_email,
         reply_to: "#{discussion.reply_email}",
         subject: "#{discussion.title}")
  end

  def notify_new_comment(target_email, parent, comment)
    @parent = parent
    @comment = comment
    @commentor = comment.user

    mail(from: "#{@commentor.name || @commentor.email} <notifications@#{ENV['DOMAIN']}>",
         to: target_email,
         reply_to: "#{parent.reply_email}",
         subject: "re: #{parent.title_for_email}")
  end


  def notify_new_note(sender, target_email, note)
    @sender = sender
    @note = note
    mail(from: "#{sender.name || sender.email} <notifications@#{ENV['DOMAIN']}>",
         to: target_email,
         reply_to: "#{note.reply_email}",
         subject: "I just published: #{note.title}")

  end

  def notify_new_todolist(target_email, todolist)
    @todolist = todolist
    @maker = todolist.user

    mail(from: "#{@maker.name || @maker.email} <notifications@#{ENV['DOMAIN']}>",
         to: target_email,
         reply_to: "#{todolist.reply_email}",
         subject: "#{todolist.task_name} was added")
  end

  def notify_todo_assignment(assignor, todo)
    @todo = todo
    @todolist = todo.user_task
    @assignee = todo.assignee
    @assignor = assignor

    mail(from: "#{@assignor.name || @assignor.email} <notifications@#{ENV['DOMAIN']}>",
        to: @assignee.email,
        reply_to: "#{todo.user_task.reply_email}",
        subject: "#{@assignor.name || @assignor.email} has assigned #{todo.sub_task_name} to you")
  end

  def feedback_assignment_email(assignor, feedback)
    @feedback = feedback
    @assignee = feedback.assignee
    @assignor = assignor

    mail(
        from: "#{@assignor.name || @assignor.email} <notifications@#{ENV['DOMAIN']}>",
        to: @assignee.email,
        reply_to: "#{feedback.reply_email}",
        subject: "#{@assignor.name || @assignor.email} has assigned a feedback to you"
    )
  end

  def notify_todo_completion(target_email, todo, completor)
    @todo = todo
    @todolist = todo.user_task
    @completor = completor

    mail(
        from: "#{@completor.name || @completor.email} <notifications@#{ENV['DOMAIN']}>",
        to: target_email,
        reply_to: "#{todo.user_task.reply_email}",
        subject: "#{@completor.name || @completor.email} completed #{todo.sub_task_name}"
    )
  end

  def daily_morning_email(user)
    @products = user.products.all
    @user = user
    mail(
        from: "EdisonPlan Reports <notifications@#{ENV['DOMAIN']}>",
        to: user.email,
        subject: "Daily Situation Report for #{Date.today.strftime("%d %b %Y")}"
    )
  end

  def end_of_day_recap
    @user = user
    mail(
        from: "EdisonPlan Reports <notifications@#{ENV['DOMAIN']}>",
        to: user,
        subject: "Daily Recap for #{Date.today.strftime("%d %b %Y")}"
    )
  end


  def notify_new_message(target_email, message)
    @message = message
    @message_author = message.user
    mail(
        from: "#{@message_author.name || @message_author.email} <notifications@#{ENV['DOMAIN']}>",
        to: target_email,
        subject: "#{@message_author.name || @message_author.email} has shared something on team chat")
  end

  def new_feedback_email(target_email, feedback)
    @feedback = feedback
    @feedback_author = feedback.user
    mail(
        from: "#{@feedback_author.name || @feedback_author.email} <notifications@#{ENV['DOMAIN']}>",
        to: target_email,
        reply_to: "#{@feedback.reply_email}",
        subject: "#{@feedback_author.name || @feedback_author.email} has added a feedback from #{@feedback.customer_name}")
  end


end
