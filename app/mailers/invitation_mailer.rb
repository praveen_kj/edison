class InvitationMailer < ActionMailer::Base
  default from: "EdisonPlan Invitations <invitations@#{ENV['DOMAIN']}>"


  def welcome_email(user)
    @user = user
    mail(to: user.email, subject: "Welcome to EdisonPlan")
  end

  def invite_to_plan(sender_id, invitee_email)
    @sender = User.find sender_id
    @invitee = User.where(email:invitee_email).first
    mail(to: invitee_email, subject: "You have been invited to #{@sender.name || @sender.email}'s EdisonPlan")
  end


end
