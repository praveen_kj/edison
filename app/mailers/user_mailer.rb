class UserMailer < ActionMailer::Base
  default from: "EdisonPlan Support <support@#{ENV['DOMAIN']}>"

  def trial_expiry_warning(user)
    @user = user
    mail(to: @user.email, subject: "Don't let your EdisonPlan trial expire!")
  end

  def welcome_mail(user)
    @user = user
    mail(to: @user.email, subject: "Welcome to Edison")
  end

  def trial_extension(user)
    @user = user
    mail(to: @user.email, subject: "Your trial is extended")
  end

  def refer_to_friends(user, referred)
    @user = user
    @referred = referred
    @user_name = @user.user_parameter.user_name
    mail(from: "#{@user_name} <#{@user.email}>",
         to: @referred,
         subject: "Your friend #{@user_name} has shared EdisonPlan with you")
  end

  def daily_email(user_id)
    @user = User.includes(:user_parameter).find(user_id)
    @tasks = @user.user_tasks.ongoing.limit(3)

    mail(to: @user.email, subject: "11 minutes startup discipline everyday")
  end

  def new_pricing_email(user_id)
    @user = User.find(user_id)
    mail(to: @user.email, subject: "Product Hunters, we heard you!")
  end


end
