class StripeNotificationsMailer < ActionMailer::Base

  add_template_helper SubscriptionsHelper

  default from: "EdisonPlan Billing <support@#{ENV['DOMAIN']}>"

  def new_subscriber_mail(subscription_id)
    @user = User.find_by_subscription_id(subscription_id)
    mail(to: @user.email, subject: "Your EdisonPlan subscription is now active")
  end

  def subscription_extension(user_id)
    @user = User.find(user_id)
    mail(to: @user.email, subject: "Your EdisonPlan subscription is renewed")
  end

  ## cancellation of subscription

  def cancel_subscription(subscription_id)
    @user = User.find_by_subscription_id(subscription_id)
    mail(to: @user.email, subject: "Your EdisonPlan subscription has been cancelled")
  end

  ## update of card info

  def card_update(customer_id)
    @user = User.find_by_customer_id(customer_id)
    mail(to: @user.email, subject: "Your new card has been updated")
  end


  private



end
