module TableHelper

  # generate a table wrapped in a div based on the blueprint of said table.
  # Used only when there is belongs_to and has_many relationship


  def generate_table(object, child_table, options = {})

    scope = options[:scope] || 'all'
    args = options[:args] || nil


    if args
      children = object.send(child_table.to_s.pluralize).send(scope, args)
    else
      children = object.send(child_table.to_s.pluralize).send(scope)
    end

    content = make_table(
        children,
        headers: options[:headers],
        class: "list-table #{child_table.to_s}-table",
        id: "#{child_table.to_s}_table_for_#{object.row_selector(false)}",
        columns: 3
    )
    content_tag(:div, content, class: 'table-enc')
  end

  # make html table from the AR collection with a blueprint (child_table)
  # by combining generated columns, thead and tbody

  def make_table(collection, options = {})

    headers = options[:headers]
    klass = options[:class]
    id = options[:id]
    columns = options[:columns] + 2

    cols = make_col_tags(columns)
    head = options[:headers] ? make_table_headers(headers) : "".html_safe
    body = make_table_body(collection)

    content_tag(:table, cols + head + body, class: klass, id: id)

  end

  # make html tbody by joining all rows generated for the table

  def make_table_body(collection)
    array = []
    collection.each_with_index do |c, i|
      array << make_table_row(c) { table_cells(c, c.class.blueprint_for_table[:rows]) }
    end

    content_tag(:tbody, array.join.html_safe)
  end

  # make html tr using each object in a provided collection

  def make_table_row(object, &block)
    ("<tr id=\"#{object.underscored}_#{object.id}\"><td class=\"list-count\"></td>"+(block.call)+'<td class="delete-cell">'+(delete_object(object))+'</td></tr>').html_safe
  end

  # make html td using options contained in blueprint

  def table_cells(object, blueprint)
    cells = ''
    blueprint.each do |row_no, options|
      cells << content_tag(:td, make_field(object, options[:field], options))
    end
    cells
  end

  # make col tags for table

  def make_col_tags(num_of_cols)
    cols =''
    num_of_cols.times do
      cols << '<col>'
    end
    content_tag(:colgroup, cols.html_safe)
  end

  # make theads

  def make_table_headers(headers)
    ths = ''

    headers.each do |header|
      ths << content_tag(:th, header)
    end

    ths = content_tag(:tr, ths.html_safe)
    content_tag(:thead, ths)
  end

end