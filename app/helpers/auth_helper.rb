module AuthHelper

  def has_valid_access?
    current_user.has_valid_access?
  end

  def owner_access_valid?
    active_product.user.has_valid_access?
  end

  def current_user_is_owner?
    current_user.id == active_product.user_id
  end

  def can_write_tools?
    current_user_is_owner? || Membership.where(product_id: active_product.id, member_email: current_user.email).first.write_tools
  end

  def can_write_utils?
    current_user_is_owner? || Membership.where(product_id: active_product.id, member_email: current_user.email).first.write_utils
  end

  def remove_member(member)
    button_to(member, method: :delete, data: {confirm: "You Sure?"}, remote: true, class: "remove-member-btn", form_class: 'remove-member-form') do
      'Remove'
    end
  end

end