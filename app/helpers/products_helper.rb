module ProductsHelper


  def goal_at_stage(stage)
    [
        'find your beachhead market through market diagnosis and analysis',
        'make a minimal product for the First Customer',
        'build your product through shipping it to the first 10 customers',
        'grow your business through improving your product\'s customer experience',
    ][1-stage]

  end

  def switch_to(product)
    button_to switch_product_path(params: {user: {active_product_id: product.id}}),
              method: :put,
              :form_class => "switch-product-form switch-to-form-#{product.id}",
              remote: false,
              class: "switch-product-btn",
              data: {disable_with: disabled_spinner} do
      product.product_name
    end
  end

  def select_stage(stage, name, both_params = true)
    parameters = both_params ? {stage_at_start: stage, current_stage: stage} : {current_stage: stage}
    button_to product_path(active_product.id, product: parameters), class: 'stage-select-btn', method: :put, remote: true do
      name
    end
  end

  def product_stages
    [
        # minimal
        [0, "I have an idea that needs to become a product",],
        [1, "I have built a prototype without customer feedback",],

        # mvp
        [2, "I have a product with very few paying customers",],

        # full
        [3, "I have a product with some customers and want to scale the business up",],
        [4, "I have a product with a good customer base and want to expand to new markets"],
    ]
  end


  module InstanceMethods


    def name_not_filled?
      product_name.blank? || product_name == 'My product'
    end

    def purpose_not_filled?
      product_purpose.blank? || product_purpose == 'My purpose'
    end

    def role_not_filled?
      owner_role.blank?
    end

    def beachhead
      businesses.where(id: active_market).first
    end

    def has_purpose?
      product_purpose.present?
    end

    def has_markets?
      businesses.all.count > 0
    end

    def has_beachhead?
      beachhead.present?
    end

    def beachhead_has_ranked_purchasing_priorities?
      has_beachhead? && beachhead.user_priorities.pluck(:rank).compact.count >= 2
    end

    def beachhead_has_business_model
      has_beachhead? && beachhead.chosen_business_model.present?
    end

    def defined_success?
      success_definition.present?
    end

    def onboard_stage
      if has_beachhead? && stage_at_start.present? && defined_success?
        6
      elsif has_beachhead? && stage_at_start.present?
        5
      elsif has_beachhead?
        4
      elsif has_markets?
        3
      elsif has_purpose?
        2
      else
        1
      end
    end

    def team_email_list
      memberships.pluck(:member_email).unshift(user.email)
    end

    def active_team
      User.where(id: memberships.pluck(:member_id) << user.id)
    end

    def team_assignment_scope
      (User.where(id: memberships.pluck(:member_id)).pluck(:id, :username) << [user.id, user.username]).to_h
    end

    def inactive_member_emails
      memberships.where(member_id: nil).pluck(:email)
    end

    def completed_canvases
      completed = []
      businesses.each do |b|
        completed << b.business_name if b.canvas_complete?
      end
      completed
    end

    def all_canvases_empty?
      result = true
      businesses.each do |b|
        unless b.canvas_empty?
          result = false
          break
        end
      end
      result
    end

    def all_canvases_complete?
      result = true
      businesses.each do |b|
        unless b.canvas_complete?
          result = false
          break
        end
      end
      result
    end

    def completed_diagnoses
      completed = []
      businesses.each do |b|
        completed << b.business_name if b.diagnosed?
      end
      completed
    end


    def all_diagnoses_empty?
      result = true
      businesses.each do |b|
        unless b.diagnosis_empty?
          result = false
          break
        end
      end
      result
    end

    def all_diagnoses_complete?
      businesses.pluck(:diagnosis_result).exclude?(nil)
    end

    def markets_in_order_of_preference
      preferred_markets_hash.values.flatten
    end


    def preferred_markets_hash
      temp = {}

      temp[0] = businesses.select { |b| b.is_good? }

      7.times do |i|
        temp[i+1] = businesses.select { |b| b.diag_result_code == (7 - i) }
      end

      temp
    end

    def market_names
      businesses.pluck(:business_name)
    end

    def tool_resources
      notes.pluck(:title) + discussions.pluck(:title) + user_tasks.pluck(:task_name)
    end

    def feedback_metrics
      feedbacks = customer_feedbacks.all
      unaddressed = feedbacks.unaddressed
      scheduled = feedbacks.scheduled
      disregarded = feedbacks.disregarded
      completed = feedbacks.completed


      [feedbacks.count, unaddressed.count, scheduled.count, disregarded.count, completed.count]
    end


    def feedback_providers_names
      customer_feedbacks.pluck(:customer_name).uniq
    end


    def all_feedback_count_for(step)
      customer_feedbacks.unaddressed.for_tool(step).count
    end

    def unaddressed_feedback_count_for(step)
      customer_feedbacks.unaddressed.for_tool(step).count
    end

    def checklist_progress
      ((checklist_items.count + beachhead.checklist_items.count)/1.05).round
    end

    def feedback_address_rate
      cf = customer_feedbacks
      if cf.count == 0
        0
      else
        ((1 - (cf.unaddressed.count / cf.count.to_f))*100).round
      end
    end


  end

end