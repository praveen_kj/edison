module BeachheadsHelper

  F_HINT = {
      "end_user" => {
          "title" => "Who is my end user in %{business_name} market?",
          "content" => "Make a Google search and you become its end user. For a toy manufacturer, the kids are the end users and not the parents who pay for the toys. Whose problem is your product solving? Whose life gets better with your product?"},
      "sales" => {
          "title" => "Who are the top influencers to make a sale in the %{business_name} market?",
          "content" => "Who are the key people who influence your product’s sale. These four category of people influence your end user and your sales: <ol><li><strong>Initiators or advocates</strong> – Person who identifies the problem and asks your end user to use it.</li> <li><strong>Primary influencers</strong> – Key people who can influence your end user to use your product.</li> <li><strong>Economic buyer</strong> – Person who pays for your product.</li><li><strong>VETO power</strong> - Person who can say a NO to your deal.</li> Capture the top influencers to your product’s sale."},
      "prod_features" => {
          "title" => "What are mandatory features expected in %{product_name} by the %{business_name} market?",
          "content" => "Google search engine’s mandatory features were search box, search result list, pagination and advertisement area. What are your mandatory features with out which your product will not solve your end users problem? Generally, it will be between 3 to 7 features."},
      "benefit" => {
          "title" => "What are top 3 purchasing priorities of the end users in %{business_name} market?",
          "content" => "Most relevant results, Quick and Free are the top three purchasing priorities of Google's end users. Convenient shopping experience, Large collection of products to shop from and lowest price are the 3 key purchasing priorities of Amazon's end users. What are the 3 things that your end user will look for in your product?"},
      "application" => {
          "title" => "What problem do I solve for the %{business_name} market?",
          "content" => "Google made search better. Amazon simplified online buying and selling. YouTube solved streaming videos. What can you make better? What is the one problem that you can solve?"},
      "partners" => {
          "title" => "Who are the key partners I need to make the product and sell it in the %{business_name} market?",
          "content" => "For an iPhone app the key distribution partner is Apple. For Windows OS the key distribution partners are the computer manufacturers. Who are the people you need to make the product available to your customers? They may be your team, vendors and sales agents."},
      "core" => {
          "title" => "What is my core or 'that one special thing'?",
          "content" => "Your core allows you to deliver the benefits your customers value with much greater effectiveness than any other competitor. You are looking for that single thing that will make it very difficult for the next company that tries to copy and compete with you. Linkedin’s core is Network Size, Apple’s is User Experience and Wal Mart's is Lowest Cost. What is it that you do better than anyone else? "},
      "marketing" => {
          "title" => "How will the customers in the %{business_name} market know about my %{product_name}?",
          "content" => "How will you spread the word about your %{product_name}? You can offer free trails or demos, make cold calls, contribute content, join groups (both in-person and online) and enter key sales tie-ups."},
      "top_competitors" => {
          "title" => "Who are my top competitors in the %{business_name} market?",
          "content" => "Who are those making similar products—real or perceived? Remember, this is from the customer’s perspective and not just yours."},
  }

  def question_status_icon(business, q_no, identifier)
    field = business.instance_eval('question'+q_no.to_s)
    if field
      link_text = raw("<i id=icon_q_#{q_no}_business_#{business.id} class=\"glyphicon glyphicon-ok\"></i>")
    elsif field.nil?
      link_text = raw("<i id=icon_q_#{q_no}_business_#{business.id} class=\"glyphicon glyphicon-minus\"></i>")
    else
      link_text = raw("<i id=icon_q_#{q_no}_business_#{business.id} class=\"glyphicon glyphicon-remove\"></i>")
    end
    link_to link_text, 'javascript:void(0)', html_options = {class: "choice-modal-activator", id: business.class.to_s.underscore+'_'+business.id.to_s+'_'+field.to_s, 'data-hint-identifier' => "#{identifier}"}
  end


  def diag_yes_btn(business, q_no)
    field = business.instance_eval('question'+q_no.to_s)
    button_to("Yes", business_path(business, :params => instance_eval("params.merge({:business => {question#{q_no}: true}, :q_no => #{q_no}})")), method: :put, remote: true, class: "q_#{q_no}_business_#{business.id}_t choice-btn btn-xs #{'selected' if field}", 'instant-select' => 'true', 'is-sibling' => "question#{q_no.to_s}_business_#{business.id}")
  end

  def diag_no_btn(business, q_no)
    field = business.instance_eval('question'+q_no.to_s)
    button_to("No", business_path(business, :params => instance_eval("params.merge({:business => {question#{q_no}: false}, :q_no => #{q_no}})")), method: :put, remote: true, class: "q_#{q_no}_business_#{business.id}_f  choice-btn btn-xs #{'selected' if field == false}", 'instant-select' => 'true', 'is-sibling' => "question#{q_no.to_s}_business_#{business.id}")
  end

  def disabled_choice_btn(name, selected = false)
    button_tag(name, class: "choice-btn #{'selected' if selected}", disabled: true)
  end

  def checklist_yes_btn(field, business)
    button_to("Yes", business_path(business, :params => params.merge({:business => {field => true}, :field => field})), method: :put, remote: true, 'instant-select' => 'true', 'is-sibling' => "#{business.id}_#{field.to_s}", class: "business_#{business.id}_#{field.to_s} yes-btn #{'selected' if business[field]}")
  end

  def checklist_no_btn(field, business)
    button_to("No", business_path(business, :params => params.merge({:business => {field => false}, :field => field})), method: :put, remote: true, 'instant-select' => 'true', 'is-sibling' => "#{business.id}_#{field.to_s}", class: "business_#{business.id}_#{field.to_s} no-btn #{'selected' if business[field] == false}")
  end

  def canvas_properties
    [['Solution', :application],
     ['End User', :end_user],
     ['Benefit', :benefit],
     ['Product Features', :prod_features],
     ['Sales', :sales],
     ['Marketing', :marketing],
     ['Partners', :partners],
     ['Core ', :core],
     ['Competition', :top_competitors]]
  end

  def diagnosis_properties

  end


  def canvas_field(business, field, canvas_heading, icon_class)
    data = {business_name: business.business_name, product_name: active_product.product_name}
    content = active_edit business, field,
                          textarea: true,
                          placeholder: F_HINT[field.to_s]['title'] % data,
                          char_limit: 300,
                          other_attrs: {'data-modal' => true,
                                        'data-modal-title' => F_HINT[field.to_s]['title'] % data,
                                        'data-modal-content' => F_HINT[field.to_s]['content'] % data}

    content_tag(:div, canvas_heading, class: 'canvas-question-title')+
        content_tag(:div, '', class: "canvas-icon #{icon_class}")+
        content_tag(:div, content, class: 'canvas-content')

  end

  def page_icon_link(page)
    link_to '', app_path(view: page), remote: true, class: "icon-#{page}"
  end

  def is_beachhead?(business)
    business == beachhead
  end

end
