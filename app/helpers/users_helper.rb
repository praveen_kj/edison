module UsersHelper

  def profile_icon(user, options = {})
    user_num = active_product.team_email_list.index(user.email) + 1

    content_tag :div, user_initials(user.name),
                data: {email: user.email},
                class: "team-member-icon profile-icon _#{user_num} #{options[:size]}"

  end

  def timezones_with_offset
    {"International Date Line West" => "-11:00 - International Date Line West",
     "Midway Island" => "-11:00 - Midway Island",
     "American Samoa" => "-11:00 - American Samoa",
     "Hawaii" => "-10:00 - Hawaii",
     "Alaska" => "-09:00 - Alaska",
     "Pacific Time (US & Canada)" => "-08:00 - Pacific Time (US & Canada)",
     "Tijuana" => "-08:00 - Tijuana",
     "Mountain Time (US & Canada)" => "-07:00 - Mountain Time (US & Canada)",
     "Arizona" => "-07:00 - Arizona",
     "Chihuahua" => "-07:00 - Chihuahua",
     "Mazatlan" => "-07:00 - Mazatlan",
     "Central Time (US & Canada)" => "-06:00 - Central Time (US & Canada)",
     "Saskatchewan" => "-06:00 - Saskatchewan",
     "Guadalajara" => "-06:00 - Guadalajara",
     "Mexico City" => "-06:00 - Mexico City",
     "Monterrey" => "-06:00 - Monterrey",
     "Central America" => "-06:00 - Central America",
     "Eastern Time (US & Canada)" => "-05:00 - Eastern Time (US & Canada)",
     "Indiana (East)" => "-05:00 - Indiana (East)",
     "Bogota" => "-05:00 - Bogota",
     "Lima" => "-05:00 - Lima",
     "Quito" => "-05:00 - Quito",
     "Atlantic Time (Canada)" => "-04:00 - Atlantic Time (Canada)",
     "Caracas" => "-04:30 - Caracas",
     "La Paz" => "-04:00 - La Paz",
     "Santiago" => "-03:00 - Santiago",
     "Newfoundland" => "-03:30 - Newfoundland",
     "Brasilia" => "-02:00 - Brasilia",
     "Buenos Aires" => "-03:00 - Buenos Aires",
     "Montevideo" => "-02:00 - Montevideo",
     "Georgetown" => "-04:00 - Georgetown",
     "Greenland" => "-03:00 - Greenland",
     "Mid-Atlantic" => "-02:00 - Mid-Atlantic",
     "Azores" => "-01:00 - Azores",
     "Cape Verde Is." => "-01:00 - Cape Verde Is.",
     "Dublin" => "+00:00 - Dublin",
     "Edinburgh" => "+00:00 - Edinburgh",
     "Lisbon" => "+00:00 - Lisbon",
     "London" => "+00:00 - London",
     "Casablanca" => "+00:00 - Casablanca",
     "Monrovia" => "+00:00 - Monrovia",
     "UTC" => "+00:00 - UTC",
     "Belgrade" => "+01:00 - Belgrade",
     "Bratislava" => "+01:00 - Bratislava",
     "Budapest" => "+01:00 - Budapest",
     "Ljubljana" => "+01:00 - Ljubljana",
     "Prague" => "+01:00 - Prague",
     "Sarajevo" => "+01:00 - Sarajevo",
     "Skopje" => "+01:00 - Skopje",
     "Warsaw" => "+01:00 - Warsaw",
     "Zagreb" => "+01:00 - Zagreb",
     "Brussels" => "+01:00 - Brussels",
     "Copenhagen" => "+01:00 - Copenhagen",
     "Madrid" => "+01:00 - Madrid",
     "Paris" => "+01:00 - Paris",
     "Amsterdam" => "+01:00 - Amsterdam",
     "Berlin" => "+01:00 - Berlin",
     "Bern" => "+01:00 - Bern",
     "Rome" => "+01:00 - Rome",
     "Stockholm" => "+01:00 - Stockholm",
     "Vienna" => "+01:00 - Vienna",
     "West Central Africa" => "+01:00 - West Central Africa",
     "Bucharest" => "+02:00 - Bucharest",
     "Cairo" => "+02:00 - Cairo",
     "Helsinki" => "+02:00 - Helsinki",
     "Kyiv" => "+02:00 - Kyiv",
     "Riga" => "+02:00 - Riga",
     "Sofia" => "+02:00 - Sofia",
     "Tallinn" => "+02:00 - Tallinn",
     "Vilnius" => "+02:00 - Vilnius",
     "Athens" => "+02:00 - Athens",
     "Istanbul" => "+02:00 - Istanbul",
     "Minsk" => "+03:00 - Minsk",
     "Jerusalem" => "+02:00 - Jerusalem",
     "Harare" => "+02:00 - Harare",
     "Pretoria" => "+02:00 - Pretoria",
     "Moscow" => "+03:00 - Moscow",
     "St. Petersburg" => "+03:00 - St. Petersburg",
     "Volgograd" => "+03:00 - Volgograd",
     "Kuwait" => "+03:00 - Kuwait",
     "Riyadh" => "+03:00 - Riyadh",
     "Nairobi" => "+03:00 - Nairobi",
     "Baghdad" => "+03:00 - Baghdad",
     "Tehran" => "+03:30 - Tehran",
     "Abu Dhabi" => "+04:00 - Abu Dhabi",
     "Muscat" => "+04:00 - Muscat",
     "Baku" => "+04:00 - Baku",
     "Tbilisi" => "+04:00 - Tbilisi",
     "Yerevan" => "+04:00 - Yerevan",
     "Kabul" => "+04:30 - Kabul",
     "Ekaterinburg" => "+05:00 - Ekaterinburg",
     "Islamabad" => "+05:00 - Islamabad",
     "Karachi" => "+05:00 - Karachi",
     "Tashkent" => "+05:00 - Tashkent",
     "Chennai" => "+05:30 - Chennai",
     "Kolkata" => "+05:30 - Kolkata",
     "Mumbai" => "+05:30 - Mumbai",
     "New Delhi" => "+05:30 - New Delhi",
     "Kathmandu" => "+05:45 - Kathmandu",
     "Astana" => "+06:00 - Astana",
     "Dhaka" => "+06:00 - Dhaka",
     "Sri Jayawardenepura" => "+05:30 - Sri Jayawardenepura",
     "Almaty" => "+06:00 - Almaty",
     "Novosibirsk" => "+06:00 - Novosibirsk",
     "Rangoon" => "+06:30 - Rangoon",
     "Bangkok" => "+07:00 - Bangkok",
     "Hanoi" => "+07:00 - Hanoi",
     "Jakarta" => "+07:00 - Jakarta",
     "Krasnoyarsk" => "+07:00 - Krasnoyarsk",
     "Beijing" => "+08:00 - Beijing",
     "Chongqing" => "+08:00 - Chongqing",
     "Hong Kong" => "+08:00 - Hong Kong",
     "Urumqi" => "+06:00 - Urumqi",
     "Kuala Lumpur" => "+08:00 - Kuala Lumpur",
     "Singapore" => "+08:00 - Singapore",
     "Taipei" => "+08:00 - Taipei",
     "Perth" => "+08:00 - Perth",
     "Irkutsk" => "+08:00 - Irkutsk",
     "Ulaanbaatar" => "+08:00 - Ulaanbaatar",
     "Seoul" => "+09:00 - Seoul",
     "Osaka" => "+09:00 - Osaka",
     "Sapporo" => "+09:00 - Sapporo",
     "Tokyo" => "+09:00 - Tokyo",
     "Yakutsk" => "+09:00 - Yakutsk",
     "Darwin" => "+09:30 - Darwin",
     "Adelaide" => "+10:30 - Adelaide",
     "Canberra" => "+11:00 - Canberra",
     "Melbourne" => "+11:00 - Melbourne",
     "Sydney" => "+11:00 - Sydney",
     "Brisbane" => "+10:00 - Brisbane",
     "Hobart" => "+11:00 - Hobart",
     "Vladivostok" => "+10:00 - Vladivostok",
     "Guam" => "+10:00 - Guam",
     "Port Moresby" => "+10:00 - Port Moresby",
     "Magadan" => "+10:00 - Magadan",
     "Solomon Is." => "+11:00 - Solomon Is.",
     "New Caledonia" => "+11:00 - New Caledonia",
     "Fiji" => "+13:00 - Fiji",
     "Kamchatka" => "+12:00 - Kamchatka",
     "Marshall Is." => "+12:00 - Marshall Is.",
     "Auckland" => "+13:00 - Auckland",
     "Wellington" => "+13:00 - Wellington",
     "Nuku'alofa" => "+13:00 - Nuku'alofa",
     "Tokelau Is." => "+13:00 - Tokelau Is.",
     "Chatham Is." => "+13:45 - Chatham Is.",
     "Samoa" => "+14:00 - Samoa"}
  end

  def email_time_slots
    {1 => "00:00 am - 01:00 am",
     2 => "01:00 am - 02:00 am",
     3 => "02:00 am - 03:00 am",
     4 => "03:00 am - 04:00 am",
     5 => "04:00 am - 05:00 am",
     6 => "05:00 am - 06:00 am",
     7 => "06:00 am - 07:00 am",
     8 => "07:00 am - 08:00 am",
     9 => "08:00 am - 09:00 am",
     10 => "09:00 am - 10:00 am",
     11 => "10:00 am - 11:00 am",
     12 => "11:00 am - 12:00 pm",
     13 => "12:00 pm - 01:00 pm",
     14 => "01:00 pm - 02:00 pm",
     15 => "02:00 pm - 03:00 pm",
     16 => "03:00 pm - 04:00 pm",
     17 => "04:00 pm - 05:00 pm",
     18 => "05:00 pm - 06:00 pm",
     19 => "06:00 pm - 07:00 pm",
     20 => "07:00 pm - 08:00 pm",
     21 => "08:00 pm - 09:00 pm",
     22 => "09:00 pm - 10:00 pm",
     23 => "10:00 pm - 11:00 pm",
     24 => "11:00 pm - 12:00 pm"}

  end


  module InstanceMethods

    ## new sign up related

    def name
      username || email.split('@').first
    end

    def name_not_filled?
      username.blank? || username == 'My name'
    end

    def active_product
      Product.where(id: active_product_id).first
    end

    def profile_missing?
      name_not_filled? || active_product.role_not_filled? || active_product.name_not_filled? || active_product.purpose_not_filled?
    end

    def short_name
      _ = username.split
      if _.count > 1
        "#{_[0]} #{_[1][0]}."
      else
        username
      end
    end


    def generate_referral_code
      self.update_attribute(:referrer_code, SecureRandom.urlsafe_base64)
    end

    def set_trial_expiry(date)
      self.update_attribute(:trial_expiry_date, date)
    end


    ## check status of user

    def admin?
      self.admin
    end

    def has_valid_access?
      active_trial_user? || active_subscriber? || unpaid_subscriber?
    end

    def active_trial_user?
      if (trial_expiry_date + 1.day) > Time.zone.now && subscription_id.nil?
        true
      else
        false
      end
    end

    def expired_trial_user?
      if (trial_expiry_date + 1.day) < Time.zone.now && subscription_id.nil?
        true
      else
        false
      end
    end

    def active_subscriber?
      subscription_status && (current_subscription_end_at ? current_subscription_end_at > Time.zone.now : false)
    end

    def unpaid_subscriber?
      subscription_status && (current_subscription_end_at ? current_subscription_end_at < Time.zone.now : false) && canceled_at.blank?
    end

    def canceled_subscriber?
      (subscription_status == false) && (current_subscription_end_at ? current_subscription_end_at > Time.zone.now : false) && canceled_at.present?
    end

    def former_subscriber?
      (subscription_status == false) && (current_subscription_end_at ? current_subscription_end_at < Time.zone.now : false) && canceled_at.present?
    end

    def early_adopter?
      created_at < Date.parse('2015-09-22')
    end

    def special_user?
      special_domains= ['edisonplan.com']
      special_domains.include?(email.split('@').last)
    end

    def days_left_on_trial
      ((trial_expiry_date - Time.now)/86400).to_i + 1
    end

    def eligible_plan
      if special_user?
        2
      elsif early_adopter?
        1
      else
        0
      end
    end


    def blank_user_parameters?
      user_parameter.mission.blank?
    end


    ## emails

    def send_trial_extension_mail
      UserMailer.trial_extension(self).deliver
    end

    def send_trial_expiry_warning_mail
      UserMailer.trial_expiry_warning(self).deliver
    end

    def send_daily_morning_email
      if receive_sitrep && around_time_slot && not_provided_sitrep_in_24hrs?
        ConversationsMailer.daily_morning_email(self).deliver
        update_attributes(daily_reminder_sent_at: Time.zone.now)
      end
    end

    def around_time_slot
      preferred_time = self.preferred_time || 7
      Time.zone = preferred_time_zone || 'UTC'
      date = Time.zone.now
      t1 = Time.zone.local(date.year, date.month, date.day, preferred_time - 1)
      t2 = Time.zone.local(date.year, date.month, date.day, preferred_time)
      date.between?(t1, t2)
    end

    def not_provided_sitrep_in_24hrs?
      if daily_reminder_sent_at
        time = lambda { Time.zone.now.in_time_zone(preferred_time_zone || 'UTC') }.call
        daily_reminder_sent_at + 24.hours - 1.minute < time
      else
        true
      end
    end

    # def send_pricing_promo_mail
    #   UserMailer.new_pricing_email(self.id).deliver
    # end

    def send_charge_warning_mail
      StripeNotificationsMailer.charge_warning_mail(self.id).deliver
    end

    def send_subscription_extension_mail
      StripeNotificationsMailer.subscription_extension(self.id).deliver
    end

    ## view related methods

    def days_left_to_launch
      target_date = user_parameter.target_date
      if target_date
        target_date > Date.today ? (target_date - Date.today).to_i : 0
      else
        0
      end
    end

    def stepname
      user_parameter.present? ? user_parameter.user_name : 'Not updated profile'
    end


    def visited_beachhead?
      Footprint.where(user_id: self.id).present? ? Footprint.where(user_id: self.id).pluck(:view).include?('beachhead') : false
    end


    def has_tasks?
      user_tasks.all.count > 0
    end

    def has_no_products?
      products.all.count == 0
    end

    def all_products
      products.all.pluck(:id) + products_where_member
    end

    def products_where_member
      Membership.all.where(member_email: email).pluck(:product_id)
    end

    ## for streak computation

    def last_streak(options = {})

      hash = {}
      if current_streak_end_at && current_streak_end_at < Date.yesterday
        hash[:start] = current_streak_start_at.to_date
        hash[:end] = current_streak_end_at.to_date
        hash[:days] = (hash[:end] - hash[:start] + 1).to_i
      elsif last_streak_start_at
        hash[:start] = last_streak_start_at.to_date
        hash[:end] = last_streak_end_at.to_date
        hash[:days] = (hash[:end] - hash[:start] + 1).to_i
      else
        hash[:start] = nil
        hash[:end] = nil
        hash[:days] = 0
      end

      options[:detailed] ? hash : hash[:days]

    end

    def current_streak(options = {})

      hash = {}
      if current_streak_end_at && current_streak_end_at >= Date.yesterday
        hash[:start] = current_streak_start_at.to_date
        hash[:end] = current_streak_end_at.to_date
        hash[:days] = (hash[:end] - hash[:start] + 1).to_i
      else
        hash[:start] = nil
        hash[:end] = nil
        hash[:days] = 0
      end

      options[:detailed] ? hash : hash[:days]

    end

    def highest_streak(options = {})

      hash = {}
      if highest_streak_end_at
        hash[:start] = highest_streak_start_at.to_date
        hash[:end] = highest_streak_end_at.to_date
        hash[:days] = (hash[:end] - hash[:start] + 1).to_i
      else
        hash[:start] = nil
        hash[:end] = nil
        hash[:days] = 0
      end

      options[:detailed] ? hash : hash[:days]

    end

    def role_in_product(product)
      if product.user == self
        product.owner_role
      else
        product.memberships.where(member_id: self.id).first.role
      end

    end

    def has_access_to?(resource)

      parent = resource.send(resource.class.reflections.select {|k,v| v.macro == :belongs_to }.first[0])

      refs = if parent.try :product_id
               [:p, parent.product_id]
             elsif parent.try :business_id
               [:b, parent.business_id]
             end

      if refs[0] == :p
        self.all_products.include?(refs[1])
      else
        Business.where(product_id: self.all_products).pluck(:id).include?(refs[1])
      end

    end

  end


end