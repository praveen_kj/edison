module GraphicsHelper

  def svg_vertical_line(height, options = {})
    path = content_tag :path, '', d: "M 3,0 V #{height}"
    content_tag :svg, path, height: height, width: 6, class: options[:class]
  end

end