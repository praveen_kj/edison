module EnduserHelper

  def enduser_motivation_to_use
    beachhead.endusers.where(parameter: 'Motivation to use').first.response || '?'
  end

  def enduser_fear_against_use
    beachhead.endusers.where(parameter: 'Fear that prevents them from using').first.response || '?'
  end

end