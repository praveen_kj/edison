module CommentsHelper
  module InstanceMethods

    extend ActiveSupport::Concern

    included do
      scope :without_comments, -> { includes(:comments).where(comments: {id: nil}) }
      scope :with_comments, -> { includes(:comments).where.not(comments: {id: nil}) }
    end

    def comment_count
      comments.count
    end

    def generate_secret_token
      update_attributes(secret_token: "reply-#{self.class.to_s[0]}-#{SecureRandom.urlsafe_base64}")
    end

    def reply_email
      "#{secret_token}@incoming.edisonplan.com"
    end

    def latest_comment
      comments.last
    end

  end
end
