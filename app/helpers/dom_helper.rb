module DomHelper

  def resource_row_ref(resource)
    "#{resource.class.to_s.downcase}_#{resource.id}"
  end

  module InstanceMethods

    def button_selector(attr, choice = nil)
      if choice
        "#{self.underscored}_#{self.id}_#{attr.to_s}_#{choice.to_s.gsub(' ', '_')}"
      else
        ".#{self.underscored}_#{self.id}_#{attr.to_s}_#{self[attr].to_s.gsub(' ', '_')}"
      end
    end

    def field_selector(result, field)
      ".#{result}-#{field.to_s}-#{self.id}"
    end

    def field_id(object, field, pound = false)
      "#{'#' if pound}#{object.underscored.pluralize}_#{field.to_s}_#{object.id}"
    end

    def modal_link_selector(attr)
      "##{self.underscored}_#{self.id}_#{attr.to_s}"
    end

    def row_selector(pound = true)
      "#{'#' if pound}#{self.underscored}_#{self.id}"
    end

    def underscored
      "#{self.class.to_s.underscore}"
    end

    def parent_table_id
      parent_class_name = self.class.reflections.keys.first.to_s
      "##{self.underscored}_table_for_#{parent_class_name}_#{self.send(parent_class_name+'_id')}"
    end

    def sibling_count
      fk = self.class.reflections.values[0].foreign_key
      self.class.where(fk.to_sym => self.send(fk)).size
    end

  end

  module ClassMethods
    def choices_array
      result = {}
      self.validators.each do |validator|
        result[validator.instance_values['attributes'].first] = validator.instance_values['options'][:in]
      end
      result
    end
  end

end

class ActiveRecord::Base

  include DomHelper::InstanceMethods

  extend DomHelper::ClassMethods

  def resource_tag
    "#{self.class.to_s.downcase}_#{self.id}"
  end

end