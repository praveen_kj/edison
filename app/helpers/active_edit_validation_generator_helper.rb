module ActiveEditValidationGeneratorHelper

  module ClassMethods

    def length_validations
      hash = {}
      data = self.validators.select { |validator| validator if validator.class == ActiveModel::Validations::LengthValidator }

      data.each do |item|
        hash[item.attributes[0]] = {}
        hash[item.attributes[0]][:maximum] = item.options[:maximum]
      end

      hash
    end

    def numericality_validations
      hash = {}
      data = self.validators.select { |validator| validator if validator.class == ActiveModel::Validations::NumericalityValidator }

      data.each do |item|
        hash[item.attributes[0]] = {}
        hash[item.attributes[0]][:maximum] = item.options[:less_than_or_equal_to]
        hash[item.attributes[0]][:minimum] = item.options[:greater_than_or_equal_to] || 0
      end

      hash
    end



  end

end


class ActiveRecord::Base

  extend ActiveEditValidationGeneratorHelper::ClassMethods

end