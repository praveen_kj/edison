module ComponentsHelper

  ### modals and modal links


  # generates a text modal and modal activation link

  def text_modal(object, field, options)

    identifier = options[:identifier]
    number_field = options.has_key?(:number_field) ? options[:number_field] : false
    hint = options.has_key?(:hint) ? options[:hint] : true
    length = options.has_key?(:length) ? options[:length] : nil
    placeholder = options.has_key?(:placeholder) ? options[:placeholder] : '--'


    title = content_tag(:div, F_HINT[identifier] ? F_HINT[identifier]['title'] : '', class: 'title')
    content = content_tag(:div, F_HINT[identifier] ? F_HINT[identifier]['content'] : '', class: 'content')
    modal_content = content_tag(:div, bip_text_field(object, field, number_field, length), class: 'td-bip-span hidden')
    modal_helper_text = content_tag(:div, title+content, class: 'static-content hidden')

    modal_content + modal_helper_text + open_modal_link2(object, field, 'bip', identifier, hint, placeholder)
  end

  # generates a choice modal and modal activation link

  def select_modal(object, field, options_array = [], options)

    identifier = options[:identifier]
    hint = options.has_key?(:hint) ? options[:hint] : true
    chosen_class = options.has_key?(:chosen_class) ? options[:chosen_class] : true
    placeholder = options.has_key?(:placeholder) ? options[:placeholder] : '--'

    title = content_tag(:div, F_HINT[identifier] ? F_HINT[identifier]['title'] : '', class: 'title')
    content = content_tag(:div, F_HINT[identifier] ? F_HINT[identifier]['content'] : '', class: 'content')

    modal_content = content_tag(:div, render('static_pages/shared/fixed_options_for_select_modal', array: options_array, object: object, field: field, apply_chosen_class: chosen_class), class: 'td-links hidden')

    modal_helper_text = content_tag(:div, title+content, class: 'static-content hidden')

    modal_content + modal_helper_text + open_modal_link2(object, field, 'select', identifier, hint, placeholder)

  end


  # generates the modal activation link used in text_modal and select_modal

  def open_modal_link2(obj, field, modal_type, identifier, hint=false, placeholder = '--')

    if modal_type == 'bip' || modal_type == 'num' || modal_type == 'select'
      link_text = obj[field].nil? ? placeholder : obj[field]
    elsif modal_type == 'yn'
      link_text = obj[field].nil? ? "?" : obj[field] ? raw("<i class=\"fa fa-check\"></i>") : raw("<i class=\"fa fa-times\"></i>")
    end

    modal_type = (modal_type == 'select' || modal_type == 'yn') ? 'choice' : modal_type

    link_text = link_text.is_a?(String) ? raw(link_text) : link_text

    link_to(link_text, 'javascript:void(0)', html_options = {class: "#{modal_type}-modal-activator #{'number' if modal_type=='num'}", id: obj.class.to_s.underscore+'_'+obj.id.to_s+'_'+field.to_s, "data-hint" => "#{hint}", 'data-hint-identifier' => "#{identifier}"})

  end

  def button_to_create(button_name, table, params = {}, options = {})
    href = instance_eval("#{table.to_s.pluralize}_path(params: {#{table.to_s}: #{params}})")
    button_to href, remote: true, method: :post, class: "#{options[:class]}" do
      button_name
    end
  end

  def dummy_add_button(button_name)
    content_tag :div, class: 'button_to' do
      button_tag class: 'add-a-field', disabled: true do
        button_name
      end
    end
  end


  def disabled_spinner
    '<i class=\'fa fa-spinner fa-spin\'></i>'.html_safe
  end

  ## generates the following kinds of fields

  # text: plain_text, showable in modal, deletable, editable or both
  # num: showable in modal, editable, uneditable
  # select: showable in modal as buttons, infield as dropdown
  # date: infield via datepicker


  def make_field(object, field, options = {})

    type = options[:type]
    deletable = options[:deletable]
    editable = options[:editable]
    modal = options[:modal]
    infield = options[:infield]
    array = options[:choices_array]
    tooltip = options[:tooltip]
    placeholder = options[:placeholder].nil? ? '--' : options[:placeholder]
    identifier = options[:identifier]
    format = options[:format]
    hint_key = options[:hint_key] || 'sample'
    prefix = options[:prefix]
    suffix = options[:suffix]

    content = ''

    case type

      when :text

        if modal
          content << active_edit_if(has_valid_access?, object, field,
                                    textarea: true,
                                    placeholder: placeholder,
                                    other_attrs: {'data-modal' => true,
                                                  'data-modal-title' => F_HINT[hint_key][:title],
                                                  'data-modal-content' => F_HINT[hint_key][:content]})

        elsif editable && format == :textarea
          content << active_edit_if(has_valid_access?, object, field,
                                    textarea: true,
                                    class: 'full-width',
                                    placeholder: placeholder,)

        elsif editable
          content << active_edit_if(has_valid_access?, object, field, placeholder: placeholder)

        else
          content << object[field].to_s

        end

      when :date
        content << active_edit_if(has_valid_access?, object, field, type: :date, placeholder: placeholder, display_with: :ldt, precision: 1)

      when :select
        if modal
          content << select_modal(object, field, array, {identifier: identifier, placeholder: placeholder})
        elsif infield
          content << content_tag(:div, array[object[field].to_i] || '--', class: 'choice-display')+choices_buttons(object, field, array)
        else
          content << active_edit_if(has_valid_access?, object, field, choices: array, type: :select)
        end

      when :num
        if editable
          content << active_edit_if(has_valid_access?, object, field, textarea: true, placeholder: placeholder, numeric: 'true', prefix: prefix, suffix: suffix, display_with: :number_with_delimiter)
        else
          content << object[field].to_s
        end

      else
        return
    end

    if deletable
      content << delete_object(object)
    end

    content.html_safe
  end


  # generate trash icon html button for deleting an object.

  def delete_object(object)
    if has_valid_access?
      button_to(object, method: :delete, data: {confirm: "You Sure?"}, remote: true, class: "del-icon", form_class: 'delete-icon-form') do
        raw "<i class='fa fa-trash'></i>"
      end
    else
      ''
    end
  end


  # default in place editable field for all text.

  def bip_default(object, field, placeholder = nil, options = {})
    best_in_place(object, field, {:class => 'bip-input-field', :inner_class => "bip_field input", :place_holder => placeholder}.merge(options))
  end

  # for numbers.

  def bip_numeric(object, field, placeholder = nil)
    best_in_place object, field, :inner_class => "bip_input_box numeric", display_with: :number_with_delimiter, :place_holder => placeholder
  end

  # for dropdowns.

  def bip_select(object, field, collection, placeholder = 'Select')
    best_in_place object, field, inner_class: "bip_input_box numeric", place_holder: placeholder, as: :select, :collection => collection
  end

  # for dates.

  def bip_date_field(object, field, placeholder = 'Target Date')
    best_in_place object, field, :as => :date, class: 'date-in-place', inner_class: 'date_input_box', :place_holder => placeholder, display_with: lambda { |dt| ldt(dt) }
  end

  # buttons for choices

  def choices_buttons(object, field, choices, options = {wrap: true})
    buttons = ''

    if choices.is_a?(Hash)
      choices.each do |key, value|
        href = instance_eval("#{object.class.to_s.underscore}_path(#{object.id}, :params => params.merge({:#{object.class.to_s.underscore} => {:#{field.to_s} => '#{key}' }}))")
        buttons << button_to(value, href,
                             method: 'put',
                             remote: true,
                             class: " choice-btn btn-xs #{'selected' if object[field] == key} #{object.button_selector(field, value)}",
                             'instant-select' => 'true',
                             'is-sibling' => "option_#{field.to_s}_#{object.id}")
      end
    else
      choices.each do |p|
        href = instance_eval("#{object.class.to_s.underscore}_path(#{object.id}, :params => params.merge({:#{object.class.to_s.underscore} => {:#{field.to_s} => '#{p}' }}))")
        buttons << button_to(p, href,
                             method: 'put',
                             remote: true,
                             class: " choice-btn btn-xs #{'selected' if object[field] == p} #{object.button_selector(field, p)}",
                             'instant-select' => 'true',
                             'is-sibling' => "option_#{field.to_s}_#{object.id}")
      end
    end

    wrapped = options[:wrap]

    if wrapped
      content_tag(:div, buttons.html_safe, class: 'btn-container choices-container')
    else
      content_tag(:div, buttons.html_safe, class: 'btn-container')
    end

  end


  ## table helper


  def table_for(ar_collection, &block)

    # block = capture(block)
    list_items = ''
    ar_collection.each do |object|
      list_items << content_tag(:li, block.call(object))
    end

    content_tag(:ul, list_items.html_safe)

  end


end