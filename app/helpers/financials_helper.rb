module FinancialsHelper

  def drill_down_link(input)
    link_to "#{input}", drill_down_path(dd: input), remote:true
  end


end
