module EventsHelper


  def event_list(limit = 100)
    _temp = []
    relation = active_product.events.limit(limit).where.not(source_id: nil)
    relation.each_with_index do |event, index|
      if index == 0
        _temp << generate_event(event)
      else
        _temp << generate_event(event) unless event.same_as(relation[index - 1])
      end

    end

    if _temp.count < 20 && active_product.created_at > Date.new(2016, 02, 20)
      _temp.push(*first_time_events)
    end

    _temp.compact.first(20)
  end


  def generate_event(event)

    case event.event_type
      when 1
        [event.created_at, "#{fa_icon('map-o', class:'event-icon')}#{event.initiator_name} updated the #{source_name(event)} tool for #{event.business_name}" ] if event.business_name
      when 2
        [event.created_at, "#{fa_icon('comments-o', class:'event-icon')} #{event.initiator_name} discussed #{source_name(event)}"]
      when 3
        [event.created_at, "#{fa_icon('comments-o', class:'event-icon')} #{event.initiator_name} discussed #{source_name(event)}"]
      when 4
        [event.created_at, "#{fa_icon('list-ul', class:'event-icon')} #{event.initiator_name} created #{source_name(event)}"]
      when 5
        [event.created_at, "#{fa_icon('list-ul', class:'event-icon')} #{event.initiator_name} discussed #{source_name(event)}"]
      when 6
        [event.created_at, "#{fa_icon('list-ul', class:'event-icon')} #{event.initiator_name} added to-dos to #{source_name(event)}"]
      when 7
        [event.created_at, "#{fa_icon('list-ul', class:'event-icon')} #{event.initiator_name} completed to-dos from #{source_name(event)}"]
      when 8
        [event.created_at, "#{fa_icon('sticky-note-o', class:'event-icon')} #{event.initiator_name} wrote #{source_name(event)}"]
      when 9
        [event.created_at, "#{fa_icon('sticky-note-o', class:'event-icon')} #{event.initiator_name} discussed #{source_name(event)}"]
      # when 10
      #   [event.created_at, "#{fa_icon('flask', class:'event-icon')} #{event.initiator_name} recorded new assumption: #{source_name(event)}"]
      # when 11
      #   [event.created_at, "#{fa_icon('flask', class:'event-icon')} #{event.initiator_name} discussed assumption: #{source_name(event)}"]
      # when 12
      #   [event.created_at, "#{fa_icon('flask', class:'event-icon')} #{event.initiator_name} added an experiment to validate #{source_name(event)}"]
      # when 13
      #   [event.created_at, "#{fa_icon('flask', class:'event-icon')} #{event.initiator_name} market an experiment in #{source_name(event)} as completed"]
    end

  end

  def first_time_events
    username = active_product.user.name
    product_name = active_product.product_name
    date = active_product.created_at



    [
        [date, "#{fa_icon('map-o', class:'event-icon')} #{username} has defined the success for #{pseudo_link product_name, products_dashboard_path}"],
        [date, "#{fa_icon('map-o', class:'event-icon')} #{username} scripted the purpose for #{pseudo_link product_name, products_dashboard_path}"],
        [date, "#{fa_icon('map-o', class:'event-icon')} #{username} created #{pseudo_link product_name, products_dashboard_path}"],
    ]
  end


  def source_name(e)
    case e.event_type
      when 1
        pseudo_link(STEP[e.source_id][:name], app_path(view: STEP[e.source_id][:name].parameterize))
      when 2, 3
        d = Discussion.find(e.source_id)
        pseudo_link d.title, discussion_path(d.id)
      when 4, 5, 6, 7
        u = UserTask.find(e.source_id)
        pseudo_link u.task_name, to_do_list_path(u.id)
      when 8, 9
        n = Note.find(e.source_id)
        pseudo_link n.title, note_path(n.id)
      when 10, 11, 12, 13
        a = Assumption.find(e.source_id)
        an = if a.preset_id
               preset_assumption(a.preset_id)
             else
               a.hypothesis
             end
        pseudo_link an, assumption_path(a.id)
    end

  end


end