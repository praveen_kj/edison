module MilestonesHelper

  def release_versions
    {
        1 => 'Minimal Product',
        2 => 'Minimum Viable Product',
        3 => 'Full Featured Product'
    }
  end

end