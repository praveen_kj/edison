module ContentHelper

  def bip_field(object, field, number=false, length=nil)
    best_in_place_if has_valid_access?, object, instance_eval(":"+field), :place_holder => "", :as => (number ? :input : :textarea), display_with: (number ? :number_with_delimiter : :simple_format), :class => 'bip-plmodal-obj', :inner_class => "bip_input_box #{number ? 'numeric' : "txt-field txt-field-#{length}"} "
  end

  def bip_text_field(object, field, number=false, length=nil)
    best_in_place object, field, :as => (number ? :input : :textarea), display_with: (number ? :number_with_delimiter : :simple_format), :class => 'text_area', :inner_class => "bip_field text_area #{number ? 'numeric' : "txt-field txt-field-#{length}"} "
  end

  def bip_text_area(object, field, placeholder = 'Click to edit', length=nil)
    best_in_place object, field, :as => :textarea, place_holder: placeholder, display_with: :simple_format, :class => 'text_area', :inner_class => "bip_field text_area  txt-field txt-field-#{length}"
  end

  def bip_field_bh(object, field)
    best_in_place_if has_valid_access?, object, instance_eval(":"+field), :place_holder => "", :as => :textarea, :class => 'bip-plmodal-obj', :inner_class => "bip_input_box txt-field-", display_with: :simple_format
  end

  def bip_num_field(object, field, tabindex=0)
    best_in_place_if has_valid_access?, object, instance_eval(":"+field), :place_holder => "", :class => 'bip-num-modal-obj', :inner_class => "bip_input_box numeric", display_with: :number_with_delimiter, tabindex: tabindex
  end

  def identifier(row, col)
    current_step.to_s+'-'+row.to_s+'-'+col.to_s
  end

  def active_beachhead?(business)
    if beachhead && beachhead.id == business.id
      true
    end
  end

  def open_modal_bhc(obj, field, placeholder)
    link_text = obj.instance_eval(field).blank? ? placeholder : obj.instance_eval(field)
    link_text = content_tag(:p, link_text, class: 'bhc-modal-activator-content')
    klass = 'content-filled' if obj.instance_eval(field).present?
    link_to(link_text, 'javascript:void(0)', html_options = {class: "bhc-modal-activator #{klass}", 'data-field' => field, 'data-placeholder' => placeholder})
  end

  def open_modal_pp(obj, field, identifier, hint=true)
    link_text = obj.instance_eval(field).blank? ? "--" : ValueMetric.find(obj.instance_eval(field)).purchasing_priority
    link_to link_text, 'javascript:void(0)', html_options = {class: "choice-modal-activator", id: obj.class.to_s.underscore+'_'+obj.id.to_s+'_'+field, "data-hint" => "#{hint}", 'data-hint-identifier' => "#{identifier}"}
  end

  def open_modal_mood_select(touchpoint, identifier, hint=true)
    link_text = touchpoint.get_moods.empty? ? '--' : touchpoint.get_moods
    link_to link_text, 'javascript:void(0)', html_options = {class: "choice-modal-activator", id: touchpoint.class.to_s.underscore+'_'+touchpoint.id.to_s, "data-hint" => "#{hint}", 'data-hint-identifier' => "#{identifier}"}
  end

  def true_btn(object, field, placeholder="Yes")
    obj_name = object.class.to_s.underscore
    href = instance_eval("#{obj_name}_path(#{object.id}, :params => params.merge({:#{obj_name} => {:#{field} => true }, :field => field  }))")
    button_to(placeholder, href, method: :put, remote: true, 'instant-select' => 'true', 'is-sibling' => "#{obj_name}-#{field.to_s}-#{object.id}", class: "choice-btn #{'selected' if object[field]}")
  end

  def false_btn(object, field, placeholder="No")
    obj_name = object.class.to_s.underscore
    href = instance_eval("#{obj_name}_path(#{object.id}, :params => params.merge({:#{obj_name} => {:#{field} => false }, :field => field  }))")
    button_to(placeholder, href, method: :put, remote: true, 'instant-select' => 'true', 'is-sibling' => "#{obj_name}-#{field.to_s}-#{object.id}", class: "choice-btn #{'selected' if object[field] == false}")
  end




  def choice_buttons(object, field, placeholders = [], options = {})
    content_tag :div, class: 'btn-container' do
      if has_valid_access?
        true_btn(object, field, placeholders[0])+
            content_tag(:div, '', class:'separator')+
            false_btn(object, field, placeholders[1])
      else
        disabled_choice_btn(placeholders[0], object[field])+
            content_tag(:div, '')+
            disabled_choice_btn(placeholders[1], object[field] == false)
      end
    end
  end


  def rank_modal_link(object, field)
    link_text = object.send(field) || 'Rank'
    link_to link_text, rank_path(object.id), remote: true, id: object.class.to_s.underscore+'_'+object.id.to_s+'_'+field
  end

  def close_button
    raw "<button type=\"button\" class=\"tab-close\" data-dismiss=\"tab\">
      <span aria-hidden=\"true\">&times;</span>
     </button>"
  end

  def other_info_row
    content = content_tag(:th, 'Other Information', colspan: 2)
    content_tag(:tr, content, class: 'other-info')
  end


end


