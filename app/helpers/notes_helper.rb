module NotesHelper
  def archive_note(note)
    name = note.archived ? 'Unarchive this note' : 'Archive this note'
    button_to note_path(note, params.merge(note: {archived: !note.archived})),
              form_class: 'archive-note-form',
              class: 'archive-note-btn',
              method: :put,
              remote: true,
              data: {disable_with: disabled_spinner} do
      name
    end
  end
end