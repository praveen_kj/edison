module ValuePropositionHelper

  def quantified_value_proposition
    (product_value - industry_value) if product_value && industry_value
  end


  def industry_value
    value_metrics.pluck(:as_is_state_value).compact.inject(:+)
  end

  def product_value
    value_metrics.pluck(:possible_state_value).compact.inject(:+)
  end

  def normalized_industry_value
    if product_value && industry_value && product_value != 0 && industry_value != 0
      150.0/product_value * industry_value
    end
  end

  def normalized_qvp
    if product_value && industry_value && product_value != 0 && industry_value != 0
      150.0/product_value * quantified_value_proposition
    end
  end

end