module DiagnosisHelper

  def diagnosis_empty?
    if question1 == nil &&
        question2 == nil &&
        question3 == nil &&
        question4 == nil &&
        question5 == nil &&
        question6 == nil &&
        question7 == nil
      true
    else
      false
    end
  end


  def diagnosed?
    if question1 != nil && question2 != nil && question3 != nil && question4 != nil && question5 != nil && question6 != nil && question7 != nil
      true
    else
      false
    end
  end

  def is_good?
    diagnosed? && question1 && question2 && question3 && question4 && question5 && question6 && question7
  end

  def diagnosis_result_array
    response_array = []
    DIAGNOSIS_RESPONSE.each do |k, v|
      if response_array.length < 1
        if send("question#{v[:question_no]}") == false
          response_array.push(k)
        end
      end
    end
    response_array
  end

  def diag_result_code
    diagnosis_result_array.present? ? diagnosis_result_array.first : 7
  end


  def update_diagnosis_result
    if diagnosed?
      update_columns(diagnosis_result: diag_result_code)
    end
  end

  def diagnosis_result_name
    if diagnosed? && DIAGNOSIS_RESPONSE[diagnosis_result]
      DIAGNOSIS_RESPONSE[diagnosis_result][:action]
    end
  end

  def negative_results
    results = []
    results << 'Customer has no money' if question1 == false
    results << 'Customer is not reachable' if question2 == false
    results << 'Customer has no reason to buy' if question3 == false
    results << 'We don\'t yet have delivery capabilites' if question4 == false
    results << 'Market has entrenched competition' if question5 == false
    results << 'Market lacks growth opportunites' if question6 == false
    results << 'We are not very passionate about this' if question7 == false
    if results.present?
      results.join(' · ')
    else
      'This market is healthy'
    end
  end

end