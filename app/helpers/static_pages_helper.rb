module StaticPagesHelper

  ## devise mapping

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def resource_class
    devise_mapping.to
  end


  ## blueprints for row generation
  #
  # def blueprint_master
  #
  #   {
  #       :enduser_user_rec => {
  #           1 => {size: 4, field: :parameter, options: {type: :text, editable: true, deletable: true, class: 'enduser-u-rec'}},
  #           2 => {size: 8, field: :response, options: {type: :text, editable: true}}
  #       },
  #
  #       :enduser_admin_rec => {
  #           1 => {size: 4, field: :parameter, options: {type: :text, editable: false, deletable: false}},
  #           2 => {size: 8, field: :response, options: {type: :text, editable: true}}
  #       },
  #
  #       :enduser_selectable => {
  #           1 => {size: 4, field: :parameter, options: {type: :text, editable: false, deletable: false}},
  #           2 => {size: 8, field: :response, options: {type: :select, modal: false, choices_array: RANGE}}
  #       },
  #
  #       :value_propositions => {
  #           1 => {size: 4, field: :proposition_name, options: {type: :text, editable: true, deletable: true, placeholder: 'Enter the proposition your product offers', class: 'vprop-rec'}},
  #           2 => {size: 2, field: :benefit_type, options: {type: :select, choices_array: BENEFIT_TYPE, modal: false}},
  #           3 => {size: 3, field: :as_is_state, options: {type: :text, modal: false, editable: true, deletable: false, placeholder: 'As-is State'}},
  #           4 => {size: 3, field: :possible_state, options: {type: :text, modal: false, editable: true, deletable: false, placeholder: 'Possible State'}},
  #       },
  #
  #       :comp_pos_user => {
  #           1 => {size: 4, field: :competitor, options: {type: :text, editable: true, deletable: true, class: 'comp-pos-rec'}},
  #           2 => {size: 4, field: :factor1, options: {class: 'calign', type: :select, modal: false, choices_array: PP_LEVEL}},
  #           3 => {size: 4, field: :factor2, options: {class: 'calign', type: :select, modal: false, choices_array: PP_LEVEL}}
  #       },
  #
  #       :comp_pos_admin => {
  #           1 => {size: 4, field: :competitor, options: {type: :text, editable: false}},
  #           2 => {size: 4, field: :factor1, options: {class: 'calign', type: :select, modal: false, choices_array: PP_LEVEL}},
  #           3 => {size: 4, field: :factor2, options: {class: 'calign', type: :select, modal: false, choices_array: PP_LEVEL}}
  #       }
  #
  #   }
  #
  # end
  #
  #
  # def generate_row(object, step)
  #   blueprint = blueprint_master[step]
  #
  #   array = ''
  #   blueprint.each do |row_no, options|
  #     size = options[:size]
  #     type = options[:type]
  #     field = options[:field]
  #     icon_ref = options[:icon]
  #     options = options[:options]
  #
  #     icon = field_icon(icon_ref)
  #     if icon
  #       array << make_column(size: size, content: icon+content_tag(:div, make_field(object, field, options), class: 'col-xs-10'), class: "#{'choice-field' if options[:infield]}")
  #     else
  #       array << make_column(size: size, content: make_field(object, field, options), class: "#{options[:class]} #{'choice-field' if options[:infield]}")
  #     end
  #
  #
  #   end
  #
  #   make_section_row(array.html_safe, {class: 'row', id: object.row_selector(false)})
  #
  # # end

  def touchpoint_category
    {
        1 => 'Acquisition',
        2 => 'Acquisition',
        3 => 'Acquisition',
        4 => 'Activation',
        5 => 'Activation',
        6 => 'Retention',
        7 => 'Retention',
        8 => 'Revenue',
        9 => 'Retention',
        10 => 'Referral'
    }
  end

  def touchpoint_content
    HashWithIndifferentAccess.new(YAML.load_file("#{Rails.root}/app/views/touchpoints/hints.yml"))
  end


  def toolfooter
    "<div class='tool-footer'>
         <button class=\"angle-nav prev-tool-section\"><i class=\"fa fa-angle-left\"></i></button>
    <button class=\"angle-nav next-tool-section\"><i class=\"fa fa-angle-right\"></i></button>
    <button class=\"angle-nav close-tool-section\"><i class=\"fa fa-close\"></i></button>
    </div>".html_safe
  end

  def serial_no(num)
    "<div class=\"sl-no\">
    <div class=\"serial-num\">#{num}</div>
    <div class=\"expand-sign\"><span class=\"fa fa-plus\"></span></div>
  </div>".html_safe
  end


  def favourite(favourite)
    if (1..20).include? favourite
      step_card favourite
    else
      '<div class="empty-fav">Select a tool from Toolkit</div>'.html_safe
    end
  end

  def workspace_tools
    ((1..15).to_a) - board_tools
  end

  def board_tools
    [9, 11, 12, 14, 15]
  end


  def related_tools

  end

  def tool_group_cards


    step_num = active_product.last_visited_step


    [
        #
        # {tool_card_title: 'Product Management',
        #  tool_card_sub_text: 'Product road map',
        #  tool_card_body: render('static_pages/shared/toolgroup_product_iteration_body'),
        #  tool_link: make_path,
        # },

        {tool_card_title: 'Strategy Execution',
         tool_card_sub_text: 'Strategy decision canvas',
         tool_card_body: render("static_pages/shared/toolgroup_strategy_body", step_num: step_num),
         tool_link: strategy_toolkit_path,
        },




        # {tool_card_title: 'Customer Interaction',
        #  tool_card_sub_text: 'Customer experience board',
        #  tool_card_body: render('static_pages/shared/toolgroup_customer_interaction_body'),
        #  tool_link: customer_insights_path,
        # },

    ]
  end

  def tools_for_stage(stage)

    keys = case stage
             when 1
               [4, 6, 7, 8, 10]
             when 2
               [11, 12, 13, 14, 15]
             when 3
               [14, 15, 17, 18, 19]
             when 4
               [1, 5, 10, 13, 14]
           end

    STEP.slice(*keys)
  end

  QUESTION_FIELDS = {
        1 =>  {question: 'What problem do I solve for the beachhead end users?',            field: :application,              placeholder:"Google made search easier. Amazon simplified online buying and selling. What is it, that my product going to make better? What is the one problem that my product will solve?"                      },
        2 =>  {question: 'What are the top challenges with the each of the markets?',       field: :follow_on_markets,        placeholder:"Challenges may relate to delivery capabilities, compelling reason to buy or unavailability of direct sales access with the customers in the particular market."                      },
        4 =>  {question: 'How big is my beachhead market?',                                 field: :beachhead_size,           placeholder:"No product lives forever, but during its lifetime it serves many customers. How many customer will my product serve during its lifetime?"                      },
        3 =>  {question: 'Who is my end user?',                                             field: :end_user,                 placeholder:"When I make a Google search, I become its end user. For a toy manufacturer, the kids are the end users and not the parents who pay for the toys. Whose problem is my product solving? Whose life gets better with my product?"                      },
        9 =>  {question: 'What is my core or \'that one special thing\'?',                  field: :core,                     placeholder:"\"Core\" allows me to deliver the benefits my customers value the most with much greater effectiveness than any other competitor. This is that one thing that will make it very difficult for the next company that tries to copy and compete. LinkedIn's core is Network Size, Apple’s is User Experience and Wal Mart's is Lowest Cost. What is it that I have do better than anyone else to improve my venture sustainability?"                      },
        5 =>  {question: 'What are top 3 purchasing priorities of the end users?',          field: :top_user_priorities,      placeholder:"Most relevant, Quick and Free results are the top three purchasing priorities of Google's end users. Convenient shopping experience, large collection of products to shop from and lowest price are the 3 key purchasing priorities of Amazon's end users. What are the 3 things that end users will look for in my product?"                      },
        13 => {question: 'Who are the top influencers to make a sale?',                     field: :dmu,                      placeholder:"Who are the key people who influence my End user's decision to buy and use my product? The four category of people who influence the end user and the sales are advocates, Influencers, Economic buyer and VETO holder. Who are these people for my product?"                      },
        6 =>  {question: 'What is the targeted customer retention rate?',                   field: :use_case,                 placeholder:"WDS survey of more than 3,000 smartphone owners asked them whether they planned to stick with their current brand the next time they bought a new device. Apple fans were the most loyal, as 76% said they planned to stick with the iPhone. Samsung came in second place, with 58% of Galaxy owners saying they planned to buy a Samsung phone with their next purchase. What is the targeted customer retention rate for my product?"                      },
        7 =>  {question: 'What are mandatory features my product will be made of?',         field: :prod_features,            placeholder:"Google search engine’s mandatory features are search box, search result list, pagination and advertisement area.What are the mandatory features without which my product will not solve end users problem? Generally, it will be between 3 to 7 features."                      },
        8 =>  {question: 'What are my top promises to every single customer?',              field: :value_proposition,        placeholder:"Steve Jobs made three promises during the iPhone 2007 launch - that it was a widescreen iPod with touch device, revolutionary mobile phone and a smart mobile device. What are the top three promises that my product will deliver to each of my customers?"                      },
        10 => {question: 'Who are my top competitors?',                                     field: :top_competitors,          placeholder:"Who are those making similar products—real or perceived? This is from the customer’s perspective and not just my view."                      },
        11 => {question: 'What should be the frequency of my inflow from each customer?',   field: :business_model,           placeholder:"Google Search uses Advertisement model, Apple MacBook uses One-time upfront model, and the KFC primarily works on Franchise model. What should be the business model that will protect the interest of my team and result in a sustainable business?"                      },
        12 => {question: 'What is the prevailing market price range?',                      field: :pricing,                  placeholder:"Gmail subscription account offers two pricing plans-  Annual and monthly payment options. What are the top pricing plans that’s fair to both my team and the customers?"                      },
        14 => {question: 'How will the customers know about my product?',                   field: :marketing,                placeholder:"How will I spread the word about my product to the early customers? Should I offer free trails or demos, make cold calls, contribute content, join groups (both in-person and online) and enter key sales tie-ups?"                      },
        15 => {question: 'How many customers have I reached out so far?',                   field: :customer_reach,           placeholder:"Facebook during its early days went to the doorsteps of the early adopting universities and recruited its users and set up the Facebook accounts for them. Early days of a startup is primarily driven by manual customer recruitment. How many customers have been recruited so far?"                      },
    }





  def canvas_fields
    [
        {icon:  1,      title: 'Beachhead',             step: 1 },
        {icon:  2,      title: 'Markets',               step: 2 },
        {icon:  3,      title: 'Market Size',           step: 4 },
        {icon:  4,      title: 'End User',              step: 3 },
        {icon:  5,      title: 'Core',                  step: 9 },
        {icon:  6,      title: 'Priorities',            step: 5 },
        {icon:  7,      title: 'DMU',                   step: 13},
        {icon:  8,      title: 'Use Case',              step: 6 },
        {icon:  9,      title: 'Product Spec',          step: 7 },
        {icon: 10,      title: 'Value Proposition',     step: 8 },
        {icon: 11,      title: 'Competition',           step: 10},
        {icon: 12,      title: 'Business Model',        step: 11},
        {icon: 13,      title: 'Pricing',               step: 12},
        {icon: 14,      title: 'Sales Planning',        step: 14},
        {icon: 15,      title: 'Customer Recruitment',  step: 15},
    ]
  end


  def default_discussions
    [
        [top_pitfalls_path, "How to get the best out of EdisonPlan?"],
        [four_pillars_path, "Four pillars of the business"],
        [welcome_path,      "Ask us anything"],
    ]
  end

end






































