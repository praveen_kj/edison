module MarketSizeHelper

  def market_size_result
    if new_business_checklist_complete?
      r = []
      r << "#{business_name} market is too wide and #{product.product_name} will end up competing with dissimilar products." if smp_fail?
      r << "#{business_name} market, as wide as it is, might require a larger sales team" if ssp_fail?
      r << "There is no word of mouth in #{business_name} market. If the customers aren't talking to each other, it may be difficult for #{product.product_name} to take off." if wom_fail?
      r << "#{business_name}  looks focused, lucrative and worthy to proceed as beachhead" unless is_too_big?
    end
    r
  end


  def size_result
    if new_business_checklist_complete?
      if is_too_big?
        'Too wide - needs to be narrowed down'
      else
        'Rightly sized'
      end
    else
      '?'
    end
  end


  def new_business_checklist_complete?
    if word_of_mouth != nil && similar_product != nil && similar_sales != nil
      true
    else
      false
    end
  end

  def new_business_checklist_not_started?
    if word_of_mouth == nil && similar_product == nil && similar_sales == nil
      true
    else
      false
    end
  end

  def is_too_big?
    if wom_fail? || smp_fail? || ssp_fail?
      true
    else
      false
    end
  end

  def is_right_sized?
    !self.is_too_big?
  end

  def wom_fail?
    if word_of_mouth == false
      true
    else
      false
    end
  end

  def smp_fail?
    if similar_product == false
      true
    else
      false
    end
  end

  def ssp_fail?
    if similar_sales == false
      true
    else
      false
    end
  end


end