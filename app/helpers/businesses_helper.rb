module BusinessesHelper

  def delete_business_link(business)
    button_to delete_business_path(business.id), remote: true, method: :get, class: 'transparent-btn delete-btn-link' do
      business.business_name
    end
  end

  def select_as_beachhead(business, button_name = 'analyze', options = {})
    other_params = options[:other_params] || {}
    button_to update_beachhead_path(params: {product: {active_market: business.id}.merge(other_params)}),
              method: :put,
              :form_class => "bh-select-form select-as-bh-form-#{business.id}",
              remote: true,
              class: "bh-select-btn-#{business.id} bh-select-btn #{options[:class]} #{'active' if is_beachhead?(business)}",
              data: {disable_with: disabled_spinner} do
      button_name
    end


  end

  def market_names
    active_product.businesses.pluck(:business_name).to_sentence
  end


  def diagnosis_response_list
    response_array = []
    DIAGNOSIS_RESPONSE.each do |k, v|
      if response_array.length < 3
        if beachhead.send("question#{v[:question_no].to_s}") == false
          response_array.push(k)
        end
      end
    end
    response_array
  end

  # def diagnosis_result_array(business)
  #   response_array = []
  #   DIAGNOSIS_RESPONSE.each do |k, v|
  #     if response_array.length < 1
  #       if business.send("question#{v[:question_no].to_s}") == false
  #         response_array.push(k)
  #       end
  #     end
  #   end
  #   response_array
  # end

  def diagnosis_result(business)
    result_array = business.diagnosis_result_array
    result_array.length == 0 ? :green : DIAGNOSIS_RESPONSE[result_array.first][:color]
  end

  def diagnosis_result_sentence(business)
    h = {
        1 => 'the customer has no compelling reason to buy your product',
        2 => 'the customer does not have enough money to buy your product',
        3 => 'your competitors are too strong',
        4 => 'your personal goals are not aligned with your business goals',
        5 => 'you don\'t have a direct sales channel',
        6 => 'you cannot expand to adjacent markets',
        7 => 'you are yet to have a team capable of building it',
    }

    result_array = business.diagnosis_result_array
    h.values_at(*[result_array]).to_sentence
  end

  # def diag_result_business_list(business)
  #   result_array = diagnosis_result_array(business)
  #   hash = {:black => 'result-1', :red => 'result-2', :orange => 'result-3', :green => 'result-4'}
  #   result = result_array.length == 0 ? 'result-4' : hash[DIAGNOSIS_RESPONSE[result_array.first][:color]]
  #   'diag-result '+result
  # end


  def diagnosis_cta(business)
    case diagnosis_result(business)
      when :black
        'Abort this market'
      when :red
        'Proceed with caution or choose a new market'
      when :orange
        'Proceed with caution'
      when :green
        'You are clear to proceed'
    end
  end

  def business_properties
    [['application', 'Application', "What problem do I solve?"],
     ['end_user', 'End User', "Who is my end user?"],
     ['benefit', 'Benefit', "What are top 3 purchasing priorities of the end users?"],
     ['sales', 'Sales', "Who are the top influencers to make a sale?"],
     ['prod_features', 'Product Features', "What are my core features?"],
     ['marketing', 'Marketing', "How will the customers know about my product?"],
     ['partners', 'Partners', "Who are the key partners I need to make the product and sell it?"],
     ['core', 'Core', "What is my core or \"that one special thing\"?"],
     ['top_competitors', 'Competitors', "Who are my top competitors?"]]
  end

  def tool_group
    {
        product: 'Identify the product that my beachhead end users want',
        customer: 'Validate the beachhead product with customers',
        competition: 'Develop my core to sail through the competition',
        pricing: 'Adopt a business model and create a suitable pricing plan',
        delivery: 'Start full fledged sales, customer service, and product iteration'
    }
  end

  def diag_result(question)
    result = beachhead.send("question#{question}")
    if result
      result = 'Yes'
    elsif result == false
      result = 'No'
    else
      result = ''
    end
    content_tag(:span, result, class: "diag-result-tag #{result.downcase}")
  end

  def end_user_personality_trait(business, param)
    h = {
        '1' => 'Highly',
        '2' => 'Reasonably',
        '3' => 'Not very',
    }

    h[business.endusers.where(parameter: param).first.response]

  end

  def end_user_motivations(business)

    m, f1, f2 = business.endusers.where(parameter: ['Motivation to use',
                                                    'Fear that prevents them from using',
                                                    'Fear that drives them to use']).to_a

    [
        "Motivated to use because... #{m.response}",
        "Fear that prevents them from using... #{f1.response}",
        "Fear that drives them to use... #{f2.response}"
    ]
  end

  def end_user_demographic_snapshot(business)

    g, a, w, e, l = business.endusers.where(parameter: ['Gender Mix',
                                                        'Age Range',
                                                        'Wealth and Income',
                                                        'Educational background',
                                                        'Location']).to_a

    "#{g.response} · aged around #{a.response} · earns #{w.response} · educated #{e.response} · located at #{l.response}"

  end


end
