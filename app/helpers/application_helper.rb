module ApplicationHelper

  def full_title(page_title)
    base_title = "Edison"
    if page_title.empty?
      base_title
    else
      page_title +" | "+ base_title
    end
  end

  def get_title(view)
    if view.nil? || view == 'home'
      'Toolkit | Edison'
    else
      view.humanize.titleize+' | Edison'
    end
  end

  def header_pages
    urlpresent?('app', 'products')
  end

  def active_product
    if @active_product
      @active_product
    else
      if current_user.all_products.include? current_user.active_product_id
        @active_product = Product.where(id: current_user.active_product_id).first
      else
        nil
      end
    end
  end

  def beachhead
    if @beachhead
      @beachhead
    else
      @beachhead = active_product ? active_product.beachhead : nil
    end
  end

  def current_step
    # session[:current_step] || 1
  end


  def form_for_step(step_no)
    case step_no
      when 8
        button_name, table, params = '+ New Value Proposition', :value_metric, {proposition_name: ''}
      when 9
        button_name, table, params = '+ New Core', :comp_advantage, {adv_name: 'New Core'}
      when 11
        button_name, table, params = '+ New Business Model', :pricing_model, {model_name: 'Business model'}
      when 12
        button_name, table, params = '+ New Pricing Plan', :pricing_plan, {plan_name: 'Pricing Plan'}
      when 14
        button_name, table, params = '+ New Sales Plan', :sales_strategy, {strategy_name: 'Sales Plan'}
      when 15
        button_name, table, params = '+ New Customer', :customer, {name: 'Customer'}
      when 20
        button_name, table, params = '+ New Milestone', :main_activity, {main_activity_name: 'New Milestone'}
      else
        return
    end
    if has_valid_access?
      button_to_create(button_name, table, params, {class: 'add-a-field'})
    else
      dummy_add_button(button_name)
    end
  end

  def urlpresent?(*args)
    args.each do |a|
      if request.original_url.include? a
        return true
      end
    end
    false
  end

  def app_restricted_container
    urlpresent?('sign_in', 'sign_up', 'password', 'support')
  end

  def business_count
    active_product.businesses.all.size
  end

  def no_business?
    business_count == 0
  end


  def ldt(date, format = "%d %b %Y")
    date.strftime(format) unless date.nil?
  end

  def sdt(date)
    date.strftime("%d %b") unless date.nil?
  end

  def admin_user?
    current_user.admin?
  end

  def step_list_by_group(step_group)
    STEP.select { |k, v| v[:group] == step_group }
  end

  def step_groups
    {market: 'Which is my beachhead market?',
     product: 'What is the solution my product provides?',
     customer: 'Who are my customers?',
     competition: 'What is my market share?',
     pricing: 'What the fair price for my product?',
     delivery: 'What does it take to deliver my product?'}
  end

  def no_pitch?
    true if beachhead.pitches.count == 0
  end

  def profile_updated?
    current_user.user_parameter.present?
  end

  def profile_not_updated?
    current_user.user_parameter.blank?
  end

  def sprites
    sprites = {
        1 => {:x => 79, :y => 657},
        2 => {:x => 79, :y => 697},
        3 => {:x => 11, :y => 697},
        4 => {:x => 11, :y => 257},
        5 => {:x => 11, :y => 297},
        6 => {:x => 79, :y => 457},
        7 => {:x => 11, :y => 337},
        8 => {:x => 11, :y => 377},
        9 => {:x => 11, :y => 457},
        10 => {:x => 11, :y => 497},
        11 => {:x => 11, :y => 537},
        12 => {:x => 11, :y => 417},
        13 => {:x => 11, :y => 577},
        14 => {:x => 11, :y => 617},
        15 => {:x => 11, :y => 657},
        16 => {:x => 79, :y => 337},
        17 => {:x => 79, :y => 377},
        18 => {:x => 74, :y => 497},
        19 => {:x => 79, :y => 257},
        20 => {:x => 79, :y => 417}
    }

  end

  def sprites2
    {
        use_case_icon: {x: 16, y: 13},
        enduser_mood: {x: 21, y: 70},
        people_involved: {x: 21, y: 107},
        things_needed: {x: 21, y: 145},
        hurdles_faced: {x: 21, y: 145}
    }
  end

  def sprites3
    {
        1 => {:x => 79, :y => 657},
        2 => {:x => 79, :y => 697},
        3 => {:x => 11, :y => 697},
        4 => {:x => 11, :y => 257},
        5 => {:x => 11, :y => 297},
        6 => {:x => 79, :y => 457},
        7 => {:x => 11, :y => 337},
        8 => {:x => 11, :y => 377},
        9 => {:x => 11, :y => 457},
        10 => {:x => 11, :y => 497},
        11 => {:x => 11, :y => 537},
        12 => {:x => 11, :y => 417},
        13 => {:x => 11, :y => 577},
        14 => {:x => 11, :y => 617},
        15 => {:x => 11, :y => 657},
        16 => {:x => 79, :y => 337},
        17 => {:x => 79, :y => 377},
        18 => {:x => 74, :y => 497},
        19 => {:x => 79, :y => 257},
        20 => {:x => 79, :y => 417}
    }
  end

  def field_icon(reference, klass = 'field-icon')
    if sprites2[reference]
      x_pos = sprites2[reference][:x]
      y_pos = sprites2[reference][:y]
      content_tag(:div, "", class: "#{klass} col-xs-2",
                  style: "background: url('#{ image_path('sprite3.png') }') no-repeat -#{x_pos}px -#{y_pos}px;")
    end
  end

  def step_icon(step_no)
    x_pos = sprites[step_no][:x]
    y_pos = sprites[step_no][:y]
    content_tag(:div, "",
                class: "step-icon",
                style: "background: url('#{ image_path('sprite-app.png') }') no-repeat -#{x_pos}px -#{y_pos}px;")
  end

  def home_icon(step_no, offset = 498)
    x_pos = sprites[step_no][:x]
    y_pos = sprites[step_no][:y] + offset
    content_tag(:div, "",
                class: "home-icon",
                style: "background: url('#{ image_path('sprite-app.png') }') no-repeat -#{x_pos}px -#{y_pos}px;")
  end


  def target_launch_date
    current_user.user_parameter.target_date
  end

  def days_left_on_trial
    ((current_user.trial_expiry_date - Time.now)/86400).to_i
  end

  def date_class(task)
    if task.target_date
      if task.target_date == Date.today
        'today'
      elsif task.target_date < Date.today
        'overdue'
      end
    end
  end

  def sidebar_tab_item(item_name, view, title=nil, hidden=false)

    default_class_condition = view == item_name || (item_name == 'steps' && step_views.include?(view))

    content_tag(:li, '', id: "#{item_name}_tab",
                class: "tab-button #{'default ' if default_class_condition} #{'hidden' if hidden}",
                'data-href' => app_path(view: item_name),
                'data-disabled' => beachhead ? false : true,
                'data-title' => item_name.capitalize,
                'data-tab-ref' => item_name
    )

  end

  def header_tab_item(item_name, content=nil)

    content ||= item_name.titleize

    content = content_tag(:span, content, class: 'inner')

    content_tag(:li, raw(close_button + content),
                class: "tab-item  row #{item_name}-tab",
                'data-target' => ".#{item_name}-view",
                'data-action' => 'show-tab',
                'data-href' => app_path(view: item_name)
    )

  end

  def target_tab(step_no)
    if (1..6).include? step_no
      {1 => "#beachhead_tab",
       2 => "#canvas_tab",
       3 => "#diagnosis_tab",
       4 => "#end-user_tab",
       5 => "#market-size_tab",
       6 => "#investment_tab"}[step_no]
    else
      '#steps_tab'
    end
  end


  def step_card(step_no)

    icon = home_icon step_no
    step_num = content_tag(:div, step_no, class: 'step-no')
    step_name = content_tag(:div, STEP[step_no][:name], class: 'step-name')
    fav_link = content_tag(:i, '', class: "fa fav-icon #{'active' if current_user.user_parameter.favourites.try(:include?, step_no.to_s)}",)

    content_tag(:div, icon + step_num + step_name + fav_link,
                'data-step-id' => step_no,
                'data-status' => false,
                'data-href' => app_path(view: all_tools.invert[step_no]),
                'data-tab-ref' => all_tools.invert[step_no],
                class: "step-circle #{STEP[step_no][:group].to_s}",
                'data-toggle' => 'tooltip',
                'data-title' => STEP[step_no][:step_text]
    )

  end

  def step_link(link_name, step_num, options = {})
    content_tag :div, link_name, 'data-href' => app_path(view: STEP[step_num][:name].parameterize), 'data-behaviour' => 'open_in_new_tab', class: "#{options[:class]}"
  end

  def step_href_path(step_num)
    app_path(view: STEP[step_num][:name].parameterize)
  end

  def pseudo_link(name, href, klass = '')
    content_tag :span, name, 'data-href' => href, 'data-behaviour' => 'open_in_new_tab', class: klass
  end

  def validate_link(link_name, assumption)

    if preset_condition(assumption.preset_id)
      content_tag :div, link_name, 'data-href' => app_path(view: 'home', tool: 'validation', resource_id: assumption.id), 'data-behaviour' => 'open_link_in_new_sheet'
    else
      content_tag :div, "#{link_name} [to be unlocked]"
    end
  end

  def validation_links(product_name, beachhead_name)

    link_names = [
        "Validate the problem that #{product_name} is trying to solve (+10%)",
        "Validate the diagnosis (+10%)",
        "Validate if #{beachhead_name} is appropriately sized (+10%)",
        "Validate the analysis of the end user in #{beachhead_name} (+10%)",
        "Validate the market value of #{beachhead_name} (+10%)",
        "Validate the purchasing priorities of the persona (+10%)",
        "Validate the Decision Making Unit in #{beachhead_name} (+10%)",
        "Validate the #{product_name}'s customer accessibilty (+10%)",
        "Validate the MVP features of #{beachhead_name} (+10%)",
        "Validate the Value Proposition offered by #{product_name} (+10%)",
    ]

    temp = {}
    assumptions = active_product.assumptions.preset

    assumptions.each_with_index do |assumption, index|
      temp[index+1] = validate_link(link_names[index], assumption)
    end
    temp
  end

  def resource_link(link_name, resource)

  end

  def prev_step_button(current_step)

    prev_step = all_tools.invert[current_step-1]

    button_tag STEP[current_step-1][:name],
               'data-tab-ref' => prev_step,
               'data-href' => app_path(view: prev_step),
               'data-behaviour' => 'open_in_new_tab',
               class: 'next-step-link'
  end

  def next_step_button(current_step)
    next_step = all_tools.invert[current_step+1]

    button_tag STEP[current_step+1][:name],
               'data-tab-ref' => next_step,
               'data-href' => app_path(view: next_step),
               'data-behaviour' => 'open_in_new_tab',
               class: 'next-step-link'
  end

  def go_to_canvas
    button_tag 'Canvas',
               'data-target-tab' => target_tab(2),
               'data-href' => app_path(view: 'canvas'),
               'data-status' => false,
               class: 'next-step-link go-to-canvas link-hover'
  end


  def rand_string
    (0...6).map { (65 + rand(26)).chr }.join
  end

  def markdown
    Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
  end


  def tool_hash

    k = 0

    STEP.values.collect do |item|
      k+=1
      [k, item[:name]]
    end.to_h

  end


  def all_tools

    k = 0

    STEP.values.collect do |item|
      k+=1
      [k, item[:name].parameterize]
    end.to_h.invert

  end


  def non_tool_views
    ['home', 'pitch', 'settings', 'team']
  end

  def render_tab(view)

    raw "$('.#{view}-view').html('#{j render "tab_views/#{view.gsub('-', '_')}_view"}');
    $('##{view}_tab').showTab();"

  end


  def notes_count(step)
    beachhead.notes.current.for_step(step).count
  end

  def assumptions_count(step)
    beachhead.assumptions.current.for_step(step).joins(:experiments).count
  end

  def todos_count(step)
    beachhead.user_tasks.pending.for_step(step).joins(:sub_tasks).count
  end

  def user_initials(name)
    if name.present?
      f, l = name.split
      "#{f.first}#{l.first if l}".upcase
    else
      '?'
    end
  end

  def header_tab_item(tab_name, options = {})

    tab_name_tag = content_tag(:span, tab_name, class: 'inner')

    tab_content = if tab_name == 'Home'
                    tab_name_tag
                  else
                    close_button + tab_name_tag
                  end

    content_tag(:li, tab_content,
                class: "row tab-item #{'active' if options[:active]}",
                data: {
                    'tab-id' => rand_tab_id,
                    'tab-href' => request.fullpath,
                    'behaviour' => 'show_tab'
                })

  end

  def rand_tab_id
    @rand_tab_id ||= rand_string
  end

  def days_ago(date)
    (Date.today.to_date - date.to_date).to_i
  end



end