module DashboardHelper

  def value_proposition_statement
    if beachhead.product_value && beachhead.industry_value
      if beachhead.product_value > beachhead.industry_value
        "Value promised to a single customer is $ #{beachhead.product_value}. Higher than Industry's promise by $ #{beachhead.quantified_value_proposition}"
      else
        "Value promised to a single customer is $ #{beachhead.product_value}. Lower than Industry's promise by $ #{beachhead.quantified_value_proposition}"
      end
    end
  end

  # def make_dashboard_content
  #
  #   vp = beachhead.product_value && beachhead.industry_value ? "#{active_product.product_name}: #{beachhead.product_value} > Industry: #{beachhead.industry_value}" : '?'
  #
  #   scale = nil
  #   if p = beachhead.accessibility_profile
  #
  #     i1 = content_tag(:div, "#{p[0]*10} %", 'data-toggle' => 'tooltip', 'data-title' => 'Good', class: 'component good', style: "width: #{p[0]*10}%")
  #     i2 = content_tag(:div, "#{p[1]*10} %", 'data-toggle' => 'tooltip', 'data-title' => 'Ok', class: 'component ok', style: "width: #{p[1]*10}%")
  #     i3 = content_tag(:div, "#{p[2]*10} %", 'data-toggle' => 'tooltip', 'data-title' => 'Bad', class: 'component bad', style: "width: #{p[2]*10}%")
  #
  #     scale = content_tag(:div, i1+i2+i3, class: 'accessibility-indicator').html_safe
  #   end
  #
  #
  #   [
  #       [
  #           step_link('Top Priority of Persona', 6),
  #           "#{beachhead.priority1 || '?'}"
  #       ],
  #
  #       [
  #           step_link("Advocate for #{active_product.product_name}", 7),
  #           "#{beachhead.advocate || '?'}"
  #       ],
  #
  #       [
  #           step_link("Customer Accessibility of #{active_product.product_name}", 8),
  #           scale || '?'
  #       ],
  #
  #       [
  #           step_link("Minimum features", 9),
  #           "#{feature_list(:minimal)}"
  #       ],
  #
  #       [
  #           step_link("MVP features", 9),
  #           "#{feature_list(:mvp)}"
  #       ],
  #
  #       [
  #           step_link("Value proposition", 10),
  #           vp
  #       ],
  #
  #
  #   ]
  # end

  def feature_classification
    ['minimal product', 'mvp', 'full featured product', 'uncategorized']
  end

  def latest_update_from_n10c
    nc = beachhead.next_customers.reorder('updated_at DESC').first
    h = {1 => 'was reached out to', 2 => "is now using #{active_product.product_name}", 3 => "quit using #{active_product.product_name}", 4 => "has paid for and using #{active_product.product_name}"}
    if nc
      "#{nc.cust_name} #{h[nc.stage]}"
    end
  end

  def latest_update_from_customers
    nc = beachhead.customers.reorder('updated_at DESC').first
    h = {1 => 'was reached out to',
         2 => "is now activated",
         3 => "is now retained",
         4 => "has paid for and using #{active_product.product_name}",
         5 => "has stopped using #{active_product.product_name}"
    }
    if nc
      "#{nc.name} #{h[nc.stage]}"
    end
  end

  def active_dash
    @active_dash ||= current_user.dashboards.where(product_id: active_product.id).first.try(:dashboard_ref)
  end

  def front_page
    num = active_dash
        case num
          when 1..15
            render 'tab_views/partials/steps_front_page', step: num
          when 16
            render 'tab_views/partials/canvas_paper', _canvas_fields: canvas_fields
          when 17
            render 'tab_views/partials/canvas_list', _canvas_fields: canvas_fields
          when 18
            render 'tool_views/discussions'
          when 19
            render 'tool_views/to_do_lists'
          when 20
            render 'tool_views/notes'
          when 21
            render 'customer_feedbacks/customer_feedback_frontpage'
          else
            render 'tab_views/partials/canvas_paper', _canvas_fields: canvas_fields
        end
  end

  def dashboard_choices
    {
        16 => 'Strategy Toolkit - sheet view',
        17 => 'Strategy Toolkit - list view',
        18 => 'Discussions',
        19 => 'To Do Lists',
        20 => 'Notes',
        21 => 'Customer Insights',
    }.merge(STEP.collect { |k, v| [k, v[:name]] }.to_h)
  end


end