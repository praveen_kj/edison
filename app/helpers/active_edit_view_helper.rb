module ActiveEditViewHelper

  include DomHelper::InstanceMethods

  def ae_live_field(object, field, options = {})
    display_method = options[:display_with]
    output = display_method.present? ? send(display_method, object.send(field)) : object.send(field)

    content_tag :span, output || options[:placeholder],
                'data-live-field' => "#{object.class.table_name}-#{field.to_s}-#{object.id}"

  end

  def active_edit(object, field, options = {})
    case options[:type]
      when :button_choose
        raise 'You need to supply "choices" for select helper' unless options[:choices]
        button_choose object, field, options[:choices], options
      when :select
        raise 'You need to supply "choices" for select helper' unless options[:choices]
        dropdown object, field, options[:choices], options
      when :date
        date_selector object, field, options
      when :checkbox
        checkbox object, field, options
      else
        text_input object, field, options
    end
  end

  def active_edit_if(condition, object, field, options = {})
    if condition
      active_edit object, field, options
    else
      content_present = object[field].present?
      placeholder = options[:placeholder] || 'click to edit'
      if options[:type] == :select
        content = object.send(field) ? options[:choices][object.send(field)] : placeholder
      else
        content = "#{options[:prefix]}#{ options[:display_with] ? send(options[:display_with], object[field], precision: options[:precision]) : object[field]}#{options[:suffix]}"
      end
      inner_content = content_tag(:div, content_present ? content : placeholder, class: "content #{'placeholder' unless content_present}")
      content_tag(:div, inner_content, class: "active-edit empty")
      # data: {toggle: 'tooltip', title: 'Subscribe to Edit', placement: 'left'}
    end
  end

  def button_choose(object, field, choices, options = {})


  end

  def dropdown(object, field, choices, options = {})

    placeholder = options[:placeholder] || 'select'

    dd_content = ''

    btn_name = if object[field] && object[field].to_i.to_s == object[field].to_s
                 choices[object[field].to_i]
               elsif object[field]
                 choices[object[field]]
               else
                 placeholder
               end

    button_name = content_tag(:span, btn_name.try(:html_safe), class: "button-name #{'placeholder' unless object[field]}")+content_tag(:span, '', class: 'caret')

    dd_content << button_tag(button_name, class: 'active-edit-dd-btn dropdown-toggle', 'data-toggle' => 'dropdown')

    list = content_tag(:ul, choice_list(choices, action_dom_id(object, field)), class: 'dropdown-menu')

    dd_content << list.html_safe

    content_tag(:div, dd_content.html_safe,
                {class: "active-edit dropdown #{options[:class]}",
                 id: action_dom_id(object, field),
                 'data-type' => 'dropdown',
                 'data-edit-url' => options[:url] || "/#{object.class.table_name}/#{object.id}",
                 'data-object-name' => "#{object.underscored}",
                 'data-edit-field' => "#{field.to_s}"}.reverse_merge!(options)
    )

  end

  def date_selector(object, field, options = {})

    placeholder = options[:placeholder] || 'set target date'

    content_present = object[field].present?

    content = content_tag(:div, ldt(object[field]) || placeholder, class: "content #{'placeholder' unless content_present}")

    content_tag(:div, content,
                {class: "active-edit date-select #{options[:class]}",
                 id: action_dom_id(object, field),
                 'data-prev-value' => "#{object[field]}",
                 'data-type' => 'date',
                 'data-format' => options[:format],
                 'data-edit-url' => "/#{object.class.table_name}/#{object.id}",
                 'data-object-name' => "#{object.underscored}",
                 'data-edit-field' => "#{field.to_s}"}.reverse_merge!(options))
  end

  def checkbox(object, field, options = {})

    other_attrs = options[:other_attrs] || {}

    default_class = options[:default_class] || 'active-check-box'
    checked_class = options[:checked_class] || 'checked'

    content_tag(:div, '',
                {id: action_dom_id(object, field),
                 class: "active-edit check_box #{default_class} #{checked_class if object[field]}",
                 'data-type' => 'checkbox',
                 'data-edit-url' => options[:url] || "/#{object.class.table_name}/#{object.id}",
                 'data-object-name' => "#{object.underscored}",
                 'data-edit-field' => "#{field.to_s}"}.merge(other_attrs)
    )

  end


  def text_input(object, field, options = {})

    placeholder = options[:placeholder] || 'click to edit'
    content_present = object[field].present?
    other_attrs = options[:other_attrs] || {}


    enhanced_content = "#{options[:prefix]}#{ options[:display_with] ? send(options[:display_with], object[field], precision: options[:precision]) : object[field]}#{options[:suffix]}"


    content = content_tag(:div, content_present ? enhanced_content : placeholder,
                          class: "content #{options[:field_class]} #{'placeholder' unless content_present}",
                          'data-parent' => action_dom_id(object, field, true),
                          'data-char-limit' => options[:char_limit],
    )


    content_tag(:div, content,
                {id: action_dom_id(object, field),
                 class: "active-edit #{options[:class]} #{options[:numeric] ? 'numeric' : 'text-field' }",
                 'data-type' => "#{options[:numeric] ? 'numeric' : 'text-field'}",
                 'data-edit-url' => "/#{object.class.table_name}/#{object.id}",
                 'data-object-name' => "#{object.underscored}",
                 'data-placeholder' => placeholder,
                 'data-prev-value' => "#{object[field]}",
                 'data-edit-field' => "#{field.to_s}",
                 'data-prefix' => options[:prefix],
                 'data-suffix' => options[:suffix],
                 'data-precision' => options[:precision],
                 'data-unique-signature' => rand_string,
                 'data-form-class' => "#{'numeric' if options[:numeric]}",
                }.reverse_merge!(other_attrs)
    )


  end


  private

  def rand_string
    (0...6).map { (65 + rand(26)).chr }.join
  end

  def field_id(object, field, pound = false)
    "#{'#' if pound}#{object.underscored.pluralize}_#{field.to_s}_#{object.id}"
  end

  def choice_list(choices, parent_id)
    list = ''
    choices.each do |k, v|
      list << content_tag(:li, v.try(:html_safe),
                          class: 'option',
                          'data-option-value' => k,
                          'data-parent' => parent_id
      )
    end

    list.html_safe
  end

  def action_dom_id(object, field, prefix = false)
    "#{'#' if prefix}active_edit_#{field_id(object, field)}"
  end


  def respond_with_active_edit(object, field)

    js_response = <<-JS
$('[data-edit-url=\"/#{object.underscored.pluralize}/#{object.id}\"][data-edit-field=#{field.to_s}]').activeUpdate(#{ (object[field].to_json)});
$('[data-live-field=\"#{object.class.table_name}-#{field.to_s}-#{object.id}\"]').html(#{ (object[field].to_json)});
    JS

    table_name = object.class.table_name
    callback = lookup_context.find_all("#{table_name}/_#{field.to_s}").any?

    if callback
      render(
          '/active_edit',
          locals: {
              default_response: js_response,
              callback_script: "#{table_name}/#{field.to_s}"
          }
      )
    else
      render text: js_response
    end


  end

end