module CalendarHelper

  def calendar(date = Date.today, &block)
    Calendar.new(self, date, block).table
  end

  def calendar_body(date = Date.today, &block)
    Calendar.new(self, date, block).tbody
  end

  class Calendar < Struct.new(:view, :date, :callback)
    HEADER = %w[SUN MON TUE WED THU FRI SAT]
    START_DAY = :sunday

    delegate :content_tag, to: :view
    delegate :raw, to: :view

    def table
      content_tag :table, class: "calendar active-edit" do
        thead + tbody
      end
    end

    def thead
      content_tag :thead, header
    end

    def tbody
      content_tag :tbody, week_rows
    end

    def header
      content_tag :tr do
        HEADER.collect { |day| content_tag :th, day }.join.html_safe
      end
    end

    def week_rows
      weeks.collect do |week|
        content_tag :tr do
          week.collect { |day| day_cell(day) }.join.html_safe
        end
      end.join.html_safe
    end

    def day_cell(day)
      options = {class: "#{day_classes(day)} option #{'disabled' if day < Date.today}", 'data-parent' => '.calendar', 'data-option-value' => day}
      content = content_tag(:span, view.capture(day, &callback) + (raw '<span class="today-tag">today</span>' if day == Date.today), class: 'day-span')
      content_tag :td, content, options
    end

    def day_classes(day)
      classes = []
      classes << "today" if day == Date.today
      classes << "notmonth" if day.month != date.month
      classes.empty? ? nil : classes.join(" ")
    end

    def weeks
      first = date.beginning_of_month.beginning_of_week(START_DAY)
      last = date.end_of_month.end_of_week(START_DAY)
      (first..last).to_a.in_groups_of(7)
    end

  end
end