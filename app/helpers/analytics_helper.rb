module AnalyticsHelper

  require 'csv'


  def generate_csv(users)

    keys = ["home", "pitch", "steps", "tasks", "canvas", "end-user", "beachhead", "diagnosis", "investment", "market-size"]

    column_names = ['id',
                    'name',
                    'email',
                    'provider',
                    'sign_up_at',
                    'last_visited_at',
                    'no_of_days_used',
                    'total_timespent',
                    'timespent-home',
                    'timespent-beachhead',
                    'timespent-canvas',
                    'timespent-diagnosis',
                    'timespent-market-size',
                    'timespent-end-user',
                    'timespent-tasks',
                    'timespent-investment',
                    'timespent-steps',
                    'timespent-pitch']

    CSV.generate do |csv|
      csv << column_names
      users.each do |user|

        line = []
        line << user.id
        line << (user.user_parameter.present? ? user.name : 'profile skipped')
        line << user.email
        line << user.provider
        line << ldt(user.created_at)
        line << ldt(user.current_streak_end_at)
        line << user.active_days
        line << (user.timespent_profile.present? ? user.timespent_profile.values.collect(&:to_i).inject(:+) : 0)
        if user.timespent_profile
          10.times do |i|
            line << user.timespent_profile[keys[i]]
          end
        else
          10.times { line << 0 }
        end

        csv << line
      end
    end
  end


  def usage_streak

    footprints = Footprint.where(user_id: self.id)
    days_used = footprints.pluck(:created_at).map(&:to_date).uniq.sort.reverse

    today = Date.today
    yesterday = Date.today - 1
    streak = 0
    if days_used.include?(today) || days_used.include?(yesterday)
      start_date = days_used.include?(today) ? today : yesterday
      streak = calculate_streak(days_used, start_date)
    else
      0
    end

    streak

  end


  def timespent
    hash = {}
    ["home", "beachhead", "canvas", "diagnosis", "market-size", "end-user", "tasks", "investment", "steps", "pitch", ""].each do |view|
      hash[view] = (UsageDuration.where(user_id: self.id, view: view).pluck(:time_spent).inject(:+).to_i)/60
    end
    hash
  end

  def total_timespent
    (UsageDuration.where(user_id: self.id).pluck(:time_spent).compact.inject(:+).to_i)/60
  end

  def usage_date_range

    dates = UsageDuration.where(user_id: self.id).pluck(:created_at).compact.collect(&:to_date).uniq.sort

    date_to_check = dates[0]

    dates.slice_before { |next_date|

      date_to_check, date_just_checked = next_date, date_to_check
      date_just_checked + 1 != next_date

    }.collect { |date_range|
      {start_date: date_range[0],
       end_date: date_range[-1],
       days: (date_range[-1] - date_range[0] + 1).to_i}
    }

  end

  def prev_footprint_date
    Footprint.where(user_id: self.id)[-2].present? ? Footprint.where(user_id: self.id)[-2].created_at.to_date : nil
  end

  def no_of_days_visited
    Footprint.where(user_id: self.id).pluck(:created_at).compact.collect(&:to_date).uniq.count
  end


  private

  def calculate_streak(array, start_date)
    streak = 0
    array.each_with_index do |date, index|
      if array.include?(start_date - index)
        streak += 1
      else
        break
      end
    end
    streak
  end


end
