module StepsHelper

  def step_stages
    {find: 'Find the beachhead market',
     make: 'Make a minimal product',
     ship: 'Ship the MVBP',
     grow: "Grow #{active_product ? active_product.product_name : 'the product'} "}
  end

  def diagnosis_business_group
    [
        [1, 'Abandon'],
        [2..4, 'Proceed with extreme caution'],
        [5..6, 'Proceed with caution'],
        [7, 'Clear to go'],
    ]

  end

  def expected_users_at_touchpoint(touchpoint)

    tp_range = (1..touchpoint)

    array = []

    tp_range.each do |tp|
      obj = beachhead.touchpoints.find_by(touchpoint_no: tp)
      array << (obj.expected_conversion_rate/100.0)
    end

    expected_users = beachhead.opening_users

    (array.inject(:*) * expected_users).to_i

  end

  def descriptive_step_title(step_num)
    case step_num
      when 1
        "Beachhead selected for #{active_product.product_name} after brainstorming and primary market research"
      when 2
        "Doable lucrative markets filtered from the noisy, focus disturbing and drowning markets"
      when 3
        "Profile of narrowly defined set of #{active_product.product_name}'s end users with similar characteristics and needs."
      when 4
        "Market size of the #{active_product.product_name}’s beachhead after bottom-up and top-down analysis"
      when 5
        "Top purchasing priorities and profile of the Persona of #{active_product.product_name}"
      when 6
        "Motivation and Ease of use for all the significant touchpoints for #{active_product.product_name}'s users."
      when 7
        "List of benefit-driven features and their sub-features of #{active_product.product_name}."
      when 8
        "Value promised for every customer of #{active_product.product_name} as against the existing industry standards"
      when 9
        "Updated track of all the types Core for running #{active_product.product_name}, including the implemented option"
      when 10
        "Competition chart for #{active_product.product_name} against its competitors and the status quo customers"
      when 11
        "Updated track of all the potential business models of #{active_product.product_name}, including the implemented model"
      when 12
        "Updated track of all the pricing plans of #{active_product.product_name}, including the implemented plans."
      when 13
        "Set of people, other than the end user, who determine the ultimate purchase decision of #{active_product.product_name}"
      when 14
        "Updated track of all the sales plans of #{active_product.product_name}, including the implemented plans."
      when 15
        "Updated track of all the customers of #{active_product.product_name}, including the recently reached out"

      else
    end
  end


  def dmu_descriptions
    {
        2 => 'Advocate is the person who wants the customer to purchase the product, typically but not necessarily your end user. Multiple people can play this role.',
        3 => 'These individuals often have a depth of experience in the subject matter, and can influence the rest of the DMU, including the advocate and end user.',
        4 => 'Other influencers in the decision-making process may include media publications, individual journalists, outside contractors, friends and family, industry groups, websites, blogs, and anyone else who the Primary Economic Buyer turns to for information and feedback',
        5 => 'This is the primary decision maker, as everyone else looks to this person to sign off on spending money to purchase your product. Most often, this person controls the budget. Sometimes, the primary economic buyer is also the champion and/or the end user.',
        6 => 'These individuals have the ability to reject a purchase for any reason. Often, in a B2B environment, this individual outranks the advocate or end user in a corporate hierarchy.'
    }

  end


  def dmu_fields

    [
        ['Who is this person in the DMU?', :entity_name],
        ['What are this person\'s major fears relating to your product?', :fears],
        ['What motivates this person to use or buy your product?', :motivations],
        ['What are this person\'s expectations from your product?', :expectations],
        ['What can you offer to meet this person\'s expectations?', :offering],
        ['What other alternative products do they prefer?', :preferred_alternatives]
    ]

  end

  def touchpoint_fields
    [
        ['What does the customer do?', :end_user_actions],
        ['How motivated are they to do this?', :motivation_level],
        ['What can I do to make this touchpoint faster, better or cheaper?', :possible_intervention],
        ['How easy is it for them to do this?', :ease_of_doing],
    ]
  end

  def touchpoint_ease
    {
        1 => 'Very easy to do',
        2 => 'Somewhat easy to do',
        3 => 'Takes some effort',
        4 => 'Somewhat difficult to do',
        5 => 'Very difficult to do',
    }
  end

  def touchpoint_motivation_level
    {
        1 => 'Cannot be stopped from doing',
        2 => 'Wants to do',
        3 => 'Will do with the right triggers',
        4 => 'Doesn\'t really care',
        5 => 'Will never do it',
    }
  end

  def touchpoint_select
    {1 => 'This feature helps the customer determine they have a need?',
     2 => 'This feature helps the customer find out about your product',
     3 => 'This feature helps the customer analyze your product',
     4 => 'This feature helps the customer acquire your product',
     5 => 'This feature helps the customer install or onboard your product',
     6 => 'This feature helps the customer use your product',
     7 => 'This feature helps the customer determine the value gained',
     8 => 'This feature helps the customer pay for your product',
     9 => 'This feature helps the customer receive support for your product',
     10 => 'This feature helps the customer buy more and spread awareness'}
  end

  def all_months
    ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  end

  def feature_sections
    {
        0 => 'How it works?',
        1 => 'Sub features',
        2 => 'Benefits',
        3 => 'Detriments'
    }
  end

  def customers_wanting_features
    {
        1 => 'All customers (95%+)',
        2 => 'Most customers (60%+)',
        3 => 'Some customers (40%+)',
        4 => 'A Few customers (10%+)',
        5 => 'No customers',
    }
  end

  def feature_importance
    {
        1 => 'Would not buy without it',
        2 => 'Would be very delighted to have it',
        3 => 'Would like to have it',
        4 => 'Can live without it for some time',
        5 => 'Indifferent to it',
    }
  end

  def importance_rating
    {
        1 => "#{fa_icon('star')*5} - Would not buy without this feature",
        2 => "#{fa_icon('star')*4 + fa_icon('star-o') * 1} - Would be very delighted to have it",
        3 => "#{fa_icon('star')*3 + fa_icon('star-o') * 2} - Would like to have it",
        4 => "#{fa_icon('star')*2 + fa_icon('star-o') * 3} - Can live without it for some time",
        5 => "#{fa_icon('star')*1 + fa_icon('star-o') * 4} - Indifferent to it",
    }
  end

  def star_rating
    {
        1 => "#{fa_icon('star')*5}",
        2 => "#{fa_icon('star')*4 + fa_icon('star-o') * 1}",
        3 => "#{fa_icon('star')*3 + fa_icon('star-o') * 2}",
        4 => "#{fa_icon('star')*2 + fa_icon('star-o') * 3}",
        5 => "#{fa_icon('star')*1 + fa_icon('star-o') * 4}",
    }
  end

  def feature_group_result(feature)
    if feature.customer_wanting_feature && feature.importance_rank
      feature_result(feature.customer_wanting_feature, feature.importance_rank)
    else
      nil
    end
  end

  def feature_result(customers, importance)
    case [customers, importance]
      when [1, 1], [1, 2]
        'Minimal Product'
      when [1, 3], [2, 1], [2, 2], [2, 3], [3, 1], [3, 2], [3, 3]
        'MVP'
      else
        'Full Featured Product'
    end
  end

  def next_customer_groups
    [
        [1, 'Reached out', ''],
        [2, 'Using', 'orange'],
        [3, 'Quit', 'red'],
        [4, 'Paid', 'green'],
    ]
  end

  def next_customer_fields
    [
        ['How satisfied was this customer with the use case?', :use_case],
        ['How satisfied was this customer with the benefits offered?', :benefits],
        ['How satisfied was this customer with the value proposition offered over the competition?', :value_created],
        ['How willing was the customer to buy the product?', :purchase_interest],
    ]
  end

  def core_and_bm_groups
    [
        [1, 'Untested', ''],
        [2, 'Testing', 'orange'],
        [3, 'Dropped', 'red'],
        [4, 'Adopted', 'green'],
    ]
  end

  def comp_advantage_fields
    [
        ["How much does this as your core help to meet your customers need?", :customer_want],
        ["How much do you have as assets and resources to build this as your core?", :assets_available],
        ["How much does this core enhance your venture’s purpose?", :founders_want],
        ["How many of your competitors have this as their core?", :competitor_ability],
        ["How comfortable are your vendors with this as your core?", :vendor_ability],
        ["How much does this core helps to fulfilling the founding team’s personal goals?", :personal_goal],
        ["How much does this core helps to fulfilling the investors’ financial goals?", :financial_goal],
    ]
  end

  def business_model_fields
    [
        ["How much does this as your business model improve your customer’s willingness to buy?", :willingness_to_buy],
        ["How much does this as your business model help you to charge a fair price?", :favourable_pricing],
        ["How comfortable are your vendors with this as your business model?", :vendor_share_protected],
        ["How many of your competitors have this as their business model?", :competitor_adoption],
        ["How much does this as your business model helps to protect the distributors’ incentive?", :allows_dist_incentives],
    ]
  end

  def business_model_notes(model_name)
    a = [
        "One-time Up-Front Charge plus Maintenance model is the most common business model, where a customer pays a large up-front charge to obtain the product, with the option to secure ongoing upgrades or maintenance of the product for a recurring fee. ",
        "In Cost Plus model, the customer pays a set percentage above the cost of producing the product. This is common in government contracts as well as situations where you and your customer want to share the risk of producing the product. ",
        "Hourly Rates model tends to reward activity as opposed to progress, which can be the wrong incentive, but when a project is poorly defined or very dynamic, this might well be the preferred model. A common business model for services firms, but the rates are set by the market demand rather than costs. ",
        "In Subscription or Leasing Model there is a set payment each month or another predetermined and agreed-upon time period. It is a great way to get a recurring revenue stream. ",
        "Licensing your Intellectual property (IP) to customers and receiving a royalty can result in a very high gross margin. In addition, if you are licensing your product, you do not have to make big investments in production and distribution capability for a whole product. ",
        "In Consumables model the customer will benefit from a low up-front costs, with ongoing costs based on usage, which the customer can usually control. A highly visible and recognized example is the razor/blade model made famous by Gillette. HP is another example, where almost all if not all their profit on printers comes from selling inkjet cartridges. ",
        "Similar to the consumables business model, the central product is sold at a very low margin, but the overall margin is increased from the sale of very high-margin add-on products. This business model is often used in consumer electronics stores or websites and frequently in new car sales. Like buying a car, it is the additional items like warranty extension, accessories, rustproofing, and the like that are the high-margin products where sellers make the lion’s share of their profits. ",
        "As with newspapers and magazines in their heyday and now with websites, the ability to attract and retain a desirable demographic can be monetized through third parties who want access to the customers you have attracted. When done properly and on a sufficient scale, this can be a very lucrative model, as Google and others have shown; but many startups have fallen substantially short when they attempt to rely solely on advertising",
        "Somewhat similar to the advertising model, reselling user data requires first attracting end users with a free product, then receiving money from third parties who pay for access to demographic and other information about your users. ",
        "Online retailers often pay or receive a commission for referrals that lead to sales. One obvious example is eBay, which receives a fee from each successful auction, paid by the seller. Transaction fee model is similar to how credit card companies work, where a percentage of each transaction goes to the credit card company. ",
        "A usage-based model—similar to how electric utilities are metered—has been used across various other industries. Cloud computing products, such as Amazon’s cloud service that hosts websites, charge by the amount used. This allows customers more control over their expenses because they only pay for the amount of bandwidth used, rather than paying for extra capacity they don’t use",
        "Cell Phone plan model is a predictable, recurring base fee charged in exchange for a certain amount of committed usage, with additional charges, often at much higher marginal rates, if the customer uses more than their allotted amount. The base charge is generally far less per amount of usage than the overage charge. ",
        "A new successful model that came into vogue with online computer games, and is now being tested to try to save newspapers, is micro transactions. In this model, the customer is asked to provide their credit card and then they make very small (defined as less than $12; often they are $1 or less) transactions for digital goods. ",
        "Shared savings model is often brainstormed, but rarely used because of the complexities in implementing it, despite its conceptual elegance. In this scenario, the customer pays only once they have realized savings or benefits from the product. ",
        "If an entrepreneur comes up with a good idea and is able to implement but does not have the desire, skills, or money to roll it out, they can use the franchise model and get paid a percentage of sales and/or receive a large initial startup fee in return for providing the knowledge and brand that has been developed",
        "A new business might not want to really sell a product but rather get paid for running a plant or other operation for a fee. While this is similar in some ways to a consulting agreement, the customer has more incentives to control or cut costs, as it will directly impact the customer’s income. This model is common in the energy sector. ",
    ]


    if BUSINESS_MODELS.values.include? model_name
      a[BUSINESS_MODELS.invert[model_name] - 1]
    else
      nil
    end

  end


  def core_notes(core_name)
    x = [
        "If this is your Core, you become the standard by achieving so much critical mass in the marketplace that it does not make sense for potential customers to use another product

Examples of business with \"Network size\" as core are Facebook, Reddit and LinkedIn.

When a network effect is present, the value of a product or service is dependent on the number of others using it.

If you choose this Core:

- You will need to have resources to accommodate a large part of your customer base at any time
- Your product must be suitably designed to make it easy for users to recruit other users",

        "By establishing processes and culture that focus on excelling at customer service, this potential core allows you to retain customers at a very high rate as compared to competitors, and thereby avoid costly churn.

It will also allow you to attract and obtain customers in a much more efficient way than others in the market, as your customers are thrilled with their experience with you and become salespeople for you by creating positive word of mouth.

Examples of business with “Customer service” as core are Amazon, USAA and Trader Joe’s.

If you choose this Core:

- You may have to adopt costly policies like \"no questions asked refunds\", \"24 × 7 × 365 support etc\"
- As founder, you may need to learn how to deliver good customer service and set the bar high for your team.",


        "The overall experience of a person using a product such as a website or computer application, especially in terms of how easy or pleasing it is to use.

Develop work culture and skills that delivers the great emotion and feel good effect on using your product.

Examples of business with “User experience” as core are Apple and Wikipedia

If you choose this Core:

- You will have to become the best at developing and continually improving the UX through the company's emphasis on it
- As founder, you will need to learn how to deliver good UX and set the bar high for your team.",


        "Develop the skills, relationships, processes, volumes, financial overhead, and culture to outcompete anyone else in the market on cost and become the long-term low-cost player.

This has been a successful Core for Wal-Mart and it is also part of the strategy behind many Asian companies, especially with Chinese companies that have recently entered the clean energy sector

If you choose this Core:

- You may need to invest in research and development to gain these cost advantages
- You may need to make your product at a low cost location away from where you are located"

    ]


    if CORE.values.include? core_name
      x[CORE.invert[core_name] - 1]
    else
      nil
    end


  end

  def pricing_groups
    [
        [1, 'Untested', ''],
        [2, 'Testing', 'orange'],
        [3, 'Dropped', 'red'],
        [4, 'Implemented', 'green'],
    ]
  end

  def pricing_plan_fields
    [
        ["What is the business model adopted you have adopted?", :model_adopted, {type: :select, choices: BUSINESS_MODELS}],
        ["Who is this pricing plan for?", :cust_group, {type: :select, choices: CUSTOMER_GROUPS}],
        ["What is your product price?", :product_price, {numeric: true, prefix: '$ ', placeholder: '$ 0'}],
        ["When do you plan to charge?", :invoice_timing, {type: :select, choices: {1 => 'Before Usage', 2 => 'After Usage', 3 => 'During Usage'}}],
        ["What is your invoice timing?", :billing_frequency, {type: :select, choices: {1 => 'Hourly', 2 => 'Daily', 3 => 'Weekly', 4 => 'Monthly', 5 => 'Annually'}}],
        ["What is the lowest competitor’s price?", :competitor_price_lb, {numeric: true, prefix: '$ ', placeholder: '$ 0'}],
        ["What is the highest competitor’s price?", :competitor_price_ub, {numeric: true, prefix: '$ ', placeholder: '$ 0'}],
        ["What is your product's LTV under this pricing plan?", :ltv, {numeric: true, prefix: '$ ', placeholder: '$ 0'}],
    ]
  end


  def sales_plan_groups
    [
        [1, 'Unimplemented', ''],
        [2, 'Implemented', 'orange'],
        [3, 'Profit', 'green'],
        [4, 'Super Profit', 'green'],
        [5, 'Loss', 'red'],
    ]
  end

  def sales_plan_fields
    [
        ["How to create problem awareness with a customer?", :prob_awareness, :prob_awareness_duration],
        ["How to create product awareness with a customer?", :prod_awarness, :prod_awarness_duration],
        ["How to spread information about the product to a customer?", :prod_information, :prod_information_duration],
        ["How to conclude sale of the product with a customer?", :prod_sale, :prod_sale_duration],
        ["How to collect sale money from a customer?", :collection, :collection_duration],
    ]
  end

  def sales_plan_fields2
    [
        ['LTV per Customer', :actual_customer_ltv, numeric: :true, prefix: '$ '],
        ['Total Sales Cost', :actual_total_cost, numeric: true, prefix: '$ '],
        ['# of customer reached', :actual_customers_acquired, numeric: true],
    ]
  end

  def milestone_groups
    [
        [1, 'Targeted', ''],
        [2, 'Ongoing', 'orange'],
        [3, 'Partially achieved', 'green'],
        [4, 'Achieved', 'green'],
        [5, 'Abandoned', 'red'],
    ]
  end

  def main_activity_fields
    [
        ['Target date', :target_date, type: :date],
        # ['Effort', :effort, numeric: true, placeholder: 'Effort required in days', suffix: ' days'],
        # ['Capital', :capital, numeric: true, prefix: '$ ', placeholder: '$ 0'],
    ]
  end


  def customer_groups
    [
        [1, 'Reached out to', ''],
        [2, 'Activated', 'orange'],
        [3, 'Retained', 'green'],
        [4, 'Revenue', 'green'],
        [5, 'Quit', 'red'],
    ]
  end

  def customer_fields
    [
        ['Sales strategy', :sales_strategy_id, {type: :select, choices: beachhead.sales_strategies.pluck(:id, :strategy_name).to_h}],
        ['Reach out date', :reach_out_date, {type: :date}],
        ['Notes', :notes, {}],

    ]
  end


  def issue_stages
    {
        1 => 'identified',
        2 => 'planned',
        3 => 'in progress',
        4 => 'fixed',
    }
  end

  def issue_fields
    [
        ['Related feature', :feature_id, {type: :select, choices: beachhead.features.pluck(:id, :feature_name).to_h}],
        ['Affected Touchpoint', :touchpoint_no, {type: :select, choices: TOUCHPOINTS}],
        ['Target Date', :target_date, {type: :date}],
    ]
  end


  def tool_empty_states
    ["The Beachhead tool provides the litmus test in deciding whether the selected beachhead market is focused and narrow enough",
     "The markets tool can be used to select the most attractive market from among all potential markets.",
     "The End User tool is a great tool for understanding the typical end user and profile their wants and characteristics.",
     "The market size tool is useful to know how many customers will buy the product and their product adoption rate.",
     "The Persona tool is smart fact sheet of persona and a record of his/her top purchasing priorities that drivers the feature building decisions.",
     "The Use Case tool can be used to understand motivation to use and easiness to do at all important touch points with the customer.",
     "The Product spec tool is a smart record of all the features, sub- features and components. It also supports in deciding what to built, when.",
     "The Value Proposition tool is used to clearly map out and quantify the additional benefits that the customer receives.",
     "The core tool is a comprehensive tracker of what is the core part of the business after exploring various options.",
     "The Competitive Position tool charts the performance against the close competitors and status quo customers.",
     "The Business Model tool lists 16 popular business models. It can be used to test and select the most profitable business model",
     "The Pricing Plan tool incorporates checklist for designing an attractive pricing plans and track these plans.",
     "The Decision Making Unit tool provides the overall control on who apart from the end user play an influential role in closing a sale.",
     "The Sales Planning tool helps to formulate sales plans and chose the top profitable plans and track their profitability",
     "The Customer Recruitment makes it easy to track the recruitment of early customers and grow the business.",]
  end


end