module AssumptionHelper


  def preset_assumptions
    if @preset_assumptions
      @preset_assumptions
    else
      @preset_assumptions = active_product.assumptions.preset
    end
  end

  def assumption_status
    {
        1 => 'Experiments ongoing',
        2 => 'Validated with experiments',
        3 => 'Invalidated with experiments',
    }
  end

  def validation_range
    {
        1 => 'Does not validate',
        2 => 'Weakly validates',
        3 => 'Strongly validates'
    }
  end

  def archive_assumption(assumption)
    name = assumption.archived ? 'Unarchive' : 'Archive'
    button_to assumption_path(assumption, params.merge(assumption: {archived: !assumption.archived})),
              form_class: 'archive-assumption-form',
              class: 'archive-assumption-btn',
              method: :put,
              remote: true,
              data: {disable_with: disabled_spinner} do
      name
    end
  end

  def preset_locked_notice(num)
    case num
      when 1
        "Will be unlocked when you complete diagnosing all markets in Diagnosis tool"
      when 2
        "Will be unlocked when you complete canvases for all markets in Analyze tool"
      when 3
        "Will be unlocked when you complete reality checking the size of your beachhead in Beachhead tool"
      when 4
        "Will be unlocked when you complete profiling your end user in End User tool"
      when 5
        "Will be unlocked when you complete estimating the market size  in Market Size tool"
      when 6
        "Will be unlocked when you complete profiling your persona in the Persona Tool"
      when 7
        "Will be unlocked when you complete identifying your DMU in then DMU tool"
      when 8
        "Will be unlocked when you complete building your Use Case in the Use Case tool"
      when 9
        "Will be unlocked when you complete identifying your Minimum and MVP features in the Features tool"
      when 10
        "Will be unlocked when you complete computing the quantified value proposition in the Value Proposition tool"
    end
  end

  def preset_condition(num)
    case num
      when 1
        active_product.all_canvases_complete?
      when 2
        active_product.all_diagnoses_complete?
      when 3
        beachhead.new_business_checklist_complete?
      when 4
        beachhead.enduser_complete?
      when 5
        beachhead.market_size_complete?
      when 6
        false
      when 7
        false
      when 8
        false
      when 9
        false
      when 10
        false

    end
  end


  def preset_assumption(num)

    beachhead_name = beachhead.business_name
    product_name = active_product.product_name

    case num
      when 1
        "The beachhead market #{beachhead_name} has no weaknesses"
      when 2
        solution = beachhead.application
        "The solution expected by the customers in the #{beachhead_name} market is: #{solution}"
      when 3
        "The beachhead market is #{beachhead_name} small enough to be taken over in short period of time"
      when 4
        end_user_day = beachhead.endusers.where(parameter: ['Wake up routines', 'Breakfast routines', 'Dinner routines']).pluck(:response).join(', ')
        "The day in the life of the end user is #{end_user_day}"
      when 5
        market_value = beachhead.market_value
        "The dollar value of #{beachhead_name} is #{market_value}"
      when 6
        priority1 = beachhead.try(:priority1)
        priority2 = beachhead.try(:priority2)
        "The top two priorities in the #{beachhead_name} market are #{priority1} and #{priority2}"
      when 7
        dmu_members = beachhead.customer_decision_making_units.pluck(:entity_name).to_sentence
        "The Decision Making Unit is composed of #{dmu_members}"
      when 8
        behaviour = beachhead.touchpoints.where(touchpoint_no: 6).first.end_user_actions
        "The end user behavior while using #{product_name} is #{behaviour}"
      when 9
        feature_list = (beachhead.features.minimal.pluck(:feature_name) + beachhead.features.mvp.pluck(:feature_name)).to_sentence
        "The mandatory features required in #{product_name} are #{feature_list}"
      when 10
        value_proposition = beachhead.quantified_value_proposition
        "The quantified value proposition provided by #{product_name} are #{value_proposition}"
    end


  end

  def assumption_of_preset(preset_id)
    active_product.assumptions.where(preset_id: preset_id).first
  end

  def experiment_groups
    [
        [1, 'New Experiment', ''],
        [2, 'Ongoing', 'orange'],
        [3, 'Weakly validates', 'light-green'],
        [4, 'Strongly validates', 'green'],
        [5, 'Does not validate', 'red'],
    ]
  end

  def experiment_fields
    [
        ['Cost of experiment', :cost, placeholder: 'What will it cost you to do this experiment?'],
        ['Validated if', :validated_if, placeholder: 'What outcome will validate this experiment'],
        ['Notes', :result_note, placeholder: 'Anything else you want to write...'],
    ]
  end

end