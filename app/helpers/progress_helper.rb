module ProgressHelper


  def stage_actions
    prod_name = active_product.product_name
    bh_name = beachhead.business_name
    validation_links_hash = validation_links(prod_name, bh_name)

    {

        1 => step_link("Start diagnosing the markets in #{prod_name} for key weaknesses (+5%)", 1),
        2 => step_link("Complete the diagnosis of the markets in #{prod_name} (+5%)", 1),
        3 => validation_links_hash[1],

        4 => step_link("Start analyzing the markets in #{prod_name} (+5%)", 2),
        5 => step_link("Complete the analysis of the markets in #{prod_name} (+5%)", 2),
        6 => validation_links_hash[2],

        7 => step_link("Start a reality check of #{bh_name}'s size (+5%)", 3),
        8 => step_link("Complete reality checking #{bh_name}'s size (+5%)", 3),
        9 => validation_links_hash[3],

        10 => step_link("Start analyzing the end user in #{bh_name} (+5%)", 4),
        11 => step_link("Complete the analysis of the end user in #{bh_name} (+5%)", 4),
        12 => validation_links_hash[4],

        13 => step_link("Start estimating the market value #{bh_name} (+5%)", 5),
        14 => step_link("Complete the estimation of market value of #{bh_name} (+5%)", 5),
        15 => validation_links_hash[5],

        16 => step_link("Start profiling the persona (+5%)", 6),
        17 => step_link("Complete the profiling of the persona (+5%)", 6),
        18 => validation_links_hash[6],

        19 => step_link("Start identifying the DMU (+5%)", 7),
        20 => step_link("Complete the identification of DMU (+5%)", 7),
        21 => validation_links_hash[7],

        22 => step_link("Start building the use case for #{prod_name} (+5%)", 8),
        23 => step_link("Complete the use case for #{prod_name} (+5%)", 8),
        24 => validation_links_hash[8],

        25 => step_link("Start identifying the MVP features of #{prod_name} must have (+5%)", 9),
        26 => step_link("Complete identifying the MVP features of #{prod_name} (+5%)", 9),
        27 => validation_links_hash[9],

        28 => step_link("Start estimating the value proposition (+5%)", 10),
        29 => step_link("Complete the estimation of value proposition (+5%)", 10),
        30 => validation_links_hash[10],
    }

  end

  def stage_progress

    if @stage_progress_data
      @stage_progress_data

    else

      stage_1_score, stage_1_next_actions, stage_2_score, stage_2_next_actions = 0, [], 0, []


      10.times do |i|

        if i < 5

          if tool_stage_conditions[i+1][:validated]
            stage_1_score += 20
          elsif tool_stage_conditions[i+1][:complete]
            stage_1_score += 10
            stage_1_next_actions << stage_actions[(i * 3) + 3]
          elsif tool_stage_conditions[i+1][:start]
            stage_1_score += 5
            stage_1_next_actions << stage_actions[(i * 3) + 2]
          else
            stage_1_next_actions << stage_actions[(i * 3) + 1]
          end

        else


          if tool_stage_conditions[i+1][:validated]
            stage_2_score += 20
          elsif tool_stage_conditions[i+1][:complete]
            stage_2_score += 10
            stage_2_next_actions << stage_actions[(i * 3) + 3]
          elsif tool_stage_conditions[i+1][:start]
            stage_2_score += 5
            stage_2_next_actions << stage_actions[(i * 3) + 2]
          else
            stage_2_next_actions << stage_actions[(i * 3) + 1]
          end

        end



      end


      #
      # # canvas
      #
      # if preset_assumptions[0] == assumption_status[2]
      #   stage_1_score += 20
      # elsif active_product.all_canvases_complete?
      #   stage_1_score += 10
      #   stage_1_next_actions << stage_actions[3]
      # elsif !active_product.all_canvases_empty?
      #   stage_1_score += 5
      #   stage_1_next_actions << stage_actions[2]
      # else
      #   stage_1_next_actions << stage_actions[1]
      # end
      #
      # # diagnosis
      #
      # if preset_assumptions[1] == assumption_status[2]
      #   stage_1_score += 20
      # elsif active_product.all_diagnoses_complete?
      #   stage_1_score += 10
      #   stage_1_next_actions << stage_actions[6]
      # elsif !active_product.all_diagnoses_empty?
      #   stage_1_score += 5
      #   stage_1_next_actions << stage_actions[5]
      # else
      #   stage_1_next_actions << stage_actions[4]
      # end
      #
      # # beachhead
      #
      # if preset_assumptions[2] == assumption_status[2]
      #   stage_1_score += 20
      # elsif beachhead.new_business_checklist_complete?
      #   stage_1_score += 10
      #   stage_1_next_actions << stage_actions[9]
      # elsif !beachhead.new_business_checklist_not_started?
      #   stage_1_score += 5
      #   stage_1_next_actions << stage_actions[8]
      # else
      #   stage_1_next_actions << stage_actions[7]
      #
      # end
      #
      # # enduser
      #
      # if preset_assumptions[3] == assumption_status[2]
      #   stage_1_score += 15
      # elsif beachhead.enduser_complete?
      #   stage_1_score += 10
      #   stage_1_next_actions << stage_actions[12]
      # elsif !beachhead.enduser_not_started?
      #   stage_1_score += 5
      #   stage_1_next_actions << stage_actions[11]
      # else
      #   stage_1_next_actions << stage_actions[10]
      # end
      #
      # # market size
      #
      # if preset_assumptions[4] == assumption_status[2]
      #   stage_1_score += 15
      # elsif beachhead.market_size_complete?
      #   stage_1_score += 10
      #   stage_1_next_actions << stage_actions[15]
      # elsif beachhead.market_size_segments_complete.count > 0
      #   stage_1_score += 5
      #   stage_1_next_actions << stage_actions[14]
      # else
      #   stage_1_next_actions << stage_actions[13]
      # end
      #
      # # market size
      #
      # if preset_assumptions[4] == assumption_status[2]
      #   stage_1_score += 15
      # elsif beachhead.market_size_complete?
      #   stage_1_score += 10
      #   stage_1_next_actions << stage_actions[15]
      # elsif beachhead.market_size_segments_complete.count > 0
      #   stage_1_score += 5
      #   stage_1_next_actions << stage_actions[14]
      # else
      #   stage_1_next_actions << stage_actions[13]
      # end
      #
      #
      #



      @stage_progress_data = [stage_1_score, stage_1_next_actions, stage_2_score, stage_2_next_actions]
    end

  end

end