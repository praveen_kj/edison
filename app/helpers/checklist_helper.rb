module ChecklistHelper


  CHECKLIST_ITEMS = {
      1 => {
          :bh1 => "First-pass guess on \"What problem do I solve?\"",
          :bh2 => "Meet customers from the beachhead market and know their view.",
          :bh3 => "Save customers' insights to improve customer empathy and knowing their unmet needs.",
          :bh4 => "Reflect, brainstorm and process the customers' insights.",
          :bh5 => "Decide on all the three key questions about the beachhead market.",
          :bh6 => "Meet customers again. May be more customers from the beachhead market and gather their view.",
          :bh7 => "Save and process insights. Continue the iteration of figuring out the solution for beachhead market.",
      },
      2 => {
          :mk1 => "First-pass guess on \"What are the top challenges with my markets?\"",
          :mk2 => "Meet customers from all the markets and know their view.",
          :mk3 => "Save customers' insights to improve customer empathy and knowing their unmet needs.",
          :mk4 => "Reflect, brainstorm and process the customers' insights.",
          :mk5 => "Decide on all the seven key questions for each of the market.",
          :mk6 => "Meet customers again. May be more customers and gather their view.",
          :mk7 => "Save and process insights. Continue the iteration of selecting the right beachhead market.",
      },
      4 => {
          :ms1 => "First-pass guess on \"How big is my beachhead market?\"",
          :ms2 => "Meet customers from the beachhead market, conduct survey and know their view of the size of their customer community.",
          :ms3 => "Save customers feedback to process it.",
          :ms4 => "Process the customers feedback to refine the estimate.",
          :ms5 => "Estimate the size based on the five groups of the beachhead customer community.",
          :ms6 => "Meet customers again. May be more customers and gather their view on their community size.",
          :ms7 => "Save their feedbacks and continue the iteration of processing it and making a better estimate."
      },
      3 => {
          :eu1 => "First-pass guess on \"Who is my end user?\"",
          :eu2 => "Meet customers from the beachhead market and know more about them.",
          :eu3 => "Save customers' insights to know more about the end users.",
          :eu4 => "Reflect, brainstorm and process the customers' insights.",
          :eu5 => "Build the End user profile.",
          :eu6 => "Meet customers again. May be more customers from the beachhead market and gather their views.",
          :eu7 => "Save and process insights. Continue the iteration of building a reliable end user profile.",
      },
      9 => {
          :co1 => "First-pass guess on \"What is my core or 'that one special thing'?\"",
          :co2 => "Meet customers from the beachhead market and know what core they expect from the venture.",
          :co3 => "Save customers' insights",
          :co4 => "Reflect, brainstorm and process the customers' insights.",
          :co5 => "Work on the set of introspective questions for all the core options.",
          :co6 => "Meet customers again. May be more customers from the beachhead market and gather their views on what core do they expect out of the venture.",
          :co7 => "Save and process insights. Continue the iteration of select the most suitable core.",
      },
      5 => {
          :pn1 => "First-pass guess on \"What are top 3 purchasing priorities of the end users?\"",
          :pn2 => "Meet few customers from the beachhead market to select a Persona.",
          :pn3 => "Save customers' insights to know who can be the Persona.",
          :pn4 => "Reflect, brainstorm and process the customers' insights.",
          :pn5 => "Build the fact sheet of the Persona.",
          :pn6 => "Live with the Persona, understand the purchasing priorities and cross verify whether other customers agree to it.",
          :pn7 => "Save and process insights. Continue the iteration to identify the top purchasing priorities.",
      },
      13 => {
          :dm1 => "First-pass guess on \"Who are the top influencers to make a sale?\"",
          :dm2 => "Meet customers from the beachhead market and know who influence their decisions to buy the product.",
          :dm3 => "Save customers' insights .",
          :dm4 => "Reflect, brainstorm and process the customers' insights.",
          :dm5 => "Create a DMU board.",
          :dm6 => "Meet customers again. May be more customers from the beachhead market and know needs of the DMU members.",
          :dm7 => "Save and process insights. Continue the iteration get clarity on what drives each of the DMU member.",
      },
      6 => {
          :uc1 => "First-pass guess on \"What is the targeted customer retention rate?\"",
          :uc2 => "Meet customers from the beachhead market and list out the use case hurdles.",
          :uc3 => "Save customers' insights on the problamatic touchpoints.",
          :uc4 => "Reflect, brainstorm and process the customers' insights.",
          :uc5 => "Generate an use case chart.",
          :uc6 => "Meet customers again. May be more customers from the beachhead market and cross examine the use case hurdles.",
          :uc7 => "Save and process insights. Continue the iteration to identify and solve the problematic touchpoints.",
      },
      7 => {
          :ps1 => "First-pass guess on \"What are mandatory features my product will be made of?\"",
          :ps2 => "Meet customers from the beachhead market and know what they look for in the product.",
          :ps3 => "Save customers' insights on what features they want from the product.",
          :ps4 => "Reflect, brainstorm and process the customers' insights.",
          :ps5 => "Detail and create a product specification.",
          :ps6 => "Meet customers again. May be more customers from the beachhead market and cross examine the product specification.",
          :ps7 => "Save and process insights. Continue the iteration to confirm the product specification.",
      },
      8 => {
          :vp1 => "First-pass guess on \"What are my top promises to every single customer?\"",
          :vp2 => "Meet customers from the beachhead market and list out their view of the top promises and worth of each promise.",
          :vp3 => "Save customers' insights on their perception of the value the product creates.",
          :vp4 => "Reflect, brainstorm and process the customers' insights.",
          :vp5 => "Estimate the add-on value created by the product.",
          :vp6 => "Meet customers again. May be more customers from the beachhead market and cross examine validity of the quantification.",
          :vp7 => "Save and process insights. Continue the iteration to confirm the quantified value proposition.",
      },
      10 => {
          :cp1 => "First-pass guess on \"Who are my top competitors?\"",
          :cp2 => "Meet customers from the beachhead market and list out the similar products they currently use.",
          :cp3 => "Save customers' insights on the alternatives they use and why.",
          :cp4 => "Reflect, brainstorm and process the customers' insights.",
          :cp5 => "Complete the competitive position chart.",
          :cp6 => "Meet customers again. May be more status quo customers from the beachhead market and know what they want.",
          :cp7 => "Save and process insights. Continue the iteration to confirm on the competitive position.",
      },
      11 => {
          :bm1 => "First-pass guess on \"What should be the frequency of my inflow from each customer? \"",
          :bm2 => "Meet customers from the beachhead market and know their most comfortable frequency of outflow to buy the product.",
          :bm3 => "Save customers' insights on their preferred outflow amount and timing.",
          :bm4 => "Reflect, brainstorm and process the customers' insights.",
          :bm5 => "Work on the questions for each of the business model to select the most preferred business model.",
          :bm6 => "Meet customers again. May be more customers from the beachhead market and gather their view of selected business model.",
          :bm7 => "Save and process insights. Continue the iteration to confirm the business model selection.",
      },
      12 => {
          :pp1 => "First-pass guess on \"What is the prevailing market price range?\"",
          :pp2 => "Meet customers from the beachhead market and know the prevailing price the customer pay for similar products.",
          :pp3 => "Save customers' insights how much they pay to the existing alternatives.",
          :pp4 => "Reflect, brainstorm and process the customers' insights.",
          :pp5 => "Process the customers feedback to create few pricing plans.",
          :pp6 => "Meet customers again. May be more customers from the beachhead market and cross examine if the pricing is fair for them.",
          :pp7 => "Save and process insights. Continue the iteration to confirm on the selected pricing plans.",
      },
      14 => {
          :sp1 => "First-pass guess on \"How will the customers know about my product?\"",
          :sp2 => "Meet customers from the beachhead market and know how will the like to be communicated on the products existence.",
          :sp3 => "Save customers' insights on creating awareness of the product.",
          :sp4 => "Reflect, brainstorm and process the customers' insights.",
          :sp5 => "Create a bunch of sales plans. Work on the questions for each of the sales plan to select the pragmatic and profitable sales plan.",
          :sp6 => "Meet customers again. May be more customers from the beachhead market and cross examine the sales plans.",
          :sp7 => "Save and process insights. Continue the iteration to implement the sales plans.",
      },
      15 => {
          :cr1 => "Enter the answer to \"How many customers have I reached out so far?\"",
          :cr2 => "Meet customers from the beachhead market and increase the count.",
          :cr3 => "Save customers' insights on how to improve relationship with them.",
          :cr4 => "Reflect, brainstorm and process the customers' insights.",
          :cr5 => "Recruit more customers.",
          :cr6 => "Meet customers again. May be more customers from the beachhead market and recruit them.",
          :cr7 => "Save and process insights. Continue recruit and retain process.",
      }
  }

  def _checklist_items_for_step(step)
    parent = step == 2 ? active_product : beachhead
    parent.checklist_items.where(step: step)
  end

  def checklist_items_for_step(step)
    if @checklist_items
      @checklist_items
    else
      parent = step == 2 ? active_product : beachhead
      @checklist_items = parent.checklist_items.where(step: step)
    end
  end

  def checklist_for_step(step, rand_id)


    completed = checklist_items_for_step(step).pluck(:id, :item_key).collect { |x| [x[1].to_sym, x[0]] }.to_h

    output = []

    CHECKLIST_ITEMS[step].each do |item_key, checklist_desc|

      if completed.keys.include?(item_key)
        output << [true, uncheck_button(completed[item_key], rand_id), checklist_desc]
      else
        output << [false, check_button(item_key, step, rand_id), checklist_desc]
      end

    end

    output

  end

  def checklist_progress(step)
    (checklist_items_for_step(step).count/7.0*100).round
  end

  def check_button(item_key, step, rand_id)
    _params = step == 2 ? {item_key: item_key, step: step, product_id: active_product.id} : {item_key: item_key, step: step, business_id: beachhead.id}
    button_to checklist_items_path(checklist_item: _params, rand_id: rand_id), remote: true, form_class: 'checklist-form', class: 'transparent-btn' do
      ""
    end
  end

  def uncheck_button(checklist_item_id, rand_id)
    button_to checklist_item_path(checklist_item_id, rand_id: rand_id), method: :delete, remote: true, form_class: 'checklist-form', class: 'transparent-btn' do
      ""
    end
  end

  def checklist_score(step)
    _checklist_items_for_step(step).count
  end


end

























































































































































































































