module SectionHelper

  def section_header(number, object, deletable = true)

    first_col_content = content_tag(:div, serial_no(number), class: 'col-xs-1')
    second_col_content = ''


    heading_options = object.class.blueprint_for_section[:header_map][:heading]
    first_col_content << content_tag(:div, make_field(object, heading_options[:field], heading_options), class: 'col-xs-11')

    rhs_fields = object.class.blueprint_for_section[:header_map][:rhs_fields]

    rhs_fields.each do |k, v|
      second_col_content << content_tag(:div, make_field(object, v[:field], v), class: 'col-xs-11')
    end

    second_col_content << content_tag(:div, (delete_object(object) if deletable), class: 'col-xs-1')

    first_col = make_column(size: 6, content: first_col_content)
    second_col = make_column(size: 6, content: second_col_content.html_safe)

    content_tag(:header,
                content_tag(:div, (first_col+second_col).html_safe,
                            class: 'row'),
                'data-toggle' => "tool-section",
                'data-parent' => object.row_selector(true))

  end


  def section_body(object, blueprint = nil)

    blueprint = blueprint || object.class.blueprint_for_section[:section_field_map]

    row_array = []

    blueprint.each do |row|
      cell_array = []
      row.each do |cell|
        num_of_cols = row.try(:count)
        colsize = num_of_cols == 3 ? 4 : 6

        if cell.is_a?(String)
          cell_array << make_column(size: colsize, content: cell, class: 'a-field')
        elsif cell.is_a?(Array) && cell.length == 2
          icon = field_icon(cell[0])
          text = content_tag(:div, cell[1], class: 'col-xs-10')
          cell_array << make_column(size: 4, content: (icon+text).html_safe, class: 'a-field')
        elsif cell.is_a?(Array)
          cell_array << make_column(size: colsize, content: render(cell[0], object: object), class: 'choice-field u-field')
        else
          cell_array << make_column(size: colsize, content: make_field(object, cell[:field], cell), class: "u-field #{'choice-field' if cell[:type] == :select}")
        end
      end
      row_array << make_section_row(cell_array.join.html_safe, {class: 'row make-task'})
    end

    content_tag(:div, row_array.join.html_safe, class: 'section-body')

  end

  def section_break(title, button = nil)
    content = title
    content << button if button
    content = make_column(content: content.html_safe, size: 12)
    make_section_row(content, {class: 'row break'})
  end


  def make_section_row(content, options = {})
    content_tag(:div, content, options)
  end

  def make_column(options = {})
    content = options[:content]
    content_tag(:div, content, class: "col-xs-#{options[:size]} section-column #{options[:class]}")

  end

  # generates html for a field to include inside a section header

  def div_field(content, tooltip = nil, icon = nil)
    icon_ = ''.html_safe
    icon_ = content_tag(:i, '', class: "fa #{icon}") if icon
    content_tag(:div, icon_+content, class: 'field', 'data-toggle' => 'tooltip', 'data-title' => tooltip)
  end

end