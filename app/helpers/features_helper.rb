module FeaturesHelper

  def feature_list(scope)
    l = beachhead.features.send(scope).pluck(:feature_name).to_sentence
    l.present? ? l : '?'
  end


  def feedback_types
    {
        1 => 'New Feature Request',
        2 => 'Error Report',
    }
  end

  def component_stages

    {
        1 => "idea",
        2 => "planned",
        3 => "under implementation",
        4 => "implemented",
    }
  end

  def component_stages_filter
    _component_counts = beachhead.component_counts
    {
        1 => ["idea", "idea (#{_component_counts[0]})"],
        2 => ["planned", "planned (#{_component_counts[1]})"],
        3 => ["under implementation", "under implementation (#{_component_counts[2]})"],
        4 => ["implemented", "implemented (#{_component_counts[3]})"],
    }
  end


end