module UserTasksHelper

  def do_this_today(task)
    today = Date.today

    if task.current? && task.assigned_to?(current_user)
      button_class = 'on'
      params = {start_date: nil, end_date: nil, assignee_id: nil}
    else
      button_class = 'off'
      params = {start_date: today, end_date: today, assignee_id: current_user.id}
    end

    button_to sub_task_path(task.id, sub_task: params),
              form_class: 'do-today-btn-form',
              class: "do-today-btn #{button_class}",
              method: :put,
              remote: true do
      ''
    end
  end

  module InstanceMethods

    def days_overdue
      if end_date.present?
        today = lambda { Date.today }.call
        today > end_date ? (today - end_date).to_i : 0
      else
        0
      end
    end

    def days_to_go
      today = lambda { Date.today }.call
      (end_date - today).to_i
    end

    def current?
      today = lambda { Date.today }.call
      if start_date.present? && end_date.present?
        today >= start_date && today <= end_date
      else
        false
      end
    end

    def assigned?
      assignee_id.present?
    end

    def assigned_to?(user)
      assignee_id == user.id
    end

    def starts_today?
      start_date == lambda { Date.today }.call
    end

    def starts_tomo?
      start_date == lambda { Date.tomorrow }.call
    end

    def overdue?
      days_overdue > 0 ? true : false
    end

    def due_now?
      end_date < lambda { Date.tomorrow }.call if end_date
    end

    def unplanned?
      end_date.nil? ? true : false
    end

    def has_tasks_due_now?(user_id)
      sub_tasks.due_now.where(assignee_id: user_id).count > 0
    end

    def has_priorities?(user_id)
      sub_tasks.due_now.where(assignee_id: user_id, priority: true).count > 0
    end

  end

  def all_tasks_count
    current_user.user_tasks.size
  end

  def tasks_for_category(category_no)
    current_user.user_tasks.for_category(category_no).size
  end

  def tasks_for_step(step_no)
    current_user.user_tasks.for_step(step_no).size
  end

  def tasks_for_market(market_id)
    current_user.user_tasks.for_business(market_id).size
  end


  def pending_tasks_count
    current_user.user_tasks.pending.size
  end

  def pending_tasks_count_for_step(step_no)
    current_user.user_tasks.for_step(step_no).pending.size
  end

  def completed_tasks_count
    current_user.user_tasks.complete.size
  end

  def unplanned_tasks_count
    current_user.user_tasks.pending.unplanned.size
  end

  ## used in dropdown menu for best in place field
  def steps_array

    hash = {}

    hash[0] = 'General'

    STEP.each do |k, v|
      hash[k]= v[:name]
    end

    hash
  end


  def tasks_category
    {1 => 'Ask',
     2 => 'Data',
     3 => 'Experiment',
     4 => 'Compare',
     5 => 'Expert'}
  end

  def task_helper_text
    {1 => "Interview and observe customers’ opinions and preferences.",
     2 => "Search for statistical data and infer the results. In can be primary data (directly from customers) or secondary data (from market reports).",
     3 => "Set and carry out a sample test and learn from it results on how the entire population will most likely to behave in reality.",
     4 => "See how your competitors do things. Learn from them and apply that in your business.",
     5 => "Gather new perspectives from your mentor and industry experts and improve your business quality.", }
  end

  def business_list
    active_product.businesses.pluck(:id, :business_name).to_h
  end

  def archive_task(user_task, options = {})
    style = options[:style] || 'warning'
    name = user_task.status ? 'Unarchive' : 'Archive'
    button_to name, user_task_path(user_task, params.merge(user_task: {status: !user_task.status})),
              form_class: 'archive-task-form',
              class: "archive-user-task #{style}",
              method: :put,
              remote: true
  end


  def time_metrics
    today = Date.today
    all = active_product.user_tasks.joins(:sub_tasks).where("sub_tasks.status IS NULL OR sub_tasks.status IS false")

    # team_total_time = all.pluck('sub_tasks.time_estimate').compact.inject(:+) || 0
    # team_today_time = all.where("? BETWEEN sub_tasks.start_date AND sub_tasks.end_date", date).pluck('sub_tasks.time_estimate').compact.inject(:+) || 0
    # team_priority_time = all.where(sub_tasks: {priority: true}).pluck('sub_tasks.time_estimate').compact.inject(:+) || 0

    user = all.where(sub_tasks: {assignee_id: current_user.id})

    current_user_total_time = user.pluck('sub_tasks.time_estimate').compact.inject(:+) || 0
    current_user_today_time = user.where("sub_tasks.end_date <= ?", today).pluck('sub_tasks.time_estimate').compact.inject(:+) || 0
    current_user_priority_time = user.where(sub_tasks: {priority: true}).pluck('sub_tasks.time_estimate').compact.inject(:+) || 0


    [
        current_user_total_time, current_user_today_time, current_user_priority_time
    ]

  end


end
