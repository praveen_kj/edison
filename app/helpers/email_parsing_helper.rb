require 'mail'

module EmailParsingHelper

  module EmailParser

    def self.parse_address(full_address)
      email_address = extract_email_address(full_address)
      name = extract_name(full_address)
      token, host = split_address(email_address)
      {
          token: token,
          host: host,
          email: email_address,
          full: full_address,
          name: name,
      }
    end

    def self.extract_reply_body(body)
      if body.blank?
        ""
      else
        remove_reply_portion(body)
            .split(/[\r]*\n/)
            .reject do |line|
          line =~ /^[[:space:]]+>/ ||
              line =~ /^[[:space:]]*Sent from my /
        end.
            join("\n").
            strip
      end
    end

    def self.extract_headers(raw_headers)
      if raw_headers.is_a?(Hash)
        raw_headers
      else
        header_fields = Mail::Header.new(raw_headers).fields

        header_fields.inject({}) do |header_hash, header_field|
          header_hash[header_field.name.to_s] = header_field.value.to_s
          header_hash
        end
      end
    end

    private

    def self.reply_delimeter_regex
      delimiter = Array(Griddler.configuration.reply_delimiter).join('|')
      %r{#{delimiter}}
    end

    def self.extract_email_address(full_address)
      full_address.split('<').last.delete('>').strip
    end

    def self.extract_name(full_address)
      full_address = full_address.strip
      name = full_address.split('<').first.strip
      if name.present? && name != full_address
        name
      end
    end

    def self.split_address(email_address)
      email_address.try :split, '@'
    end

    def self.regex_split_points
      [
          reply_delimeter_regex,
          /^[[:space:]]*[-]+[[:space:]]*Original Message[[:space:]]*[-]+[[:space:]]*$/i,
          /^[[:space:]]*--[[:space:]]*$/,
          /^[[:space:]]*\>?[[:space:]]*On.*\r?\n?.*wrote:\r?\n?$/,
          /On.*wrote:/,
          /\*?From:.*$/i,
          /^[[:space:]]*\d{4}\/\d{1,2}\/\d{1,2}[[:space:]].*[[:space:]]<.*>?$/i
      ]
    end

    def self.remove_reply_portion(body)
      regex_split_points.inject(body) do |result, split_point|
        result.split(split_point).first || ""
      end
    end


  end

  class Email
    include ActionView::Helpers::SanitizeHelper
    attr_reader :to, :from, :cc, :bcc, :subject, :body, :raw_body, :raw_text, :raw_html,
                :headers, :raw_headers, :attachments

    def initialize(params)
      @params = params

      @to = recipients(:to)
      @from = extract_address(params[:from])
      @subject = params[:subject]

      @body = extract_body
      @raw_text = params[:text]
      @raw_html = params[:html]
      @raw_body = @raw_text.presence || @raw_html

      @headers = extract_headers

      @cc = recipients(:cc)
      @bcc = recipients(:bcc)

      @raw_headers = params[:headers]

      @attachments = params[:attachments]
    end

    private

    attr_reader :params


    def recipients(type)
      params[type].to_a.map { |recipient| extract_address(recipient) }
    end

    def extract_address(address)
      EmailParsingHelper::EmailParser.parse_address(clean_text(address))
    end

    def extract_body
      EmailParsingHelper::EmailParser.extract_reply_body(text_or_sanitized_html)
    end

    def extract_headers
      if params[:headers].is_a?(Hash)
        deep_clean_invalid_utf8_bytes(params[:headers])
      else
        EmailParsingHelper::EmailParser.extract_headers(clean_invalid_utf8_bytes(params[:headers]))
      end
    end

    def extract_cc_from_headers(headers)
      EmailParsingHelper::EmailParser.extract_cc(headers)
    end

    def text_or_sanitized_html
      text = clean_text(params.fetch(:text, ''))
      text.presence || clean_html(params.fetch(:html, '')).presence
    end

    def clean_text(text)
      clean_invalid_utf8_bytes(text)
    end

    def clean_html(html)
      cleaned_html = clean_invalid_utf8_bytes(html)
      cleaned_html = strip_tags(cleaned_html)
      cleaned_html = HTMLEntities.new.decode(cleaned_html)
      cleaned_html
    end

    def deep_clean_invalid_utf8_bytes(object)
      case object
        when Hash
          object.inject({}) do |clean_hash, (key, dirty_value)|
            clean_hash[key] = deep_clean_invalid_utf8_bytes(dirty_value)
            clean_hash
          end
        when Array
          object.map { |element| deep_clean_invalid_utf8_bytes(element) }
        when String
          clean_invalid_utf8_bytes(object)
        else
          object
      end
    end

    def clean_invalid_utf8_bytes(text)
      if text && !text.valid_encoding?
        text.force_encoding('ISO-8859-1').encode('UTF-8')
      else
        text
      end
    end
  end

  module Adapter


    def normalized_params
      events.map do |event|
        {
            to: recipients(:to, event),
            cc: recipients(:cc, event),
            bcc: resolve_bcc(event),
            headers: event[:headers],
            from: full_email([event[:from_email], event[:from_name]]),
            subject: event[:subject],
            text: event[:text] || '',
            html: event[:html] || '',
            raw_body: event[:raw_msg],
            attachments: attachment_files(event),
            email: event[:email] # the email address where Mandrill received the message
        }
      end
    end

    private

    def events
      @events ||= ActiveSupport::JSON.decode(params[:mandrill_events]).map { |event|
        event['msg'].with_indifferent_access if event['event'] == 'inbound'
      }.compact
    end

    def recipients(field, event)
      Array.wrap(event[field]).map { |recipient| full_email(recipient) }
    end

    def resolve_bcc(event)
      email = event[:email]
      if !event[:to].map(&:first).include?(email) && event[:cc] && !event[:cc].map(&:first).include?(email)
        [full_email([email, email.split("@")[0]])]
      else
        []
      end
    end

    def full_email(contact_info)
      email = contact_info[0]
      if contact_info[1]
        "#{contact_info[1]} <#{email}>"
      else
        email
      end
    end

    def attachment_files(event)
      attachments = event[:attachments] || Array.new
      attachments.map do |key, attachment|
        ActionDispatch::Http::UploadedFile.new({
                                                   filename: attachment[:name],
                                                   type: attachment[:type],
                                                   tempfile: create_tempfile(attachment)
                                               })
      end
    end

    def create_tempfile(attachment)
      filename = attachment[:name].gsub(/\/|\\/, '_')
      tempfile = Tempfile.new(filename, Dir::tmpdir, encoding: 'ascii-8bit')
      content = attachment[:content]
      content = Base64.decode64(content) if attachment[:base64]
      tempfile.write(content)
      tempfile.rewind
      tempfile
    end
  end

end