Use these effective validation techniques to validate whether your founding team's passions are well aligned with your goals should you choose this market as a beachhead.
 
-	**Ask:** Interview and observe customers’ opinions and preferences. 
-	**Data:** Search for statistical data and infer the results. In can be primary data (directly from customers) or secondary data (from market reports). 
-	**Experiment:** Set and carry out tests with a sample set of individual users and learn from the results how the larger user-base will most likely behave in reality.  
-	**Compare:** See how your competitors are doing things. Learn from them and apply that in your business where possible. 
-	**Expert:** Gather new perspectives from your mentor and industry experts and improve the quality of your decision making.
