Here are a few things you can do to find out whether your customers are directly accessible to your sales force:

-	Inquire with the salesmen of your competing products about how they reach out to their customers
-	Collect data and interpret how companies during their initial days reached out to their customers.
-	Set up a sample test to see if you can each out to your target customers in this market.
-	Compare with your competitors and learn how they reach out to their customers and particularly during their startup days.
-	Arrange meetings with industry experts and with your mentor to understand how you reach your customers in the early days and why it is important to reach out directly to your customers.

