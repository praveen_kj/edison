Here are a few things that you can do to find out whether your target customer is adequately funded to buy your product.

- Inquire your potential customers about their ability to buy products similar to your  product.
- Interpreting data about your customers’ disposable income.
- Setting up a sample test to see if your customers in this market are willing to buy similar products.
- Compare with your competitors learn how much do they sell every month.
- Arrange meetings with industry experts and with your mentor to understand how well funded are your potential customers.

In the table below, make a list of the tasks you want to do find out whether your target customer is adequately funded.