Competitive position tool is primarily used to see how well the product meets the top two end user priorities and how these priorities are met by the competing products.

Often the bigger obstacle in customer recruitment will be convincing them to make a change from their status quo. The toughest competitor often becomes the status quo of the customers. Comparing the product with status quo ensures that there is a real market and not just an imaginary one.

It is better not to focus too much of the precious time on competitors. Focus should be working with customers, developing the Core, and getting products out the door. Building a good Core and executing on those lines will encourage more customers to adopt a new solution.
