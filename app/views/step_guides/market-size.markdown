Market size tool is primarily used to estimate how big the market is, by multiplying expected number of users with the ongoing price and decide the market adoption over various class of users. The best way to calculate the number of end users that fit the End User Profile is a bottom-up analysis.

A bottom-up analysis starts with “ counting noses” of the real customers that meet the end user profile and can be gathered from trade association data. Carry out a top-down analysis to assess whether the bottom-up analysis has produced reliable estimates. A top-down analysis starts by using secondary market research, such as market analysis reports.

Four rules for making a good estimate of the Market size:
 
* Estimate is only an Estimate and it is not actual. 
* An Aggressive estimate is better than No estimate.
* A Conservative estimate is better than an Aggressive estimate.
* All estimates need to be backed by data. Otherwise, estimates can be named as fantasy.
