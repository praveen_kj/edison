How to choose a Persona for the product? 

* Look for answer to this question “If I had only one end user to represent our End User Profile, who would it be?” 
* The Persona should conform very well with the end user profile while also providing more specific details. 
* Brainstorm and analyze the pros and cons of making each customer as the Persona. It can be someone from the founding team or a customer interacted with during the primary market research. 
* After this analysis, choose a Persona knowing that there might be change in the Persona itself, as more information is gathered. 
* Prepare a fact sheet about the Persona, based on the existing information. 
* List the Persona’s Purchasing Criteria in Prioritized Order, as these priorities will dictate the make to market journey of the product.
