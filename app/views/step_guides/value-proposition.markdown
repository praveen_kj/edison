Value proposition tool is primarily used to estimate the value of benefits promised to the user of the product and decide how likely is the product to fare against the existing industry standards. 

The Value Proposition focuses on what customers gain rather than going into details of technology, features, and functions.

Quantified value proposition is the estimate of a customer’s question “What value do I get out of this product?” There can be large number of assured benefits a customer can get from the product. Quantifying these assured benefits and aggregating them helps in estimating the value proposition.

In a simple view of the world, benefits fall into three categories: “better,” “faster,” and “cheaper.” Estimate the value proposition, to clearly and concisely state how the product’s benefits line up with what the customer most wants to improve.
