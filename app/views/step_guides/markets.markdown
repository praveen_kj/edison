Markets tool is primarily used for filtering out the good markets from the rest. There are seven tough questions to diagnose the markets. Apply the seven questions for all the markets, narrow down on the options and decide the best market.

Start talking directly to the customers and observe them. This will help in getting a better sense of which market opportunity is the best. It is not good to only rely on Google searches or on research reports from research firms. Gather significant majority of the information from direct interaction with real customers about their unmet needs, pain points, opportunities, and market information. Unfortunately, there are few shortcuts in this process.

Selected Extract on Talking with humans by Giff Constable

* Go in prepared. Know your goals and questions ahead of time. Ask for advice. Don’t have an endless list of questions. Don’t pitch unless you actually try to close for real money. 
* Listen. 95% of the conversation should be them talking. Follow your nose and drill down when something of interest comes up. Don’t feel like you have to rigidly stick to a script. Don’t talk so much, and don’t be afraid of silences. Let them think. 
* Prepare yourself to hear things you don’t want to hear. Don’t let your excitement and optimism bias what you hear.
