Business Model tool is primarily used to examine existing models and select the most suitable one that captures the maximum value of what the product brings to its customer. A business model is a framework by which the venture can extract from its customers some portion of the value the product creates for them.

Often the team spend a disproportionately small amount of time on their business model, creating a gap between the work done so far and work undone. Business model tool with its in-build checklist of key considerations avoids this risk of not investing sufficient time on adopting the right business model.

Innovation in a business model improves the customer reach out and profitability. Innovation in business model is the idea of amount of money the venture gets paid . This amount is based on how much value the customer gets from the product, and not some arbitrary markup based on the costs.
