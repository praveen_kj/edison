Core tool is primarily used for deciding that one special thing of the business. Core can be User Experience, Customer service, Low costs, Network effects or something else. Defining the Core is not easy. The tool provides a list of considerations for exploring and establishing the Core of the business.

Core is more inward-looking and less research-based than the others. Reliance on this internal introspection, combined with external data gathering and analysis is essential for selecting a solid Core. While the process may seem broad and general at first, the end definition of the Core should be concrete and specific.

Core exploration ends with arriving at a highly confident option that is accurate. Core cannot keep changing often; it has to remain fixed over time, once business is locked in on it. A change in the Core, often results in losing all the advantages that has been built using it.
