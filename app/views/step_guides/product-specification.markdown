Product specification tool is primarily used to select priority items that need to be developed and addressed by the production team. Decision to select is driven by “How many customers want this?” and “How important is this considering their purchasing priorities?” 

If answer to both questions are high, then the item should be ticked as priority and passed on to the production team.

Creating and updating the Product specification should be a group exercise and all the team members must be encouraged to participate and invest time in answering the above-mentioned questions. Full team participation encourages better ideas, clarity on the production schedule and the key challenges in production process.

Three rules for effective product evolution
 
1. Product evolution starts with making a detailed product specification containing the features, sub-features and components. 
2. Product evolution is a daily affair. Keep the production schedule updated on daily basis.
3. Product production is all about paying attention to detail. Keep an eye on the projected date of the release and on premature building of any feature or sub-feature.
