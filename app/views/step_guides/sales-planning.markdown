Sales planning tool is primarily used to decide the best sales plan, implement it and monitor its profitability. The metrics used to monitor its profitability is LTV/CAC times. 

Sales planning tool has inbuilt checklist for analyzing a sales plan and computing the sales length as per the plan. All the sales plans can be tracked and monitored using the profitability metric of LTV/CAC ratio.

What does LTV /CAC ratio mean?

* LTV means the revenue from the customer during the entire lifetime reduced by the production cost incurred to make that product for the customer. 
* CAC measures the cost incurred in acquiring the customer. Cost here are mostly marketing and selling costs. 
* LTV/CAC ratio measures how much money the venture earns, selling the product to a customer, for every dollar of amount spent to acquire that customer.
* A LTV/CAC Ratio of more than 2 is most preferable.
