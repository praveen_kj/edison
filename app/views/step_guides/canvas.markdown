### How to use strategy execution canvas?

1. Strategy execution is organized in form of a canvas and it is made up of 10 boxes and each box is for addressing a specific problem.

2. Click on the box and navigate to a tool that supports you to solve the mentioned problem.

3. Every tool is made from inline with startup best practices suggested in Discipline Entrepreneurship framework. 

4. Use this tool frame the strategy to address the problem. 

5. Discuss the strategy, make a To-do list to execute it and publish a detailed note on the strategy to your team. 

6. A key and powerful insight from the tool is updated on relevant box of the Strategy canvas. 

### For whom? 

For core founding team, mentor and investors. 
Core founding team may choose to share with the rest of the team 

### Frequency of use?

Frequently by the Founder/CEO, preferably once a day to take inventory of the situation.
Minimum frequency of usage by the core team to be once a week. Schedule it and brainstorm. 

