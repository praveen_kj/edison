Use Case tool is primarily used for understanding how motivated the users are and how easy is it for them to act on every significant touchpoint. A touchpoint is the interaction between the user and the product.

Work on how the Persona determines that his existing needs are not being met by existing products, and how the Persona would find out about the product. Use the Persona’s response and primary market research to know the motivation and ease of use for each of the touchpoint.

It is helpful to outline the customer’s current workflow, because by knowing the customer’s current process, it is easier to integrate the product into their operation. 

Customers who are generally satisfied with their workflow will rarely want to radically overhaul their process even if your product provides benefits over their current system.
