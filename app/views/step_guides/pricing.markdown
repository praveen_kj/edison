Pricing tool is primarily used for determining how much value the customer gets from the product, and capturing a fraction of that value back for the business. Costs are irrelevant to determining the pricing structure. Pricing tool with its in-build checklist of key considerations facilitates rapid response to change in market price points.

Getting pricing right is an iterative and ongoing process. Pricing involves multiple price points and pricing strategies, and feedback from the market about price points. Start at some point that is the best guess for that moment and then spiral closer and closer to a better answer.

Four basic pricing concepts
 
* Costs Shouldn’t Be a Factor in Deciding Price. 
* Identify Key Price Points. 
* Understand the Prices of the Customer’s Alternatives. 
* Pricing will continually change, be flexible
