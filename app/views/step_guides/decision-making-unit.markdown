Decision-Making tool or DMU tool is primarily used to learn who makes the ultimate decision to purchase the product, and who will be advocating for purchasing it and push to meet the influencers who have say over the purchasing decision.

The target customer almost always has a decision-making unit of more than one person. Understanding this group and explicitly mapping out each person’s role and interest is of critical importance not just for the sale, but also in product development.

What are the questions to ask the an interested customer to understand the DMU? 

1. Who besides you (make sure you make them feel good!) would be involved in the decision to bring our product in? 
1. Who will have the most influence? 
1. Who could stop this from happening? 
1. Who pays for it? 
1. Does this person need anyone else to sign off to pay for it? 
1. Who will feel threatened by this and how will they react?”

Generally DMU consists of End user, Advocate, primary economic buyer, VETO holder, and influencers.
