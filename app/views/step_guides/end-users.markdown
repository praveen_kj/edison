End user tool is primarily for profiling a set of end users within the market, deciding their top motivation to use the product and identifying their worst fear that prevents them from using the product.

Good, direct customer research is paramount to End User profiling process. The assumptions of who the end user is and what they want are often inaccurate. It is not a good practice to simply think through the end user profile without inputs from the primary market research. It is a huge advantage if someone who fits the End User Profile is on the team from the beginning. End user in the team bring down the assumptions involved in building the product. 

Having someone in the core team who fits the end user profile and validating assumptions about end user through primary market research if fundamental to success.

Selected Extract on Talking with humans by Giff Constable

* Go in prepared. Know your goals and questions ahead of time. Ask for advice. Don’t have an endless list of questions. Don’t pitch unless you actually try to close for real money. 
* Listen. 95% of the conversation should be them talking. Follow your nose and drill down when something of interest comes up. Don’t feel like you have to rigidly stick to a script. Don’t talk so much, and don’t be afraid of silences. Let them think. 
* Prepare yourself to hear things you don’t want to hear. Don’t let your excitement and optimism bias what you hear.
