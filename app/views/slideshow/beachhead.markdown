## Elon Musk's beachhead strategy for Tesla Motors 
> “So, in short, the master plan is:
> 
> 1. Build sports car
> 2. Use that money to build an affordable car
> 3. Use that money to build an even more affordable car
> 4. While doing above, also provide zero emission electric power generation options
> 
> Don't tell anyone.”

Elon Musk, Co-Founder & CEO of Tesla Motors August 2, 2006
Source: [The Secret Tesla Motors Master Plan](http://www.teslamotors.com/blog/secret-tesla-motors-master-plan-just-between-you-and-me)

__See the next slide to find out how you can apply the beachhead strategy__

## Doing Startups is like going to War...

__... so find your beachhead and focus your time and money on it.__

In the largest seaborne invasion in history, the Allied Forces meticulously executed a "beachhead" strategy by landing a force of 156,000 men in the beaches of Normandy rather than deploying these brave men across various parts of the Nazi Europe.

This strategy was vital in capturing the initial foothold and the forces gradually over time to marched to recapture Western Europe from Nazi control.

Elon Musk has applied this same strategy at Tesla Motors successfully.

The world's largest and successful companies like Google, Facebook and Apple adopted the same successful strategy as that of the Allied forces.

## The common thread shared by Google, Apple and Facebook when they started

Google started off with “google.stanford.edu” catering Stanford web search, expanded to early internet enthusiasts for over 6 years before slowly taking over the internet search market. Google as it took over the internet it was able to expand into other services such as emails, internet browser, maps, mobiles and even cars with the resources the Search business generated.

Apple initially started off by making computers exclusively for computer enthusiasts living in California before expanding across the US.

Facebook started off in Harvard in January 2004 and after 18 months expanded to other Ivy League universities, one by one, before taking over the world.

## What is a beachhead market?

All of these successful companies started in a small market where they could thrive rather than entering into a bigger market. Once they gained a dominant share in that market, they had the strength to attack adjacent segments with further offerings. 

That first segment which these companies started off is called the beachhead market. 

To make a marketable product, start by choosing a beachhead market.

