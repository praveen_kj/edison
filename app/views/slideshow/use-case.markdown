## What is a Use case?
Building your product’s Use Case focuses the discussion on what specifically your product will do for your customer and what your customer will do with it.
 
A "use case" is a list of touch points , defining interactions between an end user and your product.

 Aim of the tool is to define how your product is going to solve the problem at various touch points. 
 
Use the tool to describe in detail how your Persona finds out about your product, acquires it, uses it, gets value from it, pays for it, and buys more and/or tells others about it.

## What are your product’s 10 touchpoints?

1. How end users will determine they have a need and/or opportunity to do something different.
2. How they will find out about your product.
3. How they will analyze your product.
4. How they will acquire your product.
5. How they will install or get on-boarded to your product.
6. How they will use your product (in detail; see the Satisfier example further on).
7. How they will determine the value gained from your product.
8. How they will pay for your product.
9. How they will receive support for your product.
10. How they will buy more of your product and/or spread awareness (hopefully positive) about your product.

## What do I gain from Use case tool?

Clarity on what your end user will do at what time.

List of hurdles that will hinder you from delivering the best customer experience.

Estimate on the People, Material and Equipment needed during each of the touchpoints.  

Road map to make a great product development process.

Base for the Customer Experience tool.

 
