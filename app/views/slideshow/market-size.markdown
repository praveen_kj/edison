## The Product Adoption Lifecycle

A 1950s research about farmers hybrid seed corn adoption practices revealed 5 kinds of farmers.

1. __Innovators (enthusiasts)__ – had larger farms, were more educated, more prosperous, more risk-oriented and they were first to try the hybrid seed corn.
2. __Early Adopters__ – younger, more educated, tended to be community leaders, less prosperous and they adopted after the Innovators.
3. __Early Majority (realists)__ – more pragmatic but open to new ideas, active in community, influence neighbors and they adopted after Early Adopters.
4. __Late Majority (conservatives)__ – older, less educated, fairly conservative and less socially active and they adopted after the Early Majority.
5. __Laggards (skeptics)__ – very conservative, had small farms and capital, oldest and least educated and they were the last to adopt.

This end user classification and product adoption lifecycle was made famous in one of the Steve Jobs favorite book "Crossing the Chasm" by Geoffrey Moore. 


## Will every end user buy at same time?

No.

If there are 1,000 end users whose problem will be solved by your product then this 1,000 end users will be your market.
But not all of them will buy your product at the same time. 

Few of these customers will buy as soon as you launch and the rest will buy as the word about your product spreads.

First ones to buy will be the enthusiasts, followed by early adopters, realists, conservative buyers, and finally the skeptics.

## How to use this Market Size Tool?

1. Estimate the timeframe of product adoption by each of the end user groups.
2. Estimate the number of end users and the average price they pay to get the problem solved, for each of the end user group.
3. Validate the market size estimate through adding useful tasks. (Shift + V) shortcut is pretty useful! 
4. Use the market size to take a call whether this market is big enough to proceed or is it small that the scope of the market needs to be expanded. 
