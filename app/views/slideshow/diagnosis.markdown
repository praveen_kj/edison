## What is Diagnose?
Diagnosis is a set of 7 questions to measure whether a market is worth pursuing.

You need to objectively answer either a YES or a NO to each of the 7 questions to determine the market's feasibility. 

As it is hard to figure out an answer right away, you will have to find out data, conduct experiments, speak with experts to validate your assumption and make up your mind.

In the next slide you will find the most common methods of validation used by successful startups. 


## How to validate my assumptions
__Ask__: Interview and observe customers’ opinions and preferences. Learn from them, what they want and give them that thing which will satisfy them. 

__Data__: Search for statistical data and infer the results. In can be either primary data (directly from customers) or secondary data (from market reports). 

__Experiment__: Set and carry out tests with a sample set of individual users and learn from the results how the larger user-base will most likely behave in reality.  

__Compare__: See how your competitors are doing things. Learn from them and apply that in your business where possible. 

__Expert__: Gather new perspectives from your mentor and industry experts and improve the quality of your decision making.

## What do I gain from using Diagnosis?
Makes you adopt the culture of  "Get outdoors, meet people and get things done".

Follow the best practices in understanding and assessing your market viability. 

Proper usage of the Diagnosis tool improves your chances of success.

## How to add a new task?
Either 

Click on “new task” tab on the top 

Or

Press “Shift+ V”
