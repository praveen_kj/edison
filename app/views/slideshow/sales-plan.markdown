## How to plan for your sales?

- Name a sales plan
- Is it Short term, Medium term or Long term?
- Select the sales channel – In-person, Email, Sales agents, Online
- Fix the timeframe of the plan 
- Understand the sales process and the sales lifecycle
- Quantify the Cost to acquire a customer (CAC)
- Compare the CAC with LTV (Long term Value) of a customer

## What is your product’s sales process?

- Problem awareness: How will the customer become aware of this problem?
- Product awareness: How will customer become aware of your product?
- Product information: How will the customer evaluate your product?
- Product sale: How will a typical sales process close?
- Money Collection: How will you collect the consideration for your product?

## What is CAC?

CAC stands for “Customer Acquisition Cost”.

CAC is the cost associated in convincing a customer to buy your product.

CAC  = Total marketing costs / Number of customer acquired

## What is LTV?

LTV stands for “Lifetime Value”.

LTV is the total money (or consideration) received during the entire relationship (lifetime) with a customer. 

LTV = Total value received from a customer

## How to know if the sales plan is a good one or not?

If your LTV to CAC ratio is:

Less than 1:1  - On the road to oblivion, and fast

1:1 - Losing your money from every acquisition

3:1 - The perfect level. You have a thriving business and a solid business model.

4:1 - Great news.

Start more aggressive campaigns to acquire customers and bring your ratio closer to 3:1