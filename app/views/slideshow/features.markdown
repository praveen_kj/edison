## First 3 Dos
1. Always make a mock-up (proof of concept) of your product. It can be a prototype, video demo, pilot version or even a brochure.

2. Learn how to make a mock-up from your competitors, forums and industry experts.

3. Use the End user priorities and works backwards to determine the main features of your product. (Suggested 3 to 7)

## Middle Zone : 4, 5 and Six
4. Never build a feature and force it on the end user.

5. Ensure that your fantasy does not drive feature selection process.

6. Work extremely hard to ensure that the selected top features are backed by validated concepts.

## Next three Dos
7. Diligently decide the platform, framework, machine or technology on which your product will be built.

8. Always verify if your product’s use case and the product specification mock-up are consistent and do not contradict each other.

9. Use the persona’s feedback to iterate and update the mock-up.

## One Last Do

> Strive hard to make things reasonably simple.
> Customer uses your product for making his or her life simpler, better and easier.
> Build features that make your customer life better than before. 


 
