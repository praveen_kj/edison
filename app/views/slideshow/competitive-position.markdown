## Do you want to know where you stand?
Use the Competitive Position tool to graphically understand how you fare against your competitors. 

Competitive Position is where you take your Core and translate it into something that produces real value for the customer, something that they will care deeply about.
 
Competitive Position helps you to know how well your product meets the Persona’s top two priorities and how well the Persona’s priorities are met by existing products in comparison to your product. 


## Who is the toughest competitor you will ever face? 
The toughest competitor of all is the customer’s status quo. 

Status quo customers are the potential customers who have the problem but do nothing about it (not even use your competitors’ products) and continue to remain status quo about it.

Often, your largest obstacle will be convincing customers to make a change from their status quo. 

When the Sony Walkman was first created, there were few comparable devices, but the biggest competition Sony was selling to customers who did not listen to music on the go. The status quo for these customers would have included listening to music at home or going to concerts. 

## What should I do after using this tool?
If you have a good Core and people convert from the status quo to a new solution (your product), the market will take off and both you and the other small competitors will win big. 

Once you have your Core and Competitive position, don’t focus too much precious time on competitors.

Rather, spend most of it working with customers, developing your Core, and getting your product out of the door.
 
