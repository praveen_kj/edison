## What is a Value proposition ?
A value proposition is a promise of value (what your product offers) to be delivered. It’s the primary reason your end user will buy from you.

1. A value proposition explains how your product solves the end users’ problem and improves their situation (relevancy). 
2. A value proposition delivers specific benefits (quantified value). 
3. A value proposition tells the ideal customer why they should buy from you and not from your competitors (unique differentiation).


## Why to quantify it?

When a customer purchases a product, they are asking themselves, “What value do I get out of this product?” Customers must justify the investment required to acquire your product by offsetting this against how much money your product will make for them, or how you will improve their life in a way that really matters to them.

The goal of the Quantifying Value Proposition is to clearly and concisely state how your product’s benefits line up with what your customer wants.

The Quantified Value Proposition focuses on what potential customersgain rather than going into detail on technology, features, and functions. 


## How to use this tool?

1. List the propositions your customers will look on from your product and your competitors' products.
2. Capture the offers, propositions  and promises made by the competing product and your product.
3. Competing products represent the As-Is state and Your product represents the Possible State.
4. Quantify the propositions in Dollars using the Prioritized order of End User Priorities.

 
