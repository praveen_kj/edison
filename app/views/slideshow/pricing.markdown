## How to go about your product price?

Your product price is the share of the value created for a customer. 

With every new market information and feedback from your customers the product price goes for a change. 

Create few pricing plans and improve it through iteration. 

Be smart, establish fairness and exercise adaptability to accommodate any new information.

## What are the 3 good ways to test your pricing plans (before your product hits the market)?

1. Fair price test
2. Gasoline test
3. Industry expert’s test


## Fair price test

- Fair price test means: “Do to others what you want them to do to you.” 
- Add a task to repeatedly ask yourself if you were the customer, will you pay that price. 
- Introspect to ensure fairness is maintained. 
- Be open. Use the task’s outcome to adjust the product price.

## Gasoline test

- Money is like gasoline during a road trip. You don’t want to run out of gas on your trip, but you’re not doing a tour of gas stations. 
- Add a task to repeatedly ask yourself if the pricing is done with an objective of treating money as gasoline needed for your trip (running your venture) and not doing a tour of gas stations (not to run your venture with sole intention to make money). 
- Introspect to ensure the product price is not driven by greed. 
- Use the task’s outcome to adjust the product price. 

## Industry Expert test

- Approach Industry Experts who will invest quality time and go through the pricing plans.
- Ensure that Expert has excellent track record, is dependable and open- minded. 
- Be open to the Expert's validation process and ponder over the reason for the differences. 
- Understand the differences between your views and that of the Expert’s. 
- Use the task’s outcome to adjust the product price.

 
