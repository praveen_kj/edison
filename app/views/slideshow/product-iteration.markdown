## Why to iterate?
Product features you have developed over time with customers using it will evolve.

The triggers for the evolution are the customer complaints and reduction in retention rates

Outcome of the evolution is mature product that will stand overtime.

Process of this evolution is the feature iteration.
## When to iterate?

1. Set up metrics to quantitatively measure how your features are performing. 
2. You can also learn from observing what your customers do and/or create A/B tests (Split testing) .
3. Be extremely diligent to see whether a feature performs to what it is expected to perform.
4. If your data shows proof that there is an issue with the feature, you need to iterate the feature.


## How much time to invest in iteration? 
Roughly, segregate the product development time into: 

> __80%__ for iteration of existing features 
> and 
> __20%__ of for new features

## What are 3 things to do before iteration?

1. Get clarity on why to iterate. (backed with data) 
2. Record the reason to iterate. 
3. Schedule your iteration (estimate the cost, effort and time) 

 
