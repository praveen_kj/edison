## What is the End User tool?
End User tool is a collection of information about your End user.

You can collect the following critical information about your end users in this market: 

1. Demographic and psychographic parameters relevant in identifying your end user.
2. Spending traits of your end users relating to your product.
3. Reasons to use your product.
4. Day in life of your end users.

When you know the people who will use your product and what makes them use your product, your product development is half done.


## How to use this?
1. Browse through the End User tool and get a brief understanding of who will be your end user. 

2. Write down your educated guesses and assumptions about all the end user parameters listed in the tool. 

3. Do not worry about whether your guesswork is accurate at this point. 

4. Add tasks to validate and iterate the assumptions about your end users.

5. Get outdoors, meet people, discuss and conduct research to execute the tasks. 

Update the end user tool using the insights you gained from executing the tasks.

## What do I gain from profiling an End User?
__Customers are real people__:  Getting to know the real people is the first step in making a product that is usable by real people. 

__Customers are everything__: It is also a critical part of the process to help ingrain the mentality that you should build the company around the customer’s needs, not based on your interests and capabilities. The latter does matter, but it is secondary to how you should think about your business.

__Customers are visible__: This is a critical tool to frame an idea about who will be your end user and what are the key attributes of the end user. When you walk around the town you should be able to identify your end users from the crowd.
 
