## Why to estimate the investment?
Serious under-budgeting issue haunts startups over time. 

Contingencies from unforeseen activities leads to excess efforts, additional time and requirement for new materials push the budgets up. 

Acknowledge that all the four time, effort, materials and money are limited and create cramps to a startups progress, if not planned well.

## What is a milestone?
A milestone is a quantifiable achievement, be it in terms of product development, team expansion, or market adoption of your product.

Forecast the key activities associated with a milestone that need to be done to accomplish the milestone.

Make a good estimate of what it will take in time, effort, materials and money to complete these key activities.

## How to know if your budget is a good one?
Prediction and reality almost always are two different things. Is hard to precisely predict a future information. 

Estimate the milestones your venture will achieve over the next year and what is the capital you will require to accomplish the milestone.

As a matter of caution, be ready to double your estimates, but never allow your reality to become thrice your estimates.

 
