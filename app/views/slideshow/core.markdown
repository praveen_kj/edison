## What is it that your product does that your competitors can’t duplicate, or cannot duplicate easily? 
 
That is your Core.

Build a core from understanding the uniqueness of your product, the novelty it delivers and the loyalty it builds.

## What constitutes a Core?
Core is that thing which is integral part of your product that a new entrant will find hard to be replicate.

1. Core explains how your product is different from your competitors (uniqueness).

2. Core delivers something more than the promised value so that your customer stays with you (novelty).

3. Core conveys to the customer why they should be with you and not move on to your competitors (loyalty).

## What do I gain from establishing a Core?

__Better value:__ The Core is something that allows you to deliver the benefits your customers value with much greater effectiveness than any other competitor. 

__Better at competition:__ You are looking for that single thing that will make it very difficult for the next company that tries to do what you do. It could be a very small part of the overall solution, but without it, you don’t have nearly as valuable a solution. What is it that you do better than anyone else?

__Better revenue:__ The Core also provides a certain level of protection, ensuring that you don’t go through the hard work to create a new market or product category only to see someone else come in and reap the rewards with a similar business of their own. 



## 4 Highly practiced types of Core
1. __Network effect:__ If this is your Core, you become the standard by achieving so much critical mass in the marketplace that it does not make sense for potential customers to use another product. Examples of business with "Network size" as core are Facebook, Reddit and LinkedIn. 

2. __Customer service:__ By establishing processes and culture that focus on excelling at customer service, this potential core allows you to retain customers at a very high rate as compared to competitors, and thereby avoid costly churn. It will also allow you to attract and obtain customers in a much more efficient way than others in the market, as your customers are thrilled with their experience with you and become salespeople for you by creating positive word of mouth. Examples of business with “Customer service” as core are Amazon ,USAA and Trader Joe’s.

3. __Lower costs:__ Develop the skills, relationships, processes, volumes, financial overhead, and culture to outcompete anyone else in the market on cost and become the long-term low-cost player. This has been a successful Core for Wal-Mart and it is also part of the strategy behind many Asian companies, especially with Chinese companies that have recently entered the clean energy sector. 

4. __User experience:__  The overall experience of a person using a product such as a website or computer application, especially in terms of how easy or pleasing it is to use. Develop work culture and skills that delivers the great emotion and feel good effect on using your product. Examples of business with “User experience” as core are Apple and Wikipedia.

What is your core?
 
