## What is best way to effective product making?
Simple and most powerful method in product development is to build your product for this typical end user (Persona) and extend it to the other end users.

Choose a real person who will be your typical customer and find out the top user priorities. 

Use these priorities (reasons to use your product) to develop a product and later scale the product with some more customers.

An important detail to understand about the Persona is his or her Purchasing Criteria in Prioritized Order. 

You should really understand your customer and what makes them tick, not just at a rational level, but at an emotional and social level as well.  


## Who will be the best representative of your typical end user (Persona)?
It can be any of these: 

- Yourself ?
- A Family member?
- A Team member?
- A Friend?
- A Peer at work?
- Someone else?

Figure out the best representative from the lot.

## How to figure out?

Inquire, observe and understand:
 
1. How is the persona’s life with the problem? 
2. How will the persona’s life be without the problem? 
3. What is the solution persona currently uses to solve the problem? 

The better you understand the Persona’s needs, behaviors, and motivations, the more successful you will be at making a product.

A Persona who is currently affected by the problem is a better pick than a person who was affected in the past. 

Work hard to pick the right Persona. Time to again get outdoor and getting the hard things done.
 
