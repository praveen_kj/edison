## Why next 10 customers?
1. Explicitly identifying the next 10 customers after the Persona increases your level of confidence that you are on the right path and may also help you refine your path.
2. Listing and interviewing next 10 customers, you are directly testing every hypothesis you have built so far.
3. Sequentially contacting the next 10 customers you will greatly lower the risk of your new business, getting you on a direct, focused, and fast path to success.



## How to use this tool?
1. Verify that the customer is similar to the Persona. 
2. Contact the customer to verify and refine your primary market research. 
3. Validate all of your work thus far, such as the Use Case, Features and Quantified Value Proposition.
4. If you are successful in this tool, you can be significantly more confident that your business has a high probability of success—and you will be able to convince others, such as future partners, employees, customers, advisors, and investors. 
5. If you run into issues during this period, you will be able to go back and determine where the flaws in your plan are and improve them before going further.

## Is negative feedback good?

Any negative feedback is most welcome here.

Your primary market research has been designed so that you continually stay aligned with the customer’s needs; but this is your first big “systems test” where you are presenting the customer with everything you have worked on so far, so you may encounter some negative feedback at this step if your plan is not quite right. That is not only okay, but probably good. 

You are unlikely to have everything correct at this point anyway, so if the only feedback you get is “everything is okay,” then it is likely that the customer doesn’t care much about your product and its value to them.

If the customer gives you detailed feedback, even if it is negative, it shows the customer cares about the problem you are trying to solve, and that it is worth your time to iterate with them to create a good product.

> “Your most unhappy customers are your greatest source of learning” – Bill Gates, Microsoft

 
