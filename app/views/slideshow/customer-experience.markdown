## What is Customer Experience?
Customer experience is the interaction between your company (not just the product) and a customer over the duration of their relationship.

Every touch point with your customer is extremely important for delivering a great product. 

Get clarity on what experience you will deliver to your customers and over come the hurdles to deliver the best customer experience.

Every interaction is the chance to either make it great or lousy.
## What is that one key metrics that increases your product’s sustainability?

Customer Retention rate


## How to increase customer retention rate?

1. Identify hidden hurdles that will inhibit your ability to sell your product and get paid. 
2. Clear of these hurdles and improve the customer experience 
3. Enjoy a great customer retention rate.
