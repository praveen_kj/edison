## What is a Canvas?
Market Canvas is the place where you can quickly scribble down the key information about your market.

At large, there are 9 key aspects to understand about your market. 

On completing the canvas you will be able to:

1. Understand your market better than before.
2. Use it to compare with other potential markets in deciding what your beachhead should be.
3. Document your learning about the market.


## How can I validate these key aspects?
When completing the Canvas for the first time, you will have to make assumptions and guesstimates. 

Some of these assumptions are likely inaccurate. If they are inaccurate it may end up costing you a lot of money and a waste of your precious time. 

Therefore, you should validate these assumptions by adding tasks. Use (Shift + V) or click the tasks tab to add a new tasks. 

## Is the Canvas a onetime activity?
Definitely not. 

You need to update the canvas as you learn more and more about your market from your customers.

Given the high frequency of updates to the Canvas, the “canvas” button is placed on the left sidebar for quick access. 

You will also need to validate your understanding of the market on a continuous basis. 
 
