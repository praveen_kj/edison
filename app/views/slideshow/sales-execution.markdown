## How to be good at sales execution?

1. Estimate the number of end users who has the problem your product will solve.
2. Estimate the conversion and retention rates.
3. Ensure your meet your daily or weekly reach-out target.
4. Identify the hurdles that harm your conversion and retention rates.
5. Solve the hurdles within a target date.

## How significant is the role of DMU in sales?

Totally significant.

Determine who the DMU members.

Find out what each of them want from your product.

Ensure you can offer things that will meet their wants. 

## Do I need to set any targets?

Absolute necessity.

1. Set time bound targets of your topline.
2. Set processes that helps you to achieve the results. 
3. Monitor the progress closely – daily or weekly. 
4. Find out the gaps in the sales plan and process.
5. Rectify the process if you are consistently under achieving.

