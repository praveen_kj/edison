## What is a Business Model?

Business model determines how the money will flow in and where it will be spent.

Business model will determine typical a-day-in-life of yourself, your team, your product, and your company. 

A good business model helps you capture the fair share of the value generated from your customers and fairly distribute it to the all the key people involved in generating it. (i.e., vendors, team, and investors). 

## How to pick your business model from the huge list of commonly used business models?

1. Narrow down the options using the inbuilt checklist.
2. Choose a most suited business model.
3. Add some innovation.


## How to narrow down?

- You got to inquire, analyze and compare every option before dropping that it will not work well for you. 
- Be open, innovative and diligent in determining your business model.  
- Analyze, visualize and infer the possible state of affairs of your business under each of the model.  
- Based on analysis shortlist 3 to 5 business models that are more appropriate to you. 
- Ensure that these shortlisted options take care of your team’s interest, investment and brand value.

## How to choose one?

- Always be open to new ideas, analyze the options more closely and trace the most suited model among the options. 
- Ensure your bias is not affecting your business model choice. 
- Take the help of industry experts and potential customers to gather relevant data to run simple simulations to pick your business model. 
- Use the simulation results to pick your most suited business model.

## How to add some innovation?

- Certain element of innovation in business models helps you to get a better and a sustainable start to your venture. 
- Validate using tasks (Shift+V) to check whether the chosen business model is innovative enough. 
- Analyze possible outcomes the business will generate, before and after, using the innovation. 
- Difference between them shows the effect of innovation.  
