## What is a DMU?
Your target End user almost surely has a decision-making unit (DMU) of more than one person. 

Understanding this unit and explicitly mapping out each person’s role and interest is of critical importance for a sale. 

Knowing your product’s DMU earlier in the process helps you to develop a better product that caters the need of the Unit, just not the End user.

## Who are the people in your product’s DMU? 
__End Users:__ These are the people who are actually going to work with the purchased goods or services and they exert influence on the specifications. Both customers and employees may take on this role. 

__Advocates or Initiators:__ The advocate is the player who recognizes a problem and tries to find a solution for this problem and advocates it to the End user to use it. Sometimes, advocates and the end user can be the same person.

__Economic Buyers:__ The buyer is the actual paying decision maker. The buyer directly or through purchase department negotiates about contract terms and eventually places the order and pays money for it. 

## More people in a DMU 
__Main Influencers:__  These individuals often have a depth of experience in the subject matter, and can influence the rest of the DMU, including the advocate and end user. 

__Other influencers:__  These individuals may include media publications, individual journalists, outside contractors, friends and family, industry groups, websites, blogs, and anyone else who the Primary Economic Buyer turns for reliable information and feedback.

__VETO holders:__ These individuals have the ability to reject a purchase for any reason. Often, in a B2B environment, this individual outranks the advocate or end user in a corporate hierarchy.

## How frequently should I update the DMU?
A DMU is rarely stable and it often changes. 

It may change as a result of the addition of another person. 

Conflicts of interest may also play a major role as a consequence of which the outcome of the decision-making process becomes unpredictable. 

It is advisable to look at the influence, perception, roles and risks from a distance.

Be agile to sense if there is any change in purchasing pattern. Update the DMU as when necessary. 

