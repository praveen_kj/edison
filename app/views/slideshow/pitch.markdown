## What is an elevator pitch?

- Elevator Pitch comes from the idea that you and a  potential customer “accidentally” get into the same elevator and you have less than a minute to get customer’s attention. 
- What is that you will speak in that one minute?
- You need to say something crisp in a conversational style about your product and its top benefits from using it, to get your customer’s attention.


## How to write a winning pitch?

- The Pitch should be simple and easy-to-understand.
- Use conversational English.
- Clearly mention the solution your product provides.
- Write the top benefits that attracts your customer’s attention.
- Mention the reason your customer has to trust your product.
- Re-read the pitch and make it better. 


## Learn it from Facebook’s first pitch

> Thefacebook is an online directory that connects people through social networks at college.
>
> You can use Thefacebook to:
>
> - Search for people at your school
> - Find out who are in your classes
> - Look up your friends’ friend
> - See a visualization of your social network
 
Source: <a href="http://upload.wikimedia.org/wikipedia/en/f/f8/Thefacebook.png" target="_blank">http://upload.wikimedia.org/wikipedia/en/f/f8/Thefacebook.png</a>

<!--- Say what your product <strong>solves</strong>, list its <strong>top benefits</strong> and a clear
reason why should they <strong>trust</strong> your product.

Speak Less, Speak Clear, Speak Sense
in less than 1 minute -->