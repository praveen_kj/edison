$ ->
  $('.update-card-trigger').on 'click', ->
    $('.detail-explanations').slideUp ->
      $('.update-card-enc').fadeIn()

  $('.back-button').on 'click', ->
    $('.update-card-enc').slideUp ->
      $('.detail-explanations').fadeIn()

  $('.user-details-enc .normal-input').focusin ->
    $(this).parent().addClass 'icon-focus'

  $('.user-details-enc .normal-input').focusout ->
    $(this).parent().removeClass 'icon-focus'

  $('input.cc-num').payment('formatCardNumber')
  $('input.cc-cvc').payment('formatCardCVC')
  $('input.cc-exp').payment('formatCardExpiry')
  $('[data-numeric]').payment('restrictNumeric')

  $('[data-toggle=tooltip]').tooltip()


$(document).on 'keydown', '.card-num', ->
  card_num    = $(this).val()
  card_name   = $.payment.cardType(card_num)
  card_name   = 'diners-club' if card_name is 'dinersclub'

  known_cards = [
    'diners-club',
    'jcb',
    'amex',
    'discover',
    'mastercard',
    'visa'
  ]

  if card_name in known_cards
    $('.card-num-enc .card-logo').attr('class', "card-logo fa fa-cc-#{card_name}")
  else
    $('.card-num-enc .card-logo').attr('class', "card-logo fa fa-credit-card")
