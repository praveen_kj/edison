$(document).on 'ajax:success', '.new-discussion-form, .new-comment-form', ->
  $this = $(this)
  $this.parent().removeClass('expanded')
  $this[0].reset()
