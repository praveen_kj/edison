$(document).on 'click', '[data-behaviour=show_update_form]', ->
  $this = $(this)
  $this.parent().hide()
  $($this.data('target')).show()

$(document).on 'click', '[data-behaviour=cancel_account_update]', ->
  $('.user-profile-item > div').show()
  $('.user-profile-item > form').hide()