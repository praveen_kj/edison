$(document).on 'submit', '.task-form-over-list', ->
  $this = $(this)
  $toDoList = $(".to-do-list._#{$this.data('parentId')}")
  randNum = $this.find('#rand_field').val()
  newRandNum = "#{Math.floor(Math.random() * 100000)}"
  $this.find('#rand_field').val(newRandNum)
  val = $this.find('#sub_task_sub_task_name').val()

  if val isnt ''
    $toDoList.find('.sub-tasks-list').append(
      "<li class=\"sub-task-rec\" id=\"#{randNum}\"><div class=\"row\">
    <div class=\"col-xs-1\">
    <div class=\"col-xs-8\">
    <i class=\"fa fa-square-o new-task-icon\"></i>
      </div>
    </div>
    <div class=\"col-xs-9\">
    #{val}</div>
    <div class=\"col-xs-2\">
    </div></div> </li>")
    $this.find('#sub_task_sub_task_name').val('')
  else
    $this.find('#sub_task_sub_task_name').effect('shake')



$(document).on 'keypress keydown keyup change', '#task_name', ->
  $btn = $('.new_user_task').find('.add-submit')
  if validateNewTask()
    $btn.prop('disabled', false)
  else
    $btn.prop('disabled', true)

validateNewTask = ->
  result = true
  $('#task_modal_task_name').each ->
    if $(this).val() is ''
      result = false
      return
  return true if result isnt false



$(document).on 'change', '.task-complete-checkbox', ->
  $this = $(this)
  $this.parent('form').submit()
  return

$(document).on 'click', '.close-to-do, .close-exp', ->
  $container = $(this).closest('.sub-task-form-container, .exp-form-container')
  $container.find('.add-to-do-link, .add-exp-link').show()
  $container.find('form').hide()

$('.sub-task-rec').seekAndDisplay '.del-icon, .do-today-btn.off'

$(document).on 'click', '[data-behaviour=show_completed_tasks]', ->
  $this = $(this)
  $this.html('<i class="fa fa-spinner spin"></i>')
  $.getScript "/sub_tasks/complete/#{$this.data('taskId')}", ->
    $this.html 'Show completed tasks'

$(document).on 'click', '[data-behaviour=toggle_todays_tasks]', ->
  $this = $(this)
  $tab = $("[data-tab-view-id=\"/to-do-lists/#{$this.data('taskId')}\"]")
  $all = $tab.find('.sub-task-rec')
  $visible = $all.filter(':visible')
  $today = $all.find('.today')

  if $all.length is $visible.length
    $all.hide()
    $today.show()
  else
    $all.show()




$(document).on 'click', '[data-behaviour=todo-filter]', ->
  $this = $(this)
  criterion = $this.data 'criterion'
  $('.toggle-task-filter').removeClass 'active'
  $this.addClass 'active'
  $(".to-do-list").each ->
    $tdl = $(this)
    if $tdl.hasClass(criterion)
      $tdl.find('.sub-task-rec').each ->
        $str = $(this)
        $str.hide() unless $str.hasClass(criterion)
    else
      $tdl.hide()

$(document).on 'click', '[data-behaviour=remove-filter]', ->
  $('.toggle-task-filter').removeClass 'active'
  $(this).addClass 'active'
  $(".to-do-list").each ->
    $tdl = $(this)
    $tdl.show()
    $tdl.find('.sub-task-rec').each ->
      $(this).show()

#$('.sub-task-rec').seekAndDisplay '.todo-options-panel'

$(document).on 'click', '.todo-options-panel', ->
  $(this).addClass('has-baloon')


$(document).on 'click', '[data-behaviour=select_todo_list_for_new_todo]', ->
  $this = $(this)
  $('#new_todo_user_task_id').val($this.data('listId'))

