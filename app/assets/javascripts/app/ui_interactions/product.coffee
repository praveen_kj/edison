$(document).on 'click', '[data-behaviour=show-new-prod-form]', ->
  $('.products-container, .product-overview').slideUp 'fast', ->
    $('.new-product-form').show().find('.form-control').first().focus()
  return

$(document).on 'click', '[data-behaviour=cancel-prod-create]', ->
  $('.products-container, .product-overview').slideDown 'fast', ->
    $('.new-product-form').hide()
  return


$(document).on 'submit', '.new-prod-form', (e) ->
  $form = $(this)
  if $form.find('#product_product_name').val() is '' and $form.find('#product_owner_role').val() is ''
    $form.effect('shake').find('.form-control').first().focus()
  else
    submitForm($form)
  return false

submitForm = ($form) ->
  $form.get(0).submit()