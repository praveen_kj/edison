$(document).on 'blur', '#note_modal_note_title', ->
  $(this).parents('form').submit()


typingTimer = undefined
doneTypingInterval = 1000


$(document).on 'keyup', '.note-trix-editor', ->
  $this = $(this)
  clearTimeout typingTimer
  typingTimer = setTimeout(->
    $this.parents('form').submit()
  , doneTypingInterval)
  return

$(document).on 'keydown', '.note-trix-editor', ->
  clearTimeout typingTimer
  return


$(document).on 'ajax:send', '.note-form', ->
  $(this).find('.note-updates').html('<i class="fa fa-spin fa-spinner"></i> Saving...').addClass('active')

$(document).on 'click', '[data-behaviour=finish_note_editing]', ->
  $this = $(this)
  $tabView = $this.parents('[data-tab-view-id]').hide()
  $tabItem = $("[data-tab-id=#{$tabView.attr('data-tab-view-id')}]").hide()

  link = $tabView.find('.note-form').attr('action')

  $.getScript(link);

  Router.navigate link, overwrite: true


  setTimeout(
    $tabView.remove()
    $tabItem.remove()
  , 1000)