$(document).on 'keydown keypress keyup', '[data-mirror-entry=true]', ->
  $this = $(this)
  $("[data-tab-id=#{$this.parents('[data-tab-view-id]').attr('data-tab-view-id')}]").find('.inner').html($this.val())


$(document).on 'click', '[data-behaviour="reschedule_milestone"]', ->
  $this = $(this)
  $releaseDate = $('.release-date')
  $releaseDate.find('.default-state').hide()
  $releaseDate.find('.edit-state').show().find('.active-edit').trigger('click')

