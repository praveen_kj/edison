

$('.canvas-sheet-item').seekAndDisplay('.button-enc')
$('.market-name-header ').seekAndDisplay('.select-bh, .del-icon')

window.drawBarChart = ->
  $('.market-size-bar-chart').each ->
    $this               = $(this)
    chart_items         = $this.find('.chart-item')
    baseVal             = parseInt($this.data 'baseValue') or 30
    heights             = $.map(chart_items.toArray(), (item) -> parseInt($(item).attr('data-height')) or 0)
    max_val             = Math.max.apply(Math, heights)
    normalized_heights  = $.map(heights, (height) -> height/max_val * baseVal )
    chart_items.each (index) ->
      $(this).css('height', Math.max(normalized_heights[index], 1))

window.calculate_mkt_values = ->
  $('.market-size-value-enc').each ->
    chart_items   = $(this).find('.chart-item')
    market_values = $.map(chart_items.toArray(), (item) -> parseInt($(item).attr('data-height')) or 0)
    total         = 0
    $.each market_values, -> total += this
    $(this).find('.market-size-value').html("$ #{number_to_human total}")

window.calculate_global_mkt_value = ->
  market_values = $.map($('.chart-item').toArray(), (item) -> parseInt($(item).attr('data-height')) or 0)
  total = 0
  $.each market_values, -> total += this
  $('.usd-mkt-value').html "$ #{number_to_human total}"

$(document).on 'click', '[data-behaviour=new_business]', ->
  $(this).find('.form-enc').fadeIn('fast').find('.form-control').focus()

$(document).on 'click', (e) ->
  if $(e.target).closest('.new-market-btn').length is 0
    $('.new-market-btn .form-enc').fadeOut('fast')


$(document).on 'ctab:shown', '[data-tab-id=beachhead]', ->
  drawBarChart()
  calculate_mkt_values()
  calculate_global_mkt_value()

$ ->
  drawBarChart()
  calculate_mkt_values()
  calculate_global_mkt_value()


$(document).on 'ajax:success', '.del-business-confirm', ->
  disableTabsBHchange()
  return

# empty content from tabs
window.emptyTabsBHchange = ->
  $('.home-view').empty()
  $('.steps-view').empty()
  $('.canvas-view').empty()
  $('.diagnosis-view').empty()
  $('.end-user-view').empty()
  $('.investment-view').empty()
  $('.pitch-view').empty()
  $('.market-size-view').empty()
  return

# tabs must refresh and tabs are enabled if they are disabled
window.enableTabsBHchange = ->
  $('#home_tab').attr 'data-disabled', false
  $('#steps_tab').attr 'data-disabled', false
  $('#tasks_tab').attr 'data-disabled', false
  $('#canvas_tab').attr 'data-disabled', false
  $('#end-user_tab').attr 'data-disabled', false
  $('#diagnosis_tab').attr 'data-disabled', false
  $('#beachhead_tab').attr 'data-disabled', false
  $('#pitch_tab').attr 'data-disabled', false
  $('#market-size_tab').attr 'data-disabled', false
  $('#investment_tab').attr 'data-disabled', false
  return

# tabs must refresh and tabs are disabled if they are enabled
window.disableTabsBHchange = ->
  $('#home_tab').attr 'data-disabled', true
  $('#steps_tab').attr 'data-disabled', true
  $('#tasks_tab').attr 'data-disabled', true
  $('#canvas_tab').attr 'data-disabled', true
  $('#end-user_tab').attr 'data-disabled', true
  $('#diagnosis_tab').attr 'data-disabled', true
  $('#beachhead_tab').attr 'data-disabled', true
  $('#pitch_tab').attr 'data-disabled', true
  $('#market-size_tab').attr 'data-disabled', true
  $('#investment_tab').attr 'data-disabled', true
  return


