$ ->
  attachAutoComplete()

$(document).on 'focus', '[data-tags]', ->
  attachAutoComplete()

attachAutoComplete = ->
  $('.customer_feedback_customer_name').autocomplete
    source: (req, resp) ->
      x = JSON.parse(@element.attr('data-tags'))
      resp(x)

$('.customer-feedback').seekAndDisplay '.del-icon'


customerName = ()->
  $('.multi-feedback-customer-name').val()

flushFeedbackArea = ->
  $(".multi-feedbacks-entry-list").empty()
  appendNewField(disabled: true)
  appendNewField(disabled: true)

emptyFeedbackAreaCount = ->
  $('.multi-feedback-textarea:empty').filter(-> !$(this).val()).length

appendNewField = (options = {})->
  randCode = "#{Math.floor(Math.random() * 100000)}"

  $textArea = $('<textarea></textarea>').attr(
    class: "form-control multi-feedback-textarea "
    id: "feedback"
    name: "feedback"
    placeholder: "Feedback from Customer"
    'data-feedback-temp-id': randCode
  )

  $textArea.prop('disabled', true) if options['disabled']

  $textAreaEnc = $('<div></div>').attr(
    style: 'margin-top: 10px;'
    'data-feedback-enc-id': randCode
  ).html(autosize($textArea))

  $(".multi-feedbacks-entry-list").append($textAreaEnc)


submitFeedback = ($feedbackTextArea)->
  $feedbackTextArea.prop('disabled', true)
  $.ajax
    url: '/customer_feedbacks'
    method: 'post'
    dataType: 'script'
    data: {
      customer_feedback: {
        feedback: $feedbackTextArea.val()
        customer_name: customerName()
      }
      temp_id: $feedbackTextArea.data('feedbackTempId')
    }



$(document).on 'focus', '.multi-feedback-textarea', ->
  appendNewField() if emptyFeedbackAreaCount() is 1

$(document).on 'blur', '.multi-feedback-textarea', ->
  submitFeedback($(this)) if customerName() isnt ''

$(document).on 'keypress keydown keyup', '.multi-feedback-customer-name', ->
  if $(this).val() isnt ''
    $('.multi-feedback-textarea').prop('disabled', false)
  else
    $('.multi-feedback-textarea').prop('disabled', true)


$(document).on 'hidden.bs.modal', '.new-customer-feedbacks', ->
  $('.multi-feedback-customer-name').val('')
  flushFeedbackArea()

$(document).on 'change', '[name=feedback_type]', ->
  $this = $(this)
  v = $this.val()

  $parent = $this.parents('.feedback-reference-parent')

  if v is 'Strategy'
    $parent.find('.related-tool-data').show()
    $parent.find('.feature-data').hide()
  else if v is 'Product'
    $parent.find('.feature-data').show()
    $parent.find('.related-tool-data').hide()