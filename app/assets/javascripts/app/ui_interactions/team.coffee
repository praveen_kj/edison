$(document).on

  'slider:shown': ->
    $this = $(this)
    $this.find('.form-control').first().focus()

  'slider:hidden': ->
    $this = $(this)
    $this.find('.alert').remove()


, '#new-membership-form-enc'

$(document).on 'click', '[data-behaviour=edit_access]', ->
  $(this).parents('.member-access-rec').find('.access-options').slideDown()


$(document).on 'click', '[data-behaviour=set_choice]', ->
  $this = $(this)

  choice = $this.attr 'data-choice'
  access = $this.attr 'data-access'

  $parent = $this.parents('td')

  $parent.find('button').html("#{access} <span class=\"caret\"></span>")

  if choice is '1'
    $parent.find('#write-tools').val(true)
    $parent.find('#write-utils').val(true)
  else if choice is '2'
    $parent.find('#write-tools').val(false)
    $parent.find('#write-utils').val(true)
  return

$(document).on 'click', '[data-behaviour=toggle_value_on_click]', ->
  $this = $(this)
  target = $this.data('target')
  $this.toggleClass 'checked'
  if $(target).val() is ''
    $(target).val target.replace('#hidden_field_for_', '')
  else
    $(target).val ''