loadMoreSelectorMap =
  '[data-fp-discussion-id]': 'discussions'
  '[data-discussion-id]': 'discussions'
  '[data-customer-feedback-id]': 'customer_feedbacks'
  '[data-notes-id]': 'notes'
  '[data-to-do-list-id]': 'to-do-lists'


$(document).on 'click', '[data-behaviour=load_more]', ->
  $this = $(this)
  target = $this.data('target')
  offset = $(target).length
  $.getScript("#{loadMoreSelectorMap[target]}/load_more?offset=#{offset}")