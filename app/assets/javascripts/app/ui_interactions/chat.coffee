$(document).on 'keypress', '[data-behaviour=submit_on_enter]', (e)->
  if e.keyCode is 13
    e.preventDefault()
    $form = $(this).parents('form')
    $form.submit()
    $form[0].reset()

$(document).on 'click', '[data-behaviour=show_older_messages]', ()->
  $.getScript('/messages')
