window.getTimeZone = ->
  offset = new Date().toString().match(/([-\+][0-9]+)\s/)[1]
  formattedOffset = "#{offset.substring(0, 3)}:#{offset.substring(3)}"

## apply selected class on item in group that is selected


## redirect click to some other DOM element

$(document).on 'click', '[data-behaviour=mirror_click]', ->
  $($(this).data('target')).trigger('click')


## show feedback form

$(document).on 'click', '[data-behaviour=new_feedback]', ->
  $('.tab-view-layer').slideUp 'fast', ->
    $('#new-feedback-form-enc').show().find('.normal-input').focus()

## bindings to feedback form show and hide events

$(document).on

  'slider:shown': ->
    $this = $(this)
    $this.find('.form-control').focus()

  'slider:hidden': ->
    $this = $(this)
    $form = $this.find('form')
    $form.find('textarea, .btn-success').show()
    $form.find('.success-msg').remove()

, '#new-feedback-form-enc'

## apply and remove loading class between ajax requests

$(document).on(
  'ajax:send': ->
    $(this).find('[type=submit]').addClass('loading').prop('disabled', true)

  'ajax:success': ->
    $(this).find('[type=submit]').removeClass('loading').prop('disabled', false)

, '[data-remote=true]')

$(document).on 'ctab:shown', '#home_tab', ->
  $.getScript '/refresh_activity_feed'

#$(document).on 'ctab:shown', '[data-tab-href=\"/canvas\"]', ->
#  $.getScript '/canvas'