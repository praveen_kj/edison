$(document).on 'change', '.customers_select,.importance_select', ->
  $this = $(this)
  console.log 'fire'
  $form = $this.parents('form')

  [customers_want,importance] = [$form.find('.customers_select').val(), $form.find('.importance_select').val()]

  match = [customers_want, importance].join ' '

  console.log match
  if match.length is 3
    result = switch match
      when '1 1','1 2'
        'Minimal Product'
      when '1 3', '2 1', '2 2', '2 3', '3 1', '3 2', '3 3'
        'Minimum Viable Product'
      else
        'Full Featured Product'
    $responseTarget = $form.find('.classification-response')
    $responseTarget.html(result) if $responseTarget.length > 0

  return

$('.sub_component_card, .sub-component-branch').seekAndDisplay '.edit-sub-component'
$('.sub-component-branch, .issue-branch').seekAndDisplay '.del-icon'

$('.feature-title,.component-title').seekAndDisplay '.del-icon'

$(document).on 'click', '.sub-component-branch .group.tag', ->
  $(this).find('.options').fadeIn('fast')

$(document).on 'click', (e) ->
  if $(e.target).closest('.sub-component-branch .group.tag').length is 0
    $('.sub-component-branch .group.tag .options').fadeOut('fast')

$(document).on 'click', '#filter-ctrl', ->
  $(this).find('.options').fadeIn('fast')

$(document).on 'click', (e) ->
  if $(e.target).closest('#filter-ctrl').length is 0
    $('#filter-ctrl .options').fadeOut('fast')

$(document).on 'change', '.select.related-feature', ->
  $(".select.related-sub-feature").prop('disabled','disabled').addClass('loading')
  $.ajax
    url: '/features/get_sub_features'
    data: {feature_id: $(this).val()}
    method: 'post'
    dataType: 'script'