# ALL STEPS
jQuery.expr[':'].Contains = (a, i, m) ->
  jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0

jQuery.expr[':'].contains = (a, i, m) ->
  jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0


$(document).on
  focusin: ->
    $('.resource-items-container').slideDown()

  focusout: ->
    $('.resource-items-container').slideUp()

  'keypress keydown keyup': ->
    $('.resource-item').hide()
    s = ".resource-item:contains('#{$(this).val()}')"
    $(s).show()

, '[data-behaviour=show_resource_list]'


$(document).on 'click', '[data-behaviour="show_all_items_in_checklist"]', ->
  $this = $(this).hide()
  $(".checklist-for-step-#{$this.data('step')}").find('.checklist-item').show();
  $this.parent().find('[data-behaviour="hide_completed_items"]').slideDown('fast')

$(document).on 'click', '[data-behaviour="hide_completed_items"]', ->
  $this = $(this).hide()
  $ci = $(".checklist-for-step-#{$this.data('step')}").find('.checklist-item')
  $ci.slideUp('fast')
  $ci.not('.checked').first().slideDown('fast')
  $this.parent().find('[data-behaviour="show_all_items_in_checklist"]').show()




# ANALYZE STEP

$(document).on 'show.bs.modal hidden.bs.modal', '.full-page', ->
  $('body').addClass 'smoke'
  return

$(document).on 'hidden.bs.modal', '.full-page', ->
  $('body').removeClass 'smoke'
  return

$(document).on 'shown.bs.modal', '.full-page', ->
  $this = $(this)
  $this.find('.modal-dialog').css 'height', $this[0].scrollHeight
  return

$(document).on 'hidden.bs.modal', '.full-page', ->
  $this = $(this)
  $this.find('.modal-dialog').css 'height', '100%'
  return

# canvas navigation

$(document).on 'shown.bs.tab', '.canvas-tab', ->
  $this = $(this)
  $.ajax
    url: "/products/#{$this.data('productId')}"
    method: 'put'
    dataType: 'script'
    data: {product: {default_canvas: $this.data('choice')}}

# DIAGNOSE STEP

## diagnose step choices

#$(document).on
#  mouseenter: ->
#    $this = $(this)
#    $this.find('.diag-choices-enc').show()
#  mouseleave: ->
#    $this = $(this)
#    $this.find('.diag-choices-enc').hide()
#
#, '.choice-td'


# BOARD STEPS

$(document).on 'click', '[data-behaviour="open_target_in_modal"]', (e) ->
  unless $(e.target).parents('.delete-icon-form').length > 0
    $this = $(this)
    modalContent = $this.find('.in-modal-content')[0].outerHTML
    $boardModal = $($this.data('target'))
    $modalContent = $boardModal.find('.modal-content')
    $modalContent.html(modalContent)
    $boardModal.modal('show')
    initDragDrop()



$(document).on 'hidden.bs.modal', '.board-modal, .component-item-modal', ->
  $modalContent = $(this).find('.modal-content')
  #  $modalContent.css({visibility: 'hidden'})
  $inModalContent = $($modalContent.html())
  $("[data-card-id=#{$inModalContent.data('modalContentId')}]").find('.in-modal-content').replaceWith($inModalContent)


window.cardDropOptions =

  accept: '.card'
  hoverClass: 'incoming'
  addClasses: false
  drop: (event, ui) ->
    $droppable = $(this)
    $draggable = ui.draggable
    $droppable.append($draggable.css({top: '0px', left: '0px'}))

    data = {}
    object = $draggable.data('object')
    data[object] = {stage: $droppable.data('stage')}

    $.ajax(
      url: $draggable.data('url')
      data: data
      dataType: 'script'
      method: 'put'
    )

window.pieceDropOptions =

  accept: '.comp-pos-piece'
  hoverClass: 'incoming'
  addClasses: false
  drop: (event, ui) ->
    $droppable = $(this)
    $draggable = ui.draggable
    $droppable.append($draggable.css({top: '0px', left: '0px'}))


    $.ajax(
      url: $draggable.data('url')
      data: {
        comp_position: {
          factor1: $droppable.attr('data-factor-1')
          factor2: $droppable.attr('data-factor-2')
        }
      }
      dataType: 'script'
      method: 'put'
    )


window.initDragDrop = ->
  $('.card').each ->
    $(this).draggable containment: $(this).parent().parent()

  $('.board').droppable(cardDropOptions)
  $('.comp-pos-piece').draggable()
  $('.comp-pos-field').droppable(pieceDropOptions)

$ ->
  initDragDrop()


$(document).on 'ctab:shown ctab:inserted shown.bs.modal', ->
  initDragDrop()

# features

$(document).on 'click', '[data-behaviour=show_new_resource_form]', ->
  $this = $(this)
  $this.parent().find('.form-enc').show().find('.normal-input').focus()
  $this.hide()

$('.board .card').seekAndDisplay '.del-icon'

$(document).on 'click', '[data-behaviour=reschedule_milestone]', ->
  $this = $(this)
  $this.parent().find('.form-enc').show().find('.normal-input').focus()
  $this.hide()


$('.end-user-row').seekAndDisplay '.del-icon'
$('.comp-pos-piece').seekAndDisplay '.del-icon'


