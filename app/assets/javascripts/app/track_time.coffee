# when window is closed, post the time to server

$(window).on 'unload', ->
  $.ajax
    url: '/usage_durations'
    type: 'POST'
    data: usage_duration:
      time_spent: TimeMe.getTimeOnCurrentPageInSeconds()
      view: TimeMe.currentPageName


# when tab is switched record the time on the previous tab
$(document).on 'ctab:shown', '[data-tab-id], #home_tab', ->
  $this = $(this)
  TimeMe.stopTimer()
  recordTimeSpent ->
    page_name = $this.attr('data-tab-id') or $this.attr('data-tab-ref')
    TimeMe.resetAllRecordedPageTimes()
    TimeMe.setCurrentPageName page_name
    TimeMe.startTimer()
    return
  return

recordTimeSpent = (callback) ->
  timeSpentOnPage = TimeMe.getTimeOnCurrentPageInSeconds()
  page = TimeMe.currentPageName
  $.ajax(
    url: '/usage_durations'
    type: 'POST'
    data: usage_duration:
      time_spent: timeSpentOnPage
      view: page).done callback
#  console.log "#{page} -> #{timeSpentOnPage}"