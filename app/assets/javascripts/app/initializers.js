// This file contains scripts executed when the DOM is ready

var task_failure_message = {
    title: "You have to create a beachhead first",
    message: "You need to choose a beachhead before adding a task"
};

var tab_failure_message = {
    title: "You have to create a beachhead first",
    message: "You need to choose a beachhead before accessing this tab"
};


var prevent_typing_in_date = function () {
    $('.datepicker').on('keydown', function () {
        return false;
    });

};

var initialize_datepicker = function () {
    $('.datepicker').datepicker({
        format: 'dd M yyyy',
        todayHighlight: true,
        startDate: '0d'
    });

};


$(function () {

    $(".tab-list").sortable({
        appendTo: '.tab-list',
        axis: "x",
        containment: "parent"
    });

    $("[data-persona-detail-child-group=4]").sortable({
        appendTo: '[data-persona-detail-child-group=4]',
        axis: "y",
        containment: "parent",
        update: function (event, ui) {
            var $item = $(ui.item);
            var id = $item.data('personaDetailId');
            var rank =  $(ui.item).index() + 1;
            $.ajax({
                url: '/persona_details/'+id+'/rank',
                data: {persona_detail: {rank: rank}},
                method: 'put',
                dataType: 'script'
            })


        }
    });

    initialize_datepicker();

    $('.date-range').datepicker({
        format: 'dd M yyyy',
        todayHighlight: true,
        startDate: '0d',
        inputs: $('.range-date')
    });


    prevent_typing_in_date();

    $(document).on('keypress focus', 'input.active-edit-form', function () {
        $(this).autosizeInput();
    });

    $(document).on('keypress focus', 'textarea.active-edit-form', function () {
        autosize($(this));
    });


    // initialise numeric input
    $('.numeric').numeric();

    // bootstrap tooltip
    $("body").tooltip({selector: '[data-toggle="tooltip"]'});

    // specific for IE
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);


    // hide del icons

    $('.add-submit').attr('disabled', 'disabled');


});

