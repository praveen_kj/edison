(function ($) {

    // comma style

    $.fn.digits = function () {
        return this.each(function () {
            $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        })
    };


    // push contents of an array into selectors

    $.fn.updateWithArray = function (sourceArray) {
        return this.each(function (index) {
            $($(this)[0]).html(Math.round(sourceArray[index]));
        });
    };

    // get percent pattern

    $.fn.getPercentTo = function (targetSelector) {
        var sum = 0;
        var array = this.toArray();
        for (i = 0; i < array.length; i++) {
            sum += array[i]
        }
        for (i = 0; i < array.length; i++) {
            $(targetSelector[i]).html((Math.round(array[i] / sum * 100) || 0) + " %");
        }
    };

}(jQuery));

// add members of equally long arrays and output an array

var sum = function (args) {
    var sum_array = [];
    for (k = 0; k < 5; k++) {
        var output = 0;
        for (i = 0; i < arguments.length; i++) {
            output += arguments[i][k];
        }
        sum_array.push(output);
    }
    return sum_array;
};


// multiply members of equally long arrays and output an array

var product = function (args) {
    var prod_array = [];
    for (k = 0; k < 5; k++) {
        var output = 1;
        for (i = 0; i < arguments.length; i++) {
            output = output * arguments[i][k];
        }
        prod_array.push(output);
    }
    return prod_array;
};

// divide members of TWO equally long arrays and output an array

var divide = function (dividendArray, divisorArray) {
    var result_array = [];
    for (i = 0; i < dividendArray.length; i++) {
        result_array.push(Math.round(dividendArray[i] / divisorArray[i]));
    }
    return result_array;
};