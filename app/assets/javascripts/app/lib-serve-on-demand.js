
(function ($) {

    $.fn.serveOnDemand = function (callback) {

        $(document).on('click', this.selector, function () {

            var $obj = $(this);

            var status = $obj.attr('data-status');
            var href = $obj.data('href');

            if (status == 'false') {
                $.getScript(href);
            } else {
                if (typeof callback == 'function') {
                    callback.call();
                }
            }
        });

    }


}(jQuery));

