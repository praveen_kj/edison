//// FEEDBACK FORM VALIDATIONS

$(document).on('keypress keydown keyup', '#internal-feedback-form', function () {
    var save_btn = $('#internal-feedback-form .btn-success');
    var body_length = $('#internal-feedback-form .msg-field').val().length;
    if (body_length < 1) {
        save_btn.prop('disabled', 'disabled');
    } else {
        save_btn.prop('disabled', false);
    }
});

//// USER PARAMETER FORM VALIDATION

$(document).on('keypress keydown keyup change', '.user-param-form,#user_parameter_target_date', function (event) {
    var co_name = $('#user_parameter_firm_name').val();
    var title = $('#user_parameter_title').val();
    var mission = $('#user_parameter_mission').val();
    var u_name = $('#user_parameter_user_name').val();
    var target_date = $('#user_parameter_target_date').val();
    var enduser_base = $('#user_parameter_enduser_base').val();
    verify_content(co_name, title, mission, target_date, u_name, enduser_base);
});

function verify_content(co_name, title, mission, target_date, u_name, enduser_base) {

    if (typeof enduser_base == 'undefined') {
        var update_btn = $('.profile-update-btn');
        if (co_name.length > 0 && title.length > 0 && mission.length > 0 && target_date.length > 0 && u_name.length > 0) {
            update_btn.prop('disabled', false);
        } else {
            update_btn.prop('disabled', 'disabled');
        }
    }

    else {
        var update_btn = $('.profile-update-btn');
        if (co_name.length > 0 && title.length > 0 && mission.length > 0 && target_date.length > 0 && u_name.length > 0 && enduser_base.toString().length > 0) {
            update_btn.prop('disabled', false);
        } else {
            update_btn.prop('disabled', 'disabled');
        }

    }

}


//// PITCH FORM VALIDATIONS

$(document).on('keypress keydown keyup', '#pitch_pitch_content', function () {
    var save_btn = $('#pitch-form .btn');
    var length = $(this).val().length;
    if (length < 1) {
        save_btn.prop('disabled', 'disabled');
    } else {
        save_btn.prop('disabled', false);
    }
});

/// RESOURCE FORM VALIDATIONS

$('.add-submit').attr('disabled', true);

$(document).on('keypress keydown keyup', '.resource-form, .experiments-form', function () {
    var target = $(this).children('.normal-input');
    var btn = $(this).find('.add-submit');
    var disable = false;
    if (target.val() == "") {
        disable = true;
    }
    btn.attr('disabled', disable);
});

$(document).on('ajax:success', '.resource-form', function () {
    $(this)[0].reset();
    $(this).children('[type="submit"]').attr('disabled', true);
});


// NEW TASK FORM


// NEW VALIDATIONS FORM

