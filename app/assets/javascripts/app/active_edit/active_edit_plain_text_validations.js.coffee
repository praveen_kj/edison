# char restriction

$(document).on 'keydown keypress keyup focus', '.active-edit.text-field .content', (e) ->
  $this       = $(this)
  $activeEdit = $this.parent()
  object_name = $activeEdit.attr('data-object-name')
  field       = $activeEdit.attr('data-edit-field')

  max_chars   =
  if ActiveEdit.length_validations[object_name] and ActiveEdit.length_validations[object_name][field]
    ActiveEdit.length_validations[object_name][field]['maximum']
  else
    $this.attr('data-char-limit')

  if e.keyCode != 27 and max_chars isnt undefined
    $body       = $('.app-restricted-container')
    length      = if $this.hasClass('placeholder') then 0 else $this.length

    remaining = max_chars - length
    remaining_container = $body.find('#ae_char_remaining')

    if remaining_container.length > 0
      remaining_container.html remaining
    else
      $body.append '<div id="ae_char_remaining">' + remaining + '</div>'

    arrow_key_pressed = $.inArray(e.keyCode, [
      37
      38
      39
      40
    ]) >= 0

    if e.which < 32 or arrow_key_pressed
      return
    if length == max_chars
      e.preventDefault()
    else if length > max_chars
      $(this).html $(this).substring(0, max_chars)
  return


$(document).on 'blur', '.active-edit.text-field .content', (e) ->
  $this               = $(this)
  $body               = $('.app-restricted-container')
  remaining_container = $body.find('#ae_char_remaining')
  remaining_container.remove()
  return

