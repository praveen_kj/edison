$(document).on 'click', '[data-modal=true]', ->
  $this = $(this)
  $modal = $('.bip-view')
  char_limit = $this.find('.content').attr('data-char-limit')
  text_field = $(element_tag('textarea', '', 'class="active-edit-form" rows="1" data-parent=#' + $this.attr('id') + ' maxlength=' + char_limit)).val($this.attr('data-prev-value'))
  text_field = text_field.attr('rows', 4).addClass('on-modal')
  $modal.find('.modal-title').html $this.attr('data-modal-title')
  $modal.find('.modal-text').html $this.attr('data-modal-content')
  $modal.find('.modal-edit-enc').append text_field
  $modal.modal 'show'


## flush modal on hiding

$(document).on 'hidden.bs.modal', '.bip-view', ->
  $this = $(this)
  $this.find('.modal-title').html ''
  $this.find('.modal-text').html ''
  $this.find('.modal-edit-enc').html ''

## show or hide readable content based on value

$(document).on 'show.bs.modal', '.bip-view', ->
  $this = $(this)
  #  if $this.find('.active-edit-form').val() is ''
  #    $this.find('.modal-edit-enc').hide()
  #    $this.find('.modal-close').hide ''
  #    $this.find('.modal-got-it').show ''
  #  else
  $this.find('.modal-edit-enc').show()
  $this.find('.modal-close').show ''
  $this.find('.modal-got-it').hide ''

## got it click handler
#
#$(document).on 'click', '.modal-got-it', ->
#  $this = $(this)
#  $modal = $('.bip-view')
#  $modal.find('.modal-edit-enc').show().focus()
#  $modal.find('.modal-close').show ''
#  $this.hide()
