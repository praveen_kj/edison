$(document).on 'click', '.active-edit.check_box', ->
  $activeEdit = $(this)
  url = $activeEdit.attr('data-edit-url')
  objectName = $activeEdit.attr('data-object-name')
  field = $activeEdit.attr('data-edit-field')
  value = if $activeEdit.hasClass('checked') then false else true
  content = {}
  subcontent = {}
  subcontent[field] = value
  content[objectName] = subcontent

  $.ajax
    url: url,
    type: 'PUT',
    data: content,
    dataType: 'script'

  $activeEdit.toggleClass('checked')
