activeEditHelpers.parameterize = (dataObject) ->
  Object.keys(dataObject).map((k) ->
    encodeURIComponent(k) + '=' + encodeURIComponent(dataObject[k])
  ).join('&')