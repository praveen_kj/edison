window.activeEditHelpers = {}

window.ActiveEdit =

  permittedTypes: ['dropdown', 'date', 'text-field']

  dropdown: ($target, response) ->
    result = $target.find("[data-option-value=#{response}]").html()
    $target.find('.button-name').html(result)

  date: ($target, response) ->
    long_date = longDate(response)
    $target.find('.content').removeClass('placeholder').html(long_date)

  'text-field': ($target, response) ->
    @updateContent($target, response)



  insertForm: ($activeEdit) ->
    # hide the content and add 'editing' class
    $activeEdit.addClass('editing') #.find('.content').hide()

    # check for exisitng form
    $formEnc = $activeEdit.find('.active-edit-form-enc')

    if $formEnc.length is 1

      # show existing form if available

      $formEnc.show()
    else


      objectName = $activeEdit.attr('data-object-name')
      field = $activeEdit.attr('data-edit-field')

      maxChars =
        if ActiveEdit.length_validations[objectName] and ActiveEdit.length_validations[objectName][field]
          ActiveEdit.length_validations[objectName][field]['maximum']
        else
          $activeEdit.attr('data-char-limit')

      $formContainer = $("<div class=\"active-edit-form-enc\" id=\"form_for_#{$activeEdit.data('uniqueSignature')}\"></div>")
      $form = $("<textarea></textarea>").attr(
        'data-parent': "[data-edit-url=\'#{$activeEdit.data('editUrl')}'][data-edit-field=#{$activeEdit.data('editField')}]"
        class: 'active-edit-form'
        rows: 1
        maxlength: maxChars
        placeholder: $activeEdit.data('placeholder')
      ).val($activeEdit.attr('data-prev-value'))

      $formContainer.append($form)
      $formContainer.append('' +
          '<div class="btn-container">' +
          '<div data-behaviour="complete-edit" class="btn btn-success btn-xs">Save</div>' +
          '<div data-behaviour="cancel-edit" class="transparent-btn">×</div>' +
          '</div>')

      $formContainer.css
        position: 'absolute'
        top: 0
        left: 0
        width: "#{$activeEdit.width()}px"
        'min-width': '100px'
        'max-width': '600px'

      $activeEdit.append($formContainer)
      $form.focus()

  removeForm: ($activeEdit) ->
    $activeEdit.removeClass('editing')
    $activeEdit.find('.active-edit-form-enc').remove()
    $activeEdit.find('.content').show()

  applyStyle: ($activeEdit, value) ->
    prefix  = $activeEdit.attr('data-prefix') or ''
    suffix  = $activeEdit.attr('data-suffix') or ''

    if $activeEdit.hasClass('numeric')
      return prefix + number_to_delimiter(value) + suffix
    else
      return value

  updateContent: ($activeEdit, value) ->
    if value is ''
      $activeEdit.find('.content').html($activeEdit.attr('data-placeholder')).addClass('placeholder')
    else
      $activeEdit.find('.content').html(@applyStyle($activeEdit, value)).removeClass('placeholder')


  handleValue: ($activeEdit, value) ->
    value                 = if $activeEdit.hasClass('numeric') and value isnt '' then parseFloat(value) else value
    validation_result     = true
    object_name           = $activeEdit.attr('data-object-name')
    field                 = $activeEdit.attr('data-edit-field')
    old_value             = $activeEdit.attr('data-prev-value')
    url                   = $activeEdit.attr('data-edit-url')
    content               = {}
    subcontent            = {}
    subcontent[field]     = value
    content[object_name]  = subcontent
#    console.log "#####{url}####"
    validation_result     = validate_number(value, object_name, field) if $activeEdit.hasClass('numeric')

    if validation_result and value isnt old_value
      $.ajax
        url: url
        type: 'PUT'
        data: content
        dataType: 'script'
      @updateContent($activeEdit, value)
      $activeEdit.attr 'data-prev-value', value
    else
      @updateContent($activeEdit, old_value)


  submitForm: ($form) ->
    $activeEdit = $($form.data('parent'))
    $activeEdit.removeClass('editing')
    @handleValue($activeEdit, $form.val())
    @removeForm($activeEdit)

  hideForm: ($form) ->
    $activeEdit = $($form.data('parent'))
    $activeEdit.removeClass('editing')
    $activeEdit.find('.active-edit-form-enc').hide()
    $activeEdit.find('.content').show()


