# dropdownn caret and hide and seek
#$(document).on {
#  mouseenter: ->
#    $(this).find('.caret').css 'display', 'inline-block'
#    return
#  mouseleave: ->
#    $(this).find('.caret').css 'display', 'none'
#    return
#
#}, '.active-edit-dd-btn'

# Choice Select Dropdown
$(document).on 'click', '.active-edit .option', ->
  $this = $(this)
  if $this.hasClass('disabled')
    return false
  else
    $action_edit = $("[id=#{$this.attr('data-parent')}]")
    url = $action_edit.attr('data-edit-url')
    object_name = $action_edit.attr('data-object-name')
    field = $action_edit.attr('data-edit-field')
    value = $this.attr('data-option-value')
    content = {}
    subcontent = {}
    subcontent[field] = value
    content[object_name] = subcontent
    $.ajax
      url: url
      type: 'PUT'
      data: content
      dataType: 'script'
    $action_edit.find('.button-name').removeClass('placeholder').html $this.text()
  return
