# Date Select Modal Opening

$(document).on 'click', '.active-edit.date-select', ->
  $this = $(this)
  $this.find('.content').hide()

  format = $this.data('format') or 'dd M yyyy'

  prev_value = if $this.attr('data-prev-value') then longDate($this.attr('data-prev-value')) else ''

  text_field = $('<input>').attr(
    'data-date-format': format
    'class': 'active-edit-date-form datepicker'
    'data-parent': "[data-edit-url=\"#{$this.data('editUrl')}\"][data-edit-field=#{$this.data('editField')}]").val prev_value

  if !$this.hasClass('hasForm')
    $this.addClass 'hasForm'
    $this.append text_field
  text_field.datepicker({todayHighlight: true}).focus()
  prevent_typing_in_date()


$(document).on 'hide', '.active-edit-date-form', ->
  $this = $(this)
  $activeEdit= $($this.attr('data-parent'))
  $activeEdit.find('.content').show()
  $activeEdit.removeClass 'hasForm'
  $this.remove()


$(document).on {
  change: ->
    putAsync $(this)

  keydown: (event)->
    if event.keyCode == 27
      $this = $(this)
      $activeEdit = $($this.attr('data-parent'))
      $activeEdit.removeClass 'hasForm'
      $activeEdit.find('.content').show()
      $this.remove()

}, '.active-edit-date-form'


putAsync = ($form) ->
  $activeEdit = $($form.data 'parent')
  ActiveEdit.handleValue $activeEdit, $form.val()
  $activeEdit.removeClass 'hasForm'