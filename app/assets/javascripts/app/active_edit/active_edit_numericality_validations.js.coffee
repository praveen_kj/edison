## numericality validations notifier

$(document).on 'keydown keypress keyup focus blur', '.active-edit.numeric .content', (e) ->

  $this         = $(this)
  $activeEdit   = $this.parent()
  object_name   = $activeEdit.attr 'data-object-name'
  field         = $activeEdit.attr 'data-edit-field'
  content       = $this.text()

  if ActiveEdit.numericality_validations[object_name] and ActiveEdit.numericality_validations[object_name][field]
    maximum = ActiveEdit.numericality_validations[object_name][field]['maximum']
    minimum = ActiveEdit.numericality_validations[object_name][field]['minimum']



  if maximum? and minimum? and content isnt ""

    message = "<div class=\"active-edit-error-message\">The value must be within #{number_to_human minimum} and #{number_to_human maximum}</div>"

    unless minimum <= parseInt(content) <= maximum
      $activeEdit.append message unless $activeEdit.find('.active-edit-error-message').length > 0
    else
      $activeEdit.find('.active-edit-error-message').remove()

    $activeEdit.find('.active-edit-error-message').remove() if e.type == 'focusout'

  return



window.validate_number = (number, object_name, field) ->

  maximum       = undefined
  minimum       = undefined

  if ActiveEdit.numericality_validations[object_name] and ActiveEdit.numericality_validations[object_name][field]
    maximum = ActiveEdit.numericality_validations[object_name][field]['maximum']
    minimum = ActiveEdit.numericality_validations[object_name][field]['minimum']

  return true if (minimum is undefined) or (maximum is undefined) or (number is '')

  unless minimum <= parseInt(number) <= maximum
    return false
  else
    return true


window.number_to_human = (number) ->

  billion = 1000000000
  million = 1000000

  if (number / billion) >= 1
    return "#{Math.round(number / billion)} billion"
  else if (number / million) >= 1
    return "#{Math.round(number / million)} million"
  else
    return number_to_delimiter number