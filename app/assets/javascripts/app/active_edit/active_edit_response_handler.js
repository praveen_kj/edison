(function ($) {

    $.fn.activeUpdate = function (response) {
        var $target = $(this);
        var dataType = $target.attr('data-type');

        if ($.inArray(dataType, ActiveEdit.permittedTypes) >= 0) {
            ActiveEdit[dataType]($target, response)
        } else {
            //console.log('Not found')
        }
    };

}(jQuery));
