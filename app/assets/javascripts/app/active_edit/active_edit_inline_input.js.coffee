## active edit click activation

$(document).on 'click', '.active-edit.text-field, .active-edit.numeric', (e) ->
  $this = $(this)
  $activeEdit = $('.active-edit')
  $activeEdit.find('.active-edit-form-enc').hide()
  $activeEdit.find('.content').show()
  unless $this.data('modal') or $(e.target).is('[data-behaviour=complete-edit], [data-behaviour=cancel-edit]')
    ActiveEdit.insertForm($this)



$(document).on {
  keydown: (event)->
    if event.keyCode == 27
      ActiveEdit.hideForm $(this)

}, '.active-edit-form'



$(document).on 'click', '[data-behaviour=complete-edit]', (e)->
  $form = $(this).parents('.active-edit-form-enc').find('.active-edit-form')
  ActiveEdit.submitForm $form


$(document).on 'click', '[data-behaviour=cancel-edit]', ->
  $activeEdit = $(this).parents('.active-edit')
#  console.log 'canceling edit'
  ActiveEdit.removeForm $activeEdit