window.edison = {

  tabCloseButton: """<button type=\"button\" class=\"tab-close\" data-dismiss=\"tab\">
                      <span aria-hidden=\"true\">×</span>
                   </button>"""

  tabItem: (tabHref, tabName, uniqId) =>
    $tab = $("<li class=\"tab-item active row\">#{edison.tabCloseButton}<span class=\"inner\">#{tabName}</span></li>")
    $tab.attr('data-tab-href', tabHref)
    $tab.attr('data-tab-id', uniqId)
    $tab.attr('data-behaviour', 'show_tab')
    return $tab

  tabView: (tabHref, content, uniqId) =>
    $tabView = $("<li class=\"view-layer-item\">#{content}</li>")
    $tabView.attr('data-tab-view-href', tabHref)
    $tabView.attr('data-tab-view-id', uniqId)
    return $tabView

  insertTab: (tabName, content, href) ->
    uniqId = "#{Math.floor(Math.random() * 100000)}"
    $tab = @tabItem(href, tabName, uniqId)
    $view = @tabView(href, content, uniqId)
    $tab.insertBefore('.new-task-header-btn')
    $('.tab-view-layer').append($view)
    @showTab(uniqId, by: 'id', skipEvent: true)
    $tab.trigger('ctab:inserted')
    return uniqId

  showTab: (param, options = {}) ->
    $('.tab-item').removeClass('active')
    $('.view-layer-item').hide()
    $("[data-tab-view-#{options['by']}=\"#{param}\"]").show()
    $tab = $("[data-tab-#{options['by']}=\"#{param}\"]").show().addClass('active')
    $tab.trigger('ctab:shown') unless options['skipEvent']

  selectTabView: (param, options = {}) ->
    return $("[data-tab-view-#{options['by']}=\"#{param}\"]")

  resizeTabs: (options = {}) ->
    totalTabCount = $('.tab-item:visible').length
    totalWidth = if options.tabHover then 750 else 900
    if totalTabCount > 6
      width = Math.floor(totalWidth / if options.tabHover then (totalTabCount - 1) else totalTabCount)
      $('.tab-item').css('width', width)
    else
      $('.tab-item').css('width', 150)

  hasContent: (el) ->
    el.text().trim().length > 0

  emptyTab: ($tab) ->
    id = $tab.data('tabId')
    $("[data-tab-id=#{id}]").remove()
    $("[data-tab-view-id=#{id}]").remove()

  emptyAllTabs: () ->
    $('[data-tab-href]').each ->
      $this = $(this)
      edison.emptyTab($this) unless $this.data('tabHref') is '/markets'

}


$(document).on ' ctab:hidden', '.tab-list', ->
  edison.resizeTabs()


$(document).on {
  mouseenter: ->
    $this = $(this)
    unless $this.attr('id') is 'home_tab'
      edison.resizeTabs(tabHover: true)
      $this.css 'width', 150
  mouseleave: ->
    edison.resizeTabs()

}, '.tab-item'


# tab switching

$(document).on 'click', '[data-behaviour=show_tab]', ->
  $this = $(this)
  href = $this.data('tabHref')
  id = $this.data('tabId')
  $('title').html "#{$this.find('.inner').html()} | Edison"
  edison.showTab(id, by: 'id')
  Router.navigate(href)


# tab switching

$(document).on 'click', '[data-behaviour=open_in_new_tab]', ->
  $this = $(this)
  href = $this.data('href')
  $view = $("[data-tab-view-href=\"#{href}\"]")

  view_available = $view.length > 0
  content_available = $view.text().trim().length > 0

  if content_available
    edison.showTab(href, by: 'href')
  else
    edison.showTab(href) if view_available
    $.getScript href
    nanobar.inchForward()

  Router.navigate(href)


$(document).on 'click', '[data-dismiss=tab]', (e) ->
  e.stopPropagation()

  # hide the tab and view
  $tab = $(this).parent('li')
  $view = $("[data-tab-view-href=\"#{$tab.data('tabHref')}\"]")
  $tab.hide()
  $view.hide()

  $('.tab-list').trigger('ctab:hidden')

  # show preceding tab or the home tab
  precedingVisibleTab = $tab.prevAll(':visible:first')
  if precedingVisibleTab.length > 0
    precedingVisibleTab.trigger 'click'
  else
    $('#home_tab').trigger 'click'


$(document).on 'ctab:shown', (e) ->
  edison.resizeTabs()


$ -> $('.tab-item').seekAndDisplay('.tab-close')
