$(document).on 'click', '[data-behaviour=toggle_target]', ->
  $this = $(this)

  shownText = $this.data('shownText')
  hiddenText = $this.data('hiddenText')

  $target = $($this.data('target'))

  if $target.is(':visible')
    $target.slideUp 'fast', ->
      $this.html(hiddenText) if shownText? && hiddenText?
  else
    $target.slideDown 'fast', ->
      $this.html(shownText) if shownText? && hiddenText?
