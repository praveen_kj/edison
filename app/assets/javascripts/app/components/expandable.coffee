$(document).on 'focus', '.expand-on-focus', ->
  $this = $(this)
  $this.parents('[data-behaviour=expandable]').addClass('expanded')

$(document).on 'click', '.expand-on-click', ->
  $this = $(this)
  $this.parents('[data-behaviour=expandable]').addClass('expanded').find('input:visible, trix-editor:visible').first().focus()

$(document).on 'click', '.collapse-target', ->
  $this = $(this)
  $this.parents('[data-behaviour=expandable]').removeClass('expanded')
