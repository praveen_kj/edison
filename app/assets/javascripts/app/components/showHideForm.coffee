
$(document).on 'click', '[data-behaviour=show_form]', ->
  $this = $(this)
  $this.parent().find('.resource-form').show().find('.normal-input').focus()
  $this.hide()

$(document).on 'click', '[data-behaviour=hide_form]', ->
  $this = $(this)
  $parent = $this.parents('.form-container')
  $parent.find('.resource-form').hide()
  $parent.find('[data-behaviour=show_form]').show()