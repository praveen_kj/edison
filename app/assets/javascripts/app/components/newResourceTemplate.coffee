toolData =
  'to-do-list':
    name: 'New To Do List'
    href: '/to-do-lists/new'
    contentSelector: '#new_todo_list_template'
  'note':
    name: 'New Note'
    href: '/notes/new'
    contentSelector: '#new_note_template'
  'discussion':
    name: 'New Discussion'
    href: '/discussions/new'
    contentSelector: '#new_discussion_form_template'
  'insight':
    name: 'New Customer Insight'
    href: '/customer_feedbacks/new'
    contentSelector: '#new_feedback_template'


$(document).on 'click', '[data-behaviour="clone_target_in_new_tab"]', ->
  $this = $(this)
  toolType = $this.data('toolType')

  $tabContent = $(toolData[toolType]['contentSelector']).clone()
  tabHref = toolData[toolType]['href']

  tabId = edison.insertTab(toolData[toolType]['name'], $tabContent.html(), tabHref)
  edison.showTab(tabId, by: 'id')

  $form = edison.selectTabView(tabId, by: 'id').find('form')
  $form.find('#rand_tab_id').remove()
  $form.append('<input id="rand_tab_id" name="rand_tab_id" type="hidden" value=' + tabId + '>')
  $tER = $form.find('.trix-editor-reciever')
  $tES = $form.find('.trix-editor-sender')

  if $tER.length > 0 and $tES.length > 0
    rand = "#{Math.floor(Math.random() * 100000)}"
    $tER.attr('id', "trix-input-from-#{rand}")
    $tES.attr(id: "trix-input-to-#{rand}", input: "trix-input-from-#{rand}")

  $("[data-tab-view-id=#{tabId}]").find('input').first().focus()
  Router.navigate(tabHref)
