window.sliderUp =
  show: (selector)->
    sliderUp.hide()
    $('.tab-view-layer').slideUp 'fast', ->
      $(selector).show().trigger('slider:shown')

  hide: (selector)->
    if selector?
      $('.tab-view-layer').slideDown 'fast', ->
        $(selector).hide().trigger('slider:hidden')
    else
      $('.tab-view-layer').slideDown 'fast', ->
        $('.new-sub-tool-form-enc').hide().trigger('slider:hidden')

$(document).on 'click', '[data-behaviour=slider_up]', ->
  sliderUp.show($(this).data('target'))

$(document).on 'click', '[data-dismiss=slider]', ->
  sliderUp.hide('.new-sub-tool-form-enc')


window.scrollToTarget = ($target) ->
  $target.animate({
    scrollTop: 0
  }, {
    duration: 100,
    complete: ->
      $(".html, body").animate({
        scrollTop: 0
      }, 0)
  })


$(document).on 'ctab:inserted', ->
  scrollToTarget $(".app-workspace")