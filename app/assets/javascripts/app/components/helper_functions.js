// create a html element

var element_tag = function (tag, content, attrs) {
    attrs = attrs || '';
    return '<' + tag + ' ' + attrs + '>' + content + '</' + tag + '>';
};

// convert date to dd-mmm-yyyy format

var longDate = function (_date) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date = new Date(_date);
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    return (day + ' ' + months[month] + ' ' + year);
};

// convert date to yyyy-mm-dd format

var euroDate = function (_date) {
    var date = new Date(_date);
    var day = date.getDate().toString();
    var month = (date.getMonth() + 1).toString();
    var year = date.getFullYear();

    day = day.length == '1' ? '0' + day : day;
    month = month.length == '1' ? '0' + month : month;

    return (year + '-' + month + '-' + day);
};

// number with delimiter

var number_to_delimiter = function (number) {
    var string = number.toString();
    return string.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
};