options =
  bg: 'white'
  id: 'mynano'

window.nanobar = {}

$ ->
  nanobar.loader = new Nanobar(options)
  return


nanobar.inchForward = (oldValue = 60) ->
  nanobar.loader.go oldValue
#  increment = target / 20
#  slowProgress(target, 0, increment)

slowProgress = (target, progressedUpto, increment) ->
  if target > progressedUpto
    nanobar.loader.go progressedUpto
    setTimeout(->
      slowProgress(target, (progressedUpto + increment), increment)
    , 200)

