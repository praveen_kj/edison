window.edisonFilter =

  groupFilters: ['minimal-product', 'mvp', 'full-featured-product', 'uncategorized']

  stageFilters: ['idea', 'planned', 'under-implementation', 'implemented']


  currentStageFilters: []

  currentGroupFilters: []

  addFilter: (criterion, type) ->
    if type is 'stage'
      array = @currentStageFilters
      array.push criterion
      @currentStageFilters = _.uniq array
    else
      array = @currentGroupFilters
      array.push criterion
      @currentGroupFilters = _.uniq array


  removeFilter: (criterion, type) ->
    if type is 'stage'
      @currentStageFilters = _.without @currentStageFilters, criterion
    else
      @currentGroupFilters = _.without @currentGroupFilters, criterion

  extractSelector: (type) ->
    selectorArray = if type is 'stage'
      _.map @currentStageFilters, (filter) ->
        ".#{filter}"
    else
      _.map @currentGroupFilters, (filter) ->
        ".#{filter}"
    selectorArray.join(', ')


  applyFilter: () ->
    $subComponentBranch = $('.sub-component-branch, .issue-branch')
    $subComponentBranch.hide()
    stageSelector = @extractSelector('stage')
    groupSelector = @extractSelector('group')

    if @currentStageFilters.length is 0 and @currentGroupFilters.length is 0
      $subComponentBranch.show()
    else
      $subComponentBranch.each ->
        $this = $(this)

        cond = if stageSelector is ""
          $this.is(groupSelector)
        else if groupSelector is ""
          $this.is(stageSelector)
        else
          $this.is(stageSelector) and $this.is(groupSelector)

        $this.show() if cond

    $(".component-main-branch").each ->
      $this = $(this).show()
      $this.hide() if $this.find('.sub-component-branch:visible, .issue-branch:visible').length is 0


## test code

# e = edisonFilter
# e.currentStageFilters # should produce empty array
# e.addFilter('mvp', 'stage')
# e.currentStageFilters # should have mvp
# e.addFilter('minimal', 'stage')
# e.currentStageFilters # should have minimal
# e.addFilter('minimal', 'stage')
# e.currentStageFilters # should only have one minimal
# e.addFilter('idea', 'group')
# e.currentGroupFilters # should have idea
# e.currentStageFilters # should have the old stuff
# e.removeFilter('mvp', 'stage')
# e.currentStageFilters # mvp should be gone
# e.removeFilter('xx', 'stage')
# e.currentStageFilters # no change from prev
# e.extractSelector('stage') # should result in js selector string
# e.applyFilter()



$(document).on 'click', '[data-filter-action]', ->
  $this = $(this)
  if $this.data('filterAction') is 'apply'
    edisonFilter.addFilter($this.data('filter'), $this.data('filterType'))
    $this.parent().addClass('active')
  else
    edisonFilter.removeFilter($this.data('filter'), $this.data('filterType'))
    $this.parent().removeClass('active')
  edisonFilter.applyFilter()
  f = edisonFilter.currentStageFilters
  if f.length is 0 or f.length is 4
    $('.active-filter-desc').html('all')
  else
    $('.active-filter-desc').html(edisonFilter.currentStageFilters.join(', '))
