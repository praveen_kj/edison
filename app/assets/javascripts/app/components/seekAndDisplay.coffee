(($) ->
  $.fn.seekAndDisplay = (targetSelector) ->
    $(document).on(
      mouseenter: ->
        $(this).find(targetSelector).css('display', 'initial');

      mouseleave: ->
        $(this).find(targetSelector).css('display', 'none');

    , this.selector)

  return) jQuery
