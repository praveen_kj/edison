
$(document).on 'click', '[instant-select=true]', ->
  $this = $(this)
  target = $this.attr('is-sibling')
  $("[is-sibling=#{target}]").removeClass('selected')
  $this.addClass('selected')