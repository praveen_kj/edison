(($) ->

  $.toggleEdit = (targetParentSelector) ->
    $(document).on 'click', '[data-behaviour=toggle_edit]', ->
      $this = $(this)
      $this.parents(targetParentSelector).toggleClass('editing').find('edit-state').find('.active-edit').first().trigger('click')
    return

  return
) jQuery


$.toggleEdit('.sub_component_card, .sub-component-branch')



