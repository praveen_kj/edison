#$(document).on 'keydown', 'trix-editor', (e) ->
#  $this = $(this)
#  trix = $this[0].editor
#  selection = window.getSelection()
#
#  currentSentence = selection.focusNode
#  last3Typed = $(currentSentence).text().substr(-3)
#  currPos = trix.getSelectedRange()[0]
#
#  switch last3Typed.trim()
#    when '1. '.trim()
#      unless trix.attributeIsActive('number') or trix.attributeIsActive('bullet')
#        trix.composition.insertLineBreak()
#        trix.setSelectedRange([Math.max(currPos - 3, 0), currPos])
#        trix.deleteInDirection('forward')
#        trix.activateAttribute('number')
#    when '* '.trim(), '- '.trim()
#      unless trix.attributeIsActive('number') or trix.attributeIsActive('bullet')
#        trix.composition.insertLineBreak()
#        trix.setSelectedRange([Math.max(currPos - 3, 0), currPos])
#        trix.deleteInDirection('forward')
#        trix.activateAttribute('bullet')

$(document).on 'paste', 'trix-editor', (e)->
  console.log $(e.target).parents('trix-editor')
  trix = $(e.data)[0].editor
  pastedData = e.originalEvent.clipboardData.getData('text')
  regex = /(https?:\/\/.*\.(?:png|jpg))/i
  if pastedData.search "www.dropbox.com" > 0
    pastedData = pastedData.replace('www.dropbox', 'dl.dropboxusercontent')
  if regex.test(pastedData)
    trix.setSelectedRange([trix.getSelectedRange()[0] - pastedData.length, trix.getSelectedRange()[0]])
    trix.deleteInDirection('forward')
    trix.insertHTML("<img src=\"#{pastedData}\" / >")
# remote text and replace with img tag


$(document).on 'trix-selection-change', 'trix-editor', ->
  $this = $(this)
  lineHeight = parseInt $this.css('line-height')
  t = $this[0].editor
  $toolbar = $("##{$this.attr('toolbar')}")
  if t.selectionManager.selectionIsExpanded()
    rect = window.getSelection().getRangeAt(0).getClientRects()[0]
    [left, top] = [rect.left, rect.top]
    $toolbar.show().css({top: "#{top - (lineHeight * 2)}px", left: "#{left - 20}px"})
  else
    $toolbar.hide()
