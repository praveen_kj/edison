(function ($) {

    // restrict to only number input

    $.fn.numeric = function () {
        return $(document).on('keydown', this.selector ,function (e) {
            // Allow: backspace, delete, tab, escape and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                    // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

    };


    $.fn.insertCount = function () {
        var $items = $(this).find('.list-count');
        $items.each(function (index) {
            $(this).html('#' + (index + 1));
        });
    };

    $.fn.spotLight = function (opts) {
        $(document).on('click', this.selector, function () {
            var $this = $(this);
            var target = $this.attr('data-target');
            var targetClass = $this.attr('data-target-class');
            $(targetClass).hide().removeClass('active');
            $(target).show().addClass('active');
            if (typeof opts.callback == 'function') {
                opts.callback.call(this);
            }
            $this.addClass('active');
            $this.trigger('spotlight:cast')
        });
    };


    $.fn.validateLength = function (length) {
        return $(this.selector).val().length > length;
    };

    $.fn.validateFormat = function () {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test($(this.selector).val());
    };

    $.fn.validateUniqueness = function () {
        var email = $(this.selector).val();
        $.post("/email_validate", {email: email}, function (data) {
            email_uniqueness_cond = data.status;
        });
    };

}(jQuery));

