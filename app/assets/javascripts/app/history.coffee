window.Router =

  navigate: (href, options = {}) ->
    if options['overwrite']?
      window.history.replaceState('null', 'null', href);
    else
      window.history.pushState('null', 'null', href)


$(window).bind 'popstate', ->
  href = location.pathname
  if edison.selectTabView(href, by: 'href').length > 0
    edison.showTab href, by: 'href'
  else
    $.getScript href
    nanobar.inchForward()



