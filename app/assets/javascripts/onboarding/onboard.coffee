window.scrollToStage = (stage)->
  $('html, body').animate({
    scrollTop: $("[data-stage=#{stage}]").find('path').first().offset().top
  }, 300);


massCreateFormField = "<li><input class=\"list-input\" id=\"business_business_name\" name=\"business[business_name#{Math.random().toString(36).substr(2, 5)}]\" type=\"text\" value=\"\"></li>"


$(document).on "keypress", ".list-input", (event) ->
  if event.keyCode is 13
    return false



$(document).on 'click', '[data-behaviour=include_field]', ->
  $list = $('#multi-market-form').find('.input-list')
  $list.append(massCreateFormField) if $list.find('li').length < 9
  if $list.find('li').length == 8
    $(this).hide()


$(document).on 'click', '[data-behaviour=rank_priority]', ->
  $this = $(this)
  id = $this.data('objectId')
  rank = $this.data('rank')

  $.ajax
    url: "/user_priorities/rank"
    data: {user_priority: {rank: rank, id: id}}
    method: 'put'
    dataType: 'script'

  $("#dd-priority-at-rank-#{rank}").html($this.data('priority'))


$(document).on 'click', '[data-behaviour=choose_business_model]', ->
  $this = $(this)
  bmId = $this.data('bmId')
  bhId = $this.data('bhId')
  $('.modal').modal('hide');
  $.ajax
    url: "/businesses/#{bhId}"
    data: {business: {chosen_business_model_id: bmId}}
    method: 'put'
    dataType: 'script'