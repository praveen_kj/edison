window.edisonValidator =

  emailFormat: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i

  validateForm: (selector, opts = {}) ->
    $form = $(selector)
    $button = $form.find(opts['buttonSelector'])
    $mandatoryFields = $form.find(opts['presenceValidationsOn'])

    results =

    $mandatoryFields.each ->
      selector = $(this).selector
      edisonValidator.testLength selector, minLength: 1

## incomplete

  testLength: (selector, opts = {}) ->
    text = $(selector).text() or $(selector).val()
    maxLength = opts['maxLength']
    minLength = opts['minLength']
    successCallback = opts['successCallback']
    failureCallback = opts['failureCallback']
    if (maxLength? and text.length > maxLength) or (minLength? and text.length < minLength)
      failureCallback.call() if typeof successCallback is 'function'
      return false
    else
      successCallback.call() if typeof failureCallback is 'function'
      return true

  testEmailFormat: (selector, opts = {}) ->
    email = $(selector).text() or $(selector).val()
    result = edisonValidator.emailFormat.test(email)
    successCallback = opts['successCallback']
    failureCallback = opts['failureCallback']
    if result
      successCallback.call(result) if typeof successCallback is 'function'
    else
      failureCallback.call(result) if typeof successCallback is 'function'
    return result

  testUniqueEmail: (selector, opts = {}) ->
    email = $(selector).text() or $(selector).val()
    url = opts['url']
    successCallback = opts['successCallback'] or -> return true
    failureCallback = opts['failureCallback'] or -> return false
    result = $.post opts['url'], {email: email}, (data) ->
      if data.status
        successCallback.call() if typeof successCallback is 'function'
      else
        failureCallback.call() if typeof failureCallback is 'function'