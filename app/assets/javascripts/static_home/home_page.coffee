#// SIGN UP FORM VALIDATIONS
window.user =
  validFullName: undefined
  validEmailFormat: undefined
  uniqueEmail: undefined
  passwordLength: undefined

# FORM BUTTON CHECK

buttonCheck = ->
  if user.validFullName and user.validEmailFormat and user.uniqueEmail and user.passwordLength
    $('.signup-btn').prop 'disabled', false
  else
    $('.signup-btn').prop 'disabled', 'disabled'
  return

$(document).on 'blur', '#user_email', ->
  edisonValidator.testEmailFormat '#user_email',
    failureCallback: ->
      user.validEmailFormat = false
      $('#user_email_error').html '<div class="user-field-error">The email you entered is invalid</div>'
      buttonCheck()
    successCallback: ->
      $('#user_email_error').empty()
      user.validEmailFormat = true
      edisonValidator.testUniqueEmail '#user_email',
        url: '/email_validate',
        successCallback: ->
          user.uniqueEmail = true
          $('#user_email_error').empty()
          buttonCheck()
        failureCallback: ->
          $('#user_email_error').html '<div class="user-field-error">The email you entered is taken</div>'
          user.uniqueEmail = false
          buttonCheck()

# PASSWORD
$(document).on 'keypress keydown keyup', '#user_password', ->
  edisonValidator.testLength '#user_password',
    minLength: 8
    successCallback: ->
      $('#pw_error').empty()
      user.passwordLength = true
      buttonCheck()
      return
    failureCallback: ->
      $('#pw_error').html '<div class="user-field-error">Password must be at least 8 characters long</div>'
      user.passwordLength = false
      buttonCheck()
  return

# user name
$(document).on 'keypress keydown keyup', '#user_username', ->
  edisonValidator.testLength '#user_username',
    minLength: 1
    successCallback: ->
      $('#user_full_name_error').empty()
      user.validFullName = true
      buttonCheck()
    failureCallback: ->
      $('#user_full_name_error').html '<div class="user-field-error">This field is mandatory</div>'
      user.validFullName = false
      buttonCheck()
  return


# product name
$(document).on 'keypress keydown keyup', '#user_product_name', ->
  edisonValidator.testLength '#user_product_name',
    minLength: 1
    successCallback: ->
      $('#user_product_error').empty()
      user.validProductName = true
      buttonCheck()
    failureCallback: ->
      $('#user_product_error').html '<div class="user-field-error">This field is mandatory</div>'
      user.validProductName = false
      buttonCheck()
  return




#// PASSWORD RESET EMAIL VALIDATION
$(document).on 'submit', '.reset-pw-form', (event) ->
  email = $('.reset-pw-email').val()
  verify_email email
  false


# FEEDBACK FORM VALIDATIONS
$(document).on 'keypress keydown keyup', '#external-feedback-form', ->
  save_btn = $('#external-feedback-form .btn')
  email = $('#external-feedback-form .email-field').val()
  msg = $('#external-feedback-form .msg-field').val()
  email_cond = re.test(email)
  msg_cond = msg.length > 0

  if email_cond and msg_cond
    save_btn.prop 'disabled', false
  else
    save_btn.prop 'disabled', 'disabled'
  return



$(document).ready ->
  header = $('.headerOne')

  $(window).scroll ->
    if $('.caption-inner').length > 0
      sliderOffset = $('.caption-inner').offset().top + 230
      y = $(this).scrollTop()
      if y > sliderOffset
        header.addClass 'fixed'
      else
        header.removeClass 'fixed'
      return

  $('.rotate').textrotator
    animation: 'fade'
    separator: ','
    speed: 2000

  window.sr = new scrollReveal
  return

  $('.carousel').carousel
    interval: 7000



$(document).on 'click', '[data-dismiss=alert]', ->
  $(this).parent('.alert').remove()
  return

