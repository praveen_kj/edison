class EventRecorderWorker

  include Sidekiq::Worker
  sidekiq_options queue: "default"

  def perform(product_id, event_type, user_id, source_id, other_data = {})

    Event.create(
        product_id: product_id,
        event_type: event_type,
        user_id: user_id,
        source_id: source_id,
        other_data: other_data
    )

  end


end