class WelcomeMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(user_id)
    user = User.find(user_id)
    InvitationMailer.welcome_email(user).deliver
    logger.info "Successfully Delivered to #{user.email}"
  end


end