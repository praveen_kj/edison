class CardUpdateMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(customer_id)
    StripeNotificationsMailer.card_update(customer_id).deliver
    logger.info("Card Update mail processed for user with #{customer_id}")
  end


end