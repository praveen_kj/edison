class TodoCompletionMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(completor_user_id, todo_id)
    todo = SubTask.find(todo_id)
    completor = User.find(completor_user_id)
    product = todo.user_task.product
    emails = product.team_email_list
    emails.delete(completor.email)
    emails.each do |email|
      ConversationsMailer.notify_todo_completion(email, todo, completor).deliver
    end
  end


end