## redundant as of Sep 09, 2015

class InvitationMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(membership_id)
    membership = Membership.find(membership_id)
    sender_id, invitee_email = membership.product.user_id, membership.member_email
    InvitationMailer.invite_to_plan(sender_id, invitee_email).deliver
    logger.info "Successfully Delivered to #{invitee_email}"
  end


end