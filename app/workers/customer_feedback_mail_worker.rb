class CustomerFeedbackMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(customer_feedback_id)
    customer_feedback = CustomerFeedback.find(customer_feedback_id)
    starter = customer_feedback.user
    product = customer_feedback.product
    emails = product.team_email_list
    emails.delete(starter.email)
    emails.each do |email|
      ConversationsMailer.new_feedback_email(email, customer_feedback).deliver
    end
  end


end