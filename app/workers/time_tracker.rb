class TimeTracker

  include Sidekiq::Worker
  sidekiq_options queue: "default"

  def perform(user_id, view, timespent, step_no = "XX")

    usage_duration = UsageDuration.new(
        user_id: user_id,
        view: view,
        time_spent: timespent
    )
    user = User.find(user_id)
    user.update_attributes(timespent_profile: user.timespent)

    if usage_duration.save
      logger.info("User #{user_id} accessed view: #{view} while in step number ")
    else
      logger.info "Failed saving for User #{user_id} accessed view: #{view} for #{timespent}"
    end

  end

end