class FeedbackAssignmentMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(assignor_user_id, sub_task_id)
    cf = CustomerFeedback.find(sub_task_id)
    assignor = User.find(assignor_user_id)
    ConversationsMailer.feedback_assignment_email(assignor, cf).deliver unless assignor.id == cf.assigned_to

  end


end