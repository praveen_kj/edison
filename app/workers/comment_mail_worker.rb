class CommentMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(comment_id)
    comment = Comment.where(id: comment_id).first
    parent = comment.parent_instance
    starter = comment.user
    product = parent.product
    emails = product.team_email_list
    emails.delete(starter.email)
    emails.each do |email|
      ConversationsMailer.notify_new_comment(email, parent, comment).deliver
    end
  end



end