class DiscussionMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(discussion_id)
    discussion = Discussion.find(discussion_id)
    starter = discussion.user
    product = discussion.product
    emails = product.team_email_list
    emails.delete(starter.email)
    emails.each do |email|
      ConversationsMailer.notify_discussion(starter, email, discussion).deliver
    end
  end


end