class CancelSubscriptionWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(subscription_id)
    StripeNotificationsMailer.cancel_subscription(subscription_id).deliver
    logger.info("Canceled Subscription mail processed for user with subscription id: #{subscription_id}")
  end


end