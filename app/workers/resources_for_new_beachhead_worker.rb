class ResourcesForNewBeachheadWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"


  def perform(business_id)
    business = Business.find_by_id(business_id)

    logger.info("#{business.business_name} loaded")


    ## create endusers

    ENDUSER_PROPERTIES.each do |item|
      business.endusers.create(:group => item[0], :parameter => item[1], :admin_field => true)
    end

    ## create persona properties

    PERSONA_PROPERTIES.each do |item|
      business.personas.create(:group => item[0], :parameter => item[1], :admin_field => true)
    end

    PERSONA_PRIORITIES.each do |k, v|
      3.times do |i|
        business.persona_details.create(group: k)
      end
    end

    ## create user priorities is synchronous process

    ## create use case touchpoints, rsteps and hurdles

    TOUCHPOINTS.each do |k, v|
      if k == 1
        business.touchpoints.create(:touchpoint_no => k, expected_conversion_rate: 100)
      else
        business.touchpoints.create(:touchpoint_no => k)
      end
    end

    business.touchpoints.all.each do |t|
      3.times do |i|
        t.relationship_steps.create(:step_desc => "End users’ interaction ##{i+1}")
        t.cx_steps.create(:hurdle_name => "Hurdle ##{i+1}")
      end
    end

    ## features and benefits

    # 1.times do |i|
    #   feature = business.features.create(:feature_name => "Primary Feature #{i+1}")
    #
    #   ['How it works', 'Sub feature', 'Benefit', 'Detriment'].each_with_index do |item, index|
    #     3.times do |k|
    #       feature.benefits.create(:benefit_name => item+" #{k+1}", group: index)
    #     end
    #   end
    #
    #   feature.iterations.create(:iteration_name => "Reason for Iteration")
    #
    # end


    ## value propositions

    3.times do |i|
      business.value_metrics.create
    end

    ## customers and improvements

    1.times do |i|
      customer = business.next_customers.create(:cust_name => "Customer #{i+1}")
    end

    ## core

    ['Network Effects', 'Customer Service', 'User Experience', 'Lower Costs'].each do |core|
      business.comp_advantages.create(:adv_name => core, admin_field: true)
    end


    ## comp positions

    ["My Product", "Status quo"].each do |comp|
      business.comp_positions.create(:competitor => comp, :admin_field => true)
    end

    business.comp_positions.create(:competitor => "Competing Product #1")


    ## dmu

    DMU_ENTITIES.each do |entity_ref, name|
      unless entity_ref == 1
        business.customer_decision_making_units.create(entity: entity_ref)
      end
    end


    ## business model and innovations

    BUSINESS_MODELS.each do |k, v|
      bm = business.pricing_models.create(:model_name => v, admin_field: true)
      3.times do |i|
        bm.innovations.create(:innovation_name => "Business Model Innovation #{i+1}")
      end
    end


    ## pricing plan and attraction

    1.times do |i|
      pricing_plan = business.pricing_plans.create(:plan_name => "Pricing Plan #{i+1}")
      3.times do |k|
        pricing_plan.attractions.create(:attraction_name => "Attraction #{k+1}")
        pricing_plan.plan_defects.create(:defect_name => "Defect #{k+1}")
      end
    end


    ## sales strategy

    1.times do |i|
      sales_plan = business.sales_strategies.create(:strategy_name => "Strategy #{i+1}")
      3.times do |k|
        sales_plan.strategy_gaps.create(:gap_name => "Strategy Gap #{k+1}")
      end
    end
    
    ## financials

    CUSTOMER_GROUPS.each do |k, v|
      business.financials.create(:customer_segment => k)
    end

    logger.info("All resources created for #{business.business_name}: #{business.resources_created}")

  end


  private


end