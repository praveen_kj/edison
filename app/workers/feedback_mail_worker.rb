class FeedbackMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "default"

  def perform(feedback_id)
    feedback = Feedback.find(feedback_id)
    FeedbackMailer.auto_forward(feedback).deliver
    logger.info("Support request forwarded at #{Time.zone.now}")
  end


end