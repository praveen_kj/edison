class ReferToFriendsWorker

  include Sidekiq::Worker
  sidekiq_options queue: "default"

  def perform(emails, user_id)
    user = User.find(user_id)
    emails.each do |referred|
      UserMailer.refer_to_friends(user, referred).deliver
      logger.info("Email sent to #{referred}")
    end
    logger.info("All referral emails sent for #{user.user_parameter.user_name}")
  end


end