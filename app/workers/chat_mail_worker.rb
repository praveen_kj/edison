class ChatMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(message_id)
    message = Message.find(message_id)

    starter = message.user

    product = message.product
    emails = product.team_email_list

    emails.delete(starter.email)

    emails.each do |email|
      ConversationsMailer.notify_new_message(email, message).deliver
    end
  end


end