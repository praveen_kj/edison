class TodoAssignmentMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(assignor_user_id, sub_task_id)
    todo = SubTask.find(sub_task_id)
    assignor = User.find(assignor_user_id)
    ConversationsMailer.notify_todo_assignment(assignor, todo).deliver unless assignor.id == todo.assignee_id

  end


end