class NoteMailWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(note_id)
    note = Note.find(note_id)
    starter = note.user
    product = note.product
    emails = product.team_email_list
    emails.delete(starter.email)
    emails.each do |email|
      ConversationsMailer.notify_new_note(starter, email, note).deliver
    end
  end


end