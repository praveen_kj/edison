class NewSubscriberWorker

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(subscription_id)
    StripeNotificationsMailer.new_subscriber_mail(subscription_id).deliver
    logger.info("New Subscription mail processed for user with subscription id: #{subscription_id}")
  end


end