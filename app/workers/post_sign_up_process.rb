class PostSignUpProcess

  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(user_id, referrer_code)

    resource = User.find(user_id)

    resource.generate_referral_code
    logger.info("Referral code generated for User #{resource.id}")

    referrer = User.find_by_referrer_code(referrer_code)

    if referrer
      ref_count = referrer.referral_count += 1
      if referrer.active_trial_user?
        referrer.trial_expiry_date += PER_REFERRAL_REWARD if ref_count <= MAX_REFERRALS
      elsif referrer.expired_trial_user?
        referrer.trial_expiry_date = PER_REFERRAL_REWARD.from_now.utc
      end
      referrer.save
      referrer.send_trial_extension_mail
      logger.info("Referrer trial extended to #{referrer.trial_expiry_date} and trial extension email sent")
    else
      logger.info("Referrer account not found")
    end

  end





end