class StaticPagesController < ApplicationController

  include ApplicationHelper

  include AnalyticsHelper

  before_action :validate_user, only: [:account, :elevator_pitch, :faqs]

  before_action :validate_admin, only: [:returning_users]

  layout 'home', only: [:home, :about, :pricing, :privacy, :terms, :support]

  def lets_encrypt
    render text: 'AmyfUECPpG2ZdFsbTiXkAw4GSfVWPtKFM9IqYLCYKsA.eg0xjB5WWIQ3CyaXJ5u9i6yPcdcKYP8yYFUMhuIKz-k'
  end

  def home

  end

  def pricing

  end

  def support

  end


  def about

  end

  def privacy

  end

  def terms

  end

  def admin_stats

  end

  def returning_users

    permitted_scopes = ['all', 'recently_active']

    if permitted_scopes.include? params[:filter]
      @users = User.includes(:user_parameter).send(params[:filter])
      send_data generate_csv(@users)
    end

  end


  private


  def validate_admin
    unless current_user.admin?
      redirect_to app_path
    end
  end

  def validate_user
    if user_signed_in?

      # when re enabled check whether a momentary error is shown on sign up because
      # the trial expiry is set by the background process

      if current_user.trial_expiry_date && current_user.expired_trial_user? && !current_user.active_subscriber?
        flash[:notice] = "Your trial expired on #{ldt(current_user.trial_expiry_date)}. Please subscribe to continue."
        # redirect_to account_path
      elsif current_user.former_subscriber?
        flash[:notice] = "Your need to subscribe in order to access EdisonPlan."
        # redirect_to account_path
      end

    else
      redirect_to sign_in_path
      flash[:notice] = "Please sign in to access the app"
    end

  end

end


