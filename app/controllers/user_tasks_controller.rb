class UserTasksController < ApplicationController


  include UserTasksHelper
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  before_action :correct_business, only: [:destroy, :update]


  before_action :validate_user, only: [:new, :show]

  def index

    if params[:scope] == 'archived'
      @view = 'archived-to-do-lists'
      @url = '/to-do-lists/archived'
      @tab_title = 'Archived To Do Lists'
      @to_do_lists = active_product.user_tasks.archived
    else
      @view = 'to-do-lists'
      @url = '/to-do-lists'
      @tab_title = 'To Do Lists'
      @to_do_lists = active_product.user_tasks.pending
    end

  end

  def show
    @sub_tool_resource = active_product.user_tasks.find(params[:id])
    @sub_tool_title = @sub_tool_resource.task_name
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @new_sub_tool_title = 'New To Do List'
  end

  def create

    @user_task = active_product.user_tasks.build(user_task_params.merge(user_id: current_user.id))
    respond_to do |format|
      if @user_task.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @user_task.destroy
      format.html { redirect_to :back }
      format.js
    end

  end


  def update

    @user_task = UserTask.find(params[:id])
    @params = params[:user_task]
    respond_to do |format|
      if @user_task.update_attributes(user_task_params)
        format.json { respond_with_bip(@user_task) }
        format.js { respond_with_active_edit @user_task, @field }
        format.html { redirect_to :back }
      else
        format.json { respond_with_bip(@user_task) }
        format.js
        format.html { redirect_to :back }
      end
    end
  end



  def filter
    @scope = scope_match[params[:scope]]
    @offset = params[:offset].to_i

    if @scope && @offset
      @tasks = current_user.user_tasks.send(@scope).offset(@offset)
    end


  end


  private

  def user_task_params
    params.require(:user_task).permit(:task_name,
                                      :start_date,
                                      :end_date,
                                      :step_no,
                                      :status,
                                      :reason_to_do,
                                      :category,
                                      :business_id,
                                      :task_description,
    )
  end

  def correct_business
    @user_task = active_product.user_tasks.where(id: params[:id]).first
    redirect_to root_url if @user_task.nil?
  end


  def correct_user
    @user_task = active_product.user_tasks.find_by(id: params[:id])
    redirect_to root_url if @user_task.nil?
  end

  def scope_match
    {
        'upcoming' => 'starting_later',
        'someday' => 'unplanned',
        'pending' => 'overdue',
    }
  end

  def record_event
    EventRecorderWorker.perform_async(@user_task.product_id, 4, current_user.id, @user_task.id)
  end


end
