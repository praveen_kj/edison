class NextCustomersController < ApplicationController

  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @next_customer = beachhead.next_customers.build(next_customer_params)
    respond_to do |format|
      if @next_customer.save
        create_child_resources
        format.html { redirect_to :back }
        format.js
        expire_fragment([beachhead, 'next_customer'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @next_customer.destroy
      format.html { redirect_to :back }
      format.js
      expire_fragment([beachhead, 'next_customer'])
    end

  end


  def update
    @params = params[:next_customer]
    respond_to do |format|
      if @next_customer.update_attributes(next_customer_params)
        format.json { respond_with_bip(@next_customer) }
        format.js { respond_with_active_edit @next_customer, @field }
        format.html { redirect_to :back }
        expire_fragment([beachhead, 'next_customer'])

      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def create_child_resources
    3.times do |i|
      @next_customer.improvements.create(:improvement_name => "Improvement #{i+1}")
    end
  end


  def next_customer_params
    params.require(:next_customer).permit(:cust_name,
                                          :use_case,
                                          :use_case_note,
                                          :benefits,
                                          :benefits_note,
                                          :value_created,
                                          :value_created_note,
                                          :purchase_interest,
                                          :purchase_interest_note,
                                          :next_meeting_date,
                                          :stage

    )
  end


  def correct_business
    @next_customer = beachhead.next_customers.find_by(id: params[:id])
    if @next_customer.nil?
      redirect_to root_url
    else
      @obj = {id: @next_customer.id, obj_name: @next_customer.class.to_s.underscore}
    end
  end


end
