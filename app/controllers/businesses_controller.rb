class BusinessesController < ApplicationController

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  before_action :set_business, only: [:update, :destroy]

  include ApplicationHelper


  def delete_business
    @deletee_business = Business.find(params[:id])
    if @deletee_business
      respond_to do |f|
        f.js { render 'delete_modal' }
      end
    end
  end

  def mass_create
    @businesses = active_product.businesses.create(parsed_params)
    respond_to do |format|
      format.js
      format.html { redirect_to :back }
    end
  end

  def analyse

  end

  def test_fitness
    @business = Business.find(params[:beachhead])
  end

  def create
    @business = active_product.businesses.build(business_params)
    respond_to do |format|
      if @business.save
        format.js
        format.html { redirect_to :back }
      else
        format.js {}
        format.html { redirect_to :back }
      end
    end

  end

  def destroy
    @is_beachhead = @business.is_beachhead?
    @business.destroy
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end


  def update
    @business = Business.find(params[:id])
    respond_to do |format|
      if @business.update_attributes(business_params)
        format.js { respond_with_active_edit @business, @field }
        record_event(step_num_changed) if step_num_changed
      else
        format.js
      end
    end
  end


  private

  def business_params
    params.require(:business).permit(
        :business_name, :question1, :question2, :question3,
        :question4, :question5, :question6, :question7, :factor1,
        :factor2, :end_user, :application, :benefit, :marketing,
        :sales, :core, :prod_features, :top_competitors, :partners,
        :resources_created, :similar_product, :similar_sales, :word_of_mouth,
        :value_prop_conf_date, :enduser_name,
        :eu_target_date1, :eu_target_date2, :eu_target_date3,
        :eu_target_date4, :persona_meeting_date, :persona_name, :enduser_count,
        :opening_users, :validated_enduser, :analysis_start_at, :analysis_end_at,
        :chosen_business_model_id, :follow_on_markets,
        :beachhead_size,
        :top_user_priorities,
        :dmu,
        :use_case,
        :value_proposition,
        :business_model,
        :pricing,
        :customer_reach,
    )
  end


  def correct_user
    @business = active_product.businesses.find_by(id: params[:id])
    redirect_to root_url if @business.nil?
  end

  def set_business
    @business = Business.find(params[:id])
  end

  def parsed_params
    params.require(:business).values.collect { |business_name| {business_name: business_name} }
  end

  def step_num_changed
    if @business.size_checklist_changed?
      1
    elsif @business.diagnosis_changed?
      2
    end
  end

  def record_event(step_num)
    EventRecorderWorker.perform_async(@business.product_id, 1, current_user.id, step_num, {business_id: @business.id})
  end


end
