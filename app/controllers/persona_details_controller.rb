class PersonaDetailsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]
  # include ActionEditHelper


  def rank
    @persona_detail = beachhead.persona_details.find(params[:id])
    @persona_detail.rank_at(params[:persona_detail][:rank].to_i)
    respond_to do |format|
      format.text { render :status => 202, :text => 'ok' }
    end
  end

  def create

    @persona_detail = beachhead.persona_details.build(persona_detail_params)
    respond_to do |format|
      if @persona_detail.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end


  end

  def destroy
    respond_to do |format|
      @persona_detail.destroy
      format.js
      record_event
    end
  end


  def update
    respond_to do |format|
      if @persona_detail.update_attributes(persona_detail_params)
        format.json { respond_with_bip(@persona_detail) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @persona_detail, @field }
        record_event
      else
        format.html { redirect_to :back }
        format.json { respond_with_bip(@persona_detail) }
        format.js
      end
    end
  end

  private

  def persona_detail_params
    params.require(:persona_detail).permit(:detail, :priority, :group)
  end


  def correct_business
    @persona_detail = beachhead.persona_details.find_by(id: params[:id])
    if @persona_detail.nil?
      redirect_to root_url
    else
      @obj = {id: @persona_detail.id, obj_name: @persona_detail.underscored}
    end
  end

  def record_event
    b = @persona_detail.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 5, {business_id: b.id})
  end


end
