class ReferralsController < ApplicationController

  before_action :validate_user, only: [:show]

  layout 'home', only: [:show]

  def show

  end


  def email_to_references
    param = params[:email_addresses]
    emails = param.gsub(',', ' ').split
    ReferToFriendsWorker.perform_async(emails, current_user.id)
  end


  private

  def validate_user
    if user_signed_in?
      redirect_to app_path
      flash[:notice] = "You are already signed in"
    end
  end

end
