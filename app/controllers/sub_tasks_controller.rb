class SubTasksController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_task, only: [:create]

  def index
    @user_task = active_product.user_tasks.where(id: params[:id].to_i).first
    @sub_tasks = @user_task.sub_tasks.send(params[:scope])
  end

  def create
    user_task = active_product.user_tasks.find(sub_task_params[:user_task_id])
    @sub_task = user_task.sub_tasks.build(sub_task_params.merge(created_by: current_user.id))
    respond_to do |format|
      if @sub_task.save
        format.html { redirect_to :back }
        format.js
        record_event(6)
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  def show
    task =  SubTask.where(id: params[:id]).first
    if task && current_user.has_access_to?(task)
      @sub_tool_resource = task
      @sub_tool_title = @sub_tool_resource.sub_task_name
      respond_to do |format|
        format.html
        format.js
      end
    else
      render status: 404
    end
  end

  def destroy

    @sub_task = SubTask.find(params[:id])

    if active_product.user_tasks.find(@sub_task.user_task_id).nil?
      redirect_to root_url
    else
      respond_to do |format|
        @sub_task.destroy
        format.html { redirect_to :back }
        format.js
      end
    end

  end


  def update

    @sub_task = SubTask.find(params[:id])
    @params = params[:sub_task]
    respond_to do |format|
      if @sub_task.update_attributes(sub_task_params)
        format.js { respond_with_active_edit @sub_task, @field }
        format.html { redirect_to :back }
        perform_bg_tasks
        record_event(7) if @field == :status && @sub_task.status
      else
        format.js
        format.html { redirect_to :back }
      end
    end
  end


  def clear_today
    SubTask.clear_today(active_product.id, current_user.id)
    @to_do_lists = active_product.user_tasks.pending
  end


  private

  def sub_task_params
    params.require(:sub_task).permit(:user_task_id,
                                     :sub_task_name,
                                     :start_date,
                                     :end_date,
                                     :status,
                                     :assignee_id, :time_estimate, :priority, :description)
  end

  def correct_task
    redirect_to root_url if active_product.user_tasks.where(id: sub_task_params[:user_task_id]).first.nil?
  end

  def perform_bg_tasks
    TodoAssignmentMailWorker.perform_async(current_user.id, @sub_task.id) if @field == :assignee_id && @sub_task.assignee_id
    TodoCompletionMailWorker.perform_async(current_user.id, @sub_task.id) if @field == :status && @sub_task.status
  end

  def record_event(event_type)
    user_task = @sub_task.user_task
    EventRecorderWorker.perform_async(user_task.product_id, event_type, current_user.id, user_task.id, {sub_task_id: @sub_task.id})
  end

end
