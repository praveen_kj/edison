class MembershipsController < ApplicationController

  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  before_action :set_membership, only: [:update, :destroy]

  def invite_cofounders

  end


  def create
    logger.warn ">>>>> #{membership_params} <<<<<<"
    active_product.memberships.create(membership_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    respond_to do |format|
      @membership.destroy
      format.html { redirect_to :back }
      format.js
    end
  end


  def update
    respond_to do |format|
      if @membership.update_attributes(membership_params)
        format.js { respond_with_active_edit @membership, @field }
      else
        format.js
      end
    end
  end

  private

  def membership_params
    if params.require(:membership).is_a? Array
      params.require(:membership).collect { |item| item.permit(:member_email, :write_tools, :write_utils, :role) }
    else
      params.require(:membership).permit(:member_email, :write_tools, :write_utils, :role)
    end
  end


  def correct_user
    @membership = active_product.memberships.find_by(id: params[:id])
    redirect_to root_url if @membership.nil?
  end

  def set_membership
    @membership = Membership.find(params[:id])
  end

  def parsed_params
    params.require(:membership).values.collect { |member_email| {member_email: member_email} }
  end


end
