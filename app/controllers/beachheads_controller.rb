class BeachheadsController < ApplicationController

  before_action :authenticate_user!, only: [:create, :destroy]
  include ApplicationHelper

  before_action :validate_business, only: [:update]

  def update
    @old_beachhead = Business.exists?(id: active_product.active_market) ? Business.find(active_product.active_market) : nil
    respond_to do |format|
      if active_product.update_attributes(beachhead_params)
        create_resources_for_beachhead(active_product.active_market)
        format.js
      end
    end
  end

  private

  def beachhead_params
    params.require(:product).permit(:active_market)
  end

  def validate_business
    unless active_product.businesses.pluck(:id).include? (params[:product][:active_market].to_i)
      redirect_to app_path(view: 'beachhead')
    end
  end


  def create_resources_for_beachhead(business_id)
    b = Business.find(business_id)
    unless b.resources_created
      b.update_attributes(resources_created: true)
      b.create_child_resources
    end
  end

end
