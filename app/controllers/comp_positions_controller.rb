class CompPositionsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @comp_position = beachhead.comp_positions.build(comp_position_params)
    respond_to do |format|
      if @comp_position.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @comp_position.destroy
      format.html { redirect_to :back }
      format.js
      record_event
    end

  end


  def update
    @fac1 = params[:comp_position][:factor1]
    @fac2 = params[:comp_position][:factor2]
    respond_to do |format|
      if @comp_position.update_attributes(comp_position_params)
        format.json { respond_with_bip(@comp_position) }
        format.js { respond_with_active_edit @comp_position, @field }
        format.html { redirect_to :back }
        record_event
      else
        format.html { redirect_to :back }
        format.js
      end
    end
  end

  private

  def comp_position_params
    params.require(:comp_position).permit(:competitor, :factor1, :factor2)
  end


  def correct_business
    @comp_position = beachhead.comp_positions.find_by(id: params[:id])
    if @comp_position.nil?
      redirect_to root_url
    else
      @obj = {id: @comp_position.id, obj_name: @comp_position.class.to_s.underscore}
    end
  end


  def record_event
    b = @comp_position.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 10, {business_id: b.id})
  end

end
