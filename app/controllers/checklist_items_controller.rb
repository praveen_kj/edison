class ChecklistItemsController < ApplicationController
  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @checklist_item = ChecklistItem.new(checklist_item_params)
    respond_to do |format|
      if @checklist_item.save
        format.html { redirect_to :back }
        format.js

      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @checklist_item.destroy

      format.html { redirect_to :back }
      format.js
    end

  end


  def update
    respond_to do |format|
      if @checklist_item.update_attributes(checklist_item_params)
        format.json { respond_with_bip(@checklist_item) }
        format.js { respond_with_active_edit @checklist_item, @field }
        format.html { redirect_to :back }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  private


  def checklist_item_params
    params.require(:checklist_item).permit(:item_key,
                                           :product_id,
                                           :business_id, :step
    )
  end


  def correct_business
    @checklist_item = beachhead.checklist_items.find_by(id: params[:id]) || active_product.checklist_items.find_by(id: params[:id])
    if @checklist_item.nil?
      redirect_to root_url
    end
  end


end
