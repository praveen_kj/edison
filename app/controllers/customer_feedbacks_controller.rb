class CustomerFeedbacksController < ApplicationController
  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update, :remove_references, :revoke_decision]


  def unaddressed


  end


  def remove_references
    @customer_feedback.remove_reference_tags
  end

  def revoke_decision
    @customer_feedback.revoke_decision
  end

  def index
    set_tool if params[:tool]
    set_scope if params[:scope]

    @feedbacks = if @tool
                   active_product.customer_feedbacks.send(@scope).for_tool(@step)
                 else
                   active_product.customer_feedbacks.send(@scope)
                 end
    @view = "#{@scope.capitalize} Feedback #{"for #{@tool}" if @tool}"
    @url = customer_feedbacks_path(scope: @scope, tool: @tool)
    @tab_title = "#{@scope.capitalize} Feedback #{"for #{@tool}" if @tool}"
  end

  def new
    @new_sub_tool_title = 'New Customer Feedback'
  end

  def show
    @sub_tool_resource = active_product.customer_feedbacks.find(params[:id])
    @sub_tool_title = "Insights from #{@sub_tool_resource.customer_name}"
    respond_to do |format|
      format.html
      format.js
    end
  end



  def create

    @customer_feedback = active_product.customer_feedbacks.build(customer_feedback_params.merge(created_by: current_user.id))
    respond_to do |format|
      if @customer_feedback.save
        @customer_feedback.comments.create(user_id: current_user.id, comment_text: params[:customer_feedback][:feedback_comment]) if params[:customer_feedback][:feedback_comment]
        format.js
        format.html { redirect_to :back }
        FeedbackAssignmentMailWorker.perform_async(current_user.id, @customer_feedback.id) if @field == :assigned_to && @customer_feedback.assigned_to
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @customer_feedback.destroy
      format.html { redirect_to :back }
      format.js
    end

  end


  def update
    respond_to do |format|
      if @customer_feedback.update_attributes(customer_feedback_params)
        format.json { respond_with_bip(@customer_feedback) }
        format.js { respond_with_active_edit @customer_feedback, @field }
        format.html { redirect_to :back }

      else
        format.html { redirect_to :back }
      end
    end
  end

  private


  def customer_feedback_params
    params.require(:customer_feedback).permit(:customer_name,
                                              :feedback,
                                              :related_feature_id,
                                              :related_component_id,
                                              :customer_interest,
                                              :critical, :status, :created_by,
                                              :related_tool, :decision, :assigned_to, :target_date
    )
  end


  def correct_business
    @customer_feedback = active_product.customer_feedbacks.find_by(id: params[:id])
    if @customer_feedback.nil?
      redirect_to root_url
    end
  end


  def set_tool
    requested_view = params[:tool]
    @tool = permitted_views.include?(requested_view) ? requested_view : nil
    @step = all_tools[@tool]
  end

  def set_scope
    requested_scope = params[:scope]
    @scope = ['all', 'unaddressed', 'scheduled', 'disregarded', 'completed'].include?(requested_scope) ? requested_scope : 'all'
  end


end
