class ValueMetricsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: :destroy


  def create

    @value_metric = beachhead.value_metrics.build(value_metric_params)
    respond_to do |format|
      if @value_metric.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @value_metric.destroy
      format.html { redirect_to :back }
      format.js
      record_event
    end

  end


  def update

    @value_metric = ValueMetric.find(params[:id])
    respond_to do |format|
      if @value_metric.update_attributes(value_metric_params)
        format.json { respond_with_bip(@value_metric) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @value_metric, @field }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def value_metric_params
    params.require(:value_metric).permit(:proposition_name,
                                         :as_is_state,
                                         :as_is_state_value,
                                         :possible_state,
                                         :possible_state_value,
                                         :unit_of_measurement,
                                         :benefit_type)
  end


  def correct_business
    @value_metric = beachhead.value_metrics.find_by(id: params[:id])
    if @value_metric.nil?
      redirect_to root_url
    else
      @obj = {id: @value_metric.id, obj_name: @value_metric.class.to_s.underscore}
    end
  end

  def record_event
    b = @value_metric.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 8, {business_id: b.id})
  end

end
