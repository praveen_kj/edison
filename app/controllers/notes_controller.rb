class NotesController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]

  before_action :validate_user, only: [:new, :show]

  def index
    @view = 'notes'
    @url = '/notes'
    @tab_title = 'Notes'
  end

  def new
    @new_sub_tool_title = 'New Note'
  end

  def show
    @sub_tool_resource = active_product.notes.find(params[:id])
    @sub_tool_title = @sub_tool_resource.title
    respond_to do |format|
      format.html
      format.js
    end
  end

  def edit
    @sub_tool_resource = active_product.notes.find(params[:id])
    @sub_tool_title = @sub_tool_resource.title
    respond_to do |format|
      format.html
      format.js
    end
  end


  def create

    @note = active_product.notes.build(note_params.merge(user_id: current_user.id, draft: true))
    respond_to do |format|
      if @note.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end


  end

  def destroy
    respond_to do |format|
      @note.destroy
      format.js
    end
  end


  def update
    respond_to do |format|
      if @note.update_attributes(note_params)
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @note, @field }
        NoteMailWorker.perform_async(@note.id) if @field == :draft && @note.draft == false
        record_event if @field == :draft && @note.draft == false
      else
        format.html { redirect_to :back }
        format.js
      end
    end
  end

  private

  def note_params
    params.require(:note).permit(:title, :content, :step_no, :archived, :draft)
  end


  def correct_business
    @note = active_product.notes.find_by(id: params[:id])
    redirect_to root_url if @note.nil?
  end

  def record_event
    EventRecorderWorker.perform_async(@note.product_id, 8, current_user.id, @note.id)
  end


end
