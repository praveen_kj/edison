class DashboardsController < ApplicationController
  include ApplicationHelper

  before_action :authenticate_user!, only: [:create]


  def create

    @dashboard = Dashboard.new(dashboard_params.merge(user_id: current_user.id, product_id: active_product.id))
    respond_to do |format|
      if @dashboard.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end


  private


  def dashboard_params
    params.require(:dashboard).permit(:dashboard_ref)
  end

end
