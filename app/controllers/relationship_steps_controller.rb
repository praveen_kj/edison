class RelationshipStepsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @touchpoint = Touchpoint.find(params[:relationship_step][:touchpoint_id])
    @relationship_step = @touchpoint.relationship_steps.build(relationship_step_params)
    respond_to do |format|
      if @relationship_step.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([touchpoint, 'relationship_step'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @relationship_step.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([touchpoint, 'relationship_step'])
    end

  end


  def update
    @params = params[:relationship_step]
    respond_to do |format|
      if @relationship_step.update_attributes(relationship_step_params)
        format.json { respond_with_bip(@relationship_step) }
        format.js { respond_with_active_edit(@relationship_step, @field) }
        format.html { redirect_to :back }
        # expire_fragment([touchpoint, 'relationship_step'])

      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def relationship_step_params
    params.require(:relationship_step).permit(:step_desc, :touchpoint_id, :target_date)
  end


  def correct_business
    @relationship_step = RelationshipStep.find_by(id: params[:id])
    if @relationship_step.nil?
      redirect_to root_url
    else
      @obj = {id: @relationship_step.id, obj_name: @relationship_step.class.to_s.underscore}
    end
  end



end
