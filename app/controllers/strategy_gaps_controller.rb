class StrategyGapsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @sales_strategy = SalesStrategy.find(params[:strategy_gap][:sales_strategy_id])
    @strategy_gap = @sales_strategy.strategy_gaps.build(strategy_gap_params)
    respond_to do |format|
      if @strategy_gap.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([beachhead, 'product_spec'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @strategy_gap.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([beachhead, 'product_spec'])
    end

  end


  def update
    respond_to do |format|
      if @strategy_gap.update_attributes(strategy_gap_params)
        format.json { respond_with_bip(@strategy_gap) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @strategy_gap, @field }
      else
        format.json { respond_with_bip(@strategy_gap) }
        format.html { redirect_to :back }
        format.js {  }
      end
    end
  end

  private

  def strategy_gap_params
    params.require(:strategy_gap).permit(:gap_name, :closure_date, :sales_strategy_id)
  end


  def correct_business
    @strategy_gap = StrategyGap.find_by(id: params[:id])
    if @strategy_gap.nil?
      redirect_to root_url
    else
      @obj = {id: @strategy_gap.id, obj_name: @strategy_gap.class.to_s.underscore}
    end
  end



end
