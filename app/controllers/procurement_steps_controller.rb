class ProcurementStepsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]

  def create
    @procurement_step = beachhead.procurement_steps.build(procurement_step_params)
    respond_to do |format|
      if @procurement_step.save
        format.html { redirect_to :back }
        format.js
        expire_fragment([beachhead, 'procurement'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  def destroy
    respond_to do |format|
      @procurement_step.destroy
      format.html { redirect_to :back }
      format.js
      expire_fragment([beachhead, 'procurement'])
    end
  end


  def update
    respond_to do |format|
      if @procurement_step.update_attributes(procurement_step_params)
        format.json { respond_with_bip(@procurement_step) }
        format.html { redirect_to :back }
        expire_fragment([beachhead, 'procurement'])
      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def procurement_step_params
    params.require(:procurement_step).permit(:target_date,
                                             :cost,
                                             :key_partners,
                                             :product_stage,
                                             :key_proc_activity)
  end

  def correct_business
    @procurement_step = beachhead.procurement_steps.find_by(id: params[:id])
    if @procurement_step.nil?
      redirect_to root_url
    else
      @obj = {id: @procurement_step.id, obj_name: @procurement_step.class.to_s.underscore}
    end
  end

end
