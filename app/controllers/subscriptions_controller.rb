class SubscriptionsController < ApplicationController

  before_filter :authenticate_user!

  before_filter :validate_token, only: [:create, :update_card]

  layout 'subscriptions'

  def my_account

  end

  def create

    #creating a customer

    customer = Stripe::Customer.create(
        :email => current_user.email,
        :card => params[:stripeToken]
    )

    # subscribing the customer to the plan
    subscription = customer.subscriptions.create(:plan => current_user.eligible_plan)

    # extract card_id and last4 for storage
    card_id = customer.default_source
    last4 = customer.sources.retrieve(card_id).last4

    current_user.update_attributes(
        customer_id: customer.id,
        subscription_id: subscription.id,
        card_id: card_id,
        last4: last4,
        subscription_status: true,
        plan_id: current_user.eligible_plan,
        current_subscription_end_at: Time.at(subscription.current_period_end),
        billing_info: {
            name: params[:name_on_card],
            address: params[:address_line],
            city: params[:city],
            state: params[:state],
            country: params[:country][0],
            postal_code: params[:postal_code],
        }
    )

    NewSubscriberWorker.perform_async(current_user.subscription_id)


    redirect_to app_path
    flash[:info] = "Thank you for your payment. You have been subscribed successfully. We wish your product and you a great success."

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to account_path

  end

  def unsubscribe

  end


  def cancel

    if current_user.subscription_status

      customer_id = current_user.customer_id
      subscription_id = current_user.subscription_id

      customer = Stripe::Customer.retrieve(customer_id)
      customer.subscriptions.retrieve(subscription_id).delete

      current_user.update_attributes(
          subscription_status: false,
          canceled_at: Time.zone.now
      )

      CancelSubscriptionWorker.perform_async(subscription_id)
      redirect_to app_path
      flash[:info] = "You have unsubscribed successfully. Your can continue to access the app until #{current_user.current_subscription_end_at.strftime("%d-%b-%Y")}. You will no longer be charged."
    else
      redirect_to app_path
      flash[:info] = "You do not seem to have an active subscription"
    end

  end


  def update_card
    token = params[:stripeToken]
    customer_id = current_user.customer_id

    customer = Stripe::Customer.retrieve(customer_id)
    card = customer.sources.create(:card => token)
    customer.default_source = card.id
    customer.save

    last4 = card.last4

    current_user.update_attributes(
        last4: last4,
        card_id: card.id,
        billing_info: {
            name: params[:name_on_card],
            address: params[:address_line],
            city: params[:city],
            state: params[:state],
            country: params[:country][0],
            postal_code: params[:postal_code],
        }
    )

    CardUpdateMailWorker.perform_async(customer_id)

    flash[:info] = "Your card with number #{sprintf('%04d', last4)} ending has been successfully updated for future charges"

    redirect_to app_path

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to account_path
  end


  private

  def validate_token
    if params[:stripeToken].blank?
      redirect_to account_path
      flash[:info] = "You request could not be processed. Please Contact Support."
    end
  end


end
