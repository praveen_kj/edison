class CxStepsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @touchpoint = Touchpoint.find(params[:cx_step][:touchpoint_id])
    @cx_step = @touchpoint.cx_steps.build(cx_step_params)
    respond_to do |format|
      if @cx_step.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([touchpoint, 'cx_step'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @cx_step.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([touchpoint, 'cx_step'])
    end

  end


  def update
    @params = params[:cx_step]
    respond_to do |format|
      if @cx_step.update_attributes(cx_step_params)
        format.json { respond_with_bip(@cx_step) }
        format.js { respond_with_active_edit @cx_step, @field }
        format.html { redirect_to :back }
        # expire_fragment([touchpoint, 'cx_step'])

      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def cx_step_params
    params.require(:cx_step).permit(:hurdle_name, :hurdle_type, :hurdle_target_date)
  end


  def correct_business
    @cx_step = CxStep.find_by(id: params[:id])
    if @cx_step.nil?
      redirect_to root_url
    else
      @obj = {id: @cx_step.id, obj_name: @cx_step.class.to_s.underscore}
    end
  end


end
