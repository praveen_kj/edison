class EmailValidationController < ApplicationController

  layout 'home'

  def validation
    u = User.find_by_email(params[:email])
    respond_to do |format|
      if u.nil?
        format.json { render :json => {:status => true, :current_user => false} } # email is available
      elsif user_signed_in? && u.id == current_user.id
        format.json { render :json => {:status => false, :current_user => true} } # email belongs to current user
      else
        format.json { render :json => {:status => false, :current_user => false} } # email is taken but does not belong to user
      end
    end
  end

end
