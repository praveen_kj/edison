class ToolPagesController < ApplicationController

  before_action :validate_user, only: [:app]

  before_action :validate_product, only: [:app]

  before_action :validate_beachhead, only: [:app]

  before_action :validate_params, only: [:app]


  after_action :update_last_visited_step, only: [:app]

  include ApplicationHelper


  def app
    set_view if params[:view]
    set_step
  end


  private

  def validate_beachhead
    redirect_to onboarding_path unless beachhead
  end

  def validate_params
    if permitted_views.exclude?(params[:view])
      redirect_to home_path
    end
  end

  def set_view
    requested_view = params[:view]
    set_view = permitted_views.include?(requested_view) ? requested_view : 'home'
    if current_user.has_no_products? && current_user.products_where_member.count == 0
      @view = 'products'
    else
      @view = set_view
    end
  end

  def set_step
    @curr_step = beachhead ? all_tools[@view] : 1
  end


  def update_last_visited_step
    active_product.update_attributes(last_visited_step: @curr_step) if CANVAS_STEPS.include?(@curr_step)
  end

end
