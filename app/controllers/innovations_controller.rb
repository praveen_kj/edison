class InnovationsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @pricing_model = PricingModel.find(params[:innovation][:pricing_model_id])
    @innovation = @pricing_model.innovations.build(innovation_params)
    respond_to do |format|
      if @innovation.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([pricing_model, 'innovation'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @innovation.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([pricing_model, 'innovation'])
    end

  end


  def update
    @params = params[:innovation]
    respond_to do |format|
      if @innovation.update_attributes(innovation_params)
        format.json { respond_with_bip(@innovation) }
        format.js { respond_with_active_edit @innovation, @field }
        format.html { redirect_to :back }
        # expire_fragment([pricing_model, 'innovation'])

      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def innovation_params
    params.require(:innovation).permit(:target_date, :innovation_name, :revenue_increase)
  end


  def correct_business
    @innovation = Innovation.find_by(id: params[:id])
    if @innovation.nil?
      redirect_to root_url
    else
      @obj = {id: @innovation.id, obj_name: @innovation.class.to_s.underscore}
    end
  end
end
