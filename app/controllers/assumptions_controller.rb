class AssumptionsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]

  before_action :validate_user, only: [:new, :show]
  include AssumptionHelper

  def index
    @view = 'assumptions'
    @url = '/assumptions'
    @tab_title = 'Assumptions'
  end


  def show
    @sub_tool_resource = active_product.assumptions.find(params[:id])
    @sub_tool_title = @sub_tool_resource.hypothesis || preset_assumption(@sub_tool_resource.preset_id)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @new_sub_tool_title = 'New Assumption'
  end


  def create

    @assumption = active_product.assumptions.build(assumption_params.merge(user_id: current_user.id))
    respond_to do |format|
      if @assumption.save
        create_experiment
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end


  end

  def destroy
    respond_to do |format|
      @assumption.destroy
      format.js
    end
  end


  def update
    respond_to do |format|
      if @assumption.update_attributes(assumption_params)
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @assumption, @field }
      else
        format.html { redirect_to :back }
        format.js
      end
    end
  end

  private

  def assumption_params
    params.require(:assumption).permit(:hypothesis, :impact, :archived, :step_no, :status)
  end

  def correct_business
    @assumption = active_product.assumptions.find_by(id: params[:id])
    redirect_to root_url if @assumption.nil?
  end

  def create_experiment
    @assumption.experiments.create(
        description: params[:assumption][:experiment_desc],
        cost: params[:assumption][:experiment_cost],
        validated_if: params[:assumption][:experiment_outcome],
    )
  end

  def record_event
    EventRecorderWorker.perform_async(@assumption.product_id, 10, current_user.id, @assumption.id)
  end


end
