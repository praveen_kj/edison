class ExperimentsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_assumption, only: [:create]


  def create
    assumption = active_product.assumptions.find(experiment_params[:assumption_id])
    @experiment = assumption.experiments.build(experiment_params.merge(created_by: current_user.id))
    respond_to do |format|
      if @experiment.save
        format.html { redirect_to :back }
        format.js
        record_event(12)
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    @experiment = Experiment.find(params[:id])

    if active_product.assumptions.find(@experiment.assumption_id).nil?
      redirect_to root_url
    else
      respond_to do |format|
        @experiment.destroy
        format.html { redirect_to :back }
        format.js
      end
    end

  end


  def update
    @experiment = Experiment.find(params[:id])
    respond_to do |format|
      if @experiment.update_attributes(experiment_params)
        format.js { respond_with_active_edit @experiment, @field }
        format.html { redirect_to :back }
        record_event(13) if @field == :status && @sub_task.status
      else
        format.js
        format.html { redirect_to :back }
      end
    end
  end

  private

  def experiment_params
    params.require(:experiment).permit(:assumption_id,
                                       :description,
                                       :cost,
                                       :validated_if,
                                       :stage,
                                       :result_note)
  end

  def correct_assumption
    redirect_to root_url if active_product.assumptions.where(id: experiment_params[:assumption_id]).first.nil?
  end


  def record_event(event_type)
    assumption = @experiment.assumption
    EventRecorderWorker.perform_async(assumption.product_id, event_type, current_user.id, assumption.id, {experiment_id: @experiment.id})
  end

end
