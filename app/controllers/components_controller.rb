class ComponentsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_feature, only: [:create]
  before_action :correct_component, only: [:update, :destroy, :feedbacks]


  def feedbacks
    @feedbacks = @component.feedbacks
    @view = "Feedback for #{@component.name}"
    @url = component_feedbacks_path(@component.id)
    @tab_title = "Feedback for #{@component.name}"
  end


  def create
    @feature = beachhead.features.where(id: params[:component][:feature_id]).first
    @component = @feature.components.build(component_params)
    respond_to do |format|
      if @component.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    @component = Component.find(params[:id])
    respond_to do |format|
      @component.destroy
      format.html { redirect_to :back }
      format.js
    end

  end


  def update
    @component = Component.find(params[:id])
    respond_to do |format|
      if @component.update_attributes(component_params)
        format.json { respond_with_bip(@component) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @component, @field }
        expire_fragment([beachhead, 'features'])
      else
        format.json { respond_with_bip(@component) }
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  private

  def component_params
    params.require(:component).permit(:name, :customers_want, :importance, :feature_id, :built)
  end


  def correct_feature
    redirect_to root_url if beachhead.features.where(id: component_params[:feature_id]).first.nil?
  end

  def correct_component
    @component = Component.find(params[:id])
    redirect_to root_url  unless @component.feature.business == beachhead
  end

end
