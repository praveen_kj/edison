class FeaturesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @feature = beachhead.features.build(feature_params)
    respond_to do |format|
      if @feature.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def get_sub_features
    @component_details = beachhead.features.where(id: params[:feature_id]).first.components.pluck(:id, :name)
  end

  def destroy

    respond_to do |format|
      @feature.destroy
      format.html { redirect_to :back }
      format.js
      record_event
    end

  end


  def update
    @params = params[:feature]
    respond_to do |format|
      if @feature.update_attributes(feature_params)
        format.json { respond_with_bip(@feature) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @feature, @field }
        record_event
      else
        format.json { respond_with_bip(@feature) }
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  private

  def create_child_resources

    ['How it works', 'Sub feature', 'Benefit', 'Detriment'].each_with_index do |item, index|
      3.times do |i|
        @feature.benefits.create(:benefit_name => item+" #{i+1}", group: index)
      end
    end

    @feature.iterations.create(:iteration_name => "Reason for Iteration")


  end


  def feature_params
    params.require(:feature).permit(:target_build_date,
                                    :feature_name,
                                    :effort,
                                    :status,
                                    :mocked_up,
                                    :decision_notes,
                                    :iterated,
                                    :customer_wanting_feature,
                                    :importance_rank,
                                    :persona_detail_id,
                                    :description
    )
  end


  def correct_business
    @feature = beachhead.features.find_by(id: params[:id])
    if @feature.nil?
      redirect_to root_url
    else
      @obj = {id: @feature.id, obj_name: @feature.class.to_s.underscore}
    end
  end

  def record_event
    b = @feature.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 7, {business_id: b.id})
  end

end
