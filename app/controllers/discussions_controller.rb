class DiscussionsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]

  before_action :validate_user, only: [:new, :show]


  def load_more
    @discussions = active_product.discussions.includes(:user).offset(params[:offset].to_i).limit(15)
  end

  def index
    @view = 'discussions'
    @url = '/discussions'
    @tab_title = 'Discussions'
  end

  def new
    @new_sub_tool_title = 'New Discussion'
  end

  def show
    @sub_tool_resource = active_product.discussions.find(params[:id])
    @sub_tool_title = @sub_tool_resource.title
    respond_to do |format|
      format.html
      format.js
    end
  end


  def create

    @discussion = active_product.discussions.build(discussion_params.merge(user_id: current_user.id))
    respond_to do |format|
      if @discussion.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy
    respond_to do |format|
      @discussion.destroy
      format.js
    end
  end


  def update
    respond_to do |format|
      if @discussion.update_attributes(discussion_params)
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @discussion, @field }
      else
        format.html { redirect_to :back }
        format.js
      end
    end
  end

  private

  def discussion_params
    params.require(:discussion).permit(:title, :subject, :step_no)
  end

  def correct_business
    @discussion = active_product.discussions.find_by(id: params[:id])
    redirect_to root_url if @discussion.nil?
  end


  def record_event(event_type = 2)
    EventRecorderWorker.perform_async(@discussion.product_id, event_type, @discussion.user_id, @discussion.id)
  end

end
