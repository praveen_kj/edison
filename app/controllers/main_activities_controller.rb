class MainActivitiesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @main_activity = beachhead.main_activities.build(main_activity_params)
    respond_to do |format|
      if @main_activity.save
        create_child_resources
        format.html { redirect_to :back }
        format.js
        # expire_fragment([beachhead, 'product_spec'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def show
    item =  MainActivity.where(id: params[:id]).first
    if item && current_user.has_access_to?(item)
      @sub_tool_resource = item
      @sub_tool_title = @sub_tool_resource.main_activity_name
      respond_to do |format|
        format.html
        format.js
      end
    else
      render status: 404
    end
  end

  def destroy

    respond_to do |format|
      @main_activity.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([beachhead, 'product_spec'])
    end

  end


  def update
    respond_to do |format|
      if @main_activity.update_attributes(main_activity_params)
        format.json { respond_with_bip(@main_activity) }
        format.html { redirect_to :back }
        format.js {respond_with_active_edit @main_activity, @field}
        active_product.nullify_stage if @main_activity.release || @main_activity.sales_target
      else
        format.json { respond_with_bip(@main_activity) }
        format.html { redirect_to :back }
        format.js {respond_with_active_edit @main_activity, @field}
      end
    end
  end

  private

  def create_child_resources
    3.times do |i|
      @main_activity.key_activities.create(:activity_name => "Key Activity #{i+1}")
    end
  end


  def main_activity_params
    params.require(:main_activity).permit(:target_date,
                                          :effort,
                                          :capital,
                                          :main_activity_name,
                                          :notes,
                                          :stage,
                                          :release_version, :release_notes, :release, :sales_target

    )
  end


  def correct_business
    @main_activity = beachhead.main_activities.find_by(id: params[:id])
    if @main_activity.nil?
      redirect_to root_url
    else
      @obj = {id: @main_activity.id, obj_name: @main_activity.class.to_s.underscore}
    end
  end

end
