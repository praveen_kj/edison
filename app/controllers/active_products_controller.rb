class ActiveProductsController < ApplicationController

  before_action :authenticate_user!, only: [:create, :destroy]
  include ApplicationHelper

  before_action :validate_access, only: [:update]

  def update
    @old_active_product = Product.exists?(id: current_user.active_product_id) ? Product.find(current_user.active_product_id) : nil
    respond_to do |format|
      if current_user.update_attributes(active_product_params)
        format.html {redirect_to home_path}
      end
    end
  end

  private

  def active_product_params
    params.require(:user).permit(:active_product_id)
  end

  def validate_access
    requested_product_id = (params[:user][:active_product_id].to_i)
    unless current_user.products.pluck(:id).include?(requested_product_id)  || Membership.where(member_email: current_user.email).pluck(:product_id).include?(requested_product_id)
      redirect_to app_path(view:'beachhead')
    end
  end





end
