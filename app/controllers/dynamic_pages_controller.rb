class DynamicPagesController < ApplicationController

  before_action :validate_user
  before_action :validate_product, except: [:products]
  before_action :validate_beachhead, only: [:home, :toolkit, :strategy_toolkit, :customer_insights, :milestones, :make]
  include ApplicationHelper

  def markets
    @view = 'markets'
    @url = markets_path
  end

  def welcome
    @view = 'welcome'
    @url = welcome_path
  end

  def four_pillars
    @view = 'four-pillars'
    @url = four_pillars_path
  end

  def top_pitfalls
    @view = 'top-pitfalls'
    @url = top_pitfalls_path
  end

  def home
    @view = 'home'
    @url = home_path
  end

  def onboarding
    render layout: 'onboarding'
  end

  def products
    @view = 'products'
    @url = products_dashboard_path
  end


  def settings
    @view = 'settings'
    @url = settings_path
  end

  def chat
    @view = 'chat'
    @url = chat_path
  end

  def make
    @view = 'make'
    @url = make_path
  end

  def toolkit
    @view = 'toolkit'
    @url = toolkit_path
  end

  def strategy_toolkit
    @view = 'strategy-toolkit'
    @url = strategy_toolkit_path
  end

  def customer_insights
    @view = 'customer-insights'
    @url = customer_insights_path
  end

  def milestones
    @view = 'milestones'
    @url = milestones_path
  end

  def team
    @view = 'team'
    @url = team_path
  end

  def refresh_activity_feed

  end


  private

  def validate_beachhead
    redirect_to onboarding_path unless beachhead
  end

end
