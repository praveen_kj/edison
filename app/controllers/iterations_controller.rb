class IterationsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @feature = Feature.find(params[:iteration][:feature_id])
    @iteration = @feature.iterations.build(iteration_params)
    respond_to do |format|
      if @iteration.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([beachhead, 'product_spec'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @iteration.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([beachhead, 'product_spec'])
    end

  end


  def update
    respond_to do |format|
      if @iteration.update_attributes(iteration_params)
        format.json { respond_with_bip(@iteration) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @iteration, @field }
      else
        format.json { respond_with_bip(@iteration) }
        format.html { redirect_to :back }
        format.js {  }
      end
    end
  end

  private

  def iteration_params
    params.require(:iteration).permit(:iteration_name, :target_date, :feature_id)
  end


  def correct_business
    @iteration = Iteration.find_by(id: params[:id])
    if @iteration.nil?
      redirect_to root_url
    else
      @obj = {id: @iteration.id, obj_name: @iteration.class.to_s.underscore}
    end
  end



end
