class UserPrioritiesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def rank
    user_priorities = beachhead.user_priorities.all
    @user_priority = user_priorities.where(id: params[:user_priority][:id]).first
    @prev_rank = @user_priority.rank
    @currently_ranked_priority = user_priorities.where(rank: params[:user_priority][:rank]).first
    @currently_ranked_priority.try(:strip_rank)
    @user_priority.update_attributes(rank: params[:user_priority][:rank])
  end

  def create

    @user_priority = beachhead.user_priorities.build(user_priority_params)
    respond_to do |format|
      if @user_priority.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([beachhead, 'product_spec'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @user_priority.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([beachhead, 'product_spec'])
    end

  end


  def update
    respond_to do |format|
      if @user_priority.update_attributes(user_priority_params)
        format.json { respond_with_bip(@user_priority) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @user_priority, @field }
        # expire_fragment([beachhead, 'product_spec'])
      else
        format.json { respond_with_bip(@user_priority) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @user_priority, @field }
      end
    end
  end

  private

  def user_priority_params
    params.require(:user_priority).permit(:priority,
                                          :as_is_state,
                                          :poss_state,
                                          :rank,
                                          :tagline)
  end


  def correct_business
    @user_priority = beachhead.user_priorities.find_by(id: params[:id])
    if @user_priority.nil?
      redirect_to root_url
    else
      @obj = {id: @user_priority.id, obj_name: @user_priority.class.to_s.underscore}
    end
  end
end
