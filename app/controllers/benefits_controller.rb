class BenefitsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @feature = Feature.find(params[:benefit][:feature_id])
    @benefit = @feature.benefits.build(benefit_params)
    respond_to do |format|
      if @benefit.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([beachhead, 'product_spec'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @benefit.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([beachhead, 'product_spec'])
    end

  end


  def update
    respond_to do |format|
      if @benefit.update_attributes(benefit_params)
        format.json { respond_with_bip(@benefit) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @benefit, @field }
        expire_fragment([beachhead, 'features'])
      else
        format.json { respond_with_bip(@benefit) }
        format.html { redirect_to :back }
        format.js {  }
      end
    end
  end

  private

  def benefit_params
    params.require(:benefit).permit(:benefit_name, :group, :priority, :feature_id)
  end


  def correct_business
    @benefit = Benefit.find_by(id: params[:id])
    if @benefit.nil?
      redirect_to root_url
    else
      @obj = {id: @benefit.id, obj_name: @benefit.class.to_s.underscore}
    end
  end


end
