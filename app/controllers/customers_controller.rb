class CustomersController < ApplicationController
  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @customer = beachhead.customers.build(customer_params)
    respond_to do |format|
      if @customer.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @customer.destroy
      record_event
      format.html { redirect_to :back }
      format.js
    end

  end


  def update
    respond_to do |format|
      if @customer.update_attributes(customer_params)
        format.json { respond_with_bip(@customer) }
        format.js { respond_with_active_edit @customer, @field }
        format.html { redirect_to :back }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  private


  def customer_params
    params.require(:customer).permit(:name,
                                     :sales_strategy_id,
                                     :reach_out_date,
                                     :notes,
                                     :stage

    )
  end


  def correct_business
    @customer = beachhead.customers.find_by(id: params[:id])
    if @customer.nil?
      redirect_to root_url
    end
  end

  def record_event
    b = @customer.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 15, {business_id: b.id})
  end


end
