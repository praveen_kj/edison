class UserParametersController < ApplicationController

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  before_action :set_user_parameter, only: [:show, :edit, :update, :destroy, :bus_id]


  def create
    @user_parameter = current_user.build_user_parameter(user_parameter_params)
    respond_to do |format|
      if @user_parameter.save
        format.html { redirect_to app_path(view: 'home') }
        format.js { 'create.js' }
        current_user.update_attributes(profile_completed_at: @user_parameter.created_at)
      else
        format.html { redirect_to root_path }
        flash[:error] = @user_parameter.errors.full_messages
        format.js {}
      end
    end
  end


  def destroy
    respond_to do |format|
      @user_parameter.destroy
      format.html { redirect_to :back }
      format.js
    end
  end


  def edit
    @user_parameter = UserParameter.find(params[:id])
  end

  def update

    @user_parameter = UserParameter.find(params[:id])
    respond_to do |format|
      if @user_parameter.update_attributes(user_parameter_params)
        format.json { respond_with_bip(@user_parameter) }
        format.js { respond_with_active_edit(@user_parameter, @field) }
        format.html { redirect_to :back }
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  def account_settings

  end

  def favourite
    fav_step = params[:user_parameter][:favourite]
    if (1..20).include? fav_step.to_i
      @user_parameter = current_user.user_parameter
      if @user_parameter.favourites.nil?
        @user_parameter.update_attributes(favourites: [fav_step])
      else
        favs = @user_parameter.favourites
        unless favs.include? fav_step
          @new_step = fav_step
          @old_step = favs.count > 2 ? favs.last : 0
          favs.count > 2 ? favs.unshift(fav_step).pop : favs.unshift(fav_step)
          @user_parameter.favourites = favs
          @user_parameter.favourites_will_change!
          @user_parameter.save
        end
      end
    end

  end

  private

  #
  # def safely_add(array, new_step)
  #   array.shift if array.count == 3
  #   array.unshift new_step
  #   puts "{#{array.split.join(',')}}"
  #   "{#{array.split.join(',')}}"
  # end

  def user_parameter_params
    params.require(:user_parameter).permit(:mission,
                                           :company_name,
                                           :title,
                                           :user_name,
                                           :firm_name,
                                           :target_date,
                                           :subscription,
                                           :fav1, :fav2, :fav3)
  end


  def correct_user
    @user_parameter = current_user.user_parameters.find_by(id: params[:id])
    redirect_to root_url if @user_parameter.nil?
  end

  def set_user_parameter
    @user_parameter = UserParameter.find(params[:id])
  end

end
