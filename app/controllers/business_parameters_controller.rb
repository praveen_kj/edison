class BusinessParametersController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]




  def create

    business = Business.find(params[:business_parameter][:business_id])
    @business_parameter = business.business_parameters.build(business_parameter_params)
    respond_to do |format|
      if @business_parameter.save
        format.html { redirect_to :back }
        format.js
         # expire_fragment([beachhead, 'business_parameter'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end


  end

  def destroy
    respond_to do |format|
      @business_parameter.destroy
      format.js
      # expire_fragment([beachhead, 'business_parameter'])
    end
  end


  def update
    respond_to do |format|
      if @business_parameter.update_attributes(business_parameter_params)
        format.json { respond_with_bip(@business_parameter) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @business_parameter, @field }
        # expire_fragment([beachhead, 'business_parameter'])
      else
        format.html { redirect_to :back }
        format.json { respond_with_bip(@business_parameter) }
        format.js
      end
    end
  end

  private

  def business_parameter_params
    params.require(:business_parameter).permit(:parameter_name, :group)
  end


  def correct_business
    @business_parameter = beachhead.business_parameters.find_by(id: params[:id])
    if @business_parameter.nil?
      redirect_to root_url
    else
      @obj = {id: @business_parameter.id, obj_name: @business_parameter.class.to_s.underscore}
    end
  end

end
