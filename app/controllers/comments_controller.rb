class CommentsController < ApplicationController

  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_parent, only: [:create]

  def create_via_email



  end


  def create
    @comment = @parent.comments.build(comment_params.merge(user_id: current_user.id, product_id: active_product.id))
    respond_to do |format|
      if @comment.save
        format.html { redirect_to :back }
        format.js
        # record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  def destroy
    @comment = current_user.comments.find(params[:id])
    respond_to do |format|
      @comment.destroy
      format.html { redirect_to :back }
      format.js
    end
  end


  def update
    @comment = current_user.comments.find(params[:id])
    respond_to do |format|
      if @comment.update_attributes(comment_params)
        format.js { respond_with_active_edit @comment, @field }
        format.html { redirect_to :back }
      else
        format.js
        format.html { redirect_to :back }
      end
    end
  end

  private

  def set_parent
    parent_key = params.keys.last.to_s
    @parent = instance_eval(parent_key[0..-4].camelize).find(params[parent_key])
  end

  def comment_params
    params.require(:comment).permit(:comment_text, :like)
  end

  # def comment_event_type(parent)
  #   {
  #       Discussion => 3,
  #       UserTask => 5,
  #       Note => 8,
  #       Assumption => 11,
  #   }[parent.class]
  # end
  #
  # def record_event
  #   parent = @comment.discussion || @comment.user_task || @comment.note || @comment.customer_feedback
  #   event_type =  comment_event_type(parent)
  #   EventRecorderWorker.perform_async(parent.product_id, event_type, @comment.user_id, parent.id, {comment_id: @comment.id})
  # end


end
