class EndusersController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]

  after_action :expire_cache, only: [:create, :update, :destroy]


  def create

    @enduser = beachhead.endusers.build(enduser_params)
    respond_to do |format|
      if @enduser.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end


  end

  def destroy
    respond_to do |format|
      @enduser.destroy
      format.js
    end
  end


  def update
    respond_to do |format|
      if @enduser.update_attributes(enduser_params)
        format.json { respond_with_bip(@enduser) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @enduser, @field }
        record_event if @enduser.previous_changes.present?
      else
        format.html { redirect_to :back }
        format.json { respond_with_bip(@enduser) }
        format.js
      end
    end
  end

  private

  def enduser_params
    params.require(:enduser).permit(:parameter, :response, :group)
  end


  def correct_business
    @enduser = beachhead.endusers.find_by(id: params[:id])
    if @enduser.nil?
      redirect_to root_url
    else
      @obj = {id: @enduser.id, obj_name: @enduser.class.to_s.underscore}
    end
  end

  def expire_cache
    expire_fragment([beachhead, 'endusers'])
    expire_fragment([beachhead, "endusers#{@enduser.group}"])
  end

  def record_event
    b = @enduser.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 3, {business_id: b.id})
  end


end
