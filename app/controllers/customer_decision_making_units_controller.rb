class CustomerDecisionMakingUnitsController < ApplicationController

  include ApplicationHelper

  before_action :correct_business, only: [:update]

  def update
    respond_to do |format|
      if @customer_decision_making_unit.update_attributes(customer_decision_making_unit_params)
        format.json { respond_with_bip(@customer_decision_making_unit) }
        format.html { redirect_to :back }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def customer_decision_making_unit_params
    params.require(:customer_decision_making_unit).permit(:entity_name,
                                                          :fears,
                                                          :motivations,
                                                          :expectations,
                                                          :offering,
                                                          :preferred_alternatives
    )
  end


  def correct_business
    @customer_decision_making_unit = beachhead.customer_decision_making_units.find_by(id: params[:id])
    redirect_to root_url if @customer_decision_making_unit.nil?
  end

  def record_event
    b = @customer_decision_making_unit.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 13, {business_id: b.id})
  end


end
