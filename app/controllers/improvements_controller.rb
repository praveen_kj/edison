class ImprovementsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @next_customer = NextCustomer.find(params[:improvement][:next_customer_id])
    @improvement = @next_customer.improvements.build(improvement_params)
    respond_to do |format|
      if @improvement.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([next_customer, 'improvement'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @improvement.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([next_customer, 'improvement'])
    end

  end


  def update
    @params = params[:improvement]
    respond_to do |format|
      if @improvement.update_attributes(improvement_params)
        format.json { respond_with_bip(@improvement) }
        format.js { respond_with_active_edit @improvement, @field }
        format.html { redirect_to :back }
        # expire_fragment([next_customer, 'improvement'])

      else
        format.html { redirect_to :back }
        format.json { respond_with_bip(@improvement) }
      end
    end
  end

  private

  def improvement_params
    params.require(:improvement).permit(:target_date, :improvement_name, :revenue_increase)
  end


  def correct_business
    @improvement = Improvement.find_by(id: params[:id])
    if @improvement.nil?
      redirect_to root_url
    else
      @obj = {id: @improvement.id, obj_name: @improvement.class.to_s.underscore}
    end
  end
end
