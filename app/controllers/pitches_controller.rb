class PitchesController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business,   only: :destroy

  def create

    @pitch = beachhead.pitches.build(pitch_params)
    respond_to do |format|
      if @pitch.save
        format.html{ redirect_to :back}
        format.js
      else
        format.html{redirect_to :back}
        format.js {  }
      end
    end

  end

  def destroy

    respond_to do |format|
      @pitch.destroy
      format.html{ redirect_to :back}
      format.js
    end

  end




  def update

    @pitch = Pitch.find(params[:id])
    respond_to do |format|
      if @pitch.update_attributes(pitch_params)
        format.json {respond_with_bip(@pitch)}
        format.html{redirect_to :back}
      else
        format.html{redirect_to :back}
        format.json { render :json => @pitch.errors.full_messages, :status => :unprocessable_entity }
      end
    end
  end

  private

  def pitch_params
    params.require(:pitch).permit(:pitch_content)
  end


  def correct_business
    @pitch = beachhead.pitches.find_by(id: params[:id])
    redirect_to root_url if @pitch.nil?
  end



end
