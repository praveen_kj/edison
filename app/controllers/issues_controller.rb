class IssuesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_component, only: [:create]
  before_action :correct_issue, only: [:update, :destroy]


  def create
    @issue = @component.issues.build(issue_params)
    respond_to do |format|
      if @issue.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    @issue = Issue.find(params[:id])
    respond_to do |format|
      @issue.destroy
      format.html { redirect_to :back }
      format.js
    end

  end


  def update
    @issue = Issue.find(params[:id])
    respond_to do |format|
      if @issue.update_attributes(issue_params)
        format.json { respond_with_bip(@issue) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @issue, @field }
      else
        format.json { respond_with_bip(@issue) }
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  private

  def issue_params
    params.require(:issue).permit(:title, :man_hours, :component_id, :stage)
  end


  def correct_component
    @component = Component.where(id: issue_params[:component_id]).first
    redirect_to root_url unless @component.feature.business == beachhead
  end

  def correct_issue
    @issue = Issue.find(params[:id])
    redirect_to root_url  unless @issue.component.feature.business == beachhead
  end

end
