class FinancialsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]

  def drill_down
    @drill = params[:dd]
    if beachhead.financials.pluck(:duration).include?(0)
      @duration = true
    elsif beachhead.financials.pluck(:no_of_customers).include?(0)
      @no_of_customers = true
    end
  end

  def create

    @financial = beachhead.financials.build(financial_params)
    respond_to do |format|
      if @financial.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @financial.destroy
      format.html { redirect_to :back }
      format.js
    end

  end


  def update
    respond_to do |format|
      if @financial.update_attributes(financial_params)
        format.json { respond_with_bip(@financial) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @financial, @field }
        record_event if @financial.previous_changes.present?
      else
        format.html { redirect_to :back }
        format.json { render :json => {:status => "error"}, :status => 422 }
        format.js { respond_with_active_edit @financial, @field }
      end
    end
  end

  private

  def financial_params
    params.require(:financial).permit(:customer_segment,
                                      :no_of_customers,
                                      :start_date,
                                      :end_date,
                                      :relationship_duration,
                                      :one_time_revenues,
                                      :monthly_revenues,
                                      :upsell_revenues,
                                      :production_salaries,
                                      :production_material,
                                      :production_overheads,
                                      :sales_staff_salaries,
                                      :marketing_cost,
                                      :distributor_commission,
                                      :discounts,
                                      :capex,
                                      :rentals,
                                      :legal,
                                      :others,
                                      :ongoing_price,
                                      :business_model,
                                      :fair_value,
                                      :when_to_charge,
                                      :key_pricing_pt,
                                      :competitors_price,
                                      :ongoing_price,
                                      :product_price)
  end


  def correct_business
    @financial = beachhead.financials.find_by(id: params[:id])
    redirect_to root_url if @financial.nil?
  end


  def record_event
    b = @financial.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 4, {business_id: b.id})
  end

end
