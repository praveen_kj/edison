class SubComponentsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_component, only: [:create]
  before_action :correct_sub_component, only: [:update, :destroy]


  def create
    @sub_component = @component.sub_components.build(sub_component_params)
    respond_to do |format|
      if @sub_component.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    @sub_component = SubComponent.find(params[:id])
    respond_to do |format|
      @sub_component.destroy
      format.html { redirect_to :back }
      format.js
    end

  end


  def update
    @sub_component = SubComponent.find(params[:id])
    respond_to do |format|
      if @sub_component.update_attributes(sub_component_params)
        format.json { respond_with_bip(@sub_component) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @sub_component, @field }
      else
        format.json { respond_with_bip(@sub_component) }
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  private

  def sub_component_params
    params.require(:sub_component).permit(:name, :customers_want, :importance, :component_id, :stage, :man_hours)
  end


  def correct_component
    @component = Component.where(id: sub_component_params[:component_id]).first
    redirect_to root_url unless @component.feature.business == beachhead
  end

  def correct_sub_component
    @sub_component = SubComponent.find(params[:id])
    redirect_to root_url  unless @sub_component.component.feature.business == beachhead
  end

end
