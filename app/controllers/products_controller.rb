class ProductsController < ApplicationController

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  before_action :set_product, only: [:update, :destroy]

  include ApplicationHelper
  include ActiveEditViewHelper

  def index

  end

  def update_outdoor_visits
    if params[:visited_today] == 'true'
      active_product.update_outdoor_visit(true)

    elsif params[:visited_today] == 'false'
      active_product.update_outdoor_visit(false)

    end
  end

  def update_stage
    curr_stage = params[:product][:curr_stage].to_i
    if (1..4).include? curr_stage
      active_product.update_attributes(current_stage: curr_stage)
    end
    respond_to do |format|
      format.text { render :status => 202, :text => 'ok' }
    end
  end


  def create
    @product = current_user.products.build(product_params)
    respond_to do |format|
      if @product.save
        current_user.update_attributes(active_product_id: @product.id)
        format.js
        format.html { redirect_to onboarding_path }
      else
        format.js {}
        format.html { redirect_to :back }
      end
    end

  end

  def destroy
    respond_to do |format|
      @product.destroy
      format.html { redirect_to :back }
      format.js
    end
  end


  def update
    @product.assign_attributes(product_params)
    respond_to do |format|
      if @product.save(validate: false)
        format.js { respond_with_active_edit @product, @field }
      else
        format.js
      end
    end
  end


  private

  def product_params
    params.require(:product).permit(
        :product_name, :enduser_base, :product_purpose, :success_definition, :owner_role, :stage_at_start,
        :man_hours_per_day, :current_stage, :last_visited_step, :target_release_date, :default_canvas,
        :release_version, :release_note_read, :welcome_msg_read

    )
  end


  def correct_user
    @product = current_user.products.find_by(id: params[:id])
    redirect_to root_url if @product.nil?
  end

  def set_product
    @product = Product.find(params[:id])
  end


end
