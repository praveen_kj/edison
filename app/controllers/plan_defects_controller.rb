class PlanDefectsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @pricing_plan = PricingPlan.find(params[:plan_defect][:pricing_plan_id])
    @plan_defect = @pricing_plan.plan_defects.build(plan_defect_params)
    respond_to do |format|
      if @plan_defect.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([pricing_plan, 'plan_defect'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @plan_defect.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([pricing_plan, 'plan_defect'])
    end

  end


  def update
    @params = params[:plan_defect]
    respond_to do |format|
      if @plan_defect.update_attributes(plan_defect_params)
        format.json { respond_with_bip(@plan_defect) }
        format.js {respond_with_active_edit @plan_defect, @field}
        format.html { redirect_to :back }
        # expire_fragment([pricing_plan, 'plan_defect'])

      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def plan_defect_params
    params.require(:plan_defect).permit(:defect_name)
  end


  def correct_business
    @plan_defect = PlanDefect.find_by(id: params[:id])
    if @plan_defect.nil?
      redirect_to root_url
    else
      @obj = {id: @plan_defect.id, obj_name: @plan_defect.class.to_s.underscore}
    end
  end

end
