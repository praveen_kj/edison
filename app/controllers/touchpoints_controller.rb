class TouchpointsController < ApplicationController
  include ApplicationHelper

  before_action :correct_business, only: [:update]


  def update
    respond_to do |format|
      if @touchpoint.update_attributes(touchpoint_params)
        format.js { respond_with_active_edit(@touchpoint, @field) }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def touchpoint_params
    params.require(:touchpoint).permit(:intelligence,
                                       :openness,
                                       :conscientiousness,
                                       :kindness,
                                       :stability,
                                       :extraversion,
                                       :people_involved,
                                       :end_user_actions,
                                       :things_needed,
                                       :con_time,
                                       :agg_time,
                                       :target_date,
                                       :duration,
                                       :hurdles_faced,
                                       :expected_conversion_rate,
                                       :expected_users,
                                       :motivation_level,
                                       :ease_of_doing,
                                       :possible_intervention,
                                       :current_week_metrics,
                                       :past_week_metrics,
    )
  end


  def correct_business
    @touchpoint = beachhead.touchpoints.find_by(id: params[:id])
    redirect_to root_url if @touchpoint.nil?
  end

  def record_event
    b = @touchpoint.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 6, {business_id: b.id})
  end


end
