class Users::PasswordsController < Devise::PasswordsController

  prepend_before_filter :require_no_authentication
  layout 'home'


  # GET /resource/password/new
  def new
    super
  end

  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      sign_out resource
      respond_with({}, location: after_sending_reset_password_instructions_path_for(resource_name))
    else
      flash[:notice] = "Your email was not found!"
      respond_with({}, :location => forgot_password_path)
    end
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  def edit
    super
  end

  # PUT /resource/password
  # def update
  #   super
  # end

  # protected

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  def after_sending_reset_password_instructions_path_for(resource_name)
    root_path
  end

  def require_no_authentication
    assert_is_devise_resource!
    return unless is_navigational_format?
    no_input = devise_mapping.no_input_strategies

    authenticated = if no_input.present?
                      args = no_input.dup.push scope: resource_name
                      warden.authenticate?(*args)
                    else
                      warden.authenticated?(resource_name)
                    end

    # if authenticated && resource = warden.user(resource_name)
    #   flash[:alert] = I18n.t("devise.failure.already_authenticated")
    #   redirect_to after_sign_in_path_for(resource)
    # end
  end


end
