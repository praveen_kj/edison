class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_sign_up_params, only: [:create]
  before_filter :configure_account_update_params, only: [:update]

  include ApplicationHelper
  include UsersHelper

  layout 'home', only: [:new, :create]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    build_resource(sign_up_params)
    resource_saved = resource.save
    session[:un_submitted] = params[:user][:username]
    session[:em_submitted] = params[:user][:email]
    yield resource if block_given?
    if resource_saved

      ### perform before responding

      resource.create_user_parameter(favourites: [1, 2], user_name: params[:user][:username])
      resource.set_trial_expiry(TRIAL_PERIOD.from_now.utc)
      resource.perform_post_sign_up_process(params[:user][:referred_by])

      WelcomeMailWorker.perform_async(resource.id)

      ### perform before responding

      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        # respond_with resource, location: after_sign_up_path_for(resource)
        respond_to do |f|
          f.html { redirect_to after_sign_up_path }
          f.js
        end
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      @validatable = devise_mapping.validatable?
      if @validatable
        @minimum_password_length = resource_class.password_length.min
      end
      session[:signup_errors] = resource.errors.full_messages
      redirect_to :back
    end

  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    @updated_info = params[:user][:updated_info]
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    resource.skip_reconfirmation!
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
            :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, bypass: true
      # respond_with resource, location: after_update_path_for(resource)
      respond_to do |f|
        f.html { redirect_to :back }
        f.js
      end
    else
      @errors = resource.errors
      # clean_up_passwords resource
      # respond_with resource
      respond_to do |f|
        f.html { redirect_to :back }
        f.js
      end
    end
  end

  # DELETE /resource
  def destroy
    super
  end

  def delete_account

  end

  def update_missing_data
    u_params = params.require(:user).permit(:username) if params[:user]
    p_params = params.require(:product).permit(:product_name, :owner_role, :product_purpose) if params[:product]
    current_user.update_attributes(u_params) if u_params.present?
    active_product.update_attributes(p_params) if p_params.present?
    respond_to do |f|
      f.js
    end
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  def users_time_zone
    best_match = timezones_with_offset.select { |k, v| v.include?(params[:ptz]) }.first
    current_user.update_attributes(preferred_time_zone: best_match[0], preferred_time: 7) if best_match.present?
    render text: :ok
  end


  protected

  # You can put the params you want to permit in the empty array.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :referred_by) }
  end

  # You can put the params you want to permit in the empty array.
  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password, :preferred_time_zone, :receive_sitrep, :preferred_time) }
  end

  def update_resource(resource, params)
    puts params
    if params.keys.include?('email') || params.keys.include?('password') || params.keys.include?('username')
      resource.update_with_password(params)
    else
      resource.update_without_password(params)
    end
  end


  # The path used after sign up.
  def after_sign_up_path_for(resource)
    onboarding_path
  end

  def after_sign_up_path
    membership = resource.products_where_member
    if membership.present?
      resource.update_attributes(active_product_id: membership[0])
      home_path
    else
      products_dashboard_path
    end
  end


  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  # def credit_the_referrer(referrer_code)
  #   referrer = User.find_by(referrer_code: referrer_code)
  #   if referrer
  #     ref_count = referrer.referral_count += 1
  #     if referrer.active_trial_user?
  #       referrer.trial_expiry_date += PER_REFERRAL_REWARD if ref_count <= MAX_REFERRALS
  #     elsif referrer.expired_trial_user?
  #       referrer.trial_expiry_date = PER_REFERRAL_REWARD.from_now.utc
  #     end
  #     referrer.save
  #     referrer.send_trial_extension_mail
  #   end
  # end


end
