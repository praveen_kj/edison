class MessagesController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]

  def index
    @messages = active_product.messages.last
  end

  def create

    @message = active_product.messages.build(message_params.merge(user_id: current_user.id))

    respond_to do |format|
      if @message.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end


  private

  def message_params
    params.require(:message).permit(:content)
  end

end
