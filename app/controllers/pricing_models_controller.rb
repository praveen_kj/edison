class PricingModelsController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @pricing_model = beachhead.pricing_models.build(pricing_model_params)
    respond_to do |format|
      if @pricing_model.save
        create_child_resources
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def show
    item =  PricingModel.where(id: params[:id]).first
    if item && current_user.has_access_to?(item)
      @sub_tool_resource = item
      @sub_tool_title = @sub_tool_resource.model_name
      respond_to do |format|
        format.html
        format.js
      end
    else
      render status: 404
    end
  end

  def destroy

    respond_to do |format|
      @pricing_model.destroy
      format.html { redirect_to :back }
      format.js
      record_event
    end

  end


  def update
    @params = params[:pricing_model]
    respond_to do |format|
      if @pricing_model.update_attributes(pricing_model_params)
        format.json { respond_with_bip(@pricing_model) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @pricing_model, @field }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def create_child_resources
    3.times do |i|
      @pricing_model.innovations.create(:innovation_name => "Innovation #{i+1}")
    end
  end


  def pricing_model_params
    params.require(:pricing_model).permit(:model_name,
                                          :when_to_share,
                                          :willingness_to_buy,
                                          :favourable_pricing,
                                          :vendor_share_protected,
                                          :competitor_adoption,
                                          :allows_dist_incentives,
                                          :preference,
                                          :willingness_to_buy_note,
                                          :favourable_pricing_note,
                                          :vendor_share_protected_note,
                                          :competitor_adoption_note,
                                          :allows_dist_incentives_note,
                                          :rank, :stage)
  end


  def correct_business
    @pricing_model = beachhead.pricing_models.find_by(id: params[:id])
    redirect_to root_url if @pricing_model.nil?
    @obj = {id: @pricing_model.id, obj_name: @pricing_model.class.to_s.underscore}
  end

  def record_event
    b = @pricing_model.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 11, {business_id: b.id})
  end



end
