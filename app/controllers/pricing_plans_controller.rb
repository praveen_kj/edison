class PricingPlansController < ApplicationController
  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @pricing_plan = beachhead.pricing_plans.build(pricing_plan_params)
    respond_to do |format|
      if @pricing_plan.save
        create_child_resources
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def show
    item =  PricingPlan.where(id: params[:id]).first
    if item && current_user.has_access_to?(item)
      @sub_tool_resource = item
      @sub_tool_title = @sub_tool_resource.plan_name
      respond_to do |format|
        format.html
        format.js
      end
    else
      render status: 404
    end
  end

  def destroy

    respond_to do |format|
      @pricing_plan.destroy
      format.html { redirect_to :back }
      format.js
      record_event
    end

  end


  def update
    @params = params[:pricing_plan]
    respond_to do |format|
      if @pricing_plan.update_attributes(pricing_plan_params)
        format.json { respond_with_bip(@pricing_plan) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @pricing_plan, @field }
        record_event
      else
        format.json { respond_with_bip(@pricing_plan) }
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  private

  def create_child_resources
    3.times do |i|
      @pricing_plan.attractions.create(:attraction_name => "Attraction #{i+1}")
      @pricing_plan.plan_defects.create(:defect_name => "Defect #{i+1}")
    end
  end

  def pricing_plan_params
    params.require(:pricing_plan).permit(:plan_name,
                                         :preference,
                                         :model_adopted,
                                         :cust_group,
                                         :product_price,
                                         :invoice_timing,
                                         :billing_frequency,
                                         :competitor_price_lb,
                                         :competitor_price_ub,
                                         :price_proportion,
                                         :ltv, :stage)
  end


  def correct_business
    @pricing_plan = beachhead.pricing_plans.find_by(id: params[:id])
    if @pricing_plan.nil?
      redirect_to root_url
    else
      @obj = {id: @pricing_plan.id, obj_name: @pricing_plan.class.to_s.underscore}
    end
  end



  def record_event
    b = @pricing_plan.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 12, {business_id: b.id})
  end


end
