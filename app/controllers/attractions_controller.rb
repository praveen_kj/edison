class AttractionsController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @pricing_plan = PricingPlan.find(params[:attraction][:pricing_plan_id])
    @attraction = @pricing_plan.attractions.build(attraction_params)
    respond_to do |format|
      if @attraction.save
        format.html { redirect_to :back }
        format.js
        # expire_fragment([pricing_plan, 'attraction'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @attraction.destroy
      format.html { redirect_to :back }
      format.js
      # expire_fragment([pricing_plan, 'attraction'])
    end

  end


  def update
    @params = params[:attraction]
    respond_to do |format|
      if @attraction.update_attributes(attraction_params)
        format.json { respond_with_bip(@attraction) }
        format.js { respond_with_active_edit @attraction, @field }
        format.html { redirect_to :back }
        # expire_fragment([pricing_plan, 'attraction'])

      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def attraction_params
    params.require(:attraction).permit(:attraction_name, :revenue_increase)
  end


  def correct_business
    @attraction = Attraction.find_by(id: params[:id])
    if @attraction.nil?
      redirect_to root_url
    else
      @obj = {id: @attraction.id, obj_name: @attraction.class.to_s.underscore}
    end
  end
end
