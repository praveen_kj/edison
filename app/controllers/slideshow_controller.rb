class SlideshowController < ApplicationController

  include ApplicationHelper
  layout 'slideshow'

  def slideshow
    slides = File.read("#{Rails.root}/app/views/slideshow/#{params[:view]}.markdown").split('##')
    slides = slides.delete_if { |item| item == "" }
    @slides = slides.collect { |slide| '##'+slide }
  end


end