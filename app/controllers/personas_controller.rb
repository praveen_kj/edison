class PersonasController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]



  def create

    @persona = beachhead.personas.build(persona_params)
    respond_to do |format|
      if @persona.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end


  end

  def destroy
    respond_to do |format|
      @persona.destroy
      format.js
    end
  end


  def update
    respond_to do |format|
      if @persona.update_attributes(persona_params)
        format.json { respond_with_bip(@persona) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @persona, @field }
        record_event if @persona.previous_changes.present?
      else
        format.html { redirect_to :back }
        format.json { respond_with_bip(@persona) }
        format.js
      end
    end
  end

  private

  def persona_params
    params.require(:persona).permit(:parameter, :response, :group)
  end


  def correct_business
    @persona = beachhead.personas.find_by(id: params[:id])
    if @persona.nil?
      redirect_to root_url
    else
      @obj = {id: @persona.id, obj_name: @persona.class.to_s.underscore}
    end
  end


  def record_event
    b = @persona.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 5, {business_id: b.id})
  end

end
