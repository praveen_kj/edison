class SalesStrategiesController < ApplicationController

  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @sales_strategy = beachhead.sales_strategies.build(sales_strategy_params)
    respond_to do |format|
      if @sales_strategy.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @sales_strategy.destroy
      format.html { redirect_to :back }
      format.js
      record_event
    end

  end


  def update
    respond_to do |format|
      if @sales_strategy.update_attributes(sales_strategy_params)
        format.js { respond_with_active_edit(@sales_strategy, @field) }
        format.html { redirect_to :back }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  # def create_child_resources
  #   3.times do |i|
  #     @sales_strategy.strategy_gaps.create(:gap_name => "Strategy Gap #{i+1}")
  #   end
  # end

  def sales_strategy_params
    params.require(:sales_strategy).permit(
        :strategy_name,
        :sales_channel,
        :timeline,
        :start_date,
        :end_date,
        :prob_awareness,
        :prod_awarness,
        :prod_information,
        :prod_sale,
        :collection,
        :prob_awareness_duration,
        :prod_awarness_duration,
        :prod_information_duration,
        :prod_sale_duration,
        :collection_duration,
        :advocate_name,
        :ec_buyer_name,
        :primary_influencer_name,
        :other_influencers_name,
        :veto_holder_name,
        :advocate_expectations,
        :ec_buyer_expectations,
        :primary_influencer_expectations,
        :other_influencer_expectations,
        :veto_holder_expectations,
        :actual_prospects_reached,
        :actual_customers_acquired,
        :actual_customer_ltv,
        :actual_total_cost,
        :stage,

    )
  end


  def correct_business
    @sales_strategy = beachhead.sales_strategies.find_by(id: params[:id])
    if @sales_strategy.nil?
      redirect_to root_url
    else
      @obj = {id: @sales_strategy.id, obj_name: @sales_strategy.class.to_s.underscore}
    end
  end

  def record_event
    b = @sales_strategy.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 14, {business_id: b.id})
  end

end
