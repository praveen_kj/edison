class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :capture_field, only: [:update]

  include ActiveEditViewHelper

  before_action do
    if current_user && current_user.admin?
      Rack::MiniProfiler.authorize_request
    end
  end


  private

  def capture_field
    @field = params.select { |k, v| v.is_a?(Hash) }.values.first.keys.first.to_sym
  end

  def validate_user
    unless user_signed_in?
      redirect_to sign_in_path
      flash[:notice] = "Please sign in to access the app"
    end
  end

  def validate_product
    redirect_to products_dashboard_path unless active_product
  end

  def validate_beachhead
    redirect_to onboarding_path unless beachhead
  end


  def permitted_views
    all_tools.keys << 'home'
  end



end
