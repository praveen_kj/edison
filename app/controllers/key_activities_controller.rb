class KeyActivitiesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create

    @main_activity = MainActivity.find(params[:key_activity][:main_activity_id])
    @key_activity = @main_activity.key_activities.build(key_activity_params)

    respond_to do |format|
      if @key_activity.save
        format.html { redirect_to :back }
        format.js
        expire_fragment([beachhead, 'key_activity'])
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def destroy

    respond_to do |format|
      @key_activity.destroy
      format.html { redirect_to :back }
      format.js
      expire_fragment([beachhead, 'key_activity'])
    end

  end


  def update
    respond_to do |format|
      if @key_activity.update_attributes(key_activity_params)
        format.js { respond_with_active_edit(@key_activity, @field) }
        format.html { redirect_to :back }
        expire_fragment([beachhead, 'key_activity'])
      else
        format.html { redirect_to :back }
        format.js { respond_with_active_edit(@key_activity, @field) }
      end
    end
  end

  private

  def key_activity_params
    params.require(:key_activity).permit(:activity_name,
                                         :target_date,
                                         :effort,
                                         :materials,
                                         :capital,
                                         :main_activity)
  end


  def correct_business
    @key_activity = KeyActivity.find_by(id: params[:id])
    if @key_activity.nil?
      redirect_to root_url
    else
      @obj = {id: @key_activity.id, obj_name: @key_activity.class.to_s.underscore}
    end
  end

end
