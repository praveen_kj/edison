class MilestonesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def create
    @milestone = beachhead.milestones.build(milestone_params)
    respond_to do |format|
      if @milestone.save
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end


  def destroy
    respond_to do |format|
      @milestone.destroy
      format.js
    end
  end


  def update
    respond_to do |format|
      if @milestone.update_attributes(milestone_params)
        format.json { respond_with_bip(@milestone) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @milestone, @field }
      else
        format.html { redirect_to :back }
        format.json { respond_with_bip(@milestone) }
        format.js
      end
    end
  end

  private

  def milestone_params
    params.require(:milestone).permit(:step,
                                      :cost,
                                      :effort,
                                      :target_date)
  end


  def correct_business
    @milestone = beachhead.milestones.find_by(id: params[:id])
    if @milestone.nil?
      redirect_to root_url
    else
      @obj = {id: @milestone.id, obj_name: @milestone.class.to_s.underscore}
    end
  end
end
