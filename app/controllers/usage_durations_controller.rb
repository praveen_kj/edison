class UsageDurationsController < ApplicationController

  include ApplicationHelper

  def create

    TimeTracker.perform_async(current_user.id,
                              params[:usage_duration][:view],
                              params[:usage_duration][:time_spent])

    respond_to do |format|
      format.text { render :status => 202, :text => 'ok' }
    end


  end


  private

  def usage_duration_params
    params.require(:usage_duration).permit(:view,
                                           :time_spent)
  end


end
