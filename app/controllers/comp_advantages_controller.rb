class CompAdvantagesController < ApplicationController


  include ApplicationHelper

  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :correct_business, only: [:destroy, :update]


  def rank
    @comp_advantage = beachhead.comp_advantages.find_by(id: params[:comp_advantage][:id])

    @currently_ranked = beachhead.comp_advantages.find_by(rank: params[:comp_advantage][:rank])

    if @currently_ranked
      @currently_ranked.update_attributes(rank: nil)
    end
    @comp_advantage.update_attributes(rank: params[:comp_advantage][:rank])
  end

  def create

    @comp_advantage = beachhead.comp_advantages.build(comp_advantage_params)
    respond_to do |format|
      if @comp_advantage.save
        format.html { redirect_to :back }
        format.js
        record_event
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end

  end

  def show
    item =  CompAdvantage.where(id: params[:id]).first
    if item && current_user.has_access_to?(item)
      @sub_tool_resource = item
      @sub_tool_title = @sub_tool_resource.adv_name
      respond_to do |format|
        format.html
        format.js
      end
    else
      render status: 404
    end
  end

  def destroy

    respond_to do |format|
      @comp_advantage.destroy
      format.html { redirect_to :back }
      format.js
      record_event
    end

  end


  def update
    @params = params[:comp_advantage]
    respond_to do |format|
      if @comp_advantage.update_attributes(comp_advantage_params)
        format.json { respond_with_bip(@comp_advantage) }
        format.html { redirect_to :back }
        format.js { respond_with_active_edit @comp_advantage, @field }
        record_event
      else
        format.html { redirect_to :back }
      end
    end
  end

  def rank_modal
    @comp_advantage = CompAdvantage.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  private

  def comp_advantage_params
    params.require(:comp_advantage).permit(:adv_name,
                                           :customer_want,
                                           :assets_available,
                                           :founders_want,
                                           :competitor_ability,
                                           :vendor_ability,
                                           :personal_goal,
                                           :financial_goal,
                                           :rank, :stage
    )
  end


  def correct_business
    @comp_advantage = beachhead.comp_advantages.find_by(id: params[:id])
    if @comp_advantage.nil?
      redirect_to root_url
    else
      @obj = {id: @comp_advantage.id, obj_name: @comp_advantage.class.to_s.underscore}
    end
  end


  def record_event
    b = @comp_advantage.business
    EventRecorderWorker.perform_async(b.product_id, 1, current_user.id, 9, {business_id: b.id})
  end


end
