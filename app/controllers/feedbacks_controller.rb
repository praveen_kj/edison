class FeedbacksController < ApplicationController

  def create
    @feedback = Feedback.new(feedback_params)
    respond_to do |format|
      if @feedback.save
        FeedbackMailWorker.perform_async(@feedback.id)
        format.html { redirect_to :back }
        format.js
      else
        format.html { redirect_to :back }
        format.js {}
      end
    end
  end

  def update
    @feedback = Feedback.find(params[:id])
    @params = params[:feedback]
    respond_to do |format|
      if @feedback.update_attributes(feedback_params)
        format.json { respond_with_bip(@feedback) }
        format.js
        format.html { redirect_to :back }
      else
        format.html { redirect_to :back }
      end
    end
  end

  private

  def feedback_params
    params.require(:feedback).permit(:step_id,
                                     :user_id,
                                     :page,
                                     :subject,
                                     :body,
                                     :status,
                                     :email)
  end

end
