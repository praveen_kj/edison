class Benefit < ActiveRecord::Base

  belongs_to :feature, touch: true
  validates :benefit_name, length: {:maximum => 300}

  default_scope { order('created_at ASC') }

  scope :for_group, -> group { where(group: group) }

  validates :priority, inclusion: {in: PRIORITIES, message: 'you done fucked up boy'}, allow_blank: true

  def self.blueprint_for_table
    {rows: {1 => {field: :benefit_name, type: :text, deletable: true, editable: true},
            2 => {field: :priority, type: :select, modal: false, choices_array: PRIORITIES}
    },
     placeholder: 'New Benefit',
     form_default_field: :benefit_name}
  end

  def self.blueprint_for_table1
    {rows: {1 => {field: :benefit_name, type: :text, deletable: true, editable: true},
            2 => {field: :priority, type: :select, modal: false, choices_array: RANGE}
    },
     placeholder: 'New Detriment',
     form_default_field: :benefit_name}
  end

  def self.blueprint_for_table2
    {rows: {1 => {field: :benefit_name, type: :text, deletable: true, editable: true}
    },
     placeholder: 'How it Works',
     form_default_field: :benefit_name}
  end

  def self.blueprint_for_table3
    {rows: {1 => {field: :benefit_name, type: :text, deletable: true, editable: true}
    },
     placeholder: 'New Sub Feature',
     form_default_field: :benefit_name}
  end

end
