class Feature < ActiveRecord::Base
  default_scope { order('features.created_at ASC') }
  belongs_to :business

  has_many :benefits, dependent: :destroy
  has_many :components, dependent: :destroy
  has_many :sub_components, through: :components
  has_many :issues, through: :components
  has_many :iterations, dependent: :destroy

  validates :feature_name, presence:true, length: {:maximum => 50}
  validates :decision_notes, length: {:maximum => 300}

  scope :mocked_up, -> { where(mocked_up: true) }
  scope :minimal, -> { where(customer_wanting_feature: 1).where(importance_rank: 1..2) }
  scope :mvp, -> { where(customer_wanting_feature: 1..3).where(importance_rank: 1..3) }
  scope :at, -> (x, y) { where(customer_wanting_feature: x).where(importance_rank: y) }
  scope :having_components, -> { joins(:components).group("features.id") }


  def purchasing_priority
    PersonaDetail.find(persona_detail_id).detail if persona_detail_id
  end


end
