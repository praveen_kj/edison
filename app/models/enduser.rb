class Enduser < ActiveRecord::Base
  belongs_to :business
  default_scope {order('created_at ASC')}
  validates :parameter, presence: true

  validates :response, length: {maximum: 300}

  scope :for_group, -> group_no { where(group: group_no) }


end
