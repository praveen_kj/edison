class Touchpoint < ActiveRecord::Base


  belongs_to :business
  has_many :relationship_steps, dependent: :destroy
  has_many :cx_steps, dependent: :destroy

  default_scope { order('created_at ASC') }

  scope :for_tp, -> touchpoint_no { where(touchpoint_no: touchpoint_no) }
  scope :at, -> (x, y) { where(motivation_level: x).where(ease_of_doing: y) }
  scope :weak_points, -> { for_tp(2..10).where("touchpoints.motivation_level BETWEEN ? AND ?  OR touchpoints.ease_of_doing BETWEEN ? AND ?", 4, 5, 4, 5) }


  validates :end_user_actions, length: {:maximum => 300}
  validates :things_needed, length: {:maximum => 300}
  validates :people_involved, length: {:maximum => 300}
  validates :hurdles_faced, length: {:maximum => 300}

  validates :expected_conversion_rate, numericality: {allow_blank: true, less_than_or_equal_to: 100}

  def completed?
    end_user_actions.present? &&
        motivation_level.present? &&
        ease_of_doing.present? &&
        possible_intervention.present?
  end



  def get_moods
    array = []

    (array << (intelligence == false ? 'Dumb' : ('Intelligent' if intelligence == true))) unless intelligence.nil?
    (array << (openness == false ? 'Closed-minded' : ('Open-minded' if openness == true))) unless openness.nil?
    (array << (conscientiousness == false ? 'Lazy' : ('Diligent' if conscientiousness == true))) unless conscientiousness.nil?
    (array << (kindness == false ? 'Shrewd' : ('Kind' if kindness == true))) unless kindness.nil?
    (array << (stability == false ? 'Voltaile' : ('Stable' if stability == true))) unless stability.nil?
    (array << (extraversion == false ? 'Quiet' : ('Fun' if extraversion == true))) unless extraversion.nil?

    array.to_sentence

  end

  def expected_users_at_touchpoint
    (expected_conversion_rate || nil) * (self.business.opening_users || nil) / 100
  end


  def self.blueprint_for_section
    {
        header_map: {
            heading: {field: :touchpoint_no, type: :text, modal: false, editable: false},
            rhs_fields: {
                1 => {field: :target_date, type: :date}
            }
        },

        section_field_map: [

            # array format :
            # first item in array is an array with the icon reference and the static text
            # second item in the array is a hash to be used to make_field

            ['What will be the prominent trait of the end users during this touch point?',
             ['static_pages/shared/mood_select_modal_content_package']],

            ['Who are the people (other than yourself and the end users) involved in this touch point?',
             {field: :people_involved, type: :text, modal: false, editable: true, format: :textarea}],

            ['What are the materials and equipment that are required during this touch point?',
             {field: :things_needed, type: :text, modal: false, editable: true, format: :textarea}],

            ['What major hurdles you have to overcome during this touch point?',
             {field: :hurdles_faced, type: :text, modal: false, editable: true, format: :textarea}]

        ],

        section_break_text: "End users’ interactions with your product or team during this touchpoint",
        section_break_text2: "Key hurdle to be addressed, Hurdle type, Hurdle closure date",

        child_table: :relationship_step,
        child_table2: :cx_step,
    }
  end


end
