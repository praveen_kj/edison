class Improvement < ActiveRecord::Base
  default_scope { order('created_at ASC') }

  belongs_to :next_customer

  validates :improvement_name, length: {:maximum => 300}

  def self.blueprint_for_table
    {rows: {1 => {field: :improvement_name, type: :text, editable: true},
    },
     placeholder: 'New Improvement',
     form_default_field: :improvement_name,
     headers: ['', 'Improvement suggested', '']

    }


  end

end
