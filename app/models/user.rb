class User < ActiveRecord::Base

  has_many :products, dependent: :destroy
  has_many :user_tasks, dependent: :destroy
  has_many :notes, dependent: :destroy
  has_many :assumptions, dependent: :destroy
  has_many :discussions, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :dashboards, dependent: :destroy

  has_one :user_parameter, dependent: :destroy
  has_one :beachhead, dependent: :destroy



  delegate :subscription, to: :user_parameter

  include UsersHelper::InstanceMethods
  include AnalyticsHelper

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:google_oauth2]

  attr_accessor :referred_by, :updated_info, :product_name, :owner_role

  scope :active_trial_users, lambda { |*args| where("trial_expiry_date > ?", (args.first || Time.zone.now)).where(subscription_id: nil) }
  scope :expired_trial_users, lambda { |*args| where("trial_expiry_date < ?", (args.first || Time.zone.now)).where(subscription_id: nil) }
  scope :unwarned_trial_users, lambda { |*args| active_trial_users.where(tx_warning_sent_at: nil).where("trial_expiry_date < ?", (args.first || Time.zone.now + 5.days)) }
  scope :active_subscribers, -> { where(subscription_status: true) }
  scope :subscription_due, lambda { |*args| active_subscribers.where("current_subscription_end_at < ?", (args.first || Time.zone.now)) }
  scope :unwarned_subscribers, lambda { |*args| active_subscribers.where(sx_warning_sent_at: nil).where("current_subscription_end_at < ?", (args.first || Time.zone.now + 3.days)) }
  scope :recently_active, lambda { |*args| where("current_streak_end_at > ?", (args.first || (Time.zone.now - 2.days))) }
  scope :never_returned, -> { where(sign_in_count: 1) }
  scope :unreminded, lambda { |*args| includes(:user_parameter).where(user_parameters: {subscription: true}).where('daily_reminder_sent_at IS NULL OR daily_reminder_sent_at < ?', (args.first || 24.hours.ago)) }

  after_create :update_member_id


  def update_member_id
    m = Membership.where(member_email: self.email)
    if m.present?
      m.each do |m|
        m.update_attributes(member_id: self.id)
      end
    end
  end


  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    # Create a new user if they don't exist
    unless user

      user = User.new(email: data["email"],
                      provider: 'google',
                      username: data['name'],
                      password: Devise.friendly_token[0, 20],
                      trial_expiry_date: TRIAL_PERIOD.from_now.utc
      )
      user.skip_confirmation!
      user.save
      # user.create_product
      user.create_user_parameter(favourites: [1, 2], user_name: data["name"])
      user.perform_post_sign_up_process('nil')
    end
    user
  end

  def create_product(product_name = 'My product', owner_role = 'Founder')
    product = self.products.create(product_name: product_name, owner_role: owner_role)
    self.update_attributes(active_product_id: product.id)
  end

  def create_product_business_beachhead(product_name = 'My product')
    product = self.products.create(product_name: product_name)
    product.create_business_and_set_beachhead
    self.update_attributes(active_product_id: product.id)
  end


  def perform_post_sign_up_process(referral_id)
    PostSignUpProcess.perform_async(self.id, referral_id)
  end


  def last_accessed_step
    Footprint.where("footprints.step_id IS not NULL").where(user_id: id).where(step_id: CANVAS_STEPS).last.try(:step_id)
  end


  protected

  def confirmation_required?
    false
  end


end
