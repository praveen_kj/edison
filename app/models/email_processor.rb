
class EmailProcessor

  def initialize(email)
    @email = email
  end

  def process
    Rails.logger.info "Processing Email from #{@email.from[:email]}"
    Rails.logger.info "Token: #{@email.to[0][:token].split('-')[1]}"

    model = resource_selection[@email.to[0][:token].split('-')[1]]

    if model

      instance = model.find_by(secret_token: @email.to.first[:token])

      commentor = User.where(email: @email.from[:email]).first

      instance.comments.create(
          comment_text: @email.body,
          user_id: commentor.id
      )
    end

  end

  private


  def resource_selection
    {
        'D' => Discussion,
        'N' => Note,
        'A' => Assumption,
        'U' => UserTask,
        'F' => CustomerFeedback,
        'S' => SubTask,
        'M' => MainActivity,
        'C' => CompAdvantage,
        'B' => PricingModel,
        'P' => PricingPlan
    }
  end



end
