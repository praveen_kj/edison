class Iteration < ActiveRecord::Base
  default_scope { order('created_at ASC') }
  belongs_to :feature

  validates :iteration_name, length: {:maximum => 300}



  def self.blueprint_for_table
    {rows:
         {
             1 => {field: :iteration_name, type: :text, editable: true},
             2 => {field: :target_date, type: :date, placeholder:'Target Date'},

         },
     placeholder: 'Reason for iteration',
     form_default_field: :iteration_name}
  end


end
