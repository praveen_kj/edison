class Pitch < ActiveRecord::Base
  belongs_to :business
  default_scope {order('created_at DESC')}
  validates :pitch_content, presence: true
  validates :pitch_content, length: {:maximum => 300}

end
