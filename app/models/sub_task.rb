class SubTask < ActiveRecord::Base

  belongs_to :user_task


  has_many :comments, dependent: :destroy

  delegate :product, to: :user_task

  default_scope { order('sub_tasks.created_at ASC') }

  validates_presence_of :sub_task_name, allow_blank: true
  validates :sub_task_name, length: {:maximum => 200}

  scope :pending, -> { where("sub_tasks.status IS NULL OR sub_tasks.status IS false") }
  scope :assigned_to, -> user_id { where(assignee_id: user_id) }
  scope :complete, -> { where(status: true) }
  scope :ongoing, lambda { |date = Date.today| where("? BETWEEN start_date AND end_date", date) }
  scope :due_now, lambda { |date = Date.tomorrow| where("sub_tasks.end_date < ?", date) }
  scope :not_assigned_to, -> user_id { where.not(assignee_id: user_id) }

  include UserTasksHelper::InstanceMethods
  include CommentsHelper::InstanceMethods

  after_create :generate_secret_token

  def friendly_link
    "/to-dos/#{id}"
  end

  def title_for_email
    sub_task_name
  end

  def partly_set?
    [start_date, end_date, assignee_id].any?
  end

  def scheduled?
    [start_date, end_date].all?
  end

  def assignee
    User.where(id: assignee_id).first
  end

  def self.clear_today(product_id, user_id)
    product = Product.find product_id
    tasks = where(user_task_id: product.user_tasks.pending.pluck(:id)).pending.due_now
    tasks.each do |t|
      t.update_attributes(start_date: nil, end_date: nil)
    end

  end

end
