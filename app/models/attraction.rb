class Attraction < ActiveRecord::Base
  belongs_to :pricing_plan

  default_scope { order('created_at ASC') }

  validates :attraction_name, length: {:maximum => 300}

  def self.blueprint_for_table
    {rows: {1 => {field: :attraction_name, type: :text, editable: true},
    },
     placeholder: 'New Attraction',
     form_default_field: :attraction_name}
  end


end
