class UserParameter < ActiveRecord::Base
  belongs_to :user

  validates :user_name, presence: true, allow_blank: true
  validates :company_name, presence: true, allow_blank: true
  validates :firm_name, presence: true, allow_blank: true
  validates :title, presence: true, allow_blank: true
  validates :mission, presence: true, allow_blank: true
  validates :target_date, presence: true, allow_blank: true

  validates :enduser_base, numericality: {allow_blank: true, less_than_or_equal_to: 7_000_000_000}
  validates :company_name, length: {:maximum => 50}

  # serialize :favourites

end
