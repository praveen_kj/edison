class Component < ActiveRecord::Base

  default_scope { order('components.created_at ASC') }
  belongs_to :feature
  has_many :sub_components
  has_many :issues
  validates :name, presence: true


  scope :minimal, -> { where(customers_want: 1).where(importance: 1..2) }
  scope :mvp, -> { where(customers_want: 1..3).where(importance: 1..3) }


  def feedbacks
    CustomerFeedback.for_sub_feature(self.id)
  end


  def feedback_count
    feedbacks.count
  end


  def group
    case [customers_want, importance]
      when [1, 1], [1, 2]
        'minimal-product'
      when [1, 3], [2, 1], [2, 2], [2, 3], [3, 1], [3, 2], [3, 3]
        'mvp'
    end
  end



  def sub_components_for(scope)
    c = sub_components.send(scope)
  end


end
