class Event < ActiveRecord::Base

  belongs_to :product

  default_scope { order('created_at DESC') }


  # event_type    event_description
  # -----------------------------------------
  #     1         tool update
  #     2         new discussion
  #     3         comment on discusison
  #     4         new tdl
  #     5         comment tdl
  #     6         add td
  #     7         complete td
  #     8         new note
  #     9         comment note
  #     10        new assumption
  #     11        comment assumption
  #     12        add experiment
  #     13        complete experiment


  def initiator_name
    u = User.find(self.user_id)
    u.name || u.email
  end

  def business_name
    if other_data['business_id']
      Business.where(id: other_data['business_id'].to_i).first.try(:business_name) || false
    end
  end

  def same_as(event)
    [
        self.event_type,
        self.user_id,
        self.source_id,
        self.other_data
    ] == [
        event.event_type,
        event.user_id,
        event.source_id,
        event.other_data
    ]
  end


end
