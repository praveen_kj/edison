class Note < ActiveRecord::Base

  belongs_to :product
  belongs_to :user
  belongs_to :business
  has_many :comments

  default_scope { order('notes.updated_at DESC') }

  after_create :generate_secret_token

  validates_presence_of :title
  validates :title, length: {:maximum => 100}
  validates_presence_of :content, allow_blank: true

  scope :for_step, -> step_id { where(step_no: step_id) }
  scope :current, -> { where("archived IS NULL OR archived IS false") }
  scope :archived, -> { where(archived: true) }
  scope :published, -> { where("draft IS NULL OR draft IS false") }
  scope :drafts, -> { where(draft: true) }

  include CommentsHelper::InstanceMethods

  def friendly_link
    "/notes/#{id}"
  end

  def title_for_email
    title
  end

end
