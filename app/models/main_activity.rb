class MainActivity < ActiveRecord::Base
  belongs_to :business
  default_scope { order('main_activities.target_date ASC') }

  has_many :key_activities, dependent: :destroy
  has_many :comments, dependent: :destroy

  delegate :product, to: :business


  scope :for_stage, -> stage_no { where(stage: stage_no) }
  scope :releases, -> { where(release: true) }
  scope :sales_targets, -> { where(sales_target: true) }
  scope :special, -> { where('main_activities.sales_target IS TRUE OR main_activities.release is TRUE') }

  scope :upcoming, lambda { |date = Date.today| where("main_activities.target_date BETWEEN ? AND ?", date, date + 30.days) }


  validates :main_activity_name, length: {maximum: 300}
  validates :main_activity_name, presence: true
  validates :target_date, presence: true, allow_blank: true
  validates :capital, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}

  include CommentsHelper::InstanceMethods

  after_create :generate_secret_token

  def friendly_link
    "/milestones/#{id}"
  end

  def title_for_email
    main_activity_name
  end

  def overdue?
    target_date.present? && Date.today > target_date
  end

  def is_due?
    target_date.present? && Date.today >= target_date
  end

  def due_today?
    target_date.present? && Date.today == target_date
  end

  def overdue_days
    (Date.today - target_date).to_i if target_date.present? && overdue?
  end

  def achieved?
    stage == 4
  end

  def completed?
    [4, 5].include? stage
  end

  def abandoned?
    stage == 5
  end


  def days_to_go
    if target_date.present?
      if overdue?
        0
      else
        (target_date - Date.today).to_i
      end
    end
  end

  def completed_release_or_sales_target?
    sales_target || release
  end


end
