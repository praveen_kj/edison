class CustomerDecisionMakingUnit < ActiveRecord::Base
  belongs_to :business
  default_scope { order('created_at ASC') }


  validates :entity_name, length: {:maximum => 300}
  validates :fears, length: {:maximum => 300}
  validates :motivations, length: {:maximum => 300}
  validates :expectations, length: {:maximum => 300}
  validates :offering, length: {:maximum => 300}


  scope :for_entity, -> entity { where(entity: entity) }
  scope :for_category, -> category { where(category: category) }

  def profiled?
    entity_name.present? &&
        fears.present? &&
        motivations.present? &&
        expectations.present? &&
        offering.present?
  end


  def self.blueprint_for_section
    {
        section_field_map: [

            ['What are the major fears relating to your product?',
             {field: :fears, type: :text, modal: false, editable: true, format: :textarea}],

            ['What are the key motivations relating to your product?',
             {field: :motivations, type: :text, modal: false, editable: true, format: :textarea}],

            ['What does this person expect from your product or from you?',
             {field: :expectations, type: :text, modal: false, editable: true, format: :textarea}],

            ['What can you offer to match the above expectations?',
             {field: :offering, type: :text, modal: false, editable: true, format: :textarea}]

        ],
    }
  end


end