class UserTask < ActiveRecord::Base

  belongs_to :product
  belongs_to :user
  belongs_to :business
  has_many :sub_tasks
  has_many :comments

  default_scope { order('user_tasks.updated_at DESC') }

  validates_presence_of :task_name
  validates :task_name, length: {:maximum => 200}

  scope :for_step, -> step_id { where(step_no: step_id) }
  scope :for_business, -> business_id { where(business_id: business_id) }
  scope :for_category, -> category { where(category: category) }
  scope :pending, -> { where("user_tasks.status IS NULL OR user_tasks.status IS false") }
  scope :archived, -> { where(status: true) }

  scope :unplanned, -> { where("start_date IS NULL OR end_date IS null") }

  scope :ongoing, lambda { |date = Date.today| where("? BETWEEN start_date AND end_date", date) }

  scope :starting_from_tomo, lambda { |date = (Date.today + 1)| where(start_date: date) }

  scope :starting_later, lambda { |date = (Date.today + 1)| where("start_date > ?", date) }

  scope :overdue, lambda { |date = Date.today| where("end_date < ?", date) }

  scope :today, lambda { |date = Date.today| where("end_date <= ?", date) }

  scope :diagnose_question, -> question_no { where(diagnose_id: question_no) }


  attr_accessor :source

  after_create :generate_secret_token

  include CommentsHelper::InstanceMethods
  include UserTasksHelper::InstanceMethods

  def friendly_link
    "/to-do-lists/#{id}"
  end

  def title_for_email
    task_name
  end


  def list_of_task
    if starts_today?
      'today'
    elsif starts_tomo?
      'tomorrow'
    else
      'upcoming'
    end
  end


  def self.get_sort_order(field, order)
    self.reorder(field => order).pluck(:id)
  end


  def has_ongoing_tasks_assigned_to?(user_id)
    sub_tasks.pending.ongoing.pluck(:assignee_id).include?(user_id)
  end


  def self.having_ongoing_tasks_assigned_to(user_id)
    select do |t|
      t.has_ongoing_tasks_assigned_to?(user_id)
    end
  end

  def self.having_ongoing_tasks
    select do |t|
      t.sub_tasks.ongoing.pending.count > 0
    end
  end

end
