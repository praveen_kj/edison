class Financial < ActiveRecord::Base
  belongs_to :business
  default_scope { order('created_at ASC') }

  validates :no_of_customers, numericality: {allow_blank: true, less_than_or_equal_to: 7_000_000_000}
  validates :ongoing_price, numericality: {allow_blank: true, less_than_or_equal_to: 100_000_000}

  def market_size
    (no_of_customers || 0) * (ongoing_price || 0)
  end



end
