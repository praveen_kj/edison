class CxStep < ActiveRecord::Base

  belongs_to :touchpoint
  default_scope { order('created_at ASC') }

  validates :hurdle_name, length: {:maximum => 150}


  def self.blueprint_for_table
    {rows: {1 => {field: :hurdle_name, type: :text,editable: true},
            2 => {field: :hurdle_type, type: :select, modal: false, choices_array: HURDLE_TYPE},
            3 => {field: :hurdle_target_date, type: :date, editable: true},
    },
     placeholder: 'New Hurdle',
     form_default_field: :hurdle_name}
  end


end
