class Innovation < ActiveRecord::Base
  belongs_to :pricing_model

  validates :innovation_name, length: {:maximum => 300}


  default_scope { order('created_at ASC') }

  def self.blueprint_for_table
    {rows: {1 => {field: :innovation_name, type: :text, editable: true},
    },
     placeholder: 'New Innovation',
     form_default_field: :innovation_name}
  end

end
