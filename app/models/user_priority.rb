class UserPriority < ActiveRecord::Base

  default_scope { order('created_at ASC') }

  validates :tagline, length: {maximum: 100}
  validates :poss_state, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}
  validates :as_is_state, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}


  def additional_value_created
    (poss_state || 0) - (as_is_state || 0)
  end

  def strip_rank
    update_attributes(rank: nil)
  end



end
