class ChecklistItem < ActiveRecord::Base
  belongs_to :product
  belongs_to :business

  validate :unique_for_business_or_product

  def unique_for_business_or_product
    if ChecklistItem.where(product_id: product_id, business_id: business_id, item_key: item_key).first
      errors.add(:item_key, "Item already available")
    end
  end

end
