class PricingModel < ActiveRecord::Base

  belongs_to :business
  has_many :comments, dependent: :destroy
  has_many :innovations

  default_scope { order('pricing_models.created_at ASC') }
  validates :model_name, presence: true, length: {:maximum => 50}

  scope :for_stage, -> stage_no { where(stage: stage_no) }
  delegate :product, to: :business

  include CommentsHelper::InstanceMethods

  after_create :generate_secret_token

  def friendly_link
    "/business-models/#{id}"
  end

  def title_for_email
    model_name
  end


  def generate_secret_token
    update_attributes(secret_token: "reply-B-#{SecureRandom.urlsafe_base64}")
  end



  def preference

    if willingness_to_buy &&
        favourable_pricing &&
        vendor_share_protected &&
        competitor_adoption &&
        allows_dist_incentives

      array = [willingness_to_buy,
               favourable_pricing,
               vendor_share_protected,
               competitor_adoption,
               allows_dist_incentives]

      total = array.inject(:+)

      avg = (total/5.0)

      if avg > 2.25
        'Low'
      elsif avg.between?(1.51, 2.25)
        'Medium'
      else
        'High'
      end

    end


  end


end

