class NextCustomer < ActiveRecord::Base
  belongs_to :business
  has_many :improvements, dependent: :destroy
  default_scope { order('created_at ASC') }
  validates :cust_name, presence: true

  validates :cust_name, length: {:maximum => 50}
  scope :for_stage, -> stage_no { where(stage: stage_no) }


  def self.blueprint_for_section
    {
        header_map: {
            heading: {field: :cust_name, type: :text, modal: false, editable: true},
            rhs_fields: {
                1 => {field: :next_meeting_date, type: :date, placeholder: 'Next meeting dt.'}
            }
        },

        section_field_map: [

            ['Was this customer satisfied with the use case?',
             {field: :use_case, type: :select, modal: false, choices_array: RANGE, placeholder: 'High/Medium/Low'}],

            ['Was this customer satisfied with the propositions offered?',
             {field: :benefits, type: :select, modal: false, choices_array: RANGE, placeholder: 'High/Medium/Low'}],

            ['Was this customer agree to the value promised in dollars?',
             {field: :value_created, type: :select, modal: false, choices_array: RANGE, placeholder: 'High/Medium/Low'}],

            ['Did the customer show the willingness to buy your product?',
             {field: :purchase_interest, type: :select, modal: false, choices_array: RANGE, placeholder: 'High/Medium/Low'}]
        ],

        section_break_text: "Key Improvements suggested by this Customer:",

        child_table: :improvement,
    }
  end


end
