class Message < ActiveRecord::Base
  belongs_to :product
  belongs_to :user

  default_scope { order('created_at ASC') }

  after_create :notify_team

  validates :content, presence: true

  def notify_team
    ChatMailWorker.perform_async(self.id)
  end


end
