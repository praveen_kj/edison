class PricingPlan < ActiveRecord::Base

  belongs_to :business

  has_many :attractions, dependent: :destroy
  has_many :plan_defects, dependent: :destroy
  has_many :comments, dependent: :destroy

  default_scope { order('pricing_plans.created_at ASC') }

  validates :plan_name, length: {:maximum => 150}
  scope :for_stage, -> stage_no { where(stage: stage_no) }

  delegate :product, to: :business


  validates :product_price, numericality: {allow_blank: true, less_than_or_equal_to: 100_000_000}
  validates :ltv, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}
  validates :competitor_price_lb, numericality: {allow_blank: true, less_than_or_equal_to: 100_000_000}
  validates :competitor_price_ub, numericality: {allow_blank: true, less_than_or_equal_to: 100_000_000}


  include CommentsHelper::InstanceMethods

  after_create :generate_secret_token


  def friendly_link
    "/pricing-plans/#{id}"
  end

  def title_for_email
    plan_name
  end


end
