class Comment < ActiveRecord::Base

  default_scope { order('comments.created_at ASC') }

  belongs_to :user
  belongs_to :discussion, touch: true
  belongs_to :note, touch: true
  belongs_to :user_task, touch: true
  belongs_to :assumption, touch: true
  belongs_to :sub_task, touch: true
  belongs_to :main_activity, touch: true
  belongs_to :comp_advantage, touch: true
  belongs_to :pricing_model, touch: true
  belongs_to :pricing_plan, touch: true
  belongs_to :customer_feedback, touch: true

  validates :comment_text, presence: true

  after_create :notify_team

  def notify_team
    CommentMailWorker.perform_at(30.seconds.from_now, self.id)
  end


  def parent_instance
    parent_name = attributes.select { |k, v| k.include?("_id") && ['product_id', 'user_id'].exclude?(k) && v != nil }.keys.last[0..-4]
    send(parent_name)
  end


end
