class Topic < Struct.new(:parent, :id, :title, :latest_comment, :user, :event_at, :comment_count, :path)

  ## title            : title or comment's parent or discussion title
  ## latest_comment   : latest comment text
  ## user             : user instance of author of comment or discussion
  ## created at       : created_at of comment or discussion
  ## comment_count    : comment count of parent

  def self.get(product, options = {})


    limit = options[:limit] || nil
    offset = options[:offset] || nil
    output = []
    parents = [:notes, :user_tasks, :sub_tasks, :main_activities, :comp_advantages, :pricing_models, :pricing_plans, :customer_feedbacks]

    parents.each do |parent|
      product.send(parent).includes(:comments).with_comments.each do |parent_instance|
        output << Topic.new(parent, parent_instance.id, parent_instance.title_for_email, parent_instance.latest_comment.comment_text, parent_instance.latest_comment.user, parent_instance.updated_at, parent_instance.comment_count, parent_instance.friendly_link)
      end
    end

    product.discussions.includes(:comments, :user).collect do |d|
      output << Topic.new(:discussions, d.id, d.title, d.latest_comment.try(:comment_text) || d.subject , d.user, d.created_at, d.comment_count, d.friendly_link)
    end

    (limit && output) ? output.sort_by(&:event_at).reverse[offset..limit] : output.sort_by(&:event_at).reverse

  end


end
