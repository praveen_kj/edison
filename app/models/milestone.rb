class Milestone < ActiveRecord::Base
  belongs_to :business
  default_scope { order('created_at ASC') }

  scope :pending, -> { where("status IS NULL OR status IS false") }

  scope :product, -> { where(step: 7..10) }
  scope :customer, -> { where(step: 11..12) }
  scope :competition, -> { where(step: 13..14) }
  scope :pricing, -> { where(step: 15..16) }
  scope :delivery, -> { where(step: 17..20) }
  scope :profits, -> { where(step: 21..25) }


end
