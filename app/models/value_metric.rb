class ValueMetric < ActiveRecord::Base
  belongs_to :business
  default_scope { order('created_at ASC') }

  validates :benefit_type, inclusion: {in: BENEFIT_TYPE}, allow_blank: true

  validates :proposition_name, length: {:maximum => 50}
  validates :possible_state, length: {:maximum => 50}
  validates :as_is_state, length: {:maximum => 50}

  scope :for_group, -> group_no { where(group: group_no) }

  def quantified_proposition
    if possible_state_value && as_is_state_value
      possible_state_value - as_is_state_value
    end
  end


end
