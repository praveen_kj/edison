class ProcurementStep < ActiveRecord::Base
  belongs_to :business
  default_scope { order('created_at ASC') }
  validates :key_proc_activity, presence: true

  scope :for_stage, -> stage { where(product_stage: stage) }

  def self.blueprint_for_table
    {
        rows: {
            1 => {field: :key_proc_activity, type: :text, deletable: true, editable: true},
            2 => {field: :target_date, type: :date, editable: true,placeholder:'Target Date'},
            3 => {field: :key_partners, type: :text, editable: true,placeholder:'Key Partners'},
            4 => {field: :cost, type: :num, editable: true, placeholder: 'Cost'}
        }
    }
  end

end
