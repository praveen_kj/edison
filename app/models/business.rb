class Business < ActiveRecord::Base

  default_scope { order('businesses.created_at ASC') }

  belongs_to :product

  has_many :discussions, dependent: :destroy
  has_many :user_tasks, dependent: :destroy
  has_many :notes, dependent: :destroy
  has_many :assumptions, dependent: :destroy
  has_many :business_parameters, dependent: :destroy
  has_many :milestones, dependent: :destroy
  has_many :endusers, dependent: :destroy
  has_many :personas, dependent: :destroy
  has_many :persona_details, dependent: :destroy
  has_many :touchpoints, dependent: :destroy
  has_many :features, dependent: :destroy
  has_many :value_metrics, dependent: :destroy
  has_many :next_customers, dependent: :destroy
  has_many :customer_decision_making_units, dependent: :destroy
  has_many :pricing_models, dependent: :destroy
  has_many :procurement_steps, dependent: :destroy
  has_many :sales_strategies, dependent: :destroy
  has_many :comp_advantages, dependent: :destroy
  has_many :comp_positions, dependent: :destroy
  has_many :pitches, dependent: :destroy
  has_many :financials, dependent: :destroy
  has_many :main_activities, dependent: :destroy
  has_many :pricing_plans, dependent: :destroy
  has_many :user_priorities, dependent: :destroy
  has_many :customers, dependent: :destroy
  has_many :checklist_items, dependent: :destroy


  validates :business_name, presence: true, length: {maximum: 50}

  validates :application, length: {:maximum => 300}
  validates :end_user, length: {:maximum => 300}
  validates :benefit, length: {:maximum => 300}
  validates :prod_features, length: {:maximum => 300}
  validates :sales, length: {:maximum => 300}
  validates :marketing, length: {:maximum => 300}
  validates :partners, length: {:maximum => 300}
  validates :core, length: {:maximum => 300}
  validates :top_competitors, length: {:maximum => 300}

  validates :persona_name, length: {:maximum => 50}
  validates :enduser_name, length: {:maximum => 300}

  validates :enduser_count, numericality: {allow_blank: true, less_than_or_equal_to: 7_000_000_000}
  validates :opening_users, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}

  attr_accessor :source_page

  after_update :update_diagnosis_result

  scope :for_diagnosis_result, -> (result_code) { where(diagnosis_result: result_code) }
  scope :preferred, -> { reorder('diagnosis_result DESC') }


  # user_id: 'integer',
  # product_id: 'integer',
  # business_name: 'text',

  # end_user: 'text',
  # application: 'text',
  # benefit: 'text',
  # marketing: 'text',
  # sales: 'text',
  # core: 'text',
  # prod_features: 'text',
  # top_competitors: 'text',
  # partners: 'text',


  # question1: 'boolean',
  # question2: 'boolean',
  # question3: 'boolean',
  # question4: 'boolean',
  # question5: 'boolean',
  # question6: 'boolean',
  # question7: 'boolean',
  # resources_created: 'boolean',
  # similar_product: 'boolean',
  # similar_sales: 'boolean',
  # word_of_mouth: 'boolean',

  ## Redundant for the most part

  # value_prop_conf_date: 'date',
  # enduser_name: 'text',
  # eu_target_date1: 'date',
  # eu_target_date2: 'date',
  # eu_target_date3: 'date',
  # eu_target_date4: 'date',
  # persona_meeting_date: 'date',
  # persona_name: 'string',
  # enduser_count: 'integer',
  # opening_users: 'integer',
  # validated_enduser: 'boolean',
  # analysis_start_at: 'date',
  # analysis_end_at: 'date',
  # chosen_business_model_id: 'integer'

  include DiagnosisHelper
  include MarketSizeHelper
  include ValuePropositionHelper


  def create_child_resources
    PRIORITIES.each do |k, v|
      user_priorities.create(priority_id: k)
    end
    ResourcesForNewBeachheadWorker.perform_async(self.id)
  end


  def all_resources_created?
    financials.count == 5
  end


  # END USER FEARS

  def fears_and_motivations
    endusers.where(parameter: ['Motivation to use', 'Fear that prevents them from using']).pluck(:response)
  end


  # MARKET SIZE METHODS

  def total_endusers
    financials.sum(:no_of_customers)
  end

  def market_value
    market_sizes.inject(:+).to_i
  end

  def market_sizes
    array = []
    financials.each do |f|
      array.push(f.market_size)
    end
    array
  end

  def average_price
    if total_endusers == 0
      0
    else
      (market_value/total_endusers.to_f).to_i
    end
  end

  def market_size_segments_complete
    array = []
    financials.each do |f|
      array << f.customer_segment if f.market_size > 0
    end
    array
  end

  def market_size_segments_incomplete
    incomplete = [1, 2, 3, 4, 5] - market_size_segments_complete
    CUSTOMER_GROUPS.values_at(*incomplete)
  end

  def market_size_complete?
    market_size_segments_complete.count == 5
  end

  def total_additional_value_created
    arr = []
    self.user_priorities.each do |u|
      arr << u.additional_value_created
    end
    arr.inject(:+)
  end


  # DIAGNOSIS METHODS


  # CANVAS METHODS


  def canvas_empty?
    if end_user == nil &&
        application == nil &&
        benefit == nil &&
        marketing == nil &&
        sales == nil &&
        core == nil &&
        prod_features == nil &&
        top_competitors == nil &&
        partners == nil
      true
    else
      false
    end
  end


  def canvas_complete?
    if end_user != nil &&
        application != nil &&
        benefit != nil &&
        marketing != nil &&
        sales != nil &&
        core != nil &&
        prod_features != nil &&
        top_competitors != nil &&
        partners != nil
      true
    else
      false
    end
  end


  def canvas_started?
    if application != nil ||
        follow_on_markets != nil ||
        beachhead_size != nil ||
        end_user != nil ||
        core != nil ||
        top_user_priorities != nil ||
        dmu != nil ||
        use_case != nil ||
        prod_features != nil ||
        value_proposition != nil ||
        top_competitors != nil ||
        business_model != nil ||
        pricing != nil ||
        marketing != nil ||
        customer_reach != nil
      true
    else
      false
    end
  end

  def has_features?
    features.pluck(:feature_name).reject { |item| item == 'Primary Feature 1' }.count > 0
  end


  # END USERS METHODS


  def enduser_group_complete?(group)
    endusers.where(group: group).pluck(:response).compact.reject { |r| r == '' }.present?
  end


  def enduser_complete?
    endusers.pluck(:response).compact.reject { |r| r == '' }.present?
  end

  def enduser_not_started?
    endusers.pluck(:response).compact.reject { |r| r == '' }.blank?
  end

  # PERSONA METHODS

  def persona_group_complete?(group)
    personas.where(group: group).pluck(:response).compact.reject { |r| r == '' }.present?
  end

  def persona_complete?
    persona_details.for_group(4).pluck(:detail).compact.present?
  end

  def persona_not_started?
    personas.pluck(:response).compact.reject { |r| r == '' }.blank?
  end

  def purchasing_priorities
    persona_details.for_group(4).pluck(:detail).first(2)
  end


  def user_priority_ranked_at(rank)
    PRIORITIES[user_priorities.where(rank: rank).first.priority_id] if user_priorities.where(rank: rank).first
  end

  def chosen_business_model
    pricing_models.find_by(id: chosen_business_model_id).try(:model_name)
  end


  def persona_name
    personas.all.try(:first).try(:response)
  end

  def priority1
    persona_details.by_rank.for_group(4).first.try(:detail)
  end

  def priority2
    persona_details.by_rank.for_group(4)[1].try(:detail)
  end


  def dmu_not_started?
    customer_decision_making_units.pluck(:entity_name, :fears, :motivations, :expectations, :offering).flatten.compact.blank?
  end

  def dmu_started?
    customer_decision_making_units.pluck(:entity_name, :fears, :motivations, :expectations, :offering).flatten.compact.present?
  end

  def dmu_names
    customer_decision_making_units.pluck(:entity_name)
  end

  def dmu_complete?
    dmu_names.compact.delete_if { |x| x == '' }.count == 5
  end

  def advocate
    customer_decision_making_units.where(entity: 2).first.entity_name
  end


  # def usecase_not_started?
  #   touchpoints.pluck(:end_user_actions, :motivation_level, :ease_of_doing, :possible_intervention).flatten.compact.blank?
  # end
  #
  # def usecase_started?
  #   touchpoints.pluck(:end_user_actions, :motivation_level, :ease_of_doing, :possible_intervention).flatten.compact.present?
  # end

  def usecase_complete?
    touchpoints.where(touchpoint_no: 2..10).pluck(:end_user_actions, :motivation_level, :ease_of_doing).flatten.exclude? nil
  end

  def any_feature_categorized?
    features.all.pluck(:importance_rank, :customer_wanting_feature).flatten.compact.present?
  end

  def features_not_started?
    minimal = features.minimal.count
    mvp = features.mvp.count
    minimal == 0 && mvp - minimal == 0
  end

  def features_started?
    !features_not_started?
  end

  def features_complete?
    minimal = features.minimal.count
    mvp = features.mvp.count
    minimal > 0 && (mvp - minimal) > 0
  end

  def value_proposition_completed?
    value_metrics.pluck(:proposition_name, :as_is_state_value, :possible_state_value).flatten.exclude? nil
  end


  def canvas_changed?
    overlap = previous_changes.keys & ['application',
                                       'end_user',
                                       'benefit',
                                       'prod_features',
                                       'sales',
                                       'marketing',
                                       'partners',
                                       'core',
                                       'top_competitors']

    overlap.present?
  end

  def diagnosis_changed?
    overlap = previous_changes.keys & ['question1',
                                       'question2',
                                       'question3',
                                       'question4',
                                       'question5',
                                       'question6',
                                       'question7']
    overlap.present?
  end

  def size_checklist_changed?
    overlap = previous_changes.keys & ['word_of_mouth',
                                       'similar_product',
                                       'similar_sales']
    overlap.present?
  end

  def is_beachhead?
    product.beachhead.id == self.id
  end

  def accessibility_profile
    n = touchpoints.pluck(:motivation_level, :ease_of_doing)

    if n.flatten.include?(nil)
      nil
    else
      easy = n.select { |item| item[0] <= 3 && item[1] <= 3 }.count
      ok = (n.select { |item| item[0] == 4 && item[1] <= 4 } + n.select { |item| item[0] <= 3 && item[1] == 4 }).count
      difficult = 10 - (easy + ok)
      [easy, ok, difficult]
    end
  end

  def latest_release
    main_activities.releases.last
  end

  def latest_release_version
    {1 => :minimal, 2 => :mvp, 3 => :full}[latest_release.release_version] if latest_release && latest_release.release_version
  end

  def has_latest_release?
    latest_release.present? && !latest_release.completed?
  end

  def latest_milestone
    main_activities.special.last
  end

  def nearest_milestone
    main_activities.first
  end

  def latest_sales_target
    main_activities.sales_targets.last
  end


  def has_latest_sales_target?
    latest_sales_target.present? && !latest_sales_target.completed?
  end

  def post_milestone_completion?

  end


  def release_time?

    ## when the latest milestone is a sales target or a release that failed

    latest_milestone.blank? || latest_milestone.sales_target && latest_milestone.completed? || latest_milestone.release && latest_milestone.abandoned?

  end


  def sales_time?

    ## when the latest release was achieved

    latest_milestone.release && latest_milestone.achieved?

  end


  def release_count
    main_activities.releases.count
  end


  def cores_implemented
    comp_advantages.for_stage(4).pluck(:adv_name).compact
  end

  def cores_testing
    comp_advantages.for_stage(2).pluck(:adv_name).compact
  end


  def cores_implemented_or_tested
    implemented_cores = comp_advantages.for_stage(4).pluck(:adv_name)

    if implemented_cores.present?
      implemented_cores.to_sentence
    elsif testing_cores = comp_advantages.for_stage(2).pluck(:adv_name) && testing_cores.present?
      "#{testing_cores.to_sentence} are currently being tested"
    end
  end

  def business_models_implemented
    pricing_models.for_stage(4).pluck(:model_name).compact
  end

  def business_models_testing
    pricing_models.for_stage(2).pluck(:model_name).compact
  end


  def business_models_implemented_or_tested
    implemented_bms = pricing_models.for_stage(4).pluck(:model_name)


    if implemented_bms.present?
      implemented_bms.to_sentence
    elsif testing_bms = pricing_models.for_stage(2).pluck(:model_name) && testing_bms.present?
      "#{testing_bms.to_sentence} are currently being tested"
    end
  end

  def pricing_plans_implemented
    pricing_plans.for_stage(4).pluck(:plan_name).compact
  end

  def pricing_plans_testing
    pricing_plans.for_stage(2).pluck(:plan_name).compact
  end


  def pricing_plans_implemented_or_tested
    implemented_pps = pricing_plans.for_stage(4).pluck(:plan_name)


    if implemented_pps.present?
      implemented_pps.to_sentence
    elsif testing_pps = pricing_plans.for_stage(2).pluck(:plan_name) && testing_pps.present?
      "#{testing_pps.to_sentence} are currently being tested"
    end
  end


  def pricing_plans_being_tested
    pp = pricing_plans.for_stage(2).pluck(:plan_name)
    if pp.present?
      pp.to_sentence
    else
      'How to fix the right price for my product?'
    end
  end

  def better_players
    comp_positions = self.comp_positions.all
    product = comp_positions.first
    factor1, factor2 = product.factor1, product.factor2
    comp_positions.where("comp_positions.factor1 < ? OR comp_positions.factor2 < ?", factor1, factor2)
  end

  def has_better_competitors?
    better_players.present?
  end

  def has_no_better_competitors?
    better_players.blank?
  end

  def total_feedback
    customer_feedbacks.count
  end

  def total_critical_feedback
    customer_feedbacks.critical.count
  end


  def total_feature_sub_components_for(scope)

    sub_components = features.joins(:sub_components)

    case scope
      when :minimal
        sub_components.where(sub_components: {customers_want: 1}).where(sub_components: {importance: 1..2})
      when :mvp
        sub_components.where(sub_components: {customers_want: 1..3}).where(sub_components: {importance: 1..3}).count
      else
        sub_components.count
    end

  end

  def feature_sub_components_for(stage)
    features.joins(:sub_components).where(sub_components: {stage: stage})
  end

  def feature_issues_for(stage)
    features.joins(:issues).where(issues: {stage: stage})
  end


  def component_counts
    [
        feature_sub_components_for(1).count + feature_issues_for(1).count,
        feature_sub_components_for(2).count + feature_issues_for(2).count,
        feature_sub_components_for(3).count + feature_issues_for(3).count,
        feature_sub_components_for(4).count + feature_issues_for(4).count,
    ]
  end

  def release_progress
    total = feature_sub_components_for(1..4).count.to_f + feature_issues_for(1..4).count.to_f
    ongoing = feature_sub_components_for(3).count.to_f + feature_issues_for(3).count.to_f
    completed = feature_sub_components_for(4).count.to_f + feature_issues_for(4).count.to_f

    if total == 0
      [0, 0, 0]
    else
      [100,
       (ongoing/total * 100).to_i,
       (completed/total * 100).to_i,]
    end

  end

  def total_man_hours
    n = (feature_sub_components_for(2..3).pluck(:man_hours).compact.inject(:+) || 0) + (feature_issues_for(2..3).pluck(:man_hours).compact.inject(:+) || 0)
    n.round(2)
  end

  def projected_date
    num_of_days = if total_man_hours && total_man_hours > 0 && product.man_hours_per_day && product.man_hours_per_day > 0
                    total_man_hours / product.man_hours_per_day
                  end
    Date.today + num_of_days if num_of_days
  end


end
