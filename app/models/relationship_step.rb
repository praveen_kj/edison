class RelationshipStep < ActiveRecord::Base

  belongs_to :touchpoint
  default_scope { order('created_at ASC') }

  validates :step_desc, length: {:maximum => 300}


  def self.blueprint_for_table
    {

        rows:
            {1 => {field: :step_desc, type: :text, editable: true, placeholder: 'Enter the key interaction of your product with the end user.'},
            },
        placeholder: 'Add a new end users’ interaction',
        form_default_field: :step_desc
    }
  end

end
