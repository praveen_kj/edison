class Membership < ActiveRecord::Base



  belongs_to :product

  before_save :downcase_email
  after_create :save_member_id
  after_create :send_invitation

  default_scope { order('created_at ASC') }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates :member_email, uniqueness: {scope: :product_id, message: "This user has already been invited."}
  validates :member_email, format: {with: VALID_EMAIL_REGEX}

  validate :self_invite


  def self.joined
    all.select {|member| member.user.present? }
  end

  def downcase_email
    self.member_email.downcase!
  end


  def self_invite
    if self.product.user.email == self.member_email
      errors[:base] << ("Whoops! You just tried inviting yourself.")
    end
  end

  def send_invitation
    InvitationMailWorker.perform_async(id)
  end

  def save_member_id
    user = User.find_by(email: member_email)
    update_attributes(member_id: user.id) if user
  end

  def user
    User.where(email: member_email).first
  end



end
