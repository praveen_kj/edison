class CustomerFeedback < ActiveRecord::Base
  belongs_to :product
  has_many :comments
  include CommentsHelper::InstanceMethods

  scope :critical, -> { where(customer_interest: 1..2) }
  scope :unaddressed, -> { where(decision: nil) }
  scope :scheduled, -> { where(decision: 1) }
  scope :disregarded, -> { where(decision: 2) }
  scope :completed, -> { where(decision: 3) }
  scope :for_sub_feature, -> (sub_feature_id) { where(related_component_id: sub_feature_id) }
  scope :for_tool, -> (tool) { where(related_tool: tool) }


  attr_accessor :feedback_comment

  DECISION = {
      1 => 'Schedule response',
      2 => 'Disregard',
      3 => 'Completed',
  }

  default_scope { order('customer_feedbacks.created_at DESC') }

  after_create :generate_secret_token
  after_create :notify_team

  validates :feedback, presence: true
  validates :customer_name, presence: true


  def generate_secret_token
    update_attributes(secret_token: "reply-F-#{SecureRandom.urlsafe_base64}")
  end


  def descriptive_status
    case decision
      when 1
        'response scheduled'
      when 2
        'disregarded'
      when 3
        'completed'
      else
        'unaddressed'
    end
  end


  def user
    User.where(id: created_by).first || product.user
  end

  def notify_team
    CustomerFeedbackMailWorker.perform_async(self.id)
  end

  def feedback_title
    "#{customer_name}'s feedback on #{created_at.strftime('%d %b %Y')}"
  end

  def title_for_email
    feedback_title
  end

  def related_feature
    Feature.find(related_feature_id) if related_feature_id
  end

  def related_component
    Component.find(related_component_id) if related_component_id
  end

  def categorized?
    [related_tool, related_feature_id, related_component_id].any?
  end

  def reference_tag

    if related_tool
      [related_tool, STEP[related_tool][:name]]
    elsif related_component_id && related_feature_id
      [nil, "#{related_feature.feature_name} &rsaquo;&rsaquo; #{related_component.name}"]
    end

  end

  def remove_reference_tags
    update_attributes(
        related_feature_id: nil,
        related_component_id: nil,
        related_tool: nil
    )
  end

  def revoke_decision
    update_attributes(
        status: nil,

    )
  end

  def actionable?
    decision == 1
  end

  def assignee
    User.where(id: assigned_to).first
  end


  def friendly_link
    "/customer-insights/#{id}"
  end


end
