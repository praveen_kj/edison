class CompPosition < ActiveRecord::Base
  belongs_to :business
  default_scope { order('created_at ASC') }
  validates :competitor, presence: true, length: {:maximum => 50}


end
