class CompAdvantage < ActiveRecord::Base
  belongs_to :business

  default_scope { order('comp_advantages.created_at ASC') }
  validates :adv_name, presence: true
  validates :adv_name, length: {:maximum => 50}
  scope :for_stage, -> stage_no { where(stage: stage_no) }

  has_many :comments, dependent: :destroy

  delegate :product, to: :business

  include CommentsHelper::InstanceMethods

  after_create :generate_secret_token

  def friendly_link
    "/cores/#{id}"
  end

  def title_for_email
    adv_name
  end


  def preference

    if customer_want &&
        assets_available &&
        founders_want &&
        competitor_ability &&
        vendor_ability &&
        personal_goal &&
        financial_goal

      array = [customer_want,
               assets_available,
               founders_want,
               competitor_ability,
               vendor_ability,
               personal_goal,
               financial_goal]

      total = array.inject(:+)

      avg = (total/7.0)

      if avg > 2.25
        'Low'
      elsif avg.between?(1.51, 2.25)
        'Medium'
      else
        'High'
      end

    end


  end


end

