class Product < ActiveRecord::Base
  belongs_to :user
  has_many :businesses, dependent: :destroy
  has_many :notes, dependent: :destroy
  has_many :assumptions, dependent: :destroy
  has_many :user_tasks, dependent: :destroy
  has_many :sub_tasks, through: :user_tasks
  has_many :discussions, dependent: :destroy
  has_many :events, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :customer_feedbacks, dependent: :destroy
  has_many :memberships, dependent: :destroy
  has_many :dashboards, dependent: :destroy
  has_many :checklist_items, dependent: :destroy

  has_many :sub_tasks, through: :user_tasks
  has_many :main_activities, through: :businesses
  has_many :comp_advantages, through: :businesses
  has_many :pricing_models, through: :businesses
  has_many :pricing_plans, through: :businesses


  validates :enduser_base, numericality: {allow_blank: true, less_than_or_equal_to: 7_000_000_000}
  validates :product_name, presence: true
  validates :product_purpose, presence: true, allow_blank: true

  include ProductsHelper::InstanceMethods

  def nullify_stage
    update_attributes current_stage: nil
  end

  def create_business_and_set_beachhead
    business = self.businesses.create(business_name: 'Sub-market name', resources_created: true)
    self.user.update_attributes(beachheads: {self.id => business.id})
    business.create_child_resources
  end

  def has_tasks_for_today?
    date = lambda { Date.today }.call
    user_tasks.joins(:sub_tasks).where("? BETWEEN sub_tasks.start_date AND sub_tasks.end_date", date).count > 0
  end

  def task_completion_score
    tasks_that_fell_due = user_tasks.joins(:sub_tasks).where("sub_tasks.end_date >= ? AND sub_tasks.end_date <= ?", 1.week.ago, Time.zone.now)
    tasks_still_due = tasks_that_fell_due.where("sub_tasks.status IS NULL OR sub_tasks.status IS false")
    if tasks_that_fell_due.count == 0
      0
    else
      ((1 - tasks_still_due.count.to_f/ tasks_that_fell_due.count) * 100).to_i
    end
  end

  def self.having_tasks_for_today
    select do |p|
      p.has_tasks_for_today?
    end
  end

  # def send_daily_morning_email_to_team
  #   active_team.each do |u|
  #     u.send_daily_morning_email
  #   end
  # end


  def has_a_completed_canvas?
    array = []

    businesses.each do |b|
      if b.canvas_complete?
        array << true
        break
      end
    end

    array.include?(true)
  end

  def number_of_completed_canvases_and_diag
    hash = {canvases: 0, diagnoses: 0}
    businesses.each do |b|
      (hash[:canvases] = hash[:canvases] + 1) if b.canvas_complete?
      (hash[:diagnoses] = hash[:diagnoses] + 1) if b.diagnosed?
    end
    hash
  end


  def residuary_market
    array = self.businesses.pluck(:enduser_count).collect { |x| x.nil? ? 0 : x }
    identified_endusers = array.blank? ? 0 : array.inject(:+)
    (self.enduser_base || 0) - identified_endusers
  end

  def segementation_percent
    array = self.businesses.pluck(:enduser_count).collect { |x| x.nil? ? 0 : x }
    identified_endusers = array.blank? ? 0 : array.inject(:+)
    if self.enduser_base && self.enduser_base > 0
      (identified_endusers/self.enduser_base.to_f * 100).to_i
    else
      0
    end
  end

  def market_named?
    if businesses.count > 0
      businesses.first.business_name != 'Sub-market name'
    end
  end

  def global_market_value
    array = []
    businesses.each { |b| array << b.market_value }
    array.inject(:+)
  end

  def enduser_base_entered?
    enduser_base.present? && enduser_base > 0 && product_name.present?
  end

  def adequately_segmented?
    businesses.count >= 4
  end

  def all_markets_named?
    adequately_segmented? && businesses.all.pluck(:business_name).exclude?('Sub-market') && businesses.all.pluck(:business_name).exclude?('Sub-market name')
  end

  def all_markets_enduser_entered?
    businesses.all.pluck(:enduser_count).exclude?(nil) || businesses.all.pluck(:enduser_count).exclude?(0)
  end

  def ready_to_start?
    market_named? && enduser_base_entered? && adequately_segmented? && all_markets_named? && all_markets_enduser_entered?
  end

  def has_team?
    memberships.all.count > 0
  end

  def updated_outdoor_visit_today?
    date = lambda { Date.today }.call
    last_outdoor_visit_at.present? && last_outdoor_visit_at.to_date == date
  end

  def update_outdoor_visit(result)
    outdoor_visits_will_change!
    current = outdoor_visits || []
    if current.count == 7
      current.delete_at(0)
      current << result
    else
      current << result
    end
    update_attributes(outdoor_visits: current, last_outdoor_visit_at: Time.zone.now)
  end

  def getting_real_score
    if outdoor_visits.present? && last_outdoor_visit_at.present?
      unmarked_days = (Time.zone.now - last_outdoor_visit_at).div(86400)
      marked_days = outdoor_visits.last([7 - unmarked_days, 0].max)
      total = [(marked_days.count + unmarked_days), 3].min
      outside = [marked_days.count('t'), 3].min
      (5 * outside / total.to_f).to_i
    else
      0
    end
  end

  def udpate_opinion_asking(result)
    current = outdoor_visits
    if current.count == 7
      current.delete_at(0)
      current << result
    end
    update_attributes(opinions_asked: current, last_opinion_asked_at: Time.zone.now)
  end


end

