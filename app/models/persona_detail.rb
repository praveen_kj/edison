class PersonaDetail < ActiveRecord::Base
  belongs_to :business

  default_scope { order('created_at ASC') }

  validates :detail, length: {:maximum => 150}

  scope :for_group, -> group { where(group: group) }
  scope :by_rank, -> { reorder('rank ASC') }


  validates :priority, inclusion: {in: PRIORITIES, message: 'you done fucked up boy'}, allow_blank: true

  after_create :rank_at


  def self.blueprint_for_table
    {
        rows: {
            1 => {field: :detail, type: :text, editable: true, placeholder: '---'},
            2 => {field: :priority, type: :select, choices_array: PRIORITIES, modal: false},

        }
    }
  end


  def rank_at(position = 0)
    if position == 0
      rank = self.business.persona_details.for_group(4).count + 10_000
    else
      all_items = self.business.persona_details.for_group(4)
      before_position, at_position = all_items[position - 2], all_items[position - 1]
      rank = (before_position.rank - at_position.rank).abs/2
    end
    update_attributes(rank: rank)
  end

  def self.normalize_ranks(business_id)
    where(business_id: business_id).for_group(4).each_with_index do |item, index|
      item.update_attributes(rank: (index + 1) * 10_000)
    end
  end



end
