class Experiment < ActiveRecord::Base
  belongs_to :assumption

  validates :description, length: {:maximum => 300}
  validates :cost, length: {:maximum => 150}
  validates :validated_if, length: {:maximum => 300}
  validates :result_note, length: {:maximum => 300}


  scope :for_stage, -> stage_no { where(stage: stage_no) }

end
