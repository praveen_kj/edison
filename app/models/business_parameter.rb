class BusinessParameter < ActiveRecord::Base
  belongs_to :business

  default_scope {order('created_at ASC')}

  scope :competing_products, -> { where(group: 0) }
  scope :sales_strategies, -> { where(group: 1) }
  scope :end_user_groups, -> { where(group: 2) }


end
