class Customer < ActiveRecord::Base
  belongs_to :business
  default_scope { order('created_at ASC') }
  validates :name, presence: true

  validates :name, length: {:maximum => 50}
  scope :for_stage, -> stage_no { where(stage: stage_no) }



  def overdue?
    reach_out_date.present? && Date.today > reach_out_date
  end

  def days_to_go
    if reach_out_date.present?
      if overdue?
        0
      else
        (reach_out_date.to_date - Date.today).to_i
      end
    end

  end

end
