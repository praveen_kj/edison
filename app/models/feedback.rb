class Feedback < ActiveRecord::Base

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates :email, format: {with: VALID_EMAIL_REGEX}, allow_blank: true
  validate :email_or_user_id
  # validates :subject, presence: true
  validates :body, presence: true


  def email_or_user_id
    if [self.user_id, self.email].reject(&:blank?).size == 0
      errors[:base] << ("Please enter an email")
    end
  end


end
