class StrategyGap < ActiveRecord::Base

  belongs_to :sales_strategy
  default_scope { order('created_at ASC') }


  validates :gap_name, length: {:maximum => 300}


  def self.blueprint_for_table
    {rows:
         {
             1 => {field: :gap_name, type: :text, editable: true},
             2 => {field: :closure_date, type: :date, editable: true, placeholder:'target date'}

         },
     placeholder: 'Gaps in the strategy',
     form_default_field: :gap_name}
  end


end
