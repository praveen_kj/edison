class KeyActivity < ActiveRecord::Base
  default_scope {order('created_at ASC')}
  belongs_to :main_activity
  validates :activity_name, length: {maximum: 300}
  validates :capital, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}

  def self.blueprint_for_table
    {rows: {1 => {field: :activity_name, type: :text, editable: true, format: :textarea},
            2 => {field: :capital, type: :num, editable: true, placeholder: 'Capital Required', prefix:'$ '},
            3 => {field: :target_date, type: :date, editable: true, placeholder: 'Target date'},
    },
     placeholder: 'New Activity',
     form_default_field: :activity_name}
  end




end
