class Assumption < ActiveRecord::Base
  default_scope { order('created_at ASC') }

  belongs_to :product
  belongs_to :business
  belongs_to :user
  has_many :experiments
  has_many :comments

  attr_accessor :experiment_desc, :experiment_cost, :experiment_outcome

  validates :hypothesis, length: {:maximum => 300}
  validates :impact, length: {:maximum => 150}


  scope :for_step, -> step_id { where(step_no: step_id) }
  scope :user_added, -> { where(preset_id: nil) }
  scope :preset, -> { where('preset_id IS NOT NULL') }
  scope :unlocked, -> { where(unlocked: true) }
  scope :current, -> { where("archived IS NULL OR archived IS false") }
  scope :archived, -> { where(archived: true) }

  include CommentsHelper::InstanceMethods
  after_create :generate_secret_token


  def friendly_link
    "/assumptions/#{id}"
  end

  def title
    hypothesis
  end

  def assumption_status
    AssumptionHelper.assumption_status[status]
  end


end
