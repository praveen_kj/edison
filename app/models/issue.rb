class Issue < ActiveRecord::Base
  belongs_to :component
  default_scope { order('created_at ASC') }
  scope :for_stage, -> stage_no { where(stage: stage_no) }

  def which_stage_at
    {
        1 => 'idea',
        2 => 'planned',
        3 => 'under implementation',
        4 => 'implemented'
    }[stage] || '?'
  end


end
