class Dashboard < ActiveRecord::Base
  belongs_to :user
  belongs_to :product

  before_create :purge_existing

  validates :dashboard_ref, presence: true

  def purge_existing
    existing = Dashboard.where(user_id: user_id, product_id: product_id)
    existing.delete_all if existing.present?
  end

end

