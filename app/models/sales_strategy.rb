class SalesStrategy < ActiveRecord::Base

  belongs_to :business
  has_many :strategy_gaps

  default_scope { order('created_at ASC') }

  scope :for_stage, -> stage_no { where(stage: stage_no) }


  validates :strategy_name, length: {:maximum => 150}
  validates :sales_channel, length: {:maximum => 50}

  validates :prob_awareness, length: {:maximum => 300}
  validates :prod_awarness, length: {:maximum => 300}
  validates :prod_information, length: {:maximum => 300}
  validates :prod_sale, length: {:maximum => 300}
  validates :collection, length: {:maximum => 300}

  validates :advocate_name, length: {:maximum => 50}
  validates :ec_buyer_name, length: {:maximum => 50}
  validates :primary_influencer_name, length: {:maximum => 50}
  validates :other_influencer_name, length: {:maximum => 50}
  validates :veto_holder_name, length: {:maximum => 50}
  validates :advocate_expectations, length: {:maximum => 150}
  validates :ec_buyer_expectations, length: {:maximum => 150}
  validates :primary_influencer_expectations, length: {:maximum => 150}
  validates :other_influencer_expectations, length: {:maximum => 150}
  validates :veto_holder_expectations, length: {:maximum => 150}

  validates :prob_awareness_duration, numericality: {allow_blank: true, less_than_or_equal_to: 300}
  validates :prod_awarness_duration, numericality: {allow_blank: true, less_than_or_equal_to: 300}
  validates :prod_information_duration, numericality: {allow_blank: true, less_than_or_equal_to: 300}
  validates :prod_sale_duration, numericality: {allow_blank: true, less_than_or_equal_to: 300}
  validates :collection_duration, numericality: {allow_blank: true, less_than_or_equal_to: 300}
  validates :actual_prospects_reached, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}
  validates :actual_customers_acquired, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}
  validates :actual_customer_ltv, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}
  validates :actual_total_cost, numericality: {allow_blank: true, less_than_or_equal_to: 1_000_000_000}



  def cac
    if actual_customers_acquired == 0
      0
    else
      (actual_total_cost/actual_customers_acquired.to_f).to_i
    end
  end


  def ltv_cac_ratio
    if actual_customer_ltv && cac != 0
      (actual_customer_ltv/cac.to_f).round(1)
    end
  end

  def cycle_length
    if prob_awareness_duration &&
        prod_awarness_duration &&
        prod_information_duration &&
        prod_sale_duration &&
        collection_duration

      prob_awareness_duration +
          prod_awarness_duration +
          prod_information_duration +
          prod_sale_duration +
          collection_duration


    end
  end

  def self.blueprint_for_section
    {
        header_map: {
            heading: {field: :strategy_name, type: :text, modal: false, editable: true},
            rhs_fields: {
                1 => {field: :start_date, type: :date, placeholder: 'start date'},
                2 => {field: :end_date, type: :date, placeholder: 'end date'}
            }
        },

        section_break_text: "Sales Process",

        section_field_map: [

            # ['Sales Channel used',
            #  {field: :sales_channel, type: :text, editable: true, placeholder:'Direct Sales, value added reselling, email marketing etc.'}],
            #
            # ['Term',
            #  {field: :timeline, type: :select, choices_array: {1 => 'Short-term', 2 => 'Medium-term', 3 => 'Long-term'}}],

            ['Problem Awareness',
             {field: :prob_awareness, type: :text, editable: true, placeholder: 'How will the customer become aware of his problem?'},
             {field: :prob_awareness_duration, type: :num, editable: true, placeholder: 'How long will it take for the customer to become aware of his problem?', suffix: ' days'}],

            ['Product Awareness',
             {field: :prod_awarness, type: :text, editable: true, placeholder: 'How will customer become aware of your product'},
             {field: :prod_awarness_duration, type: :num, editable: true, placeholder: 'How long will it take the customer to become aware of your product', suffix: ' days'}],

            ['Product Information',
             {field: :prod_information, type: :text, editable: true, placeholder: 'How will the customer evaluate your product?'},
             {field: :prod_information_duration, type: :num, editable: true, placeholder: 'How long will it take for the customer to evaluate your product?', suffix: ' days'}],

            ['Product Sale',
             {field: :prod_sale, type: :text, editable: true, placeholder: 'How will a typical sales process close?'},
             {field: :prod_sale_duration, type: :num, editable: true, placeholder: 'How long will it take for a typical sales process to close?', suffix: ' days'}],

            ['Collection',
             {field: :collection, type: :text, editable: true, placeholder: 'How will you collect the consideration for your product?'},
             {field: :collection_duration, type: :num, editable: true, placeholder: 'How long will it take for you to collect the consideration for your product?', suffix: ' days'}],
        ],

        section_break_text1: "Gaps and Issues",

        section_break_text2: "Expectations of the DMU",

        section_field_map2: [

            ['Advocate',
             {field: :advocate_name, type: :text, editable: true, placeholder: 'Who is this person?'},
             {field: :advocate_expectations, type: :text, editable: true, placeholder: 'What do they want?'}],

            ['Economic Buyer',
             {field: :ec_buyer_name, type: :text, editable: true, placeholder: 'Who is this person?'},
             {field: :ec_buyer_expectations, type: :text, editable: true, placeholder: 'What do they want?'}],

            ['Primary Influencers',
             {field: :primary_influencer_name, type: :text, editable: true, placeholder: 'Who is this person?'},
             {field: :primary_influencer_expectations, type: :text, editable: true, placeholder: 'What do they want?'}],

            ['Other Influencers',
             {field: :other_influencers_name, type: :text, editable: true, placeholder: 'Who is this person?'},
             {field: :other_influencer_expectations, type: :text, editable: true, placeholder: 'What do they want?'}],

            ['Veto Holders',
             {field: :veto_holder_name, type: :text, editable: true, placeholder: 'Who is this person?'},
             {field: :veto_holder_expectations, type: :text, editable: true, placeholder: 'What do they want?'}],
        ],

        section_break_text3: "LTV vs CAC",

        section_field_map3: [

            ['Number of potential customer influenced by this sales strategy',
             {field: :actual_prospects_reached, type: :num, editable: true}],

            ['Number of paying customers by this sales strategy',
             {field: :actual_customers_acquired, type: :num, editable: true}],

            ['Revenue from a customer (LTV)',
             {field: :actual_customer_ltv, type: :num, editable: true, prefix: '$ '}],

            ['Total marketing and selling cost related to this sales strategy',
             {field: :actual_total_cost, type: :num, editable: true, prefix: '$ '}],
        ],

        child_table: :strategy_gap
    }
  end

end