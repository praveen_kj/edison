class SubComponent < ActiveRecord::Base
  belongs_to :component

  default_scope { order('sub_components.created_at ASC') }

  scope :for_stage, -> stage_no { where(stage: stage_no) }
  scope :minimal, -> { where(customers_want: 1).where(importance: 1..2) }
  scope :mvp, -> { where(customers_want: 1..3).where(importance: 1..3) }
  scope :full, -> { where(stage: 2..4) }

  validates :name, presence: true

  scope :permature_for_minimal, -> { for_stage(2..3).where('sub_components.customers_want BETWEEN ? AND ? OR sub_components.importance BETWEEN ? AND ?', 3, 5, 3, 5) }
  scope :permature_for_mvp, -> { for_stage(2..3).where('sub_components.customers_want BETWEEN ? AND ? OR sub_components.importance BETWEEN ? AND ?', 4, 5, 4, 5) }


  def which_stage_at
    {
        1 => 'idea',
        2 => 'planned',
        3 => 'under implementation',
        4 => 'implemented'
    }[stage] || '?'
  end

  def group
    if customers_want && importance
      case [customers_want, importance]
        when [1, 1], [1, 2]
          'minimal product'
        when [1, 3], [2, 1], [2, 2], [2, 3], [3, 1], [3, 2], [3, 3]
          'mvp'
        else
          'full featured product'
      end
    else
      'uncategorized'
    end
  end

  def groups
    cw = nil
    imp = nil
    if customers_want && importance
      cw = ['95%+', '60%+', '40%+', '10%+', ' 0%'][customers_want - 1]
      score = 6 - importance
      imp = '<i class="fa fa-star"></i>' * score
    end
    cw && imp ? "#{cw} #{imp}" : 'uncategorized'
  end


  def is_mvp?
    case [customers_want, importance]
      when [1, 1], [1, 2]
        true
      else
        false
    end
  end

  def is_minimal?
    case [customers_want, importance]
      when [1, 3], [2, 1], [2, 2], [2, 3], [3, 1], [3, 2], [3, 3]
        true
      else
        false
    end
  end


end
