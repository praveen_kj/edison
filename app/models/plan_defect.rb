class PlanDefect < ActiveRecord::Base

  belongs_to :pricing_plan

  default_scope { order('created_at ASC') }

  validates :defect_name, length: {:maximum => 300}

  def self.blueprint_for_table
    {rows: {1 => {field: :defect_name, type: :text, editable: true},
    },
     placeholder: 'New Defect',
     form_default_field: :defect_name}
  end


end
