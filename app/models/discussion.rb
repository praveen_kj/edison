class Discussion < ActiveRecord::Base

  default_scope { order('discussions.updated_at DESC') }
  belongs_to :user
  belongs_to :business
  belongs_to :product
  has_many :comments

  validates :title, presence: true
  validates :subject, presence: true

  attr_accessor :email_everyone

  after_create :generate_secret_token
  after_create :notify_team


  scope :for_step, -> step_id { where(step_no: step_id) }





  include CommentsHelper::InstanceMethods


  def friendly_link
    "/discussions/#{id}"
  end

  def title_for_email
    title
  end

  def notify_team
    DiscussionMailWorker.perform_async(self.id)
  end

end
