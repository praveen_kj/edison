# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160227131333) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "pg_stat_statements"

  create_table "assumptions", force: true do |t|
    t.integer  "business_id"
    t.integer  "user_id"
    t.text     "hypothesis"
    t.string   "impact"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "step_no"
    t.integer  "product_id"
    t.integer  "preset_id"
    t.boolean  "unlocked"
    t.integer  "status",       default: 1
    t.string   "secret_token"
  end

  add_index "assumptions", ["archived"], name: "index_assumptions_on_archived", using: :btree
  add_index "assumptions", ["business_id"], name: "index_assumptions_on_business_id", using: :btree
  add_index "assumptions", ["preset_id"], name: "index_assumptions_on_preset_id", using: :btree
  add_index "assumptions", ["product_id"], name: "index_assumptions_on_product_id", using: :btree
  add_index "assumptions", ["secret_token"], name: "index_assumptions_on_secret_token", using: :btree
  add_index "assumptions", ["step_no"], name: "index_assumptions_on_step_no", using: :btree
  add_index "assumptions", ["user_id"], name: "index_assumptions_on_user_id", using: :btree

  create_table "attractions", force: true do |t|
    t.integer  "pricing_plan_id"
    t.text     "attraction_name"
    t.integer  "revenue_increase"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attractions", ["pricing_plan_id"], name: "index_attractions_on_pricing_plan_id", using: :btree

  create_table "beachheads", force: true do |t|
    t.integer  "user_id"
    t.integer  "business_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "beachheads", ["user_id"], name: "index_beachheads_on_user_id", using: :btree

  create_table "benefits", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "benefit_name"
    t.integer  "feature_id"
    t.boolean  "admin_field"
    t.integer  "group"
    t.integer  "priority"
  end

  add_index "benefits", ["feature_id"], name: "index_benefits_on_feature_id", using: :btree
  add_index "benefits", ["group"], name: "index_benefits_on_group", using: :btree

  create_table "business_parameters", force: true do |t|
    t.integer  "business_id"
    t.text     "parameter_name"
    t.integer  "group"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "business_parameters", ["business_id"], name: "index_business_parameters_on_business_id", using: :btree

  create_table "businesses", force: true do |t|
    t.integer  "user_id"
    t.text     "business_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "end_user"
    t.text     "application"
    t.text     "benefit"
    t.text     "marketing"
    t.text     "sales"
    t.text     "core"
    t.text     "prod_features"
    t.text     "top_competitors"
    t.text     "partners"
    t.boolean  "question1"
    t.boolean  "question2"
    t.boolean  "question3"
    t.boolean  "question4"
    t.boolean  "question5"
    t.boolean  "question6"
    t.boolean  "question7"
    t.boolean  "resources_created"
    t.boolean  "similar_product"
    t.boolean  "similar_sales"
    t.boolean  "word_of_mouth"
    t.date     "value_prop_conf_date"
    t.text     "enduser_name"
    t.date     "eu_target_date1"
    t.date     "eu_target_date2"
    t.date     "eu_target_date3"
    t.date     "eu_target_date4"
    t.date     "persona_meeting_date"
    t.string   "persona_name"
    t.integer  "enduser_count",            limit: 8
    t.integer  "opening_users",                      default: 0
    t.boolean  "validated_enduser"
    t.date     "analysis_start_at"
    t.date     "analysis_end_at"
    t.integer  "product_id"
    t.integer  "chosen_business_model_id"
    t.integer  "diagnosis_result"
    t.text     "follow_on_markets"
    t.text     "beachhead_size"
    t.text     "top_user_priorities"
    t.text     "dmu"
    t.text     "use_case"
    t.text     "value_proposition"
    t.text     "business_model"
    t.text     "pricing"
    t.text     "customer_reach"
  end

  add_index "businesses", ["product_id"], name: "index_businesses_on_product_id", using: :btree
  add_index "businesses", ["user_id"], name: "index_businesses_on_user_id", using: :btree

  create_table "checklist_items", force: true do |t|
    t.integer  "product_id"
    t.integer  "business_id"
    t.string   "item_key"
    t.integer  "step"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "checklist_items", ["business_id"], name: "index_checklist_items_on_business_id", using: :btree
  add_index "checklist_items", ["product_id"], name: "index_checklist_items_on_product_id", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "user_id"
    t.integer  "discussion_id"
    t.integer  "note_id"
    t.integer  "user_task_id"
    t.integer  "assumption_id"
    t.text     "comment_text"
    t.integer  "likes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_feedback_id"
    t.integer  "product_id"
    t.integer  "sub_task_id"
    t.integer  "main_activity_id"
    t.integer  "comp_advantage_id"
    t.integer  "pricing_model_id"
    t.integer  "pricing_plan_id"
  end

  add_index "comments", ["assumption_id"], name: "index_comments_on_assumption_id", using: :btree
  add_index "comments", ["customer_feedback_id"], name: "index_comments_on_customer_feedback_id", using: :btree
  add_index "comments", ["discussion_id"], name: "index_comments_on_discussion_id", using: :btree
  add_index "comments", ["note_id"], name: "index_comments_on_note_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree
  add_index "comments", ["user_task_id"], name: "index_comments_on_user_task_id", using: :btree

  create_table "comp_advantages", force: true do |t|
    t.integer  "business_id"
    t.text     "adv_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "rank"
    t.boolean  "admin_field",             default: false
    t.text     "customer_want_note"
    t.text     "assets_available_note"
    t.text     "founders_want_note"
    t.text     "competitor_ability_note"
    t.text     "vendor_ability_note"
    t.text     "personal_goal_note"
    t.text     "financial_goal_note"
    t.integer  "customer_want"
    t.integer  "assets_available"
    t.integer  "founders_want"
    t.integer  "competitor_ability"
    t.integer  "vendor_ability"
    t.integer  "personal_goal"
    t.integer  "financial_goal"
    t.integer  "stage",                   default: 1
    t.string   "secret_token"
  end

  add_index "comp_advantages", ["business_id"], name: "index_comp_advantages_on_business_id", using: :btree

  create_table "comp_positions", force: true do |t|
    t.integer  "business_id"
    t.text     "competitor"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin_field", default: false
    t.integer  "factor1"
    t.integer  "factor2"
  end

  add_index "comp_positions", ["business_id"], name: "index_comp_positions_on_business_id", using: :btree

  create_table "components", force: true do |t|
    t.integer  "feature_id"
    t.string   "name"
    t.integer  "customers_want"
    t.integer  "importance"
    t.boolean  "built"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "components", ["feature_id"], name: "index_components_on_feature_id", using: :btree

  create_table "customer_decision_making_units", force: true do |t|
    t.integer  "business_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin_field",            default: false
    t.integer  "entity"
    t.string   "entity_name"
    t.text     "fears"
    t.text     "motivations"
    t.text     "expectations"
    t.text     "offering"
    t.text     "preferred_alternatives"
  end

  add_index "customer_decision_making_units", ["business_id"], name: "index_customer_decision_making_units_on_business_id", using: :btree

  create_table "customer_feedbacks", force: true do |t|
    t.integer  "business_id"
    t.string   "customer_name"
    t.text     "feedback"
    t.integer  "related_feature_id"
    t.integer  "related_component_id"
    t.integer  "customer_interest"
    t.boolean  "critical"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "status"
    t.integer  "product_id"
    t.string   "secret_token"
    t.integer  "created_by"
    t.integer  "related_tool"
    t.integer  "decision"
    t.date     "target_date"
    t.integer  "assigned_to"
  end

  add_index "customer_feedbacks", ["business_id"], name: "index_customer_feedbacks_on_business_id", using: :btree
  add_index "customer_feedbacks", ["product_id"], name: "index_customer_feedbacks_on_product_id", using: :btree

  create_table "customers", force: true do |t|
    t.integer  "business_id"
    t.string   "name"
    t.integer  "sales_strategy_id"
    t.datetime "reach_out_date"
    t.text     "notes"
    t.integer  "stage",             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "customers", ["business_id"], name: "index_customers_on_business_id", using: :btree

  create_table "cx_steps", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "touchpoint_id"
    t.text     "hurdle_name"
    t.date     "hurdle_target_date"
    t.integer  "no_of_customers"
    t.integer  "hurdle_type"
  end

  add_index "cx_steps", ["touchpoint_id"], name: "index_cx_steps_on_touchpoint_id", using: :btree

  create_table "dashboards", force: true do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.integer  "dashboard_ref"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dashboards", ["product_id"], name: "index_dashboards_on_product_id", using: :btree
  add_index "dashboards", ["user_id"], name: "index_dashboards_on_user_id", using: :btree

  create_table "discussions", force: true do |t|
    t.integer  "user_id"
    t.integer  "business_id"
    t.string   "title"
    t.text     "subject"
    t.integer  "step_no"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
    t.string   "secret_token"
  end

  add_index "discussions", ["business_id"], name: "index_discussions_on_business_id", using: :btree
  add_index "discussions", ["product_id"], name: "index_discussions_on_product_id", using: :btree
  add_index "discussions", ["secret_token"], name: "index_discussions_on_secret_token", using: :btree
  add_index "discussions", ["user_id"], name: "index_discussions_on_user_id", using: :btree

  create_table "endusers", force: true do |t|
    t.integer  "business_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "parameter"
    t.text     "response"
    t.boolean  "admin_field", default: false
    t.integer  "group"
  end

  add_index "endusers", ["business_id"], name: "index_endusers_on_business_id", using: :btree
  add_index "endusers", ["group"], name: "index_endusers_on_group", using: :btree

  create_table "events", force: true do |t|
    t.integer  "product_id"
    t.integer  "event_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "source_id"
    t.hstore   "other_data"
  end

  add_index "events", ["product_id"], name: "index_events_on_product_id", using: :btree

  create_table "experiments", force: true do |t|
    t.integer  "assumption_id"
    t.text     "description"
    t.text     "cost"
    t.text     "validated_if"
    t.integer  "result"
    t.text     "result_note"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by"
    t.integer  "stage"
  end

  add_index "experiments", ["assumption_id"], name: "index_experiments_on_assumption_id", using: :btree

  create_table "features", force: true do |t|
    t.integer  "business_id"
    t.date     "target_build_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "feature_name"
    t.integer  "effort"
    t.integer  "status"
    t.boolean  "mocked_up"
    t.text     "decision_notes"
    t.boolean  "iterated"
    t.integer  "customer_wanting_feature"
    t.integer  "importance_rank"
    t.integer  "persona_detail_id"
    t.text     "description"
    t.integer  "related_touchpoint"
  end

  add_index "features", ["business_id"], name: "index_features_on_business_id", using: :btree

  create_table "feedbacks", force: true do |t|
    t.string   "step_id"
    t.integer  "user_id"
    t.string   "page"
    t.string   "subject"
    t.text     "body"
    t.boolean  "status"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "financials", force: true do |t|
    t.integer  "business_id"
    t.integer  "no_of_customers",  limit: 8, default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ongoing_price",    limit: 8, default: 0
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "customer_segment"
  end

  add_index "financials", ["business_id"], name: "index_financials_on_business_id", using: :btree

  create_table "footprints", force: true do |t|
    t.integer  "user_id"
    t.string   "view"
    t.integer  "step_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "footprints", ["user_id"], name: "index_footprints_on_user_id", using: :btree

  create_table "improvements", force: true do |t|
    t.integer  "next_customer_id"
    t.date     "target_date"
    t.text     "improvement_name"
    t.integer  "revenue_increase"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "improvements", ["next_customer_id"], name: "index_improvements_on_next_customer_id", using: :btree

  create_table "innovations", force: true do |t|
    t.integer  "pricing_model_id"
    t.text     "innovation_name"
    t.integer  "revenue_increase"
    t.date     "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "innovations", ["pricing_model_id"], name: "index_innovations_on_pricing_model_id", using: :btree

  create_table "issues", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "touchpoint_no"
    t.integer  "stage",         default: 1
    t.datetime "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "component_id"
    t.float    "man_hours"
  end

  add_index "issues", ["component_id"], name: "index_issues_on_component_id", using: :btree

  create_table "iterations", force: true do |t|
    t.integer  "feature_id"
    t.text     "iteration_name"
    t.date     "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "stage",          default: 1
  end

  add_index "iterations", ["feature_id"], name: "index_iterations_on_feature_id", using: :btree

  create_table "key_activities", force: true do |t|
    t.text     "activity_name"
    t.date     "target_date"
    t.integer  "capital"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "main_activity_id"
  end

  add_index "key_activities", ["main_activity_id"], name: "index_key_activities_on_main_activity_id", using: :btree

  create_table "main_activities", force: true do |t|
    t.integer  "business_id"
    t.date     "target_date"
    t.integer  "effort"
    t.integer  "capital"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "main_activity_name"
    t.integer  "stage",              default: 1
    t.text     "notes"
    t.boolean  "release"
    t.integer  "release_version"
    t.text     "release_notes"
    t.boolean  "sales_target"
    t.string   "secret_token"
  end

  add_index "main_activities", ["business_id"], name: "index_main_activities_on_business_id", using: :btree

  create_table "memberships", force: true do |t|
    t.integer  "product_id"
    t.string   "member_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "write_tools",  default: false
    t.boolean  "write_utils",  default: true
    t.integer  "member_id"
    t.string   "role"
  end

  add_index "memberships", ["member_id"], name: "index_memberships_on_member_id", using: :btree
  add_index "memberships", ["product_id"], name: "index_memberships_on_product_id", using: :btree

  create_table "messages", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["product_id"], name: "index_messages_on_product_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "milestones", force: true do |t|
    t.integer  "business_id"
    t.integer  "step"
    t.integer  "cost"
    t.integer  "effort"
    t.date     "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "milestones", ["business_id"], name: "index_milestones_on_business_id", using: :btree

  create_table "next_customers", force: true do |t|
    t.integer  "business_id"
    t.text     "cust_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "use_case_note"
    t.text     "benefits_note"
    t.text     "value_created_note"
    t.text     "purchase_interest_note"
    t.date     "next_meeting_date"
    t.integer  "use_case"
    t.integer  "benefits"
    t.integer  "value_created"
    t.integer  "purchase_interest"
    t.integer  "stage",                  default: 1
  end

  add_index "next_customers", ["business_id"], name: "index_next_customers_on_business_id", using: :btree

  create_table "notes", force: true do |t|
    t.integer  "user_id"
    t.integer  "business_id"
    t.string   "title"
    t.text     "content"
    t.boolean  "archived"
    t.integer  "step_no"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
    t.string   "secret_token"
    t.boolean  "draft"
  end

  add_index "notes", ["archived"], name: "index_notes_on_archived", using: :btree
  add_index "notes", ["business_id"], name: "index_notes_on_business_id", using: :btree
  add_index "notes", ["product_id", "draft"], name: "index_notes_on_product_id_and_draft", using: :btree
  add_index "notes", ["product_id"], name: "index_notes_on_product_id", using: :btree
  add_index "notes", ["secret_token"], name: "index_notes_on_secret_token", using: :btree
  add_index "notes", ["step_no"], name: "index_notes_on_step_no", using: :btree
  add_index "notes", ["user_id"], name: "index_notes_on_user_id", using: :btree

  create_table "persona_details", force: true do |t|
    t.integer  "business_id"
    t.integer  "group"
    t.text     "detail"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "priority"
    t.integer  "rank"
  end

  add_index "persona_details", ["business_id"], name: "index_persona_details_on_business_id", using: :btree
  add_index "persona_details", ["group"], name: "index_persona_details_on_group", using: :btree

  create_table "personas", force: true do |t|
    t.integer  "business_id"
    t.text     "parameter"
    t.text     "response"
    t.boolean  "admin_field"
    t.integer  "group"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "personas", ["business_id"], name: "index_personas_on_business_id", using: :btree

  create_table "pitches", force: true do |t|
    t.integer  "business_id"
    t.text     "pitch_content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pitches", ["business_id"], name: "index_pitches_on_business_id", using: :btree

  create_table "plan_defects", force: true do |t|
    t.integer  "pricing_plan_id"
    t.text     "defect_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "plan_defects", ["pricing_plan_id"], name: "index_plan_defects_on_pricing_plan_id", using: :btree

  create_table "pricing_models", force: true do |t|
    t.integer  "business_id"
    t.text     "model_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin_field",                 default: false
    t.text     "willingness_to_buy_note"
    t.text     "favourable_pricing_note"
    t.text     "vendor_share_protected_note"
    t.text     "competitor_adoption_note"
    t.text     "allows_dist_incentives_note"
    t.integer  "willingness_to_buy"
    t.integer  "favourable_pricing"
    t.integer  "vendor_share_protected"
    t.integer  "competitor_adoption"
    t.integer  "allows_dist_incentives"
    t.integer  "preference"
    t.integer  "rank"
    t.integer  "stage",                       default: 1
    t.string   "secret_token"
  end

  add_index "pricing_models", ["business_id"], name: "index_pricing_models_on_business_id", using: :btree

  create_table "pricing_plans", force: true do |t|
    t.integer  "business_id"
    t.text     "preference"
    t.integer  "product_price"
    t.integer  "competitor_price_lb"
    t.integer  "competitor_price_ub"
    t.integer  "price_proportion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "plan_name"
    t.integer  "model_adopted"
    t.integer  "cust_group"
    t.integer  "invoice_timing"
    t.integer  "billing_frequency"
    t.integer  "ltv"
    t.integer  "stage",               default: 1
    t.string   "secret_token"
  end

  add_index "pricing_plans", ["business_id"], name: "index_pricing_plans_on_business_id", using: :btree

  create_table "procurement_steps", force: true do |t|
    t.integer  "business_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_stage"
    t.text     "key_proc_activity"
    t.date     "target_date"
    t.integer  "cost"
    t.text     "key_partners"
  end

  add_index "procurement_steps", ["business_id"], name: "index_procurement_steps_on_business_id", using: :btree

  create_table "products", force: true do |t|
    t.integer  "user_id"
    t.string   "product_name"
    t.integer  "enduser_base",        limit: 8
    t.text     "product_purpose"
    t.integer  "active_market"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "success_definition"
    t.string   "owner_role"
    t.integer  "stage_at_start"
    t.integer  "current_stage"
    t.integer  "man_hours_per_day"
    t.integer  "last_visited_step"
    t.date     "target_release_date"
    t.integer  "default_canvas"
    t.string   "release_version"
    t.boolean  "release_note_read"
    t.boolean  "welcome_msg_read"
  end

  add_index "products", ["user_id"], name: "index_products_on_user_id", using: :btree

  create_table "relationship_steps", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "touchpoint_id"
    t.text     "step_desc"
    t.date     "target_date"
  end

  add_index "relationship_steps", ["touchpoint_id"], name: "index_relationship_steps_on_touchpoint_id", using: :btree

  create_table "sales_strategies", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "business_id"
    t.string   "strategy_name"
    t.string   "sales_channel"
    t.integer  "timeline"
    t.date     "start_date"
    t.date     "end_date"
    t.text     "prob_awareness"
    t.text     "prod_awarness"
    t.text     "prod_information"
    t.text     "prod_sale"
    t.text     "collection"
    t.integer  "prob_awareness_duration"
    t.integer  "prod_awarness_duration"
    t.integer  "prod_information_duration"
    t.integer  "prod_sale_duration"
    t.integer  "collection_duration"
    t.text     "advocate_name"
    t.text     "ec_buyer_name"
    t.text     "primary_influencer_name"
    t.text     "other_influencer_name"
    t.text     "veto_holder_name"
    t.text     "advocate_expectations"
    t.text     "ec_buyer_expectations"
    t.text     "primary_influencer_expectations"
    t.text     "other_influencer_expectations"
    t.text     "veto_holder_expectations"
    t.integer  "actual_prospects_reached",        default: 0
    t.integer  "actual_customers_acquired",       default: 0
    t.integer  "actual_customer_ltv",             default: 0
    t.integer  "actual_total_cost",               default: 0
    t.integer  "stage",                           default: 1
  end

  add_index "sales_strategies", ["business_id"], name: "index_sales_strategies_on_business_id", using: :btree

  create_table "strategy_gaps", force: true do |t|
    t.integer  "sales_strategy_id"
    t.text     "gap_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "closure_date"
  end

  add_index "strategy_gaps", ["sales_strategy_id"], name: "index_strategy_gaps_on_sales_strategy_id", using: :btree

  create_table "sub_components", force: true do |t|
    t.integer  "component_id"
    t.string   "name"
    t.integer  "customers_want"
    t.integer  "importance"
    t.integer  "stage"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "man_hours"
  end

  add_index "sub_components", ["component_id"], name: "index_sub_components_on_component_id", using: :btree

  create_table "sub_tasks", force: true do |t|
    t.integer  "user_task_id"
    t.text     "sub_task_name"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "assignee_id"
    t.integer  "created_by"
    t.integer  "time_estimate"
    t.boolean  "priority"
    t.string   "secret_token"
    t.text     "description"
  end

  add_index "sub_tasks", ["user_task_id", "status"], name: "index_sub_tasks_on_user_task_id_and_status", using: :btree
  add_index "sub_tasks", ["user_task_id"], name: "index_sub_tasks_on_user_task_id", using: :btree

  create_table "touchpoints", force: true do |t|
    t.integer  "business_id"
    t.integer  "touchpoint_no"
    t.boolean  "intelligence"
    t.boolean  "openness"
    t.boolean  "conscientiousness"
    t.boolean  "kindness"
    t.boolean  "stability"
    t.boolean  "extraversion"
    t.text     "people_involved"
    t.text     "end_user_actions"
    t.text     "things_needed"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "con_time"
    t.integer  "agg_time"
    t.date     "target_date"
    t.integer  "duration"
    t.text     "hurdles_faced"
    t.float    "expected_conversion_rate", default: 0.0
    t.text     "possible_intervention"
    t.float    "current_week_metrics"
    t.float    "past_week_metrics"
    t.integer  "ease_of_doing"
    t.integer  "motivation_level"
  end

  add_index "touchpoints", ["business_id"], name: "index_touchpoints_on_business_id", using: :btree

  create_table "usage_durations", force: true do |t|
    t.integer  "user_id"
    t.string   "view"
    t.decimal  "time_spent"
    t.integer  "step_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "usage_durations", ["user_id"], name: "index_usage_durations_on_user_id", using: :btree

  create_table "user_parameters", force: true do |t|
    t.integer  "user_id"
    t.text     "company_name"
    t.text     "title"
    t.text     "mission"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "target_date"
    t.string   "user_name"
    t.integer  "enduser_base", limit: 8
    t.boolean  "subscription",           default: true
    t.string   "firm_name"
    t.string   "favourites",                            array: true
  end

  add_index "user_parameters", ["user_id"], name: "index_user_parameters_on_user_id", using: :btree

  create_table "user_priorities", force: true do |t|
    t.integer  "business_id"
    t.integer  "as_is_state"
    t.integer  "poss_state"
    t.integer  "rank"
    t.text     "tagline"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "priority_id"
  end

  add_index "user_priorities", ["business_id"], name: "index_user_priorities_on_business_id", using: :btree

  create_table "user_tasks", force: true do |t|
    t.integer  "business_id"
    t.string   "task_name"
    t.date     "end_date"
    t.integer  "step_no"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "task_description"
    t.integer  "category"
    t.integer  "user_id"
    t.integer  "diagnose_id"
    t.date     "start_date"
    t.integer  "product_id"
    t.string   "secret_token"
  end

  add_index "user_tasks", ["business_id"], name: "index_user_tasks_on_business_id", using: :btree
  add_index "user_tasks", ["product_id", "status"], name: "index_user_tasks_on_product_id_and_status", using: :btree
  add_index "user_tasks", ["product_id"], name: "index_user_tasks_on_product_id", using: :btree
  add_index "user_tasks", ["secret_token"], name: "index_user_tasks_on_secret_token", using: :btree
  add_index "user_tasks", ["step_no"], name: "index_user_tasks_on_step_no", using: :btree
  add_index "user_tasks", ["user_id"], name: "index_user_tasks_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                       default: "",   null: false
    t.string   "encrypted_password",          default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",               default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "username"
    t.string   "provider"
    t.string   "uid"
    t.text     "customer_id"
    t.text     "subscription_id"
    t.datetime "current_subscription_end_at"
    t.text     "referrer_code"
    t.integer  "referral_count",              default: 0
    t.datetime "trial_expiry_date"
    t.integer  "plan_id"
    t.datetime "tx_warning_sent_at"
    t.datetime "sx_warning_sent_at"
    t.datetime "canceled_at"
    t.integer  "last4"
    t.boolean  "admin"
    t.datetime "daily_reminder_sent_at"
    t.datetime "pricing_promo_sent_at"
    t.datetime "profile_completed_at"
    t.datetime "checklist_completed_at"
    t.integer  "number_of_canvases_complete"
    t.integer  "number_of_markets_diagnosed"
    t.datetime "current_streak_start_at"
    t.datetime "current_streak_end_at"
    t.datetime "highest_streak_start_at"
    t.datetime "highest_streak_end_at"
    t.datetime "last_streak_start_at"
    t.datetime "last_streak_end_at"
    t.hstore   "timespent_profile"
    t.boolean  "subscription_status"
    t.string   "card_id"
    t.hstore   "billing_info"
    t.integer  "active_days"
    t.integer  "active_product_id"
    t.hstore   "beachheads"
    t.string   "preferred_time_zone"
    t.integer  "preferred_time",              default: 6
    t.boolean  "receive_sitrep",              default: true
  end

  add_index "users", ["current_streak_end_at"], name: "index_users_on_current_streak_end_at", using: :btree
  add_index "users", ["customer_id"], name: "index_users_on_customer_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["referrer_code"], name: "index_users_on_referrer_code", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["subscription_id"], name: "index_users_on_subscription_id", using: :btree
  add_index "users", ["trial_expiry_date"], name: "index_users_on_trial_expiry_date", using: :btree

  create_table "value_metrics", force: true do |t|
    t.text     "as_is_state"
    t.text     "possible_state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin_field",          default: false
    t.text     "proposition_name"
    t.integer  "group"
    t.integer  "business_id"
    t.integer  "benefit_type"
    t.integer  "as_is_state_value"
    t.integer  "possible_state_value"
    t.string   "unit_of_measurement"
  end

  add_index "value_metrics", ["business_id"], name: "index_value_metrics_on_business_id", using: :btree

end
