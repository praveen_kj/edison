class CreateChecklistItems < ActiveRecord::Migration
  def change
    create_table :checklist_items do |t|
      t.references :product, index: true
      t.references :business, index: true
      t.string :item_key
      t.integer :step

      t.timestamps
    end
  end
end
