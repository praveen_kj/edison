class AddGroupToValueMetrics < ActiveRecord::Migration
  def change
    add_column :value_metrics, :group, :integer
  end
end
