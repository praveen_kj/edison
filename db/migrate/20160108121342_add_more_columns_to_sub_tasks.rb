class AddMoreColumnsToSubTasks < ActiveRecord::Migration
  def change
    add_column :sub_tasks, :time_estimate, :integer
    add_column :sub_tasks, :priority, :boolean
  end
end
