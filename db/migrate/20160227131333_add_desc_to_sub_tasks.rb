class AddDescToSubTasks < ActiveRecord::Migration
  def change
    add_column :sub_tasks, :description, :text
  end
end
