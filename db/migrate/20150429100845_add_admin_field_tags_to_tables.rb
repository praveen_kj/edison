class AddAdminFieldTagsToTables < ActiveRecord::Migration
  def change
    add_column :endusers, :admin_field, :boolean, default:false
    add_column :relationship_steps, :admin_field, :boolean, default:false
    add_column :value_metrics, :admin_field, :boolean, default:false
    add_column :comp_advantages, :admin_field, :boolean, default:false
    add_column :customer_decision_making_units, :admin_field, :boolean, default:false
    add_column :cx_steps, :admin_field, :boolean, default:false
    add_column :sales_strategies, :admin_field, :boolean, default:false
    add_column :comp_positions, :admin_field, :boolean, default:false
    add_column :procurement_steps, :admin_field, :boolean, default:false
    add_column :pricing_models, :admin_field, :boolean, default:false
  end
end
