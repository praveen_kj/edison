class AddUnlockedToAssumptions < ActiveRecord::Migration
  def change
    add_column :assumptions, :unlocked, :boolean
  end
end
