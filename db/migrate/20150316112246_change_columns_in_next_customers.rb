class ChangeColumnsInNextCustomers < ActiveRecord::Migration
  def change
    remove_column  :relationship_steps, :when_does_it_happen, :text
    remove_column  :next_customers, :purch_priorities_same, :text
    remove_column  :next_customers, :other_purch_priorities, :text
    remove_column  :next_customers, :purch_in_curr_form, :text
    remove_column  :next_customers, :addnl_needs, :text
    add_column :next_customers, :contacted, :boolean
    add_column :next_customers, :fluc, :boolean
    add_column :next_customers, :pspec, :boolean
    add_column :next_customers, :vprop, :boolean
    add_column :next_customers, :purch, :boolean
    add_column :next_customers, :improvements, :text
  end
end
