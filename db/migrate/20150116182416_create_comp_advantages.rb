class CreateCompAdvantages < ActiveRecord::Migration
  def change
    create_table :comp_advantages do |t|
      t.references :business, index: true
      t.text :adv_name
      t.text :how_build

      t.timestamps
    end
  end
end
