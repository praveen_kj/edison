class CreateCustomerFeedbacks < ActiveRecord::Migration
  def change
    create_table :customer_feedbacks do |t|
      t.references :business, index: true
      t.string :customer_name
      t.text :feedback
      t.integer :related_feature_id
      t.integer :related_component_id
      t.integer :customer_interest
      t.boolean :critical

      t.timestamps
    end
  end
end
