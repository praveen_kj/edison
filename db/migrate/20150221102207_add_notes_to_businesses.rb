class AddNotesToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :q1_notes, :text
    add_column :businesses, :q2_notes, :text
    add_column :businesses, :q3_notes, :text
    add_column :businesses, :q4_notes, :text
    add_column :businesses, :q5_notes, :text
    add_column :businesses, :q6_notes, :text
    add_column :businesses, :q7_notes, :text
  end
end
