class RemoveColsFromFinancials < ActiveRecord::Migration
  def change
    remove_column :financials, :duration, :integer
    remove_column :financials, :relationship_duration, :integer
    remove_column :financials, :one_time_revenues, :integer
    remove_column :financials, :monthly_revenues, :integer
    remove_column :financials, :upsell_revenues, :integer
    remove_column :financials, :production_salaries, :integer
    remove_column :financials, :production_material, :integer
    remove_column :financials, :production_overheads, :integer
    remove_column :financials, :sales_staff_salaries, :integer
    remove_column :financials, :marketing_cost, :integer
    remove_column :financials, :distributor_commission, :integer
    remove_column :financials, :discounts, :integer
    remove_column :financials, :capex, :integer
    remove_column :financials, :rentals, :integer
    remove_column :financials, :legal, :integer
    remove_column :financials, :others, :integer
    remove_column :financials, :business_model, :text
    remove_column :financials, :fair_value, :text
    remove_column :financials, :when_to_charge, :text
    remove_column :financials, :competitors_price, :integer
    remove_column :financials, :product_price, :integer
  end
end
