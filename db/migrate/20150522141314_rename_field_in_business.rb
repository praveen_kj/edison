class RenameFieldInBusiness < ActiveRecord::Migration
  def change
    rename_column :businesses, :enduser, :end_user
  end
end
