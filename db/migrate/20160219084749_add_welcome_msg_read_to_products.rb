class AddWelcomeMsgReadToProducts < ActiveRecord::Migration
  def change
    add_column :products, :welcome_msg_read, :boolean
  end
end
