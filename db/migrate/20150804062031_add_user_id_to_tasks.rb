class AddUserIdToTasks < ActiveRecord::Migration
  def change
    add_column :user_tasks, :user_id, :integer
  end
end
