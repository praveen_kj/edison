class ChangesToDmuTable < ActiveRecord::Migration
  def change
    remove_column :customer_decision_making_units, :member_type, :text
    remove_column :customer_decision_making_units, :title, :text
    remove_column :customer_decision_making_units, :function, :text
    remove_column :customer_decision_making_units, :veto_power, :text
    remove_column :customer_decision_making_units, :have_budget, :text
    remove_column :customer_decision_making_units, :part_of_eug, :text
    add_column :customer_decision_making_units, :parameter, :text
    add_column :customer_decision_making_units, :enduser, :text
    add_column :customer_decision_making_units, :champion, :text
    add_column :customer_decision_making_units, :ec_buyer, :text
  end
end





