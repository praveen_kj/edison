class AddCompanyNameToUserParameters < ActiveRecord::Migration
  def change
    add_column :user_parameters, :firm_name, :string
  end
end
