class AddColumnsToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :sales_vol_pm, :integer
    add_column :businesses, :prod_time, :integer
    add_column :businesses, :gm_reqd, :integer
    add_column :businesses, :roi_reqd, :integer
  end
end
