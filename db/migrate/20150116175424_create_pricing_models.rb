class CreatePricingModels < ActiveRecord::Migration
  def change
    create_table :pricing_models do |t|
      t.references :business, index: true
      t.text :model_name
      t.text :adoption

      t.timestamps
    end
  end
end
