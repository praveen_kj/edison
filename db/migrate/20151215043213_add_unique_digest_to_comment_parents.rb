class AddUniqueDigestToCommentParents < ActiveRecord::Migration
  def change

    add_column :notes, :secret_token, :string
    add_column :discussions, :secret_token, :string
    add_column :user_tasks, :secret_token, :string
    add_column :assumptions, :secret_token, :string

    add_index :notes, :secret_token
    add_index :discussions, :secret_token
    add_index :user_tasks, :secret_token
    add_index :assumptions, :secret_token



  end

end

