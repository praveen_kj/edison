class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :step_id
      t.integer :user_id
      t.string :page
      t.string :subject
      t.text :body
      t.boolean :status
      t.string :email

      t.timestamps
    end
  end
end
