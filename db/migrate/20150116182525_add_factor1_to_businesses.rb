class AddFactor1ToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :factor1, :text
    add_column :businesses, :factor2, :text
  end
end
