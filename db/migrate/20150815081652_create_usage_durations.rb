class CreateUsageDurations < ActiveRecord::Migration
  def change
    create_table :usage_durations do |t|

      t.integer :user_id, index: true
      t.string :view
      t.decimal :time_spent
      t.integer :step_id

      t.timestamps
    end
  end
end
