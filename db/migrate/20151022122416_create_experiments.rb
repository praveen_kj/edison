class CreateExperiments < ActiveRecord::Migration
  def change
    create_table :experiments do |t|
      t.references :assumption, index: true
      t.text :description
      t.text :cost
      t.text :validated_if
      t.integer :result
      t.text :result_note

      t.timestamps
    end
  end
end
