class AddProductIdToCustomerFeedbacks < ActiveRecord::Migration
  def change
    add_column :customer_feedbacks, :product_id, :integer
    add_index :customer_feedbacks, :product_id
  end
end
