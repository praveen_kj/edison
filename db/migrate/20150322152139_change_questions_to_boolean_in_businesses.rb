class ChangeQuestionsToBooleanInBusinesses < ActiveRecord::Migration
  def change

    remove_column :businesses, :question1, :text
    remove_column :businesses, :question2, :text
    remove_column :businesses, :question3, :text
    remove_column :businesses, :question4, :text
    remove_column :businesses, :question5, :text
    remove_column :businesses, :question6, :text
    remove_column :businesses, :question7, :text

    add_column :businesses, :question1, :boolean
    add_column :businesses, :question2, :boolean
    add_column :businesses, :question3, :boolean
    add_column :businesses, :question4, :boolean
    add_column :businesses, :question5, :boolean
    add_column :businesses, :question6, :boolean
    add_column :businesses, :question7, :boolean


  end
end
