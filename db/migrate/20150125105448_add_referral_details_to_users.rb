class AddReferralDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :referrer_code, :text, index: true
    add_column :users, :referral_count, :integer, :default => 0
    add_column :users, :trial_expiry_date, :datetime
  end
end
