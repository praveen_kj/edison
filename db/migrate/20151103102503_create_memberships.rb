class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.references :product, index: true
      t.string :member_email, index:true

      t.timestamps
    end
  end
end
