class AddParentIdToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :parent_id, :integer
  end
end
