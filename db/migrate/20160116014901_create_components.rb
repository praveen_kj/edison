class CreateComponents < ActiveRecord::Migration
  def change
    create_table :components do |t|
      t.references :feature, index: true
      t.string :name
      t.integer :customers_want
      t.integer :importance
      t.boolean :built

      t.timestamps
    end
  end
end
