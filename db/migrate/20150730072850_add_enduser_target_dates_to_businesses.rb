class AddEnduserTargetDatesToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :eu_target_date1, :date
    add_column :businesses, :eu_target_date2, :date
    add_column :businesses, :eu_target_date3, :date
    add_column :businesses, :eu_target_date4, :date
  end
end
