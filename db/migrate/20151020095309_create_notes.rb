class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.references :user, index: true
      t.references :business, index: true
      t.string :title
      t.text :content
      t.boolean :archived
      t.integer :step_no

      t.timestamps
    end
  end
end
