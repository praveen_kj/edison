class AddColumnsToProduct < ActiveRecord::Migration
  def change
    add_column :products, :outdoor_visits, :string, array:true
    add_column :products, :last_outdoor_visit_at, :datetime
    add_column :products, :opinions_asked, :string, array:true
    add_column :products, :last_opinion_asked_at, :datetime
  end
end
