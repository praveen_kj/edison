class AddColumnToRSteps < ActiveRecord::Migration
  def change
    add_column :relationship_steps, :target_date, :date
  end
end
