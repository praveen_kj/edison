class CreateSubTasks < ActiveRecord::Migration
  def change
    create_table :sub_tasks do |t|
      t.references :user_task, index: true
      t.text :sub_task_name
      t.date :start_date
      t.date :end_date
      t.boolean :status

      t.timestamps
    end
  end
end
