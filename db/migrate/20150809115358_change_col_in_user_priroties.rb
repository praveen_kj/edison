class ChangeColInUserPriroties < ActiveRecord::Migration
  def change
    remove_column :user_priorities, :priority, :text
    add_column :user_priorities, :priority_id, :integer
  end
end
