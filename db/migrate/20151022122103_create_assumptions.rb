class CreateAssumptions < ActiveRecord::Migration
  def change
    create_table :assumptions do |t|
      t.references :business, index: true
      t.references :user, index: true
      t.text :hypothesis
      t.string :impact
      t.boolean :archived

      t.timestamps
    end
  end
end
