class ChangeColumnsInPricingModels < ActiveRecord::Migration
  def change
    remove_column :pricing_models, :adoptable, :text
    remove_column :pricing_models, :adoption, :text
    add_column :pricing_models, :willingness_to_buy, :boolean
    add_column :pricing_models, :percent_value_captured, :text
    add_column :pricing_models, :when_to_share, :text
    add_column :pricing_models, :competitor_adoption, :boolean
    add_column :pricing_models, :allows_dist_incentives, :boolean
  end
end
