class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|

      t.references :user, index: true
      t.text :business_name


      t.timestamps
    end
  end
end



