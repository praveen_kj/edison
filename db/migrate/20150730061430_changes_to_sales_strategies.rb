class ChangesToSalesStrategies < ActiveRecord::Migration
  def change
    remove_column :sales_strategies, :business_id, :integer
    add_column :sales_strategies, :timeline_id, :integer
  end
end
