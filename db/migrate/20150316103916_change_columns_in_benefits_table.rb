class ChangeColumnsInBenefitsTable < ActiveRecord::Migration
  def change
    remove_column  :benefits, :category
    remove_column  :benefits, :importance
    remove_column  :benefits, :build
    rename_column :benefits, :metric_to_measure, :function
  end
end
