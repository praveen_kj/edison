class ChangeSalesStrategies < ActiveRecord::Migration
  def change
    remove_column :sales_strategies, :admin_field, :boolean
    remove_column :sales_strategies, :strategy_name, :text
    remove_column :sales_strategies, :target_date, :date
    remove_column :sales_strategies, :revenue_target, :integer
    remove_column :sales_strategies, :timeline_id, :integer

    add_column :sales_strategies, :business_id, :integer, index:true
    add_column :sales_strategies, :strategy_name,	:string
    add_column :sales_strategies, :sales_channel,	:string
    add_column :sales_strategies, :timeline,	:integer
    add_column :sales_strategies, :start_date,	:date
    add_column :sales_strategies, :end_date,	:date
    add_column :sales_strategies, :prob_awareness,	:text
    add_column :sales_strategies, :prod_awarness,	:text
    add_column :sales_strategies, :prod_information,	:text
    add_column :sales_strategies, :prod_sale,	:text
    add_column :sales_strategies, :collection,	:text
    add_column :sales_strategies, :prob_awareness_duration,	:integer
    add_column :sales_strategies, :prod_awarness_duration,	:integer
    add_column :sales_strategies, :prod_information_duration,	:integer
    add_column :sales_strategies, :prod_sale_duration,	:integer
    add_column :sales_strategies, :collection_duration,	:integer
    add_column :sales_strategies, :advocate_name,	:text
    add_column :sales_strategies, :ec_buyer_name,	:text
    add_column :sales_strategies, :primary_influencer_name,	:text
    add_column :sales_strategies, :other_influencer_name,	:text
    add_column :sales_strategies, :veto_holder_name,	:text
    add_column :sales_strategies, :advocate_expectations,	:text
    add_column :sales_strategies, :ec_buyer_expectations,	:text
    add_column :sales_strategies, :primary_influencer_expectations,	:text
    add_column :sales_strategies, :other_influencer_expectations,	:text
    add_column :sales_strategies, :veto_holder_expectations,	:text
    add_column :sales_strategies, :actual_prospects_reached,	:integer
    add_column :sales_strategies, :actual_customers_acquired,	:integer
    add_column :sales_strategies, :actual_customer_ltv,	:integer
    add_column :sales_strategies, :actual_total_cost,	:integer
    add_column :sales_strategies, :expected_prospects_reached,	:integer
    add_column :sales_strategies, :expected_customers_acquired,	:integer
    add_column :sales_strategies, :expected_customer_ltv,	:integer
    add_column :sales_strategies, :expected_total_cost,	:integer

  end
end
