class ChangeColsInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :current_subscription_end, :current_subscription_end_at
    change_column :users, :plan_id, 'integer USING CAST(plan_id AS integer)'
  end
end
