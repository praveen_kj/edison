class AddMoreColsToValueMetrics < ActiveRecord::Migration
  def change
    add_column :value_metrics, :as_is_state_value, :integer
    add_column :value_metrics, :possible_state_value, :integer
    add_column :value_metrics, :unit_of_measurement, :string
  end
end
