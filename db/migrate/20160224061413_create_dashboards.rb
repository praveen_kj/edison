class CreateDashboards < ActiveRecord::Migration
  def change
    create_table :dashboards do |t|
      t.references :user, index: true
      t.references :product, index: true
      t.integer :dashboard_ref

      t.timestamps
    end
  end
end
