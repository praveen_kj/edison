class CreateEndusers < ActiveRecord::Migration
  def change
    create_table :endusers do |t|
      t.references :business, index: true
      t.text :gender
      t.text :age
      t.text :income_range
      t.text :education_background
      t.text :geo_location
      t.text :motivations
      t.text :fears
      t.text :hero
      t.text :personality
      t.text :newspapers_read
      t.text :websites
      t.text :tv_shows
      t.text :purchase_motivations
      t.text :spl_identifiable
      t.text :story
      t.text :notes
      t.timestamps
    end
  end
end
