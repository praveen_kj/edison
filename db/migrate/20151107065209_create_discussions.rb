class CreateDiscussions < ActiveRecord::Migration
  def change
    create_table :discussions do |t|
      t.references :user, index: true
      t.references :business, index: true
      t.string :title
      t.text :subject
      t.integer :step_no

      t.timestamps
    end
  end
end
