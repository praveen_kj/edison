class ChangeColumnsInCompAdvantages < ActiveRecord::Migration
  def change
    remove_column  :comp_advantages, :how_build, :text
    add_column :comp_advantages, :customer_want, :boolean
    add_column :comp_advantages, :assets_available, :boolean
    add_column :comp_advantages, :founders_want, :boolean
    add_column :comp_advantages, :competitor_ability, :boolean
    add_column :comp_advantages, :vendor_ability, :boolean
    add_column :comp_advantages, :personal_goal, :boolean
    add_column :comp_advantages, :financial_goal, :boolean
    add_column :comp_advantages, :rank, :integer
  end
end
