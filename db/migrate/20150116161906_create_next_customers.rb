class CreateNextCustomers < ActiveRecord::Migration
  def change
    create_table :next_customers do |t|
      t.references :business, index: true
      t.text :cust_name
      t.text :purch_priorities_same
      t.text :other_purch_priorities
      t.text :purch_in_curr_form
      t.text :addnl_needs

      t.timestamps
    end
  end
end
