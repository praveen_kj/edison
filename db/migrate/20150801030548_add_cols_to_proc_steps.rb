class AddColsToProcSteps < ActiveRecord::Migration
  def change

    add_column :procurement_steps, :cost, :integer
    add_column :procurement_steps, :key_partners, :text

  end
end
