class AddDescriptionToFeatures < ActiveRecord::Migration
  def change
    add_column :features, :description, :text
    add_column :features, :related_touchpoint, :integer
  end
end
