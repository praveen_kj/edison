class AddIndexesToNotes < ActiveRecord::Migration
  def change
    add_index :notes, :step_no
    add_index :assumptions, :step_no
    add_index :user_tasks, :step_no
    add_index :user_tasks, :business_id
  end
end
