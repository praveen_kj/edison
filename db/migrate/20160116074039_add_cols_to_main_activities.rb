class AddColsToMainActivities < ActiveRecord::Migration
  def change
    add_column :main_activities, :release_version, :integer
    add_column :main_activities, :release_notes, :text
  end
end
