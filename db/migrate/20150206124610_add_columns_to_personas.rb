class AddColumnsToPersonas < ActiveRecord::Migration
  def change
    add_column :cx_steps, :metric, :text
    add_column :pricing_models, :adoptable, :text
    remove_column :procurement_steps, :top_3_vendors, :text
    add_column :procurement_steps, :vendor1, :text
    add_column :procurement_steps, :vendor2, :text
    add_column :procurement_steps, :vendor3, :text
  end
end
