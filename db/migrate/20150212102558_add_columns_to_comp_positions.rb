class AddColumnsToCompPositions < ActiveRecord::Migration
  def change
    add_column :comp_positions, :factor_one_rank, :integer
    add_column :comp_positions, :factor_two_rank, :integer
  end
end
