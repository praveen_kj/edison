class RemovePlatformFromBusinesses < ActiveRecord::Migration
  def change
    remove_column :businesses, :platform, :text
  end
end
