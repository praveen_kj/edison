class AddStageToTables < ActiveRecord::Migration
  def change
    add_column :comp_advantages, :stage, :integer, default: 1
    add_column :pricing_models, :stage, :integer, default: 1
    add_column :pricing_plans, :stage, :integer, default: 1
    add_column :sales_strategies, :stage, :integer, default: 1
    add_column :main_activities, :stage, :integer, default: 1
    add_column :iterations, :stage, :integer, default: 1
  end
end
