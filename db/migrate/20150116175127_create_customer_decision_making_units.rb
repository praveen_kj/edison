class CreateCustomerDecisionMakingUnits < ActiveRecord::Migration
  def change
    create_table :customer_decision_making_units do |t|
      t.references :business, index: true
      t.text :member_type
      t.text :title
      t.text :function
      t.text :veto_power
      t.text :have_budget
      t.text :part_of_eug

      t.timestamps
    end
  end
end
