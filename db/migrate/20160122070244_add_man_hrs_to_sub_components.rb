class AddManHrsToSubComponents < ActiveRecord::Migration
  def change
    add_column :sub_components, :man_hours, :integer
    add_column :products, :man_hours_per_day, :integer
  end
end
