class AddIndexToUserIdInUsageDurations < ActiveRecord::Migration
  def change
    add_index :usage_durations, :user_id
  end
end
