class AddDiagnoseIdToUserTasks < ActiveRecord::Migration
  def change
    add_column :user_tasks, :diagnose_id, :integer
  end
end
