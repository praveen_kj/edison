class AddMoreColsToFeatures < ActiveRecord::Migration
  def change
    add_column :features, :customer_wanting_feature, :integer
    add_column :features, :importance_rank, :integer
  end
end
