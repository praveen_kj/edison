class AddMetricsToTouchpoints < ActiveRecord::Migration
  def change

    add_column :touchpoints, :current_week_metrics, :float
    add_column :touchpoints, :past_week_metrics, :float

  end
end
