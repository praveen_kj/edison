class AddColsToFinancials < ActiveRecord::Migration
  def change
    add_column :financials, :ongoing_price, :integer, limit:8, default: 0
    add_column :financials, :business_model, :text
    add_column :financials, :fair_value, :text
    add_column :financials, :when_to_charge, :text
    add_column :financials, :key_pricing_pt, :text
    add_column :financials, :competitors_price, :integer, limit:8, default: 0
    add_column :financials, :product_price, :integer, limit:8, default: 0
  end
end
