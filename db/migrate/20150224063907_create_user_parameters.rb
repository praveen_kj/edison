class CreateUserParameters < ActiveRecord::Migration
  def change
    create_table :user_parameters do |t|
      t.references :user, index: true
      t.text :company_name
      t.text :title
      t.text :mission

      t.timestamps
    end
  end
end
