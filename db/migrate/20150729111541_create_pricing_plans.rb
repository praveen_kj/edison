class CreatePricingPlans < ActiveRecord::Migration
  def change
    create_table :pricing_plans do |t|
      t.references :business, index: true
      t.text :preference
      t.text :model_adopted
      t.text :cust_group
      t.integer :product_price
      t.text :invoice_timing
      t.text :billing_frequency
      t.integer :competitor_price_lb
      t.integer :competitor_price_ub
      t.integer :price_proportion

      t.timestamps
    end
  end
end
