class ChangesToValueProposition < ActiveRecord::Migration
  def change
    rename_column :value_metrics, :metric, :purchasing_priority
    rename_column :value_metrics, :current_state, :as_is_state
    rename_column :value_metrics, :user_product, :possible_state
    add_column :value_metrics, :benefit_type, :text
    add_column :value_metrics, :quantified_value, :text
  end
end
