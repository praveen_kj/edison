class ChangeColTypesInCxSteps < ActiveRecord::Migration
  def change
    change_column :cx_steps, :agg_time_estimate, :text
    change_column :cx_steps, :con_time_estimate, :text
  end
end
