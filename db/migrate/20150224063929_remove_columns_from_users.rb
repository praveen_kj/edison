class RemoveColumnsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :company_name
    remove_column :users, :mission_stmt
    remove_column :users, :title
  end
end
