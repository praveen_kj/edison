class CreateValueMetrics < ActiveRecord::Migration
  def change
    create_table :value_metrics do |t|
      t.text :metric
      t.text :current_state
      t.text :user_product
      t.references :business, index: true

      t.timestamps
    end
  end
end
