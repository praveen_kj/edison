class AddMissingIndexes < ActiveRecord::Migration
  def change

    add_index :benefits, :feature_id
    add_index :benefits, :group
    add_index :cx_steps, :touchpoint_id
    add_index :endusers, :group
    add_index :key_activities, :main_activity_id
    add_index :persona_details, :business_id
    add_index :persona_details, :group
    add_index :relationship_steps, :touchpoint_id
    add_index :sales_strategies, :business_id
    add_index :user_priorities, :business_id
    add_index :user_tasks, :user_id
    add_index :value_metrics, :business_id

  end
end
