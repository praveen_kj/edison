class AddTargetReleaseDateToProducts < ActiveRecord::Migration
  def change
    add_column :products, :target_release_date, :date
  end
end
