class AddStageColsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :stage_at_start, :integer
    add_column :products, :current_stage, :integer
  end
end
