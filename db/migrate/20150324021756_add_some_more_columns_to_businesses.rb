class AddSomeMoreColumnsToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :q_customer, :text
    add_column :businesses, :q_sales_channel, :text
    add_column :businesses, :q_revenue_model, :text
    add_column :businesses, :q_offering, :text
    add_column :businesses, :q_operation, :text
    add_column :businesses, :q_costs, :text
    add_column :businesses, :q_scale, :text
  end
end
