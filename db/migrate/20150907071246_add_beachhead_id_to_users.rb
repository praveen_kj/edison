class AddBeachheadIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :beachhead_id, :integer
  end
end
