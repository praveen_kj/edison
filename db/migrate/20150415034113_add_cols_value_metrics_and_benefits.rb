class AddColsValueMetricsAndBenefits < ActiveRecord::Migration
  def change
    add_column :value_metrics, :priority, :text
    add_column :benefits, :purchasing_priority, :text
  end
end
