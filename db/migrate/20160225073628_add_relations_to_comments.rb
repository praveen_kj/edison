class AddRelationsToComments < ActiveRecord::Migration
  def change

    add_column :comments, :product_id, :integer
    add_column :comments, :sub_task_id, :integer
    add_column :comments, :main_activity_id, :integer
    add_column :comments, :comp_advantage_id, :integer
    add_column :comments, :pricing_model_id, :integer
    add_column :comments, :pricing_plan_id, :integer


  end
end
