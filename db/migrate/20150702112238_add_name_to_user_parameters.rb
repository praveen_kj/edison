class AddNameToUserParameters < ActiveRecord::Migration
  def change
    add_column :user_parameters, :user_name, :string
  end
end
