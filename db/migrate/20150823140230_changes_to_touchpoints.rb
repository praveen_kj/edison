class ChangesToTouchpoints < ActiveRecord::Migration
  def change

    add_column :touchpoints, :expected_conversion_rate, :integer
    add_column :touchpoints, :expected_users, :integer

    remove_column :sales_strategies, :expected_prospects_reached,	:integer
    remove_column :sales_strategies, :expected_customers_acquired,	:integer
    remove_column :sales_strategies, :expected_customer_ltv,	:integer
    remove_column :sales_strategies, :expected_total_cost,	:integer

  end
end
