class AddStatusToCustomerFeedbacks < ActiveRecord::Migration
  def change
    add_column :customer_feedbacks, :status, :boolean

  end
end
