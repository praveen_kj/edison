class AddMoreColsToBusinesses < ActiveRecord::Migration
  def change


    add_column :businesses, :mvp_time_to_build, :text
    add_column :businesses, :mvp_effort_to_build, :text
    add_column :businesses, :mvp_cost_to_build, :integer
    add_column :businesses, :op_time_to_build, :text
    add_column :businesses, :op_effort_to_build, :text
    add_column :businesses, :op_cost_to_build, :integer
    add_column :businesses, :fop_time_to_build, :text
    add_column :businesses, :fop_effort_to_build, :text
    add_column :businesses, :fop_cost_to_build, :integer

  end
end
