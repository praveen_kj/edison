class AddCustomerFeedbackIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :customer_feedback_id, :integer
    add_index :comments, :customer_feedback_id
  end
end
