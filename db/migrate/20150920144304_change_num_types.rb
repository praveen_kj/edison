class ChangeNumTypes < ActiveRecord::Migration
  def change
    change_column :user_parameters, :enduser_base, :bigint
    change_column :businesses, :enduser_count, :bigint
  end
end
