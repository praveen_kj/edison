class CreateSalesStrategies < ActiveRecord::Migration
  def change
    create_table :sales_strategies do |t|
      t.references :business, index: true
      t.text :period
      t.text :length
      t.text :mix

      t.timestamps
    end
  end
end
