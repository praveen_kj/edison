class CreatePersonas < ActiveRecord::Migration
  def change
    create_table :personas do |t|
      t.references :business, index: true
      t.text :parameter
      t.text :response
      t.boolean :admin_field
      t.integer :group

      t.timestamps
    end
  end
end
