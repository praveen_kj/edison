class ChangeChoiceDrivenFieldToIntegers < ActiveRecord::Migration
  def change

    remove_column :persona_details, :priority, :text

    remove_column :features, :benefit_type, :text
    remove_column :features, :status, :text
    remove_column :benefits, :benefit_type, :text
    remove_column :value_metrics, :benefit_type, :text

    remove_column :next_customers, :use_case, :text
    remove_column :next_customers, :benefits, :text
    remove_column :next_customers, :value_created, :text
    remove_column :next_customers, :purchase_interest, :text
    remove_column :comp_advantages, :customer_want, :text
    remove_column :comp_advantages, :assets_available, :text
    remove_column :comp_advantages, :founders_want, :text
    remove_column :comp_advantages, :competitor_ability, :text
    remove_column :comp_advantages, :vendor_ability, :text
    remove_column :comp_advantages, :personal_goal, :text
    remove_column :comp_advantages, :financial_goal, :text
    remove_column :comp_positions, :factor1, :text
    remove_column :comp_positions, :factor2, :text
    remove_column :cx_steps, :hurdle_type, :text
    remove_column :pricing_models, :willingness_to_buy, :text
    remove_column :pricing_models, :favourable_pricing, :text
    remove_column :pricing_models, :vendor_share_protected, :text
    remove_column :pricing_models, :competitor_adoption, :text
    remove_column :pricing_models, :allows_dist_incentives, :text
    remove_column :pricing_models, :preference, :text
    remove_column :pricing_plans, :model_adopted, :text
    remove_column :pricing_plans, :cust_group, :text
    remove_column :pricing_plans, :invoice_timing, :text
    remove_column :pricing_plans, :billing_frequency, :text
    remove_column :pricing_models, :when_to_share, :text
    remove_column :features, :user_priorities, :text


    add_column :persona_details, :priority, :integer
    add_column :features, :benefit_type, :integer
    add_column :features, :status, :integer
    add_column :benefits, :priority, :integer
    add_column :value_metrics, :benefit_type, :integer
    add_column :next_customers, :use_case, :integer
    add_column :next_customers, :benefits, :integer
    add_column :next_customers, :value_created, :integer
    add_column :next_customers, :purchase_interest, :integer
    add_column :comp_advantages, :customer_want, :integer
    add_column :comp_advantages, :assets_available, :integer
    add_column :comp_advantages, :founders_want, :integer
    add_column :comp_advantages, :competitor_ability, :integer
    add_column :comp_advantages, :vendor_ability, :integer
    add_column :comp_advantages, :personal_goal, :integer
    add_column :comp_advantages, :financial_goal, :integer
    add_column :comp_positions, :factor1, :integer
    add_column :comp_positions, :factor2, :integer
    add_column :cx_steps, :hurdle_type, :integer
    add_column :pricing_models, :willingness_to_buy, :integer
    add_column :pricing_models, :favourable_pricing, :integer
    add_column :pricing_models, :vendor_share_protected, :integer
    add_column :pricing_models, :competitor_adoption, :integer
    add_column :pricing_models, :allows_dist_incentives, :integer
    add_column :pricing_models, :preference, :integer
    add_column :pricing_plans, :model_adopted, :integer
    add_column :pricing_plans, :cust_group, :integer
    add_column :pricing_plans, :invoice_timing, :integer
    add_column :pricing_plans, :billing_frequency, :integer
    add_column :features, :target_mock_date, :date







  end
end
