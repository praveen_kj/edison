class AddNextMeetingDateToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :persona_meeting_date, :date
    add_column :businesses, :persona_name, :string
  end
end
