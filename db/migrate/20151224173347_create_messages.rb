class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :product, index: true
      t.references :user, index: true
      t.text :content

      t.timestamps
    end
  end
end
