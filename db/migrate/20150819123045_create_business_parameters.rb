class CreateBusinessParameters < ActiveRecord::Migration
  def change
    create_table :business_parameters do |t|
      t.references :business, index: true
      t.text :parameter_name
      t.integer :group

      t.timestamps
    end
  end
end
