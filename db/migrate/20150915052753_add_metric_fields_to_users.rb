class AddMetricFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :profile_completed_at, :datetime
    add_column :users, :checklist_completed_at, :datetime
    add_column :users, :number_of_canvases_complete, :integer
    add_column :users, :number_of_markets_diagnosed, :integer
    add_column :users, :current_streak_start_at, :datetime
    add_column :users, :current_streak_end_at, :datetime
    add_column :users, :highest_streak_start_at, :datetime
    add_column :users, :highest_streak_end_at, :datetime
    add_column :users, :last_streak_start_at, :datetime
    add_column :users, :last_streak_end_at, :datetime
    add_column :users, :timespent_profile, :hstore
  end
end
