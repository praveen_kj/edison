class AddClosureDateToStrategyGaps < ActiveRecord::Migration
  def change
    add_column :strategy_gaps, :closure_date, :date
  end
end
