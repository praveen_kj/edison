class AddPricingPromoDateToUsers < ActiveRecord::Migration

  def change
    add_column :users, :pricing_promo_sent_at, :datetime
  end

end
