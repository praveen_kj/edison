class ChangeColumnsInEndusers < ActiveRecord::Migration
  def change
    remove_column :endusers, :gender
    add_column :endusers, :males, :text
    add_column :endusers, :females, :text
    add_column :endusers, :languages, :text
  end
end
