class CreateRelationshipSteps < ActiveRecord::Migration
  def change
    create_table :relationship_steps do |t|
      t.references :business, index: true
      t.text :event
      t.text :what_happens
      t.text :why_does_it_happen
      t.text :who_is_involved
      t.text :where_does_it_happen
      t.text :when_does_it_happen
      t.text :how_long_does_it_take

      t.timestamps
    end
  end
end
