class AddProductIdToDiscussions < ActiveRecord::Migration
  def change
    add_column :discussions, :product_id, :integer
    add_index :discussions, :product_id
  end
end
