class ChangesToProcSteps < ActiveRecord::Migration
  def change
    remove_column :procurement_steps, :parameter, :text
    remove_column :procurement_steps, :mvbp, :text
    remove_column :procurement_steps, :tam, :text
    remove_column :procurement_steps, :followon, :text
    remove_column :procurement_steps, :admin_field, :boolean

    add_column :procurement_steps, :product_stage, :integer
    add_column :procurement_steps, :key_proc_activity, :text
    add_column :procurement_steps, :target_date, :date
  end
end
