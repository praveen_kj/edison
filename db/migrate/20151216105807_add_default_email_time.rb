class AddDefaultEmailTime < ActiveRecord::Migration
  def change
    change_column_default :users, :preferred_time, 6
  end
end
