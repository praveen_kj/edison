class CreateUserPriorities < ActiveRecord::Migration
  def change
    create_table :user_priorities do |t|
      t.references :business
      t.text :priority
      t.integer :as_is_state
      t.integer :poss_state
      t.integer :rank
      t.text :tagline

      t.timestamps
    end
  end
end
