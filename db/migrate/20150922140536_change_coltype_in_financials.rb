class ChangeColtypeInFinancials < ActiveRecord::Migration
  def change
    change_column :financials, :no_of_customers, :bigint
  end
end
