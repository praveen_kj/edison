class ChangeTypeInProducts < ActiveRecord::Migration
  def change
    change_column :products, :enduser_base, :bigint
  end
end
