class AddSecretTokenToCustomerFeedbacks < ActiveRecord::Migration
  def change
    add_column :customer_feedbacks, :secret_token, :string
  end
end
