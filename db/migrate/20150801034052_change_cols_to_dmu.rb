class ChangeColsToDmu < ActiveRecord::Migration
  def change
    remove_column :customer_decision_making_units, :name, :text
    remove_column :customer_decision_making_units, :category, :text

    add_column :customer_decision_making_units, :entity, :integer
    add_column :customer_decision_making_units, :category, :integer

  end
end
