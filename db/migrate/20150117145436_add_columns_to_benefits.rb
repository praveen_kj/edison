class AddColumnsToBenefits < ActiveRecord::Migration
  def change
    add_column :benefits, :category, :text
    add_column :benefits, :importance, :text
    add_column :benefits, :build, :text
  end
end
