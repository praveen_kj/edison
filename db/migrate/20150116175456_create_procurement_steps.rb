class CreateProcurementSteps < ActiveRecord::Migration
  def change
    create_table :procurement_steps do |t|
      t.references :business, index: true
      t.text :key_activity
      t.text :top_3_vendors
      t.text :time_taken

      t.timestamps
    end
  end
end
