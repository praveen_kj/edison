class ChangeColsInUserTasks < ActiveRecord::Migration
  def change

    add_column :user_tasks, :start_date, :date
    rename_column :user_tasks, :target_date, :end_date

  end
end
