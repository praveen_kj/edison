class AddMoreColsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :canceled_at,	:datetime
    add_column :users, :last4,	:integer
    add_index :users, :customer_id
    add_index :users, :subscription_id
    add_index :users, :referrer_code
  end
end
