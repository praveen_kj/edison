class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.references :business, index: true
      t.integer :feature_id
      t.string :title
      t.text :description
      t.integer :touchpoint_no
      t.integer :stage, default: 1
      t.datetime :target_date

      t.timestamps
    end
  end
end
