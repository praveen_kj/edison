class ChangeColTypesNextCustomers < ActiveRecord::Migration
  def change
    change_column :next_customers, :use_case, :text
    change_column :next_customers, :benefits, :text
    change_column :next_customers, :value_created, :text
    change_column :next_customers, :purchase_interest, :text
  end
end
