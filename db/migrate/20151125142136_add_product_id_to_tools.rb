class AddProductIdToTools < ActiveRecord::Migration
  def change

    add_column :notes, :product_id, :integer
    add_column :user_tasks, :product_id, :integer
    add_column :assumptions, :product_id, :integer

    add_index :notes, :product_id
    add_index :user_tasks, :product_id
    add_index :assumptions, :product_id

  end
end
