class MoveOpgUsersToBusinesses < ActiveRecord::Migration
  def change
    remove_column :touchpoints, :expected_users, :integer
    add_column :businesses, :opening_users, :integer, default: 0
  end
end
