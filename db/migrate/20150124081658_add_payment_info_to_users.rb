class AddPaymentInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :customer_id, :text, index: true
    add_column :users, :subscription_id, :text, index: true
    add_column :users, :current_subscription_end, :datetime
  end
end
