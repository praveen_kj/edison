class AddIndexToNotes < ActiveRecord::Migration
  def change
    add_index :notes, [:product_id, :draft]
  end
end
