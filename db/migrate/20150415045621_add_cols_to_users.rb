class AddColsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :tx_warning_sent_at, :datetime
    add_column :users, :sx_warning_sent_at, :datetime
  end
end
