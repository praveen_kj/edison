class AddBeachheadsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :beachheads, :hstore
  end
end
