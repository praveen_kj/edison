class AddReleaseNoteReadToProducts < ActiveRecord::Migration
  def change
    add_column :products, :release_note_read, :boolean
  end
end
