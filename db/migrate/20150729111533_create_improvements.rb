class CreateImprovements < ActiveRecord::Migration
  def change
    create_table :improvements do |t|
      t.references :next_customer, index: true
      t.date :target_date
      t.text :improvement_name
      t.integer :revenue_increase

      t.timestamps
    end
  end
end
