class ChangesToBusinesses < ActiveRecord::Migration
  def change
    rename_column :businesses, :influencers, :marketing
    rename_column :businesses, :sales_channel, :sales
    rename_column :businesses, :market_size, :prod_features
    rename_column :businesses, :assets_required, :partners
    add_column :businesses, :enduser_count, :integer
    remove_column :businesses, :parent_id, :integer
    add_column :user_parameters, :enduser_base, :integer
  end
end
