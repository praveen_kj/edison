class AddSavingsToBusiness < ActiveRecord::Migration
  def change
    add_column :businesses, :latest_savings, :integer
  end
end
