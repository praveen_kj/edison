class AddReleaseVersionToProducts < ActiveRecord::Migration
  def change
    add_column :products, :release_version, :string
  end
end
