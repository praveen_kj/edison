class AddDatesToFinancials < ActiveRecord::Migration
  def change

    add_column :financials, :start_date, :date
    add_column :financials, :end_date, :date
    remove_column :financials, :customer_segment, :text
    add_column :financials, :customer_segment, :integer

  end
end
