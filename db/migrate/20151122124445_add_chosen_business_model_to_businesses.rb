class AddChosenBusinessModelToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :chosen_business_model_id, :integer
  end
end
