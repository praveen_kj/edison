class ChangesColsInIssues < ActiveRecord::Migration
  def change

    remove_column :issues, :business_id, :integer

    remove_column :issues, :feature_id, :integer

    add_column :issues, :component_id, :integer

    add_column :issues, :man_hours, :float

    add_index :issues, :component_id

  end
end
