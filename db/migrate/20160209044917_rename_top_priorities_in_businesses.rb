class RenameTopPrioritiesInBusinesses < ActiveRecord::Migration
  def change
    rename_column :businesses, :user_priorities, :top_user_priorities
  end
end
