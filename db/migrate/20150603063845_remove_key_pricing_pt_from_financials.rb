class RemoveKeyPricingPtFromFinancials < ActiveRecord::Migration
  def change
    remove_column :financials, :key_pricing_pt, :text
  end
end
