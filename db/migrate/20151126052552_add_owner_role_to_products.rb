class AddOwnerRoleToProducts < ActiveRecord::Migration
  def change
    add_column :products, :owner_role, :string
  end
end
