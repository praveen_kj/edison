class ChangesToDmu < ActiveRecord::Migration
  def change

    remove_column :customer_decision_making_units, :parameter, :text
    remove_column :customer_decision_making_units, :enduser, :text
    remove_column :customer_decision_making_units, :champion, :text
    remove_column :customer_decision_making_units, :ec_buyer, :text


    add_column :customer_decision_making_units, :entity, :text
    add_column :customer_decision_making_units, :incentive, :text
    add_column :customer_decision_making_units, :primary_influencer, :text
    add_column :customer_decision_making_units, :secondary_influencer, :text
    add_column :customer_decision_making_units, :veto, :text


  end
end
