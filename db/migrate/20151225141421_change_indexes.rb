class ChangeIndexes < ActiveRecord::Migration
  def change

    remove_index :user_tasks, :status
    add_index :user_tasks, [:product_id, :status]
    add_index :sub_tasks, [:user_task_id, :status]

  end
end
