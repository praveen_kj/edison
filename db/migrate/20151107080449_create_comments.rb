class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :user, index: true
      t.references :discussion, index: true
      t.references :note, index: true
      t.references :user_task, index: true
      t.references :assumption, index: true
      t.text :comment_text
      t.integer :likes

      t.timestamps
    end
  end
end
