class ChangeProducts < ActiveRecord::Migration
  def change
    remove_column :products, :outdoor_visits, :integer
    remove_column :products, :opinions_asked, :integer
    remove_column :products, :last_outdoor_visit_at, :date
    remove_column :products, :last_opinion_asked_at, :date
    add_column :products, :last_visited_step, :integer

  end
end
