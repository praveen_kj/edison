class NewMilestonesTable < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.references :business, index:true
      t.integer :step
      t.integer :cost
      t.integer :effort
      t.date :target_date

      t.timestamps
    end
  end
end
