class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.references :business, index: true
      t.text :user_priorities
      t.text :how_it_works
      t.date :target_build_date

      t.timestamps
    end
  end
end
