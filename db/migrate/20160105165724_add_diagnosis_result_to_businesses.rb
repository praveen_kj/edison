class AddDiagnosisResultToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :diagnosis_result, :integer
  end
end
