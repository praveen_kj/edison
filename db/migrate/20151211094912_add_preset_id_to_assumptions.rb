class AddPresetIdToAssumptions < ActiveRecord::Migration
  def change
    add_column :assumptions, :preset_id, :integer
    add_index :assumptions, :preset_id
  end
end
