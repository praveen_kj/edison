class AddMemberIdToMemberships < ActiveRecord::Migration
  def change
    add_column :memberships, :member_id, :integer
    add_column :memberships, :role, :string
    add_index :memberships, :member_id
  end
end
