class CreatePersonaDetails < ActiveRecord::Migration
  def change
    create_table :persona_details do |t|
      t.references :business
      t.integer :group
      t.text :detail
      t.text :priority

      t.timestamps
    end
  end
end
