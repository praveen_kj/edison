class ChangeColumsBusinessesAndEndusers < ActiveRecord::Migration
  def change
    remove_column :businesses, :q1_notes, :text
    remove_column :businesses, :q2_notes, :text
    remove_column :businesses, :q3_notes, :text
    remove_column :businesses, :q4_notes, :text
    remove_column :businesses, :q5_notes, :text
    remove_column :businesses, :q6_notes, :text
    remove_column :businesses, :q7_notes, :text
    remove_column :businesses, :customers_in_next_12mos, :text
    remove_column :businesses, :sales_vol_pm, :text
    remove_column :businesses, :prod_time, :text
    remove_column :businesses, :gm_reqd, :text
    remove_column :businesses, :roi_reqd, :text
    remove_column :businesses, :retention_rate, :text
    remove_column :businesses, :next_purchase_rate, :text
    remove_column :businesses, :product_life, :text
    remove_column :businesses, :coc_rate, :text
    remove_column :businesses, :latest_savings, :text
    remove_column :businesses, :q_customer, :text
    remove_column :businesses, :q_sales_channel, :text
    remove_column :businesses, :q_revenue_model, :text
    remove_column :businesses, :q_offering, :text
    remove_column :businesses, :q_operation, :text
    remove_column :businesses, :q_costs, :text
    remove_column :businesses, :q_scale, :text
  end
end
