class AddMetricToMeasureToBenefits < ActiveRecord::Migration
  def change
    add_column :benefits, :metric_to_measure, :text
  end
end
