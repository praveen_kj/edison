class CreateUserTasks < ActiveRecord::Migration
  def change
    create_table :user_tasks do |t|
      t.references :business
      t.string :task_name
      t.date :target_date
      t.integer :step_no
      t.boolean :status

      t.timestamps
    end
  end
end
