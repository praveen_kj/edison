class CreateBeachheads < ActiveRecord::Migration
  def change
    create_table :beachheads do |t|
      t.references :user, index: true
      t.integer :business_id

      t.timestamps
    end
  end
end
