class ChangeRelationshipSteps < ActiveRecord::Migration
  def change
    remove_column :relationship_steps, :business_id, :integer
    remove_column :relationship_steps, :event, :text
    remove_column :relationship_steps, :how, :text
    remove_column :relationship_steps, :admin_field, :boolean

    add_column :relationship_steps, :touchpoint_id, :integer, index:true
    add_column :relationship_steps, :step_desc, :text
  end
end
