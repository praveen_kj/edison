class MoreChangesToBusinesses < ActiveRecord::Migration
  def change

    add_column :businesses, :validated_enduser, :boolean

    remove_column :businesses, :adv_1_name, :text
    remove_column :businesses, :adv_2_name, :text
    remove_column :businesses, :adv_3_name, :text
    remove_column :businesses, :ec_buyer_name, :text
    remove_column :businesses, :mvp_time_to_build, :integer
    remove_column :businesses, :mvp_effort_to_build, :integer
    remove_column :businesses, :mvp_cost_to_build, :integer
    remove_column :businesses, :op_time_to_build, :integer
    remove_column :businesses, :op_effort_to_build, :integer
    remove_column :businesses, :op_cost_to_build, :integer
    remove_column :businesses, :fop_time_to_build, :integer
    remove_column :businesses, :fop_effort_to_build, :integer
    remove_column :businesses, :fop_cost_to_build, :integer



  end
end
