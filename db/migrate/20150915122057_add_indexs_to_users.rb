class AddIndexsToUsers < ActiveRecord::Migration
  def change
    add_index :users, :current_streak_end_at
    add_index :users, :trial_expiry_date
  end
end
