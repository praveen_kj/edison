class CreateTouchpoints < ActiveRecord::Migration
  def change
    create_table :touchpoints do |t|
      t.references :business, index: true
      t.integer :touchpoint_no
      t.boolean :intelligence
      t.boolean :openness
      t.boolean :conscientiousness
      t.boolean :kindness
      t.boolean :stability
      t.boolean :extraversion
      t.text :people_involved
      t.text :end_user_actions
      t.text :things_needed

      t.timestamps
    end
  end
end
