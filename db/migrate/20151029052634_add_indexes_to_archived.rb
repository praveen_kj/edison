class AddIndexesToArchived < ActiveRecord::Migration
  def change

    add_index :user_tasks, :status
    add_index :notes, :archived
    add_index :assumptions, :archived

  end
end
