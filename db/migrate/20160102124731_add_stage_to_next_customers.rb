class AddStageToNextCustomers < ActiveRecord::Migration
  def change
    add_column :next_customers, :stage, :integer, default: 1
  end
end
