class ChangeColTypeInUsers < ActiveRecord::Migration
  def change
    change_column :users, :daily_reminder_sent_at, :datetime
  end
end
