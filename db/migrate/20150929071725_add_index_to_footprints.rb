class AddIndexToFootprints < ActiveRecord::Migration
  def change
    add_index :footprints, :user_id
  end
end
