class AddIndexToBusinesses < ActiveRecord::Migration
  def change
    add_index :businesses, :product_id
  end
end
