class ChangeColNameInUserTasks < ActiveRecord::Migration
  def change
    rename_column :user_tasks, :reason_to_do, :task_description
  end
end
