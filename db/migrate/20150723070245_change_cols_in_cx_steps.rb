class ChangeColsInCxSteps < ActiveRecord::Migration
  def change

    remove_column :cx_steps, :sequence, :integer
    remove_column :cx_steps, :event, :text
    remove_column :cx_steps, :enduser, :boolean
    remove_column :cx_steps, :economic_buyer, :boolean
    remove_column :cx_steps, :champion, :boolean
    remove_column :cx_steps, :hurdles, :text
    remove_column :cx_steps, :agg_time_estimate, :integer
    remove_column :cx_steps, :con_time_estimate, :integer
    remove_column :cx_steps, :admin_field, :boolean
    remove_column :cx_steps, :business_id, :integer

    add_column :cx_steps, :touchpoint_id, :integer, index:true
    add_column :cx_steps, :hurdle_name, :text
    add_column :cx_steps, :hurdle_type, :text
    add_column :cx_steps, :hurdle_target_date, :date

  end
end
