class ChangeMoreColsInFeatures < ActiveRecord::Migration
  def change

    remove_column :features, :how_it_works, :text
    remove_column :features, :benefit_type, :text
    remove_column :features, :target_mock_date, :date

    add_column :features, :mocked_up, :boolean
    add_column :features, :decision_notes, :text

  end
end
