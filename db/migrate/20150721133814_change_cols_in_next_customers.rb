class ChangeColsInNextCustomers < ActiveRecord::Migration
  def change
    add_column :next_customers, :target_date, :date
    remove_column :next_customers, :contacted, :boolean
  end
end
