class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :user, index: true
      t.string :product_name
      t.integer :enduser_base
      t.text :product_purpose
      t.integer :active_market

      t.timestamps
    end
  end
end
