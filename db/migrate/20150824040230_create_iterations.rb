class CreateIterations < ActiveRecord::Migration
  def change
    create_table :iterations do |t|
      t.references :feature, index: true
      t.text :iteration_name
      t.date :target_date

      t.timestamps
    end
  end
end
