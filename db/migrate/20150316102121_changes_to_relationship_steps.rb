class ChangesToRelationshipSteps < ActiveRecord::Migration
  def change
    remove_column  :relationship_steps, :what_happens
    remove_column  :relationship_steps, :who_is_involved
    remove_column  :relationship_steps, :where_does_it_happen
    remove_column  :relationship_steps, :how_long_does_it_take
    rename_column :relationship_steps, :why_does_it_happen, :how

  end
end
