class ChangesToBusinessesForCanvas < ActiveRecord::Migration
  def change
    rename_column :businesses, :market_characteristics, :acquisition
    rename_column :businesses, :partners, :core
  end
end
