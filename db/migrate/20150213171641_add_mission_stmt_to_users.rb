class AddMissionStmtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mission_stmt, :text
    add_column :users, :title, :text
  end
end
