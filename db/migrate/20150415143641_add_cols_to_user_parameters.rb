class AddColsToUserParameters < ActiveRecord::Migration
  def change
    add_column :user_parameters,	:s1,	:boolean
    add_column :user_parameters,	:s2,	:boolean
    add_column :user_parameters,	:s3,	:boolean
    add_column :user_parameters,	:s4,	:boolean
    add_column :user_parameters,	:s5,	:boolean
  end
end
