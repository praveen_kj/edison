class RenameColsInBusiness < ActiveRecord::Migration
  def change
    rename_column :businesses, :acquisition, :sales_channel
    rename_column :businesses, :complementary_assets_required, :assets_required
    rename_column :businesses, :competition, :top_competitors
    rename_column :businesses, :lead_customers, :influencers
  end
end
