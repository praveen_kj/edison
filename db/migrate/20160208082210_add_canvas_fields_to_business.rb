class AddCanvasFieldsToBusiness < ActiveRecord::Migration
  def change

    add_column :businesses, :follow_on_markets, :text
    add_column :businesses, :beachhead_size, :text
    add_column :businesses, :user_priorities, :text
    add_column :businesses, :dmu, :text
    add_column :businesses, :use_case, :text
    add_column :businesses, :value_proposition, :text
    add_column :businesses, :business_model, :text
    add_column :businesses, :pricing, :text
    add_column :businesses, :customer_reach, :text


  end
end
