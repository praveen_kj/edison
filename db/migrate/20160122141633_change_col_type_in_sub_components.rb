class ChangeColTypeInSubComponents < ActiveRecord::Migration
  def change
    change_column :sub_components, :man_hours, :float
  end
end
