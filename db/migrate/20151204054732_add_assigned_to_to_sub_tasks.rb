class AddAssignedToToSubTasks < ActiveRecord::Migration
  def change
    add_column :sub_tasks, :assignee_id, :integer
  end
end
