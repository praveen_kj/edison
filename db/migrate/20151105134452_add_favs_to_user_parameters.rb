class AddFavsToUserParameters < ActiveRecord::Migration
  def change
    add_column :user_parameters, :favourites, :string, array: true
  end
end
