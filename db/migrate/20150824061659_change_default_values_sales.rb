class ChangeDefaultValuesSales < ActiveRecord::Migration
  def change

    change_column_default :sales_strategies, :actual_prospects_reached, 0
    change_column_default :sales_strategies, :actual_customers_acquired, 0
    change_column_default :sales_strategies, :actual_customer_ltv, 0
    change_column_default :sales_strategies, :actual_total_cost, 0

  end
end
