class AddReleaseToMilestones < ActiveRecord::Migration
  def change
    add_column :main_activities, :release, :boolean
  end
end
