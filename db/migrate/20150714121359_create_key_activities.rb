class CreateKeyActivities < ActiveRecord::Migration
  def change
    create_table :key_activities do |t|
      t.references :business, index: true
      t.text :activity_name
      t.date :target_date
      t.integer :effort
      t.text :materials
      t.integer :capital

      t.timestamps
    end
  end
end
