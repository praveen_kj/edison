class AddStageToExperiments < ActiveRecord::Migration
  def change
    add_column :experiments, :stage, :integer
  end
end
