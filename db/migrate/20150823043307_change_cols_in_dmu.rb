class ChangeColsInDmu < ActiveRecord::Migration
  def change

    remove_column :customer_decision_making_units, :who_is_this, :text
    remove_column :customer_decision_making_units, :what_they_expect, :text
    remove_column :customer_decision_making_units, :what_to_give, :text
    remove_column :customer_decision_making_units, :revenue_increase, :integer
    remove_column :customer_decision_making_units, :start_date, :date
    remove_column :customer_decision_making_units, :entity, :integer
    remove_column :customer_decision_making_units, :category, :integer


    add_column :customer_decision_making_units, :entity, :integer
    add_column :customer_decision_making_units, :entity_name, :string
    add_column :customer_decision_making_units, :fears, :text
    add_column :customer_decision_making_units, :motivations, :text
    add_column :customer_decision_making_units, :expectations, :text
    add_column :customer_decision_making_units, :offering, :text

  end
end
