class AddMoreColumnsToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :enduser, :text
    add_column :businesses, :application, :text
    add_column :businesses, :benefit, :text
    add_column :businesses, :lead_customers, :text
    add_column :businesses, :market_characteristics, :text
    add_column :businesses, :partners, :text
    add_column :businesses, :market_size, :text
    add_column :businesses, :competition, :text
    add_column :businesses, :platform, :text
    add_column :businesses, :complementary_assets_required, :text

  end
end
