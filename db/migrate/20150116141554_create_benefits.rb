class CreateBenefits < ActiveRecord::Migration
  def change
    create_table :benefits do |t|
      t.references :business, index: true
      t.text :benefit
      t.text :feature

      t.timestamps
    end
  end
end
