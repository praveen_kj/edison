class ChangesToEvents < ActiveRecord::Migration
  def change
    remove_column :events, :description, :string
    remove_column :events, :users, :string
    remove_column :events, :main_resource_id, :string
    remove_column :events, :sub_resource_id, :string

    add_column :events, :user_id, :integer
    add_column :events, :source_id, :integer
    add_column :events, :other_data, :hstore
  end
end
