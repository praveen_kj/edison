class ChangesToColumnsSeveralTables < ActiveRecord::Migration
  def change

    # touchpoints

    add_column :touchpoints, :target_date, :date

    # benefits

    add_column :benefits, :benefit_name, :text
    add_column :benefits, :benefit_type, :text
    add_column :benefits, :feature_id, :integer
    add_column :benefits, :admin_field, :boolean

    remove_column :benefits, :benefit, :text
    remove_column :benefits, :feature, :text
    remove_column :benefits, :function, :text
    remove_column :benefits, :purchasing_priority, :text
    remove_column :benefits, :business_id, :integer

    # value metrics

    add_column :value_metrics, :proposition_name, :text

    remove_column :value_metrics, :quantified_value, :text
    remove_column :value_metrics, :priority, :text
    remove_column :value_metrics, :priority_desc, :text
    remove_column :value_metrics, :purchasing_priority, :text
    remove_column :value_metrics, :business_id, :integer

    # business

    add_column :businesses, :value_prop_conf_date, :date
    add_column :businesses, :enduser_name, :text
    add_column :businesses, :adv_1_name, :text
    add_column :businesses, :adv_2_name, :text
    add_column :businesses, :adv_3_name, :text
    add_column :businesses, :ec_buyer_name, :text

    # key activities

    add_column :key_activities, :milestone_id, :integer

    remove_column :key_activities, :effort, :text
    remove_column :key_activities, :materials, :text
    remove_column :key_activities, :business_id, :integer

    # next customers

    add_column :next_customers, :use_case, :boolean
    add_column :next_customers, :use_case_note, :text
    add_column :next_customers, :benefits, :boolean
    add_column :next_customers, :benefits_note, :text
    add_column :next_customers, :value_created, :boolean
    add_column :next_customers, :value_created_note, :text
    add_column :next_customers, :purchase_interest, :boolean
    add_column :next_customers, :purchase_interest_note, :text
    add_column :next_customers, :next_meeting_date, :date

    remove_column :next_customers, :fluc, :boolean
    remove_column :next_customers, :pspec, :boolean
    remove_column :next_customers, :vprop, :boolean
    remove_column :next_customers, :purch, :boolean
    remove_column :next_customers, :improvements, :text
    remove_column :next_customers, :target_date, :date

    # comp advantages

    add_column :comp_advantages, :customer_want_note, :text
    add_column :comp_advantages, :assets_available_note, :text
    add_column :comp_advantages, :founders_want_note, :text
    add_column :comp_advantages, :competitor_ability_note, :text
    add_column :comp_advantages, :vendor_ability_note, :text
    add_column :comp_advantages, :personal_goal_note, :text
    add_column :comp_advantages, :financial_goal_note, :text

    # DMU

    add_column :customer_decision_making_units, :who_is_this, :text
    add_column :customer_decision_making_units, :what_they_expect, :text
    add_column :customer_decision_making_units, :what_to_give, :text
    add_column :customer_decision_making_units, :revenue_increase, :integer
    add_column :customer_decision_making_units, :start_date, :date
    add_column :customer_decision_making_units, :name, :text
    add_column :customer_decision_making_units, :category, :text


    remove_column :customer_decision_making_units, :entity, :text
    remove_column :customer_decision_making_units, :incentive, :text
    remove_column :customer_decision_making_units, :primary_influencer, :text
    remove_column :customer_decision_making_units, :secondary_influencer, :text
    remove_column :customer_decision_making_units, :veto, :text

    # cx_steps

    add_column :cx_steps, :no_of_customers, :integer

    # pricing_models

    remove_column :pricing_models, :willingness_to_buy, :boolean
    remove_column :pricing_models, :competitor_adoption, :boolean
    remove_column :pricing_models, :allows_dist_incentives, :boolean
    remove_column :pricing_models, :value_captured, :boolean

    add_column :pricing_models, :willingness_to_buy, :text
    add_column :pricing_models, :favourable_pricing, :text
    add_column :pricing_models, :vendor_share_protected, :text
    add_column :pricing_models, :competitor_adoption, :text
    add_column :pricing_models, :allows_dist_incentives, :text
    add_column :pricing_models, :preference, :text
    add_column :pricing_models, :willingness_to_buy_note, :text
    add_column :pricing_models, :favourable_pricing_note, :text
    add_column :pricing_models, :vendor_share_protected_note, :text
    add_column :pricing_models, :competitor_adoption_note, :text
    add_column :pricing_models, :allows_dist_incentives_note, :text


    # sales_strategy

    add_column :sales_strategies, :strategy_name, :text
    add_column :sales_strategies, :revenue_target, :integer
    add_column :sales_strategies, :target_date, :date

    remove_column :sales_strategies, :parameter, :text
    remove_column :sales_strategies, :short_term, :text
    remove_column :sales_strategies, :medium_term, :text
    remove_column :sales_strategies, :long_term, :text

  end
end
