class AddCexPeriodToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :analysis_start_at, :date
    add_column :businesses, :analysis_end_at, :date
  end
end
