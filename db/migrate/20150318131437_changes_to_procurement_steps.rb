class ChangesToProcurementSteps < ActiveRecord::Migration
  def change
    remove_column :procurement_steps, :key_activity, :text
    remove_column :procurement_steps, :time_taken, :text
    remove_column :procurement_steps, :vendor1, :text
    remove_column :procurement_steps, :vendor2, :text
    remove_column :procurement_steps, :vendor3, :text
    add_column :procurement_steps, :parameter, :text
    add_column :procurement_steps, :mvbp, :text
    add_column :procurement_steps, :tam, :text
    add_column :procurement_steps, :followon, :text
  end
end
