class AddColsToCustomerFeedbacks < ActiveRecord::Migration
  def change
    add_column :customer_feedbacks, :decision, :integer
    add_column :customer_feedbacks, :target_date, :date
    add_column :customer_feedbacks, :assigned_to, :integer
  end
end
