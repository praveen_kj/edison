class AddDurationToTouchpoints < ActiveRecord::Migration
  def change
    add_column :touchpoints, :duration, :integer
  end
end
