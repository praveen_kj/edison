class AddStatusToAssumptions < ActiveRecord::Migration
  def change
    add_column :assumptions, :status, :integer, default: 1
  end
end
