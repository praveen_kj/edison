class AddStepNoToAssumptions < ActiveRecord::Migration
  def change
    add_column :assumptions, :step_no, :integer
  end
end
