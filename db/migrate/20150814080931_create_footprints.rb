class CreateFootprints < ActiveRecord::Migration
  def change
    create_table :footprints do |t|
      t.integer :user_id, index: true
      t.string :view
      t.integer :step_id, index: true

      t.timestamps
    end
  end
end
