class AddMoreColsToTouchpoints < ActiveRecord::Migration
  def change
    add_column :touchpoints, :motivation_level, :string
    add_column :touchpoints, :ease_of_doing, :string
    add_column :touchpoints, :possible_intervention, :text
    add_column :customer_decision_making_units, :preferred_alternatives, :text
  end
end
