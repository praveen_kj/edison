class ChangesToEndusersAndPersonas < ActiveRecord::Migration
  def change
    remove_column :endusers, :age, :text
    remove_column :endusers, :income_range, :text
    remove_column :endusers, :education_background, :text
    remove_column :endusers, :geo_location, :text
    remove_column :endusers, :motivations, :text
    remove_column :endusers, :fears, :text
    remove_column :endusers, :hero, :text
    remove_column :endusers, :personality, :text
    remove_column :endusers, :newspapers_read, :text
    remove_column :endusers, :websites, :text
    remove_column :endusers, :tv_shows, :text
    remove_column :endusers, :purchase_motivations, :text
    remove_column :endusers, :spl_identifiable, :text
    remove_column :endusers, :story, :text
    remove_column :endusers, :notes, :text
    remove_column :endusers, :note_title, :text
    remove_column :endusers, :males, :text
    remove_column :endusers, :females, :text
    remove_column :endusers, :languages, :text
    add_column :endusers, :parameter, :text
    add_column :endusers, :response, :text
  end
end
