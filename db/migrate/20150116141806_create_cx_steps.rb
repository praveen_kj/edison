class CreateCxSteps < ActiveRecord::Migration
  def change
    create_table :cx_steps do |t|
      t.references :business, index: true
      t.integer :sequence
      t.text :what_customer_does
      t.text :customers_mood
      t.text :who_else_involved
      t.text :items_involved_in_step
      t.text :feature_used
      t.text :benefit_realized

      t.timestamps
    end
  end
end
