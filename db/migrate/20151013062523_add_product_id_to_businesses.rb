class AddProductIdToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :product_id, :integer, index:true
  end
end
