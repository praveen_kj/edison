class ChangesToTables18May < ActiveRecord::Migration
  def change
    remove_column :cx_steps, :con_time_estimate, :text
    remove_column :cx_steps, :agg_time_estimate, :text
    remove_column :pricing_models, :when_to_share, :text
    add_column :cx_steps, :con_time_estimate, :integer
    add_column :cx_steps, :agg_time_estimate, :integer
    add_column :pricing_models, :when_to_share, :boolean
  end
end
