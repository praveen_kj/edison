class ChangeDefaultValues < ActiveRecord::Migration
  def change
    change_column_default :businesses, :question1, nil
    change_column_default :businesses, :question2, nil
    change_column_default :businesses, :question3, nil
    change_column_default :businesses, :question4, nil
    change_column_default :businesses, :question5, nil
    change_column_default :businesses, :question6, nil
    change_column_default :businesses, :question7, nil
    change_column_default :financials, :no_of_customers, 0
    change_column_default :financials, :duration, 0
    change_column_default :financials, :relationship_duration, 0
    change_column_default :financials, :one_time_revenues, 0
    change_column_default :financials, :monthly_revenues, 0
    change_column_default :financials, :upsell_revenues, 0
    change_column_default :financials, :production_salaries, 0
    change_column_default :financials, :production_material, 0
    change_column_default :financials, :production_overheads, 0
    change_column_default :financials, :sales_staff_salaries, 0
    change_column_default :financials, :marketing_cost, 0
    change_column_default :financials, :distributor_commission, 0
    change_column_default :financials, :discounts, 0
    change_column_default :financials, :capex, 0
    change_column_default :financials, :rentals, 0
    change_column_default :financials, :legal, 0
    change_column_default :financials, :others, 0
  end
end
