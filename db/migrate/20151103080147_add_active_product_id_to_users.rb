class AddActiveProductIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :active_product_id, :integer
    remove_column :users, :beachhead_id, :integer
  end
end
