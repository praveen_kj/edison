class CreateStrategyGaps < ActiveRecord::Migration
  def change
    create_table :strategy_gaps do |t|
      t.references :sales_strategy, index: true
      t.text :gap_name

      t.timestamps
    end
  end
end
