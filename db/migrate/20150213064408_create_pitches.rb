class CreatePitches < ActiveRecord::Migration
  def change
    create_table :pitches do |t|
      t.references :business, index: true
      t.text :pitch_content

      t.timestamps
    end
  end
end
