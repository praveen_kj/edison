class AddAuthToMemberships < ActiveRecord::Migration
  def change
    add_column :memberships, :write_tools, :boolean, default: false
    add_column :memberships, :write_utils, :boolean, default: true
  end
end
