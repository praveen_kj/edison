class ChangeDefaultValuesTouchpoints < ActiveRecord::Migration
  def change

    change_column_default :touchpoints, :expected_conversion_rate, 0
    change_column_default :touchpoints, :expected_users, 0

  end
end
