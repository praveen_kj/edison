class AddGroupToBenefits < ActiveRecord::Migration
  def change
    add_column :benefits, :group, :integer
  end
end
