class AddBusinessIdToValueMetrics < ActiveRecord::Migration
  def change
    add_column :value_metrics, :business_id, :integer, index:true
  end
end
