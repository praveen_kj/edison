class AddColsToUserTaskAndUserParams < ActiveRecord::Migration
  def change
    add_column :user_tasks, :reason_to_do, :text
    add_column :user_parameters, :target_date, :date
  end
end
