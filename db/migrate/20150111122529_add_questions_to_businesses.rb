class AddQuestionsToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :question1, :text, default: "No" #Is the target customer well-funded?
    add_column :businesses, :question2, :text, default: "No" #Is the target customer readily accessible to your sales force?
    add_column :businesses, :question3, :text, default: "No" #Does the target customer have a compelling reason to buy?
    add_column :businesses, :question4, :text, default: "No" #Can you today, with the help of partners, deliver a whole product?
    add_column :businesses, :question5, :text, default: "No" #Is there entrenched competition that could block you?
    add_column :businesses, :question6, :text, default: "No" #If you win this segment, can you leverage it to enter additional segments?
    add_column :businesses, :question7, :text, default: "No" #Is the market consistent with the values, passions, and goals of the founding team?
  end
end
