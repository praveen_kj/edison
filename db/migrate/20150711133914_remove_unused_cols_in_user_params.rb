class RemoveUnusedColsInUserParams < ActiveRecord::Migration
  def change
    remove_column :user_parameters, :s1, :boolean
    remove_column :user_parameters, :s2, :boolean
    remove_column :user_parameters, :s3, :boolean
    remove_column :user_parameters, :s4, :boolean
    remove_column :user_parameters, :s5, :boolean
  end
end
