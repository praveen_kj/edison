class AddUnsubscribeToUserParameters < ActiveRecord::Migration
  def change

    add_column :user_parameters, :subscription, :boolean, default: true

  end
end
