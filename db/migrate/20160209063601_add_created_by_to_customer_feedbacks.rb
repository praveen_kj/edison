class AddCreatedByToCustomerFeedbacks < ActiveRecord::Migration
  def change
    add_column :customer_feedbacks, :created_by, :integer
    add_column :customer_feedbacks, :related_tool, :integer
  end
end
