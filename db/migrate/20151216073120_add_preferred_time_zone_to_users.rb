class AddPreferredTimeZoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :preferred_time_zone, :string
    add_column :users, :preferred_time, :integer
    add_column :users, :receive_sitrep, :boolean, default: true

  end
end
