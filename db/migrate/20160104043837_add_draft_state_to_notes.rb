class AddDraftStateToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :draft, :boolean
  end
end
