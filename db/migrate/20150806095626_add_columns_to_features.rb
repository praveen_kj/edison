class AddColumnsToFeatures < ActiveRecord::Migration
  def change
    add_column :features, :status, :integer
    add_column :features, :effort, :integer
  end
end
