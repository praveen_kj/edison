class CreateSubComponents < ActiveRecord::Migration
  def change
    create_table :sub_components do |t|
      t.references :component, index: true
      t.string :name
      t.integer :customers_want
      t.integer :importance
      t.integer :stage

      t.timestamps
    end
  end
end
