class ChangesToCxSteps < ActiveRecord::Migration
  def change
    remove_column :cx_steps, :what_customer_does, :text
    remove_column :cx_steps, :customers_mood, :text
    remove_column :cx_steps, :who_else_involved, :text
    remove_column :cx_steps, :items_involved_in_step, :text
    remove_column :cx_steps, :feature_used, :text
    remove_column :cx_steps, :benefit_realized, :text
    remove_column :cx_steps, :metric, :text
    add_column :cx_steps, :event, :text
    add_column :cx_steps, :enduser, :boolean
    add_column :cx_steps, :champion, :boolean
    add_column :cx_steps, :economic_buyer, :boolean
    add_column :cx_steps, :agg_time_estimate, :integer
    add_column :cx_steps, :con_time_estimate, :integer
    add_column :cx_steps, :hurdles, :text
  end
end
