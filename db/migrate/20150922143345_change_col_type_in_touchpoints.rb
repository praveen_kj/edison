class ChangeColTypeInTouchpoints < ActiveRecord::Migration
  def change
    change_column :touchpoints, :expected_conversion_rate, :float
  end
end
