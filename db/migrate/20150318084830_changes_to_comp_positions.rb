class ChangesToCompPositions < ActiveRecord::Migration
  def change
    remove_column :comp_positions, :factor_one_rank, :integer
    remove_column :comp_positions, :factor_two_rank, :integer
    remove_column :businesses, :factor1, :text
    remove_column :businesses, :factor2, :text

  end
end
