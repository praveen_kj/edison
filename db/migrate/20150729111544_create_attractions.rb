class CreateAttractions < ActiveRecord::Migration
  def change
    create_table :attractions do |t|
      t.references :pricing_plan, index: true
      t.text :attraction_name
      t.integer :revenue_increase

      t.timestamps
    end
  end
end
