class AddSalesTargetToMainActivities < ActiveRecord::Migration
  def change
    add_column :main_activities, :sales_target, :boolean
  end
end
