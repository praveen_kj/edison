class RenameMilestonesTable < ActiveRecord::Migration
  def change

    rename_table :milestones, :main_activities
    rename_column :main_activities, :milestone_name, :main_activity_name
    rename_column :key_activities, :milestone_id, :main_activity_id


  end
end
