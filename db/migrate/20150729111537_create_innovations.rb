class CreateInnovations < ActiveRecord::Migration
  def change
    create_table :innovations do |t|
      t.references :pricing_model, index: true
      t.text :innovation_name
      t.integer :revenue_increase
      t.date :target_date

      t.timestamps
    end
  end
end
