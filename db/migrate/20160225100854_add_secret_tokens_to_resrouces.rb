class AddSecretTokensToResrouces < ActiveRecord::Migration

  def change

    add_column :sub_tasks, :secret_token, :string
    add_column :main_activities, :secret_token, :string
    add_column :comp_advantages, :secret_token, :string
    add_column :pricing_models, :secret_token, :string
    add_column :pricing_plans, :secret_token, :string


  end

end
