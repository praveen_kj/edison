class CreateMilestones < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.references :business, index: true
      t.date :target_date
      t.integer :effort
      t.integer :capital

      t.timestamps
    end
  end
end
