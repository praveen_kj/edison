class AddColsToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :resources_created, :boolean
    add_column :businesses, :similar_product, :boolean
    add_column :businesses, :similar_sales, :boolean
    add_column :businesses, :word_of_mouth, :boolean
  end
end
