class CreateCompPositions < ActiveRecord::Migration
  def change
    create_table :comp_positions do |t|
      t.references :business, index: true
      t.text :competitor
      t.text :factor1
      t.text :factor2

      t.timestamps
    end
  end
end
