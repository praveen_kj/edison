class CreatePlanDefects < ActiveRecord::Migration
  def change
    create_table :plan_defects do |t|
      t.references :pricing_plan, index: true
      t.text :defect_name



      t.timestamps
    end
  end
end
