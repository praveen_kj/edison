class AddColsToPricingModelsAndPricingPlan < ActiveRecord::Migration

  def change

    add_column :pricing_plans, :ltv, :integer
    add_column :pricing_models, :rank, :integer

  end
end
