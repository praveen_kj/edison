class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :product, index: true
      t.integer :event_type
      t.text :description
      t.string :users, array: true, default: []
      t.integer :main_resource_id
      t.integer :sub_resource_id

      t.timestamps
    end
  end
end
