class AddCompanyNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :company_name, :text
  end
end
