class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.references :business, index: true
      t.string :name
      t.integer :sales_strategy_id
      t.datetime :reach_out_date
      t.text :notes
      t.integer :stage, default: 1

      t.timestamps
    end
  end
end
