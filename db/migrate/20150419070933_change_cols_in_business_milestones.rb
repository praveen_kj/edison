class ChangeColsInBusinessMilestones < ActiveRecord::Migration
  def change
    remove_column :pricing_models, :percent_value_captured, :text
    add_column :pricing_models, :value_captured, :boolean
  end
end
