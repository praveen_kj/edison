class AddCardIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :card_id, :string
    add_column :users, :billing_info, :hstore
  end
end
