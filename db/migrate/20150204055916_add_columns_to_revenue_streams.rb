class AddColumnsToRevenueStreams < ActiveRecord::Migration
  def change
    add_column :businesses, :retention_rate, :float
    add_column :businesses, :next_purchase_rate, :float
    add_column :businesses, :product_life, :integer
    add_column :businesses, :coc_rate, :integer
  end
end
