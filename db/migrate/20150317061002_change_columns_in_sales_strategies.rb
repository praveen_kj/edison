class ChangeColumnsInSalesStrategies < ActiveRecord::Migration
  def change
    remove_column :sales_strategies, :period, :text
    remove_column :sales_strategies, :length, :text
    remove_column :sales_strategies, :mix, :text
    add_column :sales_strategies, :parameter, :text
    add_column :sales_strategies, :short_term, :text
    add_column :sales_strategies, :medium_term, :text
    add_column :sales_strategies, :long_term, :text
  end
end
