class ChangeTouchpointCols < ActiveRecord::Migration
  def change
    remove_column :touchpoints, :ease_of_doing, :string
    remove_column :touchpoints, :motivation_level, :string
    add_column :touchpoints, :ease_of_doing, :integer
    add_column :touchpoints, :motivation_level, :integer
  end
end
