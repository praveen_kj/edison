class AddDefaultCanvasToProducts < ActiveRecord::Migration
  def change
    add_column :products, :default_canvas, :integer
  end
end
