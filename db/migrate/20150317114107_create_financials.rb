class CreateFinancials < ActiveRecord::Migration
  def change
    create_table :financials do |t|
      t.references :business, index: true
      t.text :customer_segment
      t.integer :no_of_customers, default: 0, limit:8
      t.integer :duration, default: 0, limit:8
      t.integer :relationship_duration, default: 0, limit:8
      t.integer :one_time_revenues, default: 0, limit:8
      t.integer :monthly_revenues, default: 0, limit:8
      t.integer :upsell_revenues, default: 0, limit:8
      t.integer :production_salaries, default: 0, limit:8
      t.integer :production_material, default: 0, limit:8
      t.integer :production_overheads, default: 0, limit:8
      t.integer :sales_staff_salaries, default: 0, limit:8
      t.integer :marketing_cost, default: 0, limit:8
      t.integer :distributor_commission, default: 0, limit:8
      t.integer :discounts, default: 0, limit:8
      t.integer :capex, default: 0, limit:8
      t.integer :rentals, default: 0, limit:8
      t.integer :legal, default: 0, limit:8
      t.integer :others, default: 0, limit:8

      t.timestamps
    end
  end
end
