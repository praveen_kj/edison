class AddDailyReminderSentAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :daily_reminder_sent_at, :date
  end
end
