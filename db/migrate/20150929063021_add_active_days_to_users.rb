class AddActiveDaysToUsers < ActiveRecord::Migration
  def change
    add_column :users, :active_days, :integer
  end
end
