# Preview all emails at http://localhost:3000/rails/mailers/invitation_mailer
class InvitationPreview < ActionMailer::Preview
  def invite_to_plan
    InvitationMailer.invite_to_plan(User.first.id, 'arun@example.com')
  end

  def welcome_email
    InvitationMailer.welcome_email(User.first)
  end
end
