# Preview all emails at http://localhost:3000/rails/mailers/stripe_notifications_mailer
class StripeNotificationsMailerPreview < ActionMailer::Preview

  def new_subscriber_mail
    StripeNotificationsMailer.new_subscriber_mail('sub_77sqlSydq30qS4')
  end

  def subscription_extension
    StripeNotificationsMailer.subscription_extension(User.last.id)
  end

  def cancel_subscription
    StripeNotificationsMailer.cancel_subscription('sub_77sqlSydq30qS4')
  end

  def card_update
    StripeNotificationsMailer.card_update('cus_77sqgeoZfI2i1r')

  end

end
