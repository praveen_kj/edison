# Preview all emails at http://localhost:3000/rails/mailers/conversations_mailer
class OpinionMailerPreview < ActionMailer::Preview

  def step_1
    u = User.first
    product = u.products.first
    target = product.team_email_list.last
    step_no = 1
    OpinionMailer.opinion_on_step(User.first, target, product, step_no)
  end

end
