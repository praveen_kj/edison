# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  def trial_expiry_warning
    UserMailer.trial_expiry_warning(User.first)
  end

  def trial_extension
    UserMailer.trial_extension(User.first)
  end

  def daily_email
    UserMailer.daily_email(User.first.id)
  end

  def promo_email
    UserMailer.new_pricing_email(User.first.id)
  end

end
