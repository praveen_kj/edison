# Preview all emails at http://localhost:3000/rails/mailers/conversations_mailer
class ConversationsMailerPreview < ActionMailer::Preview

  def discussion
    ConversationsMailer.notify_discussion(User.first, 'some@example.com', Discussion.first)
  end

  def comment
    ConversationsMailer.notify_new_comment('some@example.com', CustomerFeedback.last, Comment.last)
  end

  def customer_feedback
    ConversationsMailer.new_feedback_email('some@example.com', CustomerFeedback.last)
  end

  def note
    ConversationsMailer.notify_new_note(User.first, 'arunl.web@gmail.com' ,Note.first)
  end

  def todolist
    ConversationsMailer.notify_new_todolist('arunl.web@gmail.com' ,UserTask.first)
  end

  def todo_assignment
    ConversationsMailer.notify_todo_assignment(User.first, SubTask.first)
  end


  def feedback_assignment
    ConversationsMailer.feedback_assignment_email(User.last, CustomerFeedback.last)
  end

  def todo_completion
    ConversationsMailer.notify_todo_completion('arunl.web@gmail.com', SubTask.first, User.first)
  end


  def daily_morning_email
    ConversationsMailer.daily_morning_email(User.find(4))
  end

end
