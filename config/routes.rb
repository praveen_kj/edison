require 'sidekiq/web'

Rails.application.routes.draw do


  ## PUBLIC PAGES
  root 'static_pages#home'
  match '/about', to: 'static_pages#about', via: 'get'
  match '/privacy', to: 'static_pages#privacy', via: 'get'
  match '/terms', to: 'static_pages#terms', via: 'get'
  match '/support', to: 'static_pages#support', via: 'get'
  match '/email_validate', to: 'email_validation#validation', via: 'post'

  ## PRIVATE PAGES

  # onboarding
  match '/mass_create', to: 'businesses#mass_create', via: :post, as: 'mass_create'

  # app

  match '/customer_feedbacks/new', to: 'customer_feedbacks#new', via: 'get', as: 'new_customer_feedback'

  match 'features/get_sub_features', to: 'features#get_sub_features', via: 'post'

  match '/discussions', to: 'discussions#index', via: 'get', as: 'discussions'
  match '/discussions/new', to: 'discussions#new', via: 'get', as: 'new_discussion'
  match '/discussions/load_more', to: 'discussions#load_more', via: 'get', as: 'more_discussions'
  match '/discussions/:id', to: 'discussions#show', via: 'get', as: 'discussion'

  match '/notes', to: 'notes#index', via: 'get', as: 'notes'
  match '/notes/new', to: 'notes#new', via: 'get', as: 'new_note'
  match '/notes/:id', to: 'notes#show', via: 'get', as: 'note'
  match '/notes/:id/edit', to: 'notes#edit', via: 'get', as: 'edit_note'

  match '/to-do-lists/new', to: 'user_tasks#new', via: 'get', as: 'new_to_do_list'
  match '/to-do-lists', to: 'user_tasks#index', via: 'get', as: 'to_do_lists'
  match '/to-do-lists/archived', to: 'user_tasks#index', via: 'get', as: 'archived_to_do_lists'
  match '/sub_tasks/clear_today', to: 'sub_tasks#clear_today', via: 'post', as: 'clear_todays_tasks'
  match '/to-do-lists/:id', to: 'user_tasks#show', via: 'get', as: 'to_do_list'
  match '/sub_tasks/(:scope)/(:id)', to: 'sub_tasks#index', via: :get, as: 'sub_tasks'


  match '/sub-features/:id/feedbacks', to: 'components#feedbacks', via: :get, as: 'component_feedbacks'

  match '/customer-insights/:id', to: 'customer_feedbacks#show', via: 'get', as: 'customer_insight'
  match '/customer_feedbacks/remove_references/:id', to: 'customer_feedbacks#remove_references', via: 'get', as: 'remove_references'
  match '/customer_feedbacks/revoke_decision/:id', to: 'customer_feedbacks#revoke_decision', via: 'get', as: 'revoke_decision'
  match '/customer_feedbacks/(:scope)/(:tool)', to: 'customer_feedbacks#index', via: 'get', as: 'customer_feedbacks'

  match '/to-dos/:id', to: 'sub_tasks#show', via: :get, as: 'to_do'
  match '/milestones/:id', to: 'main_activities#show', via: :get, as: 'milestone'
  match '/cores/:id', to: 'comp_advantages#show', via: :get, as: 'core'
  match '/business-models/:id', to: 'pricing_models#show', via: :get, as: 'business_model'
  match '/pricing-plans/:id', to: 'pricing_plans#show', via: :get, as: 'pricing_plan'


  match '/onboarding', to: 'dynamic_pages#onboarding', via: 'get', as: 'onboarding'
  match '/app/(:view)', to: 'tool_pages#app', via: 'get', as: 'app'
  match '/make', to: 'dynamic_pages#make', via: 'get', as: 'make'
  match '/settings', to: 'dynamic_pages#settings', via: 'get', as: 'settings'
  match '/home', to: 'dynamic_pages#home', via: 'get', as: 'home'
  match '/toolkit', to: 'dynamic_pages#toolkit', via: 'get', as: 'toolkit'
  match '/strategy-toolkit', to: 'dynamic_pages#strategy_toolkit', via: 'get', as: 'strategy_toolkit'
  match '/milestones', to: 'dynamic_pages#milestones', via: 'get', as: 'milestones'
  match '/team', to: 'dynamic_pages#team', via: 'get', as: 'team'
  match '/customer-insights', to: 'dynamic_pages#customer_insights', via: 'get', as: 'customer_insights'

  match '/chat', to: 'dynamic_pages#chat', via: 'get', as: 'chat'
  match '/products', to: 'dynamic_pages#products', via: 'get', as: 'products_dashboard'
  match '/markets', to: 'dynamic_pages#markets', via: 'get', as: 'markets'
  match '/welcome', to: 'dynamic_pages#welcome', via: 'get', as: 'welcome'
  match '/four-pillars', to: 'dynamic_pages#four_pillars', via: 'get', as: 'four_pillars'
  match '/top-pitfalls', to: 'dynamic_pages#top_pitfalls', via: 'get', as: 'top_pitfalls'


  # ADMIN
  match '/returning_users', to: 'static_pages#returning_users', via: 'get', as: 'returning_users'


  # SCRIPTS
  match '/invite_cofounders', to: 'memberships#invite_cofounders', via: 'get', as: 'invite_cofounders'
  match '/products/update_stage', to: 'products#update_stage', via: 'put', as: 'update_stage'

  match '/refresh_activity_feed', to: 'dynamic_pages#refresh_activity_feed', via: 'get', as: 'refresh_activity_feed'

  match 'delete_business/:id', to: 'businesses#delete_business', via: 'get', as: :delete_business


  ## switch beachhead and products

  match '/update_beachhead', to: 'beachheads#update', via: 'put', as: 'update_beachhead'
  match '/switch_product', to: 'active_products#update', via: 'put', as: 'switch_product'

  ## rank persona details
  match '/persona_details/:id/rank', to: 'persona_details#rank', via: 'put', as: 'persona_details_rank'


  ## STRIPE PAYMENT GATEWAY
  match '/my_account', to: 'subscriptions#my_account', via: 'get', as: 'account'
  match '/new_subscription', to: 'subscriptions#create', via: 'post', as: 'new_subscription'
  match '/update_card', to: 'subscriptions#update_card', via: 'post'
  match '/cancel_subscription', to: 'subscriptions#cancel', via: [:get, :post]
  match '/unsubscribe', to: 'subscriptions#unsubscribe', via: 'get', as: 'unsubscribe'
  mount StripeEvent::Engine, at: '/webhooks'

  ## EMAIL RECIEVING

  # match '/email_processor', to: 'emails#create', via: :post

  match '/email_processor', to: 'griddler/emails#create', via: :post


  ## DEVISE ROUTES

  devise_for :users, controllers: {
      registrations: 'users/registrations',
      sessions: 'users/sessions',
      confirmations: 'users/confirmations',
      passwords: 'users/passwords',
      omniauth_callbacks: 'users/omniauth_callbacks'
  }

  devise_scope :user do
    match '/sign_up', to: 'users/registrations#new', via: 'get', as: 'sign_up'
    match '/sign_in', to: 'users/sessions#new', via: 'get', as: 'sign_in'
    match '/sign_out', to: 'users/sessions#destroy', via: 'get'
    match '/delete_account', to: 'users/registrations#delete_account', via: 'post', as: 'delete_account'
    match '/forgot_password', to: 'users/passwords#new', via: 'get', as: 'forgot_password'
    match '/users_time_zone', to: 'users/registrations#users_time_zone', via: 'post'
    match '/update_missing_data', to: 'users/registrations#update_missing_data', via: 'post'
  end


  ## SIDEKIQ WEB INTERFACE

  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end


  get '.well-known/acme-challenge/AmyfUECPpG2ZdFsbTiXkAw4GSfVWPtKFM9IqYLCYKsA', to: 'static_pages#lets_encrypt'

  ## RESOURCES

  # CREATE ONLY

  resources :usage_durations, :opinions, only: [:create]

  # CREATE, UPDATE, DESTROY, INDEX

  resources :products

  # CREATE, UPDATE, DESTROY

  resources :businesses, :business_parameters, :endusers, :personas, :persona_details, :benefits,
            :value_metrics, :next_customers, :pricing_models, :sales_strategies, :comp_advantages,
            :comp_positions, :procurement_steps, :pitches, :relationship_steps, :cx_steps,
            :feedbacks, :key_activities, :features, :main_activities, :improvements,
            :innovations, :attractions, :pricing_plans, :iterations, :strategy_gaps, :user_priorities,
            :plan_defects, :sub_tasks, :experiments, :memberships, :messages, :customers, :issues,
            :components, :customer_feedbacks, :sub_components, :checklist_items, :dashboards, only: [:create, :update, :destroy]

  resources(:discussions, :notes, :customer_feedbacks, :user_tasks, :sub_tasks,
            :main_activities, :comp_advantages, :pricing_models, :pricing_plans,
            only: [:create, :update, :destroy]) do

    resources :comments, only: [:create, :update, :destroy]

  end


  # CREATE, UPDATE, DESTROY, AND EDIT

  resources :user_parameters, only: [:create, :update, :destroy, :edit]

  # ONLY UPDATE

  resources :touchpoints, :financials, :customer_decision_making_units, only: [:update]

  # ONLY SHOW

  resources :referrals, only: [:show]


end
