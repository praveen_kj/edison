#  Trial Period allowed for user. Used in user.rb and registrations_controller.rb
TRIAL_PERIOD = 30.days


#  No. of days by which the trial is extended on a successful referral. Used in registrations_controller.
PER_REFERRAL_REWARD = 3.days

#  Maximum referrals on which the per referral reward can be earned.
MAX_REFERRALS = 5


# Step Hash

CANVAS_STEPS = [1, 2, 4, 8, 9, 10, 11, 12, 13]

STEP = {


    1 => {
        name: 'Beachhead',
        subtext: 'Narrow down and finalize your beachhead',
        empty_state: "The Beachhead tool provides the litmus test in deciding whether the selected beachhead market is focused and narrow enough",
    },
    2 => {
        name: 'Markets',
        subtext: 'Filter out risky or unprofitable markets',
        empty_state: "The markets tool can be used to select the most attractive market from among all potential markets.",
    },
    3 => {
        name: 'End Users',
        subtext: 'Profile the typical customer in the beachhead ',
        empty_state: "The End User tool is a great tool for understanding the typical end user and profile their wants and characteristics.",
    },

    4 => {
        name: 'Market Size',
        subtext: 'Estimate the size of your beachhead',
        empty_state: "The market size tool is useful to know how many customers will buy the product and their product adoption rate.",
    },
    5 => {
        name: 'Persona',
        subtext: 'Find a real user to build the product for',
        empty_state: "The Persona tool is smart fact sheet of persona and a record of his/her top purchasing priorities that drivers the feature building decisions.",
    },
    6 => {
        name: 'Use Case',
        subtext: 'Build a thorough use case and identify what stand between you and your customer',
        empty_state: "The Use Case tool can be used to understand motivation to use and easiness to do at all important touch points with the customer.",
    },
    7 => {
        name: 'Product Specification',
        subtext: 'Determine what the customer needs and wants from your product',
        empty_state: "The Product spec tool is a smart record of all the features, sub- features and components. It also supports in deciding what to built, when.",
    },
    8 => {
        name: 'Value Proposition',
        subtext: 'How much better are you compared to the industry - in USD terms',
        empty_state: "The Value Proposition tool is used to clearly map out and quantify the additional benefits that the customer receives.",
    },
    9 => {
        name: 'Core',
        subtext: 'What is your secret sauce?',
        empty_state: "The core tool is a comprehensive tracker of what is the core part of the business after exploring various options.",
    },
    10 => {
        name: 'Competitive Position',
        subtext: 'How do you fare against your competitors on what matters to your customer?',
        empty_state: "The Competitive Position tool charts the performance against the close competitors and status quo customers.",
    },
    11 => {
        name: 'Business Model',
        subtext: 'Find the best way your product can make money',
        empty_state: "The Business Model tool lists 16 popular business models. It can be used to test and select the most profitable business model",
    },
    12 => {
        name: 'Pricing',
        subtext: 'Find a fair and profitable pricing plan',
        empty_state: "The Pricing Plan tool incorporates checklist for designing an attractive pricing plans and track these plans.",
    },
    13 => {
        name: 'Decision Making Unit',
        subtext: 'Find out how your customer makes purchase decisions',
        empty_state: "The Decision Making Unit tool provides the overall control on who apart from the end user play an influential role in closing a sale.",
    },
    14 => {
        name: 'Sales Planning',
        subtext: 'Develop sales plans and find the one that works best for you',
        empty_state: "The Sales Planning tool helps to formulate sales plans and chose the top profitable plans and track their",
    },
    15 => {
        name: 'Customer Recruitment',
        subtext: 'Get to the field and recruit early customers to your product',
        empty_state: "The Customer Recruitment makes it easy to track the recruitment of early customers and grow the business.",
    }
}


# Maximum Businesses for a User

MAX_BUSINESSES = 10


# Touchpoitns for USe Case and CEX

DMU_ENTITIES = {
    1 => 'Enduser',
    2 => 'Advocate or Initiator',
    3 => 'Key Influencer',
    4 => 'Other Influencer',
    5 => 'Economic Buyer',
    6 => 'Veto Holder'
}


ENDUSER_GROUPS = {
    1 => 'What are my end users like?',
    3 => 'Why will end users use my product?',
    4 => 'What does a day in the life of my end users look like?',
    # 2 => 'What are your end users\' core personality traits?',
}

PERSONA_PRIORITIES = {1 => 'Pains',
                      2 => 'Goals',
                      3 => 'Needs',
                      4 => 'Purchasing Priorities'
}


ENDUSER_PROPERTIES = [[1, 'Gender Mix'], # group, property
                      [1, 'Age Range'],
                      [1, 'Wealth and Income'],
                      [1, 'Educational background'],
                      [1, 'Location'],
                      # [2, 'Intelligence'],
                      # [2, 'Openness'],
                      # [2, 'Diligence'],
                      # [2, 'Agreeableness'],
                      # [2, 'Stability'],
                      # [2, 'Extraversion'],
                      [3, 'Motivation to use'],
                      [3, 'Fear that prevents them from using'],
                      [3, 'Fear that drives them to use'],
                      [4, 'Wake up routines'],
                      [4, 'Breakfast routines'],
                      [4, 'Dinner routines']]

PERSONA_GROUPS = {
    1 => 'Who is my persona?',
    2 => 'What does a day in the life of my persona look like?',
    # 3 => 'What are your persona\'s core personality traits?'
}

PERSONA_PROPERTIES = [
    [1, 'Name'], # group, property
    [1, 'Gender'],
    [1, 'Age'],
    [1, 'Wealth and Income level'],
    [1, 'Educational Background'],
    [1, 'Location'],
    [2, 'Wake up routines'],
    [2, 'Breakfast routines'],
    [2, 'Work routines'],
    [2, 'Lunch routines'],
    [2, 'Commute routines'],
    [2, 'Dinner routines'],
    [2, 'Sleep routines'],
# [3, 'Intelligence'],
# [3, 'Openness'],
# [3, 'Diligence'],
# [3, 'Agreeableness'],
# [3, 'Stability'],
# [3, 'Extraversion']
]


TOUCHPOINTS = {1 => 'How will customer determine they have a need?',
               2 => 'How will they find out about my product?',
               3 => 'How will they analyze my product?',
               4 => 'How will they acquire my product?',
               5 => 'How will they install or onboard my product?',
               6 => 'How will they use my product?',
               7 => 'How will they determine the value gained?',
               8 => 'How will they pay for my product?',
               9 => 'How will they receive support for my product?',
               10 => 'How will they buy more and spread awareness?'}

TIMELINES = {1 => 'Short Term',
             2 => 'Medium Term',
             3 => 'Long Term'}

PRODUCT_STAGES = {1 => {name: 'Minimum Viable Product', prefix: 'mvp'},
                  2 => {name: 'Operational Product', prefix: 'op'},
                  3 => {name: 'Follow On Product', prefix: 'fop'}}


CUSTOMER_GROUPS = {1 => 'Enthusiasts',
                   2 => 'Early adopters',
                   3 => 'Pragmatists',
                   4 => 'Conservatives',
                   5 => 'Skeptics'}

CUST_GROUP_TEXT = {
    1 => "Venturesome; Enthusiasts make up a very small part of the total market, they play a very important role. They are interested in anything new, and are quick to adopt new and innovative products.",
    2 => "Young and restless; Early adopters are opinion leaders. They pay attention to what the enthusiasts have discovered. They then communicate to their followers the usefulness of the new product. They play a very important role by influencing the attitude and changing the behavior of the Realists.",
    3 => "Value shoppers; Realists carefully observe the Early adopters, but wait to adopt new product until they are sure they will get value from them. The Realists will only adopt a new product if they are sure the new product will provide usefulness to their lives - and not be a waste of their time and money.",
    4 => "Late majority; Conservatives wait until a new product has been accepted by a majority of consumers and the price has dropped to adopt the new product. The conservatives typically adopt new products because they feel as if everyone else is doing it.",
    5 => "Traditionalists; Skeptics are the very last group to adopt a new product. Skeptics are content with what they have, and they adopt new products unenthusiastically and only because they feel as if they have to."
}


BUSINESS_MODELS = {1 => "One-time upfront",
                   2 => "Cost plus",
                   3 => "Hourly Rates",
                   4 => "Subscription and leasing",
                   5 => "Licensing ",
                   6 => "Consumables",
                   7 => "Upselling with high margin",
                   8 => "Advertising",
                   9 => "Reselling data collected",
                   10 => "Transaction fee",
                   11 => "Usage based",
                   12 => "“Cell Phone” plan",
                   13 => "Microtransactions",
                   14 => "Shared savings",
                   15 => "Franchise",
                   16 => "Operating and maintenance"}

CORE = {
    1 => 'Network Effects',
    2 => 'Customer Service',
    3 => 'User Experience',
    4 => 'Lower Costs'
}

# Purchasing Priorities
PRIORITIES = {1 => 'Growth',
              2 => 'Reliability',
              3 => 'Productivity',
              4 => 'Convenience',
              5 => 'Lower Costs/Higher Profits',
              6 => 'Ego stroking'}

# Purchasing Priority Range
PP_LEVEL = {1 => 'very high',
            2 => 'high',
            3 => 'moderate',
            4 => 'low',
            5 => 'very low'}

# Benefit Type Range
BENEFIT_TYPE = {1 => 'faster', 2 => 'cheaper/more profits', 3 => 'better'}

RANGE = {1 => 'High', 2 => 'Medium', 3 => 'Low'}

HURDLE_TYPE = {1 => 'Budgetary', 2 => 'Operational', 3 => 'Regulatory'}

FEATURE_STATUS = {1 => 'Build',
                  2 => 'Drop',
                  3 => 'Hold'}


# Ranking Hash for Diagnosis

DIAGNOSIS_RESPONSE = {
    1 => {:color => :black, action: 'Abandon this market', :question_no => 3, response: "Your target customer will not switch from the current solution to your product as your product is not offering a compelling reason to use your product."},
    2 => {:color => :red, action: 'Proceed with extreme caution', :question_no => 1, response: "Your customer does not have adequate money to buy your product. The market is not attractive enough and raises concern about your product ability to generate positive cash flow for the new venture to grow."},
    3 => {:color => :red, action: 'Proceed with extreme caution', :question_no => 5, response: "Your competitors are strong from the customer’s viewpoint (not your viewpoint or from a technical standpoint). The competition blocks you from starting a business relationship with a customer and you have to stand out from what your customer perceives as alternatives."},
    4 => {:color => :red, action: 'Proceed with extreme caution', :question_no => 7, response: "Your personal goals, reasons for starting a business and drive to make your product are under threat. It is likely that your purpose will take a back seat against the market circumstances."},
    5 => {:color => :orange, action: 'Proceed with caution', :question_no => 2, response: "You do not have the direct sales channel to reach to your customers. At early stages of your venture its is best to work directly with the customers build relationship with them and improve your product with their feedback rather than depending on third parties, who either are very expensive or have poor understanding of your product."},
    6 => {:color => :orange, action: 'Proceed with caution', :question_no => 6, response: "You are not able to dominate the adjacent opportunities where you can sell your product with only slight modifications. You would need to radically revise your product or sales strategy in order to take advantage of additional market opportunities."},
    7 => {:color => :green, action: 'Clear to proceed', :question_no => 4, response: "You are yet to convince a dependable team, vendors and distributors to work with to deliver a solution that incorporates into your product. Find the people who will do what is needed to make your product a reality."}
}


DIAGNOSIS_QUESTIONS = {
    1 => {title: 'Money', question: 'Is the target customer adequately funded to buy my product?'},
    2 => {title: 'Sales Access', question: 'Is the target customer directly accessible to my sales force?'},
    3 => {title: 'Reason to buy', question: 'Does the target customer have a compelling reason to buy my product?'},
    4 => {title: 'Delivery Capabilities', question: 'Can I today, with the help of partners, deliver a whole product?'},
    5 => {title: 'Competition', question: 'Is it a less competitive market, that I can enter the market and win customers over my competitors?'},
    6 => {title: 'Growth', question: 'If I win this market, can I leverage it to enter additional markets?'},
    7 => {title: 'Passion', question: 'Is the market consistent with the values, passions, and goals of the founding team?'}
}

