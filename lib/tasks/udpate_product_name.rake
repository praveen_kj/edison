desc "This task is for updating Product model in between User and Business"

task :update_product_name => :environment do

  products = Product.where(product_name:nil)

  count = products.count

  products.each do |product|
    product.update_attributes(product_name: 'My product')
    puts "Done. Completed for Product #{product.id}."
  end

  puts "Done. Completed updating name for #{count} users."
end
