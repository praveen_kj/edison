
### WARNING : NON-IDEMPOTENT

task :add_persona_tables_for_existing_business => :environment do
  businesses = Business.where(resources_created: true)

  businesses.each do |business|

    PERSONA_PROPERTIES.each do |item|
      business.personas.create(:group => item[0], :parameter => item[1], :admin_field => true)
    end

    puts "Completed Persona for #{business.id}"

    3.times do |i|
      business.persona_details.create(group: 4)
    end
    puts "Completed Persona Detail: Purchasing Priorities for #{business.id}"

  end

  puts "completed for all"
end
