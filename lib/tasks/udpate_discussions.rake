desc "This task is for updating Product id in discussions"

task :update_discussions => :environment do

  Discussion.all.each do |discussion|

    if discussion.product_id
      puts "Already available for Discussion #{discussion.id}"
    else
      if discussion.business
        discussion.update_attributes(product_id: discussion.business.product_id)
        puts "Completed for Discussion #{discussion.id}"
      else
        puts "Ignored for Discussion #{discussion.id} because parent business is not available"
      end
    end

  end

  puts "All complete"

end
