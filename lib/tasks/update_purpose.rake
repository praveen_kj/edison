task :update_name_and_purpose => :environment do

  User.all.each do |u|
    up = u.user_parameter
    u.products.first.update_attributes(product_purpose: up.mission)
    u.update_attributes(username: up.user_name)
    p "Completed for #{u.id}"
  end

  p "completed for all"

end

