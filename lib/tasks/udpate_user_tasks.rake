desc "This task is for updating Product id in usertasks"

task :update_user_tasks => :environment do


  UserTask.all.reorder('id ASC').each do |task|

    if task.product_id
      puts "Already available for Task #{task.id}"
    else
      if task.business
        task.update_attributes(product_id: task.business.product_id)
        puts "Completed for Task #{task.id}"
      else
        puts "Ignored for Task #{task.id} because parent business is not available"
      end
    end

  end

end
