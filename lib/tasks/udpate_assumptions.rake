desc "This task is for updating Product id in assumptions"

task :update_assumptions => :environment do

  Assumption.all.each do |assumption|
    assumption.update_attributes(product_id: assumption.business.product.id)
    puts "Completed for #{assumption.id}"
  end

end
