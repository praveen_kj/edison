desc "This task is for updating Product model in between User and Business"

task :update_active_product => :environment do
  puts "Updating active products..."
  users = User.all
  count = users.count

  users.each do |u|
    if u.active_product_id.blank?
      u.update_attributes(active_product_id: u.products.first.id)
      p "Updated now for user #{u.id} with product #{u.active_product_id}"
    else
      p "Already completed for user #{u.id} with product #{u.active_product_id}"
    end
  end

  puts "Done. Completed for #{count} users."

end
