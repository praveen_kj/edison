desc "This task is for updating Product model in between User and Business"

task :update_beachheads => :environment do
break
  puts "Updating active products..."
  users = User.all
  count = users.count

  users.each do |u|
    product = u.products.first
    product_id = product.id
    beachhead_id = product.active_market

    first_beachhead = {product_id => beachhead_id}

    u.update_attributes(beachheads: first_beachhead) if u.beachheads.blank?
    p "updated for user #{u.id}"
  end

  puts "Done. Completed for #{count} users."
end
