task :generate_secret_tokens => :environment do

  Discussion.all.each do |d|
    d.generate_secret_token
    puts "Done for Discussion #{d.id}"
  end
  puts 'Discussions completed'

  UserTask.all.each do |d|
    d.generate_secret_token
    puts "Done for UserTask #{d.id}"
  end
  puts 'UserTask completed'

  Note.all.each do |d|
    d.generate_secret_token
    puts "Done for Note #{d.id}"
  end
  puts 'Note completed'

  puts "completed for all"
end
