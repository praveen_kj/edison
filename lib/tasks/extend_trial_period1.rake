task :extend_trial_period_1 => :environment do


  user_emails = ['kalebf@marque.co.nz',
   'jonas@appelsinen.net',
   'lizzie@globalsoccermetrix.com',
   'ryan@blabpredicts.com',
   'ian.goodhind@steljes.co.uk',
   'jeff@jeffponders.com',
   'johanna@boombapps.com',
   'juan@napzzz.co',
   'pranav.tej@datasisar.com',
   'keith@twinenginelabs.com',
   'kevin@kevingainey.com',
   'jasonh@takelessons.com',
   'katalin@knittedforyou.com',
   'marcus.putz@teneoapp.de',
   'khuon@bionicsolution.com',
   'erikstocker@me.com',
   'gj@geoffjordan.ca',
   'shashank.bijapur@rupeepower.com',
   'chad.lonberger@modetwo.com',
   'shadi@mahassel.com',
   'chellie@appster.com.au',
   'richardkaliarik@olery.com',
   'jens@hovalt.dk',
   'craig@purview.net',
   'df.aero@verizon.net',
   'sebastian.esser@krautreporter.de',
   'nick@isarr.com',
   'eden@checkitnow.com',
   'rachel.nurse@lbresearch.com',
   'avi@stickies.xyz',
   'b@thelab.io',
   'vtn6@cornell.edu',
   'ycul11@126.com',
   'jmw@overture.me',
   'steve@canyu.do',
   'akarsha@datasisar.com',
   'vquncsua@grr.la',
   'steffen@steffen.co.nz',
   'mspykerman@opswat.com']


  user_emails.each do |email|
    u = User.find_by(email: email)
    u.set_trial_expiry(30.days.from_now)
    puts "Extending for #{u.name}"
  end


end
