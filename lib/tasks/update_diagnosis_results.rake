task :update_diagnosis_results => :environment do
  businesses = Business.all

  businesses.each do |b|
    b.update_diagnosis_result
    puts "completed for #{b.id}"
  end
  puts "all diag results updated"

end
