task :generate_secret_tokens2 => :environment do

  SubTask.all.each do |d|
    d.generate_secret_token
    puts "Done for Sub Task #{d.id}"
  end
  puts 'Sub Tasks completed'

  MainActivity.all.each do |d|
    d.generate_secret_token
    puts "Done for MainAct #{d.id}"
  end
  puts 'Main Act completed'

  CompAdvantage.all.each do |d|
    d.generate_secret_token
    puts "Done for Core #{d.id}"
  end
  puts 'Cores completed'

  PricingModel.all.each do |d|
    d.generate_secret_token
    puts "Done for BM #{d.id}"
  end
  puts 'BMs completed'

  PricingPlan.all.each do |d|
    d.generate_secret_token
    puts "Done for PP #{d.id}"
  end
  puts 'PPs completed'

  puts "completed for all"
end
