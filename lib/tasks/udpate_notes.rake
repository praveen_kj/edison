desc "This task is for updating Product id in notes"

task :update_notes => :environment do

  Note.all.each do |note|
    if note.product_id
      puts "Already available for Note #{note.id}"
    else
      if note.business
        note.update_attributes(product_id: note.business.product_id)
        puts "Completed for Note #{note.id}"
      else
        puts "Ignored for Note #{note.id} because parent business is not available"
      end
    end

  end

end
