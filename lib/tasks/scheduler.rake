desc "This task is called by the Heroku scheduler add-on"


task :warn_trial_expiry => :environment do
  puts "Sending trial expiry warning emails..."
  users = User.unwarned_trial_users.limit(200)
  count = users.count
  users.each do |u|
    u.send_trial_expiry_warning_mail
    u.tx_warning_sent_at = Time.zone.now
    u.save
    puts "Mail sent to #{u.id} at #{u.tx_warning_sent_at}"
  end
  puts "Done. Sent to #{count} users."
end


task :verify_subscriptions => :environment do
  puts "Verifying customers for successful extenstions"

  Stripe.api_key = ENV['SECRET_KEY']

  users = User.subscription_due
  count = users.count
  success, failure = 0, 0

  users.each do |u|
    customer = Stripe::Customer.retrieve(u.customer_id)
    subscription = customer.subscriptions.retrieve(u.subscription_id)

    if Time.at(subscription.current_period_end) == u.current_subscription_end_at
      puts "Subscription was not renewed for User #{u.id}"
      failure += 1
    else
      u.update_attributes(
          current_subscription_end_at: Time.at(subscription.current_period_end),
          sx_warning_sent_at: nil
      )
      u.send_subscription_extension_mail
      puts "Subscription was renewed for User #{u.id} and email sent"
      success += 1
    end

  end

  puts "Done. Verified for #{count} users. Successful extensions: #{success}; Failed extensions: #{failure}"

end

