class ActiveEditValidationsGenerator < Rails::Generators::Base


  def create_validation_files
    create_file "app/assets/javascripts/app/active_edit/length_validations.js", <<-FILE
    ActiveEdit.length_validations = #{all_length_validations};
    FILE

    create_file "app/assets/javascripts/app/active_edit/numericality_validations.js", <<-FILE
    ActiveEdit.numericality_validations = #{all_numericality_validations};
    FILE

  end

  private

  def model_list
    Rails.application.eager_load!
    ActiveRecord::Base.descendants.collect(&:model_name).collect(&:name)
  end

  def all_length_validations
    hash = {}
    model_list.each do |model|
      unless instance_eval(model).length_validations.empty?
        hash[model.underscore] = instance_eval(model).length_validations
      end
    end
    hash.to_json
  end

  def all_numericality_validations
    hash = {}
    model_list.each do |model|
      unless instance_eval(model).numericality_validations.empty?
        hash[model.underscore] = instance_eval(model).numericality_validations
      end
    end
    hash.to_json
  end

end
